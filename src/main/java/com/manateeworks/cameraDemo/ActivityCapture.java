/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * A Derivative Work, changed by Manatee Works, Inc.
 *
 */

package com.manateeworks.cameraDemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.grassroots.petition.R;
import com.manateeworks.BarcodeScanner;
import com.manateeworks.camera.CameraManager;
import org.apache.log4j.Logger;

import java.io.IOException;


/**
 * The barcode reader activity itself. This is loosely based on the
 * CameraPreview example included in the Android SDK.
 */
public final class ActivityCapture extends Activity implements SurfaceHolder.Callback {
    private static final String TAG = ActivityCapture.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    // !!! Rects are in format: x, y, width, height !!!
    public static final Rect RECT_LANDSCAPE_1D = new Rect(0, 20, 100, 60);
    public static final Rect RECT_LANDSCAPE_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_PORTRAIT_1D = new Rect(20, 0, 60, 100);
    public static final Rect RECT_PORTRAIT_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_FULL_1D = new Rect(0, 0, 100, 100);
    public static final Rect RECT_FULL_2D = new Rect(20, 5, 60, 90);

    private static final int ABOUT_ID = Menu.FIRST;
    private static final int MAX_RESULT_IMAGE_SIZE = 150;
    private static final float BEEP_VOLUME = 0.10f;
    private static final long VIBRATE_DURATION = 200L;

    private static final String PACKAGE_NAME = "com.manateeworks.cameraDemo";
    public static final String BARCODE_KEY = "BARCODE_KEY";
    private ActivityCaptureHandler handler;

    
    private View statusView;
    private View resultView;
    private static MediaPlayer mediaPlayer;
    private byte[] lastResult;
    private boolean hasSurface;
    private InactivityTimer inactivityTimer;
    private static boolean playBeep;
    private static boolean vibrate;
    private boolean copyToClipboard;
    private String versionName;
    private ImageView debugImage;
    public static String lastStringResult;

    

    public Handler getHandler()
    {
        return handler;
    }

    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);
        
        // register your copy of the mobiScan SDK with the given user name / key
        BarcodeScanner.MWBregisterCode(BarcodeScanner.MWB_CODE_MASK_PDF, "Grassroots.Android.PDF.1DL", "94EFEF62F6E65B4F31C521567B11BB17C59F1C8946EDD1AFCBE0002192F9989E");
        

        // set flags for VIN scan modes
        // BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_39, BarcodeScanner.MWB_CFG_CODE39_VIN_MODE);
        // BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_DM, BarcodeScanner.MWB_CFG_DM_VIN_MODE);
        
        
        // choose code type or types you want to search for
        
        // Our sample app is configured by default to search all supported barcodes...
        // BarcodeScanner.MWBsetActiveCodes(BarcodeScanner.MWB_CODE_MASK_25     | 
        //                               BarcodeScanner.MWB_CODE_MASK_39     | 
        //                               BarcodeScanner.MWB_CODE_MASK_128    | 
        //                               BarcodeScanner.MWB_CODE_MASK_AZTEC  | 
        //                               BarcodeScanner.MWB_CODE_MASK_DM     |
        //                               BarcodeScanner.MWB_CODE_MASK_EANUPC | 
        //                               BarcodeScanner.MWB_CODE_MASK_PDF    | 
        //                               BarcodeScanner.MWB_CODE_MASK_QR     | 
        //                               BarcodeScanner.MWB_CODE_MASK_RSS);

        // But for better performance, only activate the symbologies your application requires...
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_25 ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_39 ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_128 ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_AZTEC ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_DM ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_EANUPC ); 
        BarcodeScanner.MWBsetActiveCodes(BarcodeScanner.MWB_CODE_MASK_PDF);
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_QR ); 
        // BarcodeScanner.MWBsetActiveCodes( BarcodeScanner.MWB_CODE_MASK_RSS ); 
        

        // Our sample app is configured by default to search both directions...
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL | BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);
        // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,     RECT_FULL_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,     RECT_FULL_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,    RECT_FULL_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,  RECT_FULL_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,     RECT_FULL_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC, RECT_FULL_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,    RECT_FULL_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,     RECT_FULL_2D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,    RECT_FULL_1D);     

        // But for better performance, set like this for PORTRAIT scanning...
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);
        // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,     RECT_PORTRAIT_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,     RECT_PORTRAIT_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,    RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,  RECT_PORTRAIT_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,     RECT_PORTRAIT_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC, RECT_PORTRAIT_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,    RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,     RECT_PORTRAIT_2D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,    RECT_PORTRAIT_1D);     
        
        // or like this for LANDSCAPE scanning - Preferred for dense or wide codes...
        BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL);
        // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,     RECT_LANDSCAPE_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,     RECT_LANDSCAPE_1D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,    RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,  RECT_LANDSCAPE_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,     RECT_LANDSCAPE_2D);    
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC, RECT_LANDSCAPE_1D);     
        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF, RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,     RECT_LANDSCAPE_2D);     
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,    RECT_LANDSCAPE_1D);     
        
        
        // set decoder effort level (1 - 5)
        // for live scanning scenarios, a setting between 1 to 3 will suffice
		// levels 4 and 5 are typically reserved for batch scanning 
        BarcodeScanner.MWBsetLevel(2);
        

        CameraManager.init(getApplication());
        resultView = findViewById(R.id.result_view);
        statusView = findViewById(R.id.status_view);
        
        handler = null;
        lastResult = null;
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        resetStatusView();

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface)
        {
            // The activity was paused but not stopped, so the surface still
            // exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            initCamera(surfaceHolder);
        }
        else
        {
            // Install the callback and wait for surfaceCreated() to init the
            // camera.
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

      
        int ver = BarcodeScanner.MWBgetLibVersion();
        int v1 = (ver >> 16);
        int v2 = (ver >> 8) & 0xff;
        int v3 = (ver & 0xff);
        String libVersion = "Lib version: " + String.valueOf(v1)+"."+String.valueOf(v2)+"."+String.valueOf(v3);
        LOGGER.debug(libVersion);
        
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (handler != null)
        {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME)
        {
            if (lastResult != null)
            {
                resetStatusView();
                if (handler != null)
                {
                    handler.sendEmptyMessage(R.id.restart_preview);
                }
                return true;
            }
        }
        else
            if (keyCode == KeyEvent.KEYCODE_FOCUS || keyCode == KeyEvent.KEYCODE_CAMERA)
            {
                // Handle these events so they don't launch the Camera app
                return true;
            }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onConfigurationChanged(Configuration config)
    {
        // Do nothing, this is to prevent the activity from being restarted when
        // the keyboard opens.
        super.onConfigurationChanged(config);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        if (!hasSurface)
        {
            hasSurface = true;
            initCamera(holder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        hasSurface = false;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {

    }

    /**
     * A valid barcode has been found, so give an indication of success and show
     * the results.
     * 
     * @param rawResult
     *            The contents of the barcode.
     */

    public void handleDecode(byte[] rawResult)
    {
        inactivityTimer.onActivity();
        lastResult = rawResult;
        statusView.setVisibility(View.GONE);
        resultView.setVisibility(View.VISIBLE);
        String result = "";
        for (int i = 0; i < rawResult.length; i++)
            result = result + (char) rawResult[i];
        LOGGER.debug("Read barcode: " + result);
        Intent results = new Intent();
        results.putExtra(BARCODE_KEY,  result);
        if(getParent() == null) {
            setResult(RESULT_OK, results);
        }else {
            getParent().setResult(RESULT_OK, results);
        }
        finish();
    }

  

    private void initCamera(SurfaceHolder surfaceHolder)
    {
        try
        {
            // Select desired camera resoloution. Not all devices supports all resolutions, closest available will be chosen
            // If not selected, closest match to screen resolution will be chosen
            // High resolutions will slow down scanning proccess on slower devices
            CameraManager.setDesiredPreviewSize(800, 480);
            
            CameraManager.get().openDriver(surfaceHolder, false);
        }
        catch (IOException ioe)
        {
            displayFrameworkBugMessageAndExit();
            return;
        }
        catch (RuntimeException e)
        {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            displayFrameworkBugMessageAndExit();
            return;
        }
        if (handler == null)
        {
            handler = new ActivityCaptureHandler(this);
        }
    }

    private void displayFrameworkBugMessageAndExit()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialogInterface, int i)
            {
                finish();
            }
        });
        builder.show();
    }

    private void resetStatusView()
    {
        resultView.setVisibility(View.GONE);
        statusView.setVisibility(View.VISIBLE);
        statusView.setBackgroundColor(getResources().getColor(R.color.status_view));

        TextView textView = (TextView) findViewById(R.id.status_text_view);
        textView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        textView.setTextSize(14.0f);
        textView.setText(R.string.msg_default_status);
        lastResult = null;
    }

}
