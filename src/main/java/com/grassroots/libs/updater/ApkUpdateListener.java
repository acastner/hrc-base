package com.grassroots.libs.updater;

/**
 * Created by jason on 10/25/2016.
 */
public interface ApkUpdateListener {
    void onUpdateCheckCompleted(boolean updateAvailable, String updateVersion, String updateUrl, String appName);
    void onUpdateCheckFailed(String title, String message, String appName);
    void onDownloadComplete(String path, String appName);
    void onDownloadFailed(String title, String message, String appName);
    void onDownloadProgress(int percentage);
}
