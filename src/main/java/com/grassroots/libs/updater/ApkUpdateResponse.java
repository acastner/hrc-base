package com.grassroots.libs.updater;

/**
 * Created by jason on 10/24/2016.
 */
public class ApkUpdateResponse {

    boolean hasUpdate;
    String updateUrl;
    String version;

    public String getUpdateUrl() {
        return this.updateUrl;
    }

    public String getUpdateVersionName() { return this.version; }

    public boolean getHasUpdate() { return this.hasUpdate; }

}
