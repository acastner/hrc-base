package com.grassroots.libs.updater;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by jason on 10/24/2016.
 */
public interface ApkUpdateService {

    @GET("utils/check-app-updates")
    Call<ApkUpdateResponse> getApkUpdateResponse(@Header("imei") String imei,
                                                 @Query("packageName") String packageName,
                                                 @Query("versionType") String versionType,
                                                 @Query("versionNumber") String versionNumber);

    @Streaming @GET
    Call<ResponseBody> downloadApk(@Url String url);

}
