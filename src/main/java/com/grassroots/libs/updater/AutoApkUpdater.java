package com.grassroots.libs.updater;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.grassroots.petition.R;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jason on 10/25/2016.
 *
 * Instantiating this class will fail if the following permissions aren't found:
 * android.permission.INTERNET
 * android.permission.ACCESS_WIFI_STATE
 * android.permission.ACCESS_NETWORK_STATE
 */
public class AutoApkUpdater {

    public static final String TAG = AutoApkUpdater.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private static final String REQUIRED_PERMISSIONS =
            "android.permission.INTERNET\n" +
            "android.permission.ACCESS_WIFI_STATE\n" +
            "android.permission.ACCESS_NETWORK_STATE";

    private final static String UPDATE_FILE = "update_file";
    private final static String UPDATE_RPS_FILE = "rps_update_file";

    private static final String UPDATE_ERROR_TITLE = "Update Check Error for ";
    private static final String UPDATE_ERROR_NO_SERVER = "Could not contact server.";
    private static final String UPDATE_ERROR_UNSUCCESSFUL_RESPONSE = "Server failed check for updates.";
    private static final String UPDATE_ERROR_VERSION_NAME = "Could not determine application version name.";

    private static final String DOWNLOAD_ERROR_TITLE = "Download Error for ";
    private static final String DOWNLOAD_ERROR_UNEXPECTED = "Unexpected error while downloading update.";
    private static final String DOWNLOAD_ERROR_UNSUCCESSFUL_RESPONSE = "Could not download file from server.";
    private static final String DOWNLOAD_ERROR_NO_RESPONSE = "Could not get a response from server.";

    public final static String ANDROID_PACKAGE = "application/vnd.android.package-archive";

    public final static String APP_NAME = "HRC Pavilion";
    public final static String RPS = "Request Performer Service";

    private Context context = null;
    private SharedPreferences preferences;
    private ApkUpdateListener updateListener;
    private static Retrofit retrofit;
    private static ApkUpdateService apkUpdateService;

    public AutoApkUpdater(Context ctx, ApkUpdateListener listener) {
        this.updateListener = listener;
        initialize(ctx);
        if(!haveInternetPermissions()) {
            throw new Error("The Apk Auto Updater class requires the following permissions which " +
                    "were not found.\n" + REQUIRED_PERMISSIONS);
        }
    }

    private void initialize(Context context) {
        this.context = context;
        retrofit = getRetrofit();
        preferences = context.getSharedPreferences(context.getPackageName() + "_" + TAG,
                Context.MODE_PRIVATE);
        deleteOldUpdate(UPDATE_FILE);
        deleteOldUpdate(UPDATE_RPS_FILE);
    }

    private void deleteOldUpdate(String key) {
        String update_file = preferences.getString(key, "");
        if (update_file.length() > 0) {
            if (new File(context.getFilesDir().getAbsolutePath() + "/" + update_file).delete()) {
                preferences.edit().remove(UPDATE_FILE).commit();
                LogDebug("Deleting previous update apk file: " + update_file);
            }
        }
    }

    private String saveFileNameToDisk(String updateType, String filename) {
        String key = UPDATE_FILE;
        if(updateType.equals(RPS)) {
            key = UPDATE_RPS_FILE;
        }
        preferences.edit().putString(key, filename).commit();
        return context.getFilesDir().getAbsolutePath() + "/" + filename;
    }

    private Retrofit getRetrofit() {
        if(retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);
            retrofit = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.base_url_canvass) + "/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    private ApkUpdateService getApkUpdateService() {
        if(apkUpdateService == null) {
            apkUpdateService = getRetrofit().create(ApkUpdateService.class);
        }
        return apkUpdateService;
    }

    private Call<ApkUpdateResponse> createApiUpdateCall(String updateType) {
        String versionType = context.getResources().getString(R.string.versionType);
        String pkgName = context.getPackageName();

        if(updateType.equals(RPS)) {
            //We have RPS and internal-RPS, use this so RPS can be tested internally without
            //causing an update for all the clients.
            if(versionType.equals("internal") || versionType.equals("internal-develop")) {
                versionType = "RPS-internal";
            } else if(versionType.equals("internal-develop")) {
                versionType = "RPS-internal-develop";
            } else {
                versionType = "RPS";
            }

            pkgName = "com.grassroots";
        }

        String versionName = null;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(pkgName, 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {

            /* NameNotFoundException occurs when the app with the specified package name
            is not found. This should never happen for GRUAPP, because this code runs
            from GRUAPP and should therefore always be found. This exception will be
            thrown in the case that RPS is not currently installed on the device. In that
            case, set the versionNumber to 0.0.0 so the app will always grab the latest
            version of RPS. */

            if(updateType.equals(APP_NAME)) {
                LogError("Failed to check for updates because the application with package name "
                        + pkgName + " could not be found.");
                updateListener.onUpdateCheckFailed(UPDATE_ERROR_TITLE,
                        UPDATE_ERROR_VERSION_NAME,
                        updateType);
                return null;
            }
            versionName = "0.0.0";
        }
        TelephonyManager telephonyManager =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        return getApkUpdateService().getApkUpdateResponse(imei, pkgName, versionType, versionName);
    }

    public static String getImei(Context cont) {
        TelephonyManager telephonyManager =
                (TelephonyManager) cont.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        return imei;
    }

    public void checkUpdate(final String updateType) {
        Call<ApkUpdateResponse> apkUpdateCall = createApiUpdateCall(updateType);
        if(apkUpdateCall == null)
            return;

        apkUpdateCall.enqueue(new Callback<ApkUpdateResponse>() {
            @Override
            public void onResponse(Call<ApkUpdateResponse> call,
                                   Response<ApkUpdateResponse> response) {
                if(response.isSuccessful()) {
                    ApkUpdateResponse res = response.body();
                    updateListener.onUpdateCheckCompleted(res.getHasUpdate(),
                            res.getUpdateVersionName(),
                            res.getUpdateUrl(),
                            updateType);
                    LogDebug("Received response while checking for apk update.");
                    LogDebug("Has update was " + res.getHasUpdate());
                    LogDebug("Update version name is " + res.getUpdateVersionName());
                    LogDebug("Update url was " + res.getUpdateUrl());
                } else {
                    LogError("Problem with server response body while checking for apk update.");
                    updateListener.onUpdateCheckFailed(UPDATE_ERROR_TITLE,
                            UPDATE_ERROR_UNSUCCESSFUL_RESPONSE,
                            updateType);
                }
            }
            @Override
            public void onFailure(Call<ApkUpdateResponse> call, Throwable t) {
                LogError("Api call to check-app-updates failed");
                LogError("Associated error was: " + t.getMessage());
                updateListener.onUpdateCheckFailed(UPDATE_ERROR_TITLE,
                        UPDATE_ERROR_NO_SERVER,
                        updateType);
            }
        });
    }

    public void downloadUpdate(final String updateType) {
        Call<ApkUpdateResponse> apkUpdateCall = createApiUpdateCall(updateType);
        if(apkUpdateCall == null)
            return;

        apkUpdateCall.enqueue(new Callback<ApkUpdateResponse>() {
            @Override
            public void onResponse(Call<ApkUpdateResponse> call,
                                   Response<ApkUpdateResponse> response) {
                final ApkUpdateResponse res = response.body();
                final String url = res.getUpdateUrl();
                Call<ResponseBody> downloadCall = getApkUpdateService().downloadApk(url);

                LogDebug("Received response while checking for apk update.");
                LogDebug("Has update was " + res.getHasUpdate());
                LogDebug("Update version name is " + res.getUpdateVersionName());
                LogDebug("Update url was " + res.getUpdateUrl());
                LogDebug("Attempting to download updated apk");

                downloadCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           final Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            LogDebug("Server contacted, beginning download of updated apk.");
                            String fileName = url.substring(url.lastIndexOf('/') + 1);
                            final String fname = fileName.substring(0, fileName.lastIndexOf(".apk") + 4);
                            LogDebug("Saving apk as " + fname);
                            new DownloadProgressTask(response.body(), fname, updateType).execute();
                        } else {
                            LogError("Problem with server response body while downloading apk. Url was: \n" + url);
                            updateListener.onDownloadFailed(DOWNLOAD_ERROR_TITLE, DOWNLOAD_ERROR_UNSUCCESSFUL_RESPONSE, updateType);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        LogError("Error while making call to download apk from url: \n" + url);
                        LogError("Associated error was: " + t.getMessage());
                        updateListener.onDownloadFailed(DOWNLOAD_ERROR_TITLE, DOWNLOAD_ERROR_NO_RESPONSE, updateType);
                    }
                });
            }

            @Override
            public void onFailure(Call<ApkUpdateResponse> call, Throwable t) {
                LogError("Api call to check-app-updates failed");
                LogError("Associated error was: " + t.getMessage());
                updateListener.onUpdateCheckFailed(UPDATE_ERROR_TITLE, UPDATE_ERROR_NO_SERVER, updateType);
            }
        });
    }

    private class DownloadProgressTask extends AsyncTask<Void, Integer, Boolean> {
        private ResponseBody body;
        private String filename;
        private String updateType;

        public DownloadProgressTask(ResponseBody body, String filename, String updateType) {
            this.body = body;
            this.filename = filename;
            this.updateType = updateType;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            updateListener.onDownloadProgress(progress[0]);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return writeResponseBodyToDisk(body, filename, this);
        }

        @Override
        protected void onPostExecute(Boolean writtenToDisk) {
            if(writtenToDisk) {
                LogDebug("Updated apk was downloaded and written to disk.");
                updateListener.onDownloadComplete(saveFileNameToDisk(updateType, filename), updateType);
            } else {
                LogError("Error while downloading apk and writing to disk.");
                updateListener.onDownloadFailed(DOWNLOAD_ERROR_TITLE, DOWNLOAD_ERROR_UNEXPECTED, updateType);
            }
        }

        public void doProgress(int progress) {
            publishProgress(progress);
        }
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String filename, DownloadProgressTask task) {
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = context.openFileOutput(filename, Context.MODE_WORLD_READABLE);

                int count = 0;

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    count++;
                    if(fileSize > 0 && count > 50) {
                        task.doProgress((int) (fileSizeDownloaded * 100 / fileSize));
                        count = 0;
                        Log.v(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                    }
                }
                outputStream.flush();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void LogError(String message) {
        Log.e(TAG, message);
        LOGGER.error(message);
    }

    private void LogDebug(String message) {
        Log.d(TAG, message);
        LOGGER.debug(message);
    }

    private boolean haveInternetPermissions() {
        Set<String> required_perms = new HashSet<String>();
        required_perms.add("android.permission.INTERNET");
        required_perms.add("android.permission.ACCESS_WIFI_STATE");
        required_perms.add("android.permission.ACCESS_NETWORK_STATE");

        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        int flags = PackageManager.GET_PERMISSIONS;
        PackageInfo packageInfo = null;

        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        if (packageInfo.requestedPermissions != null) {
            for (String p : packageInfo.requestedPermissions) {
                required_perms.remove(p);
            }
            if (required_perms.size() == 0) {
                return true; // permissions are in order
            }
            // something is missing
            for (String p : required_perms) {
                Log.e(TAG, "required permission missing: " + p);
            }
        }
        Log.e(TAG, "INTERNET/WIFI access required, but no permissions are found in Manifest.xml");
        return false;
    }

}


