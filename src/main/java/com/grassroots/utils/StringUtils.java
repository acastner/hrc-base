package com.grassroots.utils;

/**
 * Created by jason on 3/21/2017.
 */

public class StringUtils {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.equals("");
    }
}
