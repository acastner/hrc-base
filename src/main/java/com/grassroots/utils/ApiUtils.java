package com.grassroots.utils;

import android.util.Base64;

/**
 * Created by jason on 2/16/2017.
 */

public class ApiUtils {

    /**
     * Create Authentication header, will be added to every request to the server to identify user
     * @param username
     * @param password
     * @return Basic Authorization Header String
     */
    public static String createBasicAuthenticationHeader(String username, String password) {
        String encoded = Base64.encodeToString((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
        return "Basic " + encoded;
    }
}
