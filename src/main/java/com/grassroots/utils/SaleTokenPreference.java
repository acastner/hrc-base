package com.grassroots.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by jasongallagher on 6/9/17.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface SaleTokenPreference {
}

