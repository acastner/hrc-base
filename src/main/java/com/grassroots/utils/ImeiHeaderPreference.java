package com.grassroots.utils;

/**
 * Created by Jason on 12/29/2016.
 */

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ImeiHeaderPreference {
}