package com.grassroots.models;

/**
 * Created by jasongallagher on 6/26/17.
 */

public class ExecuteSaleResponse {


    /**
     * PaymentToken : da5ccdad-e07e-40bd-9f45-906a78eacce5
     * ResponseData : could be XML or JSON response from payment processor
     * Status : 0
     * Message : message regarding payment validation
     */

    private String PaymentToken;

    public String getLocalToken() {
        return LocalToken;
    }

    public void setLocalToken(String localToken) {
        LocalToken = localToken;
    }

    private String LocalToken;
    private String ResponseData;
    private int Status;
    private String Message;

    public String getPaymentToken() {
        return PaymentToken;
    }

    public void setPaymentToken(String PaymentToken) {
        this.PaymentToken = PaymentToken;
    }

    public String getResponseData() {
        return ResponseData;
    }

    public void setResponseData(String ResponseData) {
        this.ResponseData = ResponseData;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}