package com.grassroots.models;

import com.google.gson.annotations.SerializedName;

public class ExecuteSaleData {
    /**
     * ScriptId : 123
     * Amount : 123.45
     * PaymentMethodType : 1
     * Currency : USD
     * BillingAddress : {"Name":"John Doe","Address1":"123 Some Street","Address2":"Suite 100","City":"Some City","StateProvince":"Some State","PostalCode":"ABC12345","PhoneNumber":"123-456-7890","EmailAddress":"test@test.com","Country":"US"}
     * AccountInfo : {"AccountNumber":"ABC12345","RoutingNumber":"ABC12345","Kns":"ABC12345","EncryptedTrack":"ABC12345"}
     * PaymentToken : da5ccdad-e07e-40bd-9f45-906a78eacce5
     * NoProcessing : false
     * ProductSku
     * PaymentProcessorId
     */

    private int ScriptId;
    private double Amount;
    private int PaymentMethodType;
    private String Currency;
    private BillingAddress BillingAddress;
    private AccountInfo AccountInfo;
    private String LocalToken;
    private boolean NoProcessing;
    private String ProductSku;
    private int PaymentProcessorTypeId;

    public int getPaymentProcessorTypeId()  { return PaymentProcessorTypeId; }

    public void setPaymentProcessorTypeId(int PaymentProcessorTypeId) { this.PaymentProcessorTypeId = PaymentProcessorTypeId; }

    public String getProductSku()   { return ProductSku; }

    public void setProductSku(String sku)    { this.ProductSku = sku; }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getRecurringPattern() {
        return RecurringPattern;
    }

    public void setRecurringPattern(String recurringPattern) {
        RecurringPattern = recurringPattern;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    private String Time;
    private String OrderId;
    private String RecurringPattern;
    private String IMEI;


    public int getScriptId() {
        return ScriptId;
    }

    public void setScriptId(int ScriptId) {
        this.ScriptId = ScriptId;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }

    public int getPaymentMethodType() {
        return PaymentMethodType;
    }

    public void setPaymentMethodType(int PaymentMethodType) {
        this.PaymentMethodType = PaymentMethodType;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public BillingAddress getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(BillingAddress BillingAddress) {
        this.BillingAddress = BillingAddress;
    }

    public AccountInfo getAccountInfo() {
        return AccountInfo;
    }

    public void setAccountInfo(AccountInfo AccountInfo) {
        this.AccountInfo = AccountInfo;
    }

    public String getLocalToken() {
        return LocalToken;
    }

    public void setLocalToken(String localToken) {
        this.LocalToken = localToken;
    }

    public boolean isNoProcessing() {
        return NoProcessing;
    }

    public void setNoProcessing(boolean NoProcessing) {
        this.NoProcessing = NoProcessing;
    }

    @SerializedName("MobileSurveyId")
    private String mobileSurveyId;

    public String getMobileSurveyId() {
        return mobileSurveyId;
    }

    public void setMobileSurveyId(String mobileSurveyId) {
        this.mobileSurveyId = mobileSurveyId;
    }

    public String getSurveyData() {
        return surveyData;
    }

    public void setSurveyData(String surveyData) {
        this.surveyData = surveyData;
    }

    @SerializedName("SurveyData")
    private String surveyData;

    public static class BillingAddress {
        /**
         * Name : John Doe
         * Address1 : 123 Some Street
         * Address2 : Suite 100
         * City : Some City
         * StateProvince : Some State
         * PostalCode : ABC12345
         * PhoneNumber : 123-456-7890
         * EmailAddress : test@test.com
         * Country : US
         */

        private String Name;
        private String Address1;
        private String Address2;
        private String City;
        private String StateProvince;
        private String PostalCode;
        private String PhoneNumber;
        private String EmailAddress;
        private String Country;




        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String Address1) {
            this.Address1 = Address1;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String Address2) {
            this.Address2 = Address2;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getStateProvince() {
            return StateProvince;
        }

        public void setStateProvince(String StateProvince) {
            this.StateProvince = StateProvince;
        }

        public String getPostalCode() {
            return PostalCode;
        }

        public void setPostalCode(String PostalCode) {
            this.PostalCode = PostalCode;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getEmailAddress() {
            return EmailAddress;
        }

        public void setEmailAddress(String EmailAddress) {
            this.EmailAddress = EmailAddress;
        }

        public String getCountry() {
            return Country;
        }

        public void setCountry(String Country) {
            this.Country = Country;
        }
    }

    public static class AccountInfo {
        /**
         * AccountNumber : ABC12345
         * RoutingNumber : ABC12345
         * Kns : ABC12345
         * EncryptedTrack : ABC12345
         */

        private String AccountType = "Checking";

        private String AccountNumber;
        private String RoutingNumber;
        private String Kns;
        private String EncryptedTrack;

        public String getAccountNumber() {
            return AccountNumber;
        }

        public void setAccountNumber(String AccountNumber) {
            this.AccountNumber = AccountNumber;
        }

        public String getRoutingNumber() {
            return RoutingNumber;
        }

        public void setRoutingNumber(String RoutingNumber) {
            this.RoutingNumber = RoutingNumber;
        }

        public String getKns() {
            return Kns;
        }

        public void setKns(String Kns) {
            this.Kns = Kns;
        }

        public String getEncryptedTrack() {
            return EncryptedTrack;
        }

        public void setEncryptedTrack(String EncryptedTrack) {
            this.EncryptedTrack = EncryptedTrack;
        }
    }
}