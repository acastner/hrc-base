package com.grassroots.models;

/**
 * Created by jason on 4/7/2017.
 */

public class AddressFromZipResponse {

    /**
     * CityState : {"Zip5":"19426","City":"Collegeville","State":"PA"}
     */

    public AddressFromZipResponse() {
        CityState = new AddressFromZip();
        CityState.setCity("");
        CityState.setState("");
    }
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private String errorMessage;

    private AddressFromZip CityState;

    public AddressFromZip getCityState() {
        return CityState;
    }

    public void setCityState(AddressFromZip CityState) {
        this.CityState = CityState;
    }

    public static class AddressFromZip {
        /**
         * Zip5 : 19426
         * City : Collegeville
         * State : PA
         */

        private String Zip5;
        private String City;
        private String State;

        public String getZip5() {
            return Zip5;
        }

        public void setZip5(String Zip5) {
            this.Zip5 = Zip5;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getState() {
            return State;
        }

        public void setState(String State) {
            this.State = State;
        }
    }
}
