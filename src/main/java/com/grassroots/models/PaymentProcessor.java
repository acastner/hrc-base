package com.grassroots.models;

/**
 * Created by aniruddh on 2/6/2018.
 */

public class PaymentProcessor {
    int processorTypeId;
    String settings;
    int preferenceOrder;
    boolean backupOnly;

    public PaymentProcessor() {}

    public PaymentProcessor(int processorTypeId, boolean backupOnly)    {
        this.processorTypeId = processorTypeId;
        this.backupOnly = backupOnly;
    }

    /**
     * Get Processor Type ID
     * @return processorTypeId
     */
    public int getProcessorTypeId() {
        return processorTypeId;
    }

    /**
     * Set Processor Type Id
     * @param processorTypeId
     */
    public void setProcessorTypeId(int processorTypeId) {
        this.processorTypeId = processorTypeId;
    }

    /**
     * Get settings for Payment Processor
     * @return
     */
    public String getSettings() {
        return settings;
    }

    /**
     * Set settings for Payment Processor
     * @param settings
     */
    public void setSettings(String settings) {
        this.settings = settings;
    }

    /**
     * Get Preference order for multiple back-up payment processors
     * @return
     */
    public int getPreferenceOrder() {
        return preferenceOrder;
    }

    /**
     * Set Preference order for Payment Processor
     * @param preferenceOrder
     */
    public void setPreferenceOrder(int preferenceOrder) {
        this.preferenceOrder = preferenceOrder;
    }

    /**
     * Enable/Disable Back-up
     *
     * @param backupOnly
     */
    public void setBackupOnly(boolean backupOnly) {
        this.backupOnly = backupOnly;
    }

    /**
     * Get back-up status for Payment Processor
     * @return
     */
    public boolean getBackUpOnly()  {
        return backupOnly;
    }
}
