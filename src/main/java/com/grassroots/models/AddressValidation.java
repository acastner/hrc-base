package com.grassroots.models;

/**
 * Created by jason on 4/10/2017.
 */

public class AddressValidation {


    /**
     * Address : {"AddressID":"","Address1":"","Address2":"","City":"","State":"","ZipCode4":"","ZipCode5":""}
     * IsValid : false
     * Suggestion : false
     */

    private ValidatedAddress Address;
    private boolean IsValid;
    private boolean Suggestion;

    public boolean isValid() {
        return IsValid;
    }

    public void setValid(boolean valid) {
        IsValid = valid;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private String errorMessage;

    public ValidatedAddress getAddress() {
        return Address;
    }

    public void setAddress(ValidatedAddress Address) {
        this.Address = Address;
    }


    public boolean isSuggestion() {
        return Suggestion;
    }

    public void setSuggestion(boolean Suggestion) {
        this.Suggestion = Suggestion;
    }

    public static class ValidatedAddress {
        /**
         * AddressID :
         * Address1 :
         * Address2 :
         * City :
         * State :
         * ZipCode4 :
         * ZipCode5 :
         */

        private String AddressID;
        private String Address1;
        private String Address2;
        private String City;
        private String State;
        private String StateAbbreviation;
        private String ZipCode4;
        private String ZipCode5;

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        private String countryCode;

        public String getAddressID() {
            return AddressID;
        }

        public void setAddressID(String AddressID) {
            this.AddressID = AddressID;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String Address1) {
            this.Address1 = Address1;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String Address2) {
            this.Address2 = Address2;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getState() {
            return State;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getStateAbbreviation() { return StateAbbreviation; }
        public void setStateAbbreviation(String ab) {this.StateAbbreviation=ab;}

        public String getZipCode4() {
            return ZipCode4;
        }

        public void setZipCode4(String ZipCode4) {
            this.ZipCode4 = ZipCode4;
        }

        public String getZipCode5() {
            return ZipCode5;
        }

        public void setZipCode5(String ZipCode5) {
            this.ZipCode5 = ZipCode5;
        }
    }
}
