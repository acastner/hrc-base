package com.grassroots.petition.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.osmdroid.tileprovider.tilesource.IStyledTileSource;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.NetworkInfo.DetailedState;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.WalkListUpdateListener;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.adapters.LastNameListAdapter;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.MarketingMaterialType;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.tasks.UpdateWalklistTask;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.WalklistListAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 11.06.13
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
public class WalklistListFragment extends Fragment implements WalkListUpdateListener{
    private static final String TAG = WalklistListFragment.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    public static interface ViewListener {
        public void onSelectSubject(Subject subject);
        public void onFilterStreetName(String filter);
        public void onShowHouseNumber(Walklist.HouseNumberFilter filter);
        public void onFilterKnockStatus(KnockStatus knockStatus);
        public void onFilterLastName(String filter);
		/**
		 * Create a new subject record so they can be surveyed
		 */
		public void onNewRecord();
		/**
		 * 
		 * @param subject
		 */
		public void onSelectLastName(String lastname);
		
		
		public void  onTally();
    }

    private Walklist walklist = null;
    private Subject selectedSubject = null;

    protected ViewListener listener;
    private ListView listView;
    private LayoutInflater inflater;
    private Spinner filterSpinner;
    private Spinner filterKnockStatusSpinner;
    private Spinner lastNameSpinner;
    private ArrayAdapter<String> streetListAdapter;
    private ArrayAdapter<String> knockStatusListAdapter;
    private ArrayAdapter<String> lastNameListAdapter;
    private WalklistListAdapter listAdapter;
    private Button filterResetButton;
    private Button refreshWalklistButton;

    private String streetNameFilter = "";
    private Walklist.HouseNumberFilter houseNumberFilter = Walklist.HouseNumberFilter.ALL;
    private KnockStatus knockStatusFilter;
    private int filterPosition,knockFilterPosition,lastNameFilterPosition;
    private ProgressDialog pd = null;
    private Button houseNumberToggle = null;
    private Walklist.HouseNumberFilter filter = null;
    private String houseNumberName = null;
    private String lastNameFilter = "";
    private boolean isLocationList,isTablet = false;
    
    private LastNameListAdapter lastnameAdapter;
    public ArrayList<String> lastNamesFromDatabase;
    private String selectedLastName;
    
    private int filtercount,knockfiltercount,lastnamefiltercount = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.walklist_list_fragment, container, false);
        setHasOptionsMenu(true);
        Context context = this.getActivity().getApplicationContext();
        pd = new ProgressDialog(context);
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            // Assume Android 4.0, 7.0" tablet screen size
            isTablet = true;
        } else {
            // dart
            isTablet = false;
        }
        TypeFaceSetter.setRobotoFont(context, rootView);
        listView = (ListView) rootView.findViewById(R.id.walk_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            	if(isLocationList){
            	    String lastName = (String) adapterView.getItemAtPosition(i);
            	    selectedLastName = lastName;
            		listener.onSelectLastName(lastName);
            	}else{
            		Subject subject = (Subject) adapterView.getItemAtPosition(i);
                    listener.onSelectSubject(subject);
            	}
                
            }
        });
        

        /*filterSpinner = (Spinner) rootView.findViewById(R.id.filter_street_text_spinner);
        filterSpinner.setSelection(filterPosition);
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(filtercount > 0){
                int selectedItemPosition = filterSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                	filterPosition = selectedItemPosition;
                    String selectedItemString = filterSpinner.getSelectedItem().toString();
                    listener.onFilterStreetName(selectedItemString);
                } else {
                    filterSpinner.setSelection(filterPosition);
                    listener.onFilterStreetName("");
                    filterSpinner.setSelection(filterPosition);

                }
            	}
            	filtercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });*/

        lastNameSpinner = (Spinner) rootView.findViewById(R.id.last_name_spinner);
        lastNameSpinner.setSelection(lastNameFilterPosition);
        lastNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(lastnamefiltercount > 0){
            	if(isLocationList && !(((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation()==null)){
                int selectedItemPosition = lastNameSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                    lastNameFilterPosition = selectedItemPosition;
                    String selectedItemString = lastNameSpinner.getSelectedItem().toString();
                    listener.onFilterLastName(selectedItemString);
                } else {
                    listener.onFilterLastName("");
                    lastNameSpinner.setSelection(selectedItemPosition);

                }
            	}
            	}
            	lastnamefiltercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        filterKnockStatusSpinner = (Spinner) rootView.findViewById(R.id.filter_knock_status_spinner);
        filterKnockStatusSpinner.setSelection(knockFilterPosition);

        filterKnockStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	if(knockfiltercount > 0){
                int selectedItemPosition = filterKnockStatusSpinner.getSelectedItemPosition();
                if (selectedItemPosition != 0) {
                	knockFilterPosition = selectedItemPosition;
                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(KnockStatus.fromOrdinal(selectedItemPosition - 1).name());
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(knockFilterPosition);
                    listener.onFilterKnockStatus(KnockStatus.fromOrdinal(selectedItemPosition - 1));  
                } else {
                	((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(null);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(0);
                	filterKnockStatusSpinner.setSelection(knockFilterPosition);
                    listener.onFilterKnockStatus(null);
                    
                }
            	}
            	knockfiltercount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

       /* houseNumberToggle = (Button) rootView.findViewById(R.id.street_number_filter);
        if(houseNumberFilter != null){
            houseNumberToggle.setText(getResources().getString(houseNumberFilter.getDisplayTextId()));
        }
        houseNumberToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter = getHouseNumberFilter().getNext();
                listener.onShowHouseNumber(filter);
                houseNumberToggle.setText(getResources().getString(filter.getDisplayTextId()));
            }
        });*/

        filterResetButton = (Button) rootView.findViewById(R.id.reset_all_filters);
        filterResetButton.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View view) {
            	if(filterSpinner.getVisibility()==View.VISIBLE){
                    setFilterPosition(0);
                    filterSpinner.setSelection(0);
            	}
                setKnockFilterPosition(0);
                filterKnockStatusSpinner.setSelection(0);
                if(!isLocationList && ((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation() == null){
                Walklist.HouseNumberFilter filter = Walklist.HouseNumberFilter.ALL;
                setHouseNumberFilter(filter);
                setKnockStatusFilter(null);
                houseNumberToggle.setText(getResources().getString(filter.getDisplayTextId()));
                onSelectSubject(walklist.getSubject(0));
                }
                if(lastNameSpinner.getVisibility() == View.VISIBLE){
                	lastNameFilter = null;
                    lastNameFilterPosition = 0;
                    lastNameSpinner.setSelection(0);
                    knockStatusFilter = null;
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilter(lastNameFilter);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilterPosition(lastNameFilterPosition);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(null);
                    ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(0);
                    setWalklist(walklist);
                    listener.onSelectLastName(lastNamesFromDatabase.get(0));
                }
              
            }
        });
        
        refreshWalklistButton = (Button) rootView.findViewById(R.id.refreshButton);
        refreshWalklistButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (BaseWalklistActivity) getActivity();;
                if(pd.isShowing()){
                    pd.dismiss();
                }
                pd = ProgressDialog.show(getActivity(),getString(R.string.pleasewaitmsg), getString(R.string.loadingmsg));
                UpdateWalklistTask walklistUpdateTask = new UpdateWalklistTask(((BasePetitionActivity)activity), ((BasePetitionActivity)activity).getGlobalData().getLastWalklistSyncTime(),WalklistListFragment.this, ((BasePetitionActivity)activity).getGlobalData().getLocation());
                walklistUpdateTask.execute();
            }
        });
		configureNewRecordButton(rootView);
        configureTallyRecordButton(rootView);
        configureMarketingMaterialsButton(rootView);
        
		if(selectedSubject != null){
		    onSelectSubject(selectedSubject);
		}
		if(((BasePetitionActivity)getActivity()).getGlobalData().getLocation() == null){
			isLocationList = false;
		}else{
			isLocationList = true;
		}
        return rootView;
    }
    
    
    private Walklist.HouseNumberFilter getHouseNumberFilter() {
        String toggleDisplayText = houseNumberToggle.getText().toString();
        if (getResources().getString(R.string.all).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.ALL;
        } else if (getResources().getString(R.string.even).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.EVEN;
        } else if (getResources().getString(R.string.odd).equals(toggleDisplayText)) {
            return Walklist.HouseNumberFilter.ODD;
        }
        LOGGER.error("No matching resource for text: " + toggleDisplayText + ", showing all houses");
        return Walklist.HouseNumberFilter.ALL;
    }
   
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideMarketingIfHasNoMaterials();
    }
    
    public void updateFilters() {
        if(((BasePetitionActivity)getActivity()).getGlobalData().getLocation() == null){
            //traditional walklist
        	//houseNumberToggle.setVisibility(View.VISIBLE);
            //filterSpinner.setVisibility(View.VISIBLE);
            //lastNameSpinner.setVisibility(View.GONE);
            //listView.setVisibility(View.VISIBLE);
        }else{
            //location based walklist
        	//houseNumberToggle.setVisibility(View.GONE);
            //lastNameSpinner.setVisibility( View.VISIBLE);
            //filterSpinner.setVisibility(View.GONE);
            //listView.setVisibility(View.INVISIBLE);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("StreetFilter", streetNameFilter);
        savedInstanceState.putString("HouseNumberFilter", houseNumberFilter.name());
        savedInstanceState.putInt("StreetFilterPosition",filterPosition );
        savedInstanceState.putString("LastNameFilter", lastNameFilter);
        savedInstanceState.putInt("LastNameFilterPosition", lastNameFilterPosition);

        if (null != knockStatusFilter)
            savedInstanceState.putString("KnockStatusFilter", knockStatusFilter.name());
    }

	/**
	 * Configures new record button.
	 */
	private void configureNewRecordButton (View v)
	{
		v.findViewById( R.id.new_record_button ).setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick (View view)
			{
				listener.onNewRecord();
			}
		} );
		
	}

	private void configureTallyRecordButton(View v){
	    Button tallyButton = (Button) v.findViewById(R.id.tally_button);
	    if(isTablet){
	        tallyButton.setVisibility(View.VISIBLE);
	        tallyButton.setOnClickListener( new View.OnClickListener()
	        {
	            @Override
	            public void onClick (View view)
	            {
	                listener.onTally();
	            }
	        } );
	    }else{
	        tallyButton.setVisibility(View.GONE);
	    }
	    
	}

    private void configureMarketingMaterialsButton (View v)
    {
    	 if (MarketingMaterialType.NONE == MarketingMaterialType.getMarketingMaterialType(this.getActivity())) {
             View marketing = v.findViewById(R.id.marketing_material_button);
             if (null != marketing) {
                 marketing.setVisibility(View.GONE);
             }
         }
    	 
    	 File imgDir = new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
    	 if(imgDir.list() != null)
    	 {
    		 View marketing = v.findViewById(R.id.marketing_material_button);
    		 if (null != marketing) {
                 marketing.setVisibility(View.VISIBLE);
             }
    	 }
    	 
    	 if(v.findViewById(R.id.marketing_material_button).getVisibility() == (View.VISIBLE)){
        	 Button marketingButton = (Button) v.findViewById(R.id.marketing_material_button);
        	 marketingButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onMarketing();
					
				}
			});
         }
    }

    
    /**
     * Marketing button tap handler
     */
    protected void onMarketing() 
    {
    	((BaseWalklistActivity)(getActivity())).getGlobalData().onMarketing(getActivity());
    }
    
	
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    public void setWalklist(Walklist walklist) {
        if (null == walklist) {
            LOGGER.error("Null walklist passed to set walklist, not displaying any walklist");
            return;
        }
        this.walklist = walklist;
        if (null != listView) {
        	if(!isLocationList){
        		listAdapter = new WalklistListAdapter(walklist, inflater,(BaseWalklistActivity)(getActivity()));
                listView.setAdapter(listAdapter);	
        	}else{
        		lastNamesFromDatabase = (new WalklistDataSource(getActivity()).getDistinctLastNames());
        		lastnameAdapter = new LastNameListAdapter(walklist, inflater, (BaseWalklistActivity)(getActivity()),lastNamesFromDatabase,selectedLastName);
        		listView.setAdapter(lastnameAdapter);
        		 if(((BaseWalklistActivity)(getActivity())).isTablet){
        		if(selectedLastName == null || selectedLastName.equals("")){
        		    listener.onSelectLastName(lastNamesFromDatabase.get(0));
        		}else{
        		    listener.onSelectLastName(selectedLastName);
        		}
        		}
        	}
//            
        }
        List<String> addresses = walklist.getStreetNames();
        List<String> addressList = new ArrayList<String>(addresses.size() + 1);
        addressList.add("Street Filter");
        addressList.addAll(addresses);
        Context context = this.getActivity();
        if (context != null) {
            streetListAdapter = new ArrayAdapter<String>(context, R.layout.walklist_street_filter_item, addressList);
            if (null != filterSpinner) {
                filterSpinner.setAdapter(streetListAdapter);
                filterSpinner.setPrompt("Filter Street Name");
                filterSpinner.setSelection(0);
            }
        }
        
        List<String> lastNames = walklist.getAllLastNames(getActivity());
        if(lastNames.size()>1){
        List<String> lastNamesList = new ArrayList<String>(lastNames.size() + 1);
        lastNamesList.add("LastName");
        lastNamesList.addAll(lastNames);
        if (context != null) {
            lastNameListAdapter = new ArrayAdapter<String>(context, R.layout.walklist_street_filter_item, lastNamesList);
            if (null != lastNameListAdapter) {
                lastNameSpinner.setAdapter(lastNameListAdapter);
                lastNameSpinner.setPrompt("Filter LastName");
            }
        }
        }

        KnockStatus[] knockStatuses = KnockStatus.values();
        List<String> knockStatusList = new ArrayList<String>(knockStatuses.length + 1);
        knockStatusList.add("Knock Filter");
        for (KnockStatus status: knockStatuses) {
            knockStatusList.add(status.displayText());
        }

        if (context != null) {
            knockStatusListAdapter = new ArrayAdapter<String>(context, R.layout.walklist_street_filter_item, knockStatusList);
            if (null != filterKnockStatusSpinner) {
                filterKnockStatusSpinner.setAdapter(knockStatusListAdapter);
                filterKnockStatusSpinner.setPrompt("Filter Knock Status");
                filterKnockStatusSpinner.setSelection(0);
            }
        }
        streetNameFilter = ((BaseWalklistActivity)(getActivity())).getGlobalData().getStreetFilter();
        filterPosition = ((BaseWalklistActivity)(getActivity())).getGlobalData().getStreetFilterPosition();
        knockFilterPosition = ((BaseWalklistActivity)(getActivity())).getGlobalData().getKnockFilterPosition();
        if(((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation() != null){
            lastNameFilter = ((BaseWalklistActivity)(getActivity())).getGlobalData().getLastNameFilter();
            lastNameFilterPosition = ((BaseWalklistActivity)(getActivity())).getGlobalData().getLastNameFilterPosition();
            if(lastNameFilter!= null && !lastNameFilter.equals("")){
                setLastNameFilter(lastNameFilter);
                //lastNameSpinner.setSelection(lastNameFilterPosition);
            }
        }
        if(!isLocationList){
        //filterSpinner.setSelection(filterPosition);
        if (null == streetNameFilter) {
            streetNameFilter = "";
        }else{
            setStreetNameFilter(streetNameFilter);
        }
        houseNumberName = ((BaseWalklistActivity)(getActivity())).getGlobalData().getHouseNumberFilter();
        if (null == houseNumberName || houseNumberName.equals("")) {
            houseNumberFilter = Walklist.HouseNumberFilter.ALL;
        } else {
            houseNumberFilter = Walklist.HouseNumberFilter.valueOf(houseNumberName);
            setHouseNumberFilter(houseNumberFilter);
            //houseNumberToggle.setText(getResources().getString(houseNumberFilter.getDisplayTextId()));
        }
        }
        

        String knockStatusName = ((BaseWalklistActivity)(getActivity())).getGlobalData().getKnockStatusFilter();
        if (null == knockStatusName || knockStatusName.equals("")) {
            knockStatusFilter = null;
        } else {
            knockStatusFilter = KnockStatus.valueOf(knockStatusName);
            setKnockStatusFilter(knockStatusFilter);
            filterKnockStatusSpinner.setSelection(knockFilterPosition);
        }
        
        if(!isLocationList && selectedSubject != null){
        	onSelectSubject(selectedSubject);
        }
    }

    public void refreshWalklist() {
    	if(!isLocationList){
        if (null != listAdapter) {
            listAdapter.notifyDataSetChanged();
        }
        listAdapter.setHouseNumberFilter(houseNumberFilter);
        
        listAdapter.setStreetNameFilter(streetNameFilter);
    	}else{
    		if(lastnameAdapter != null){
    			lastnameAdapter.notifyDataSetChanged();
    		}
    	}
    	onSelectSubject(selectedSubject);
        if(houseNumberFilter != null){
            houseNumberToggle.setText(getResources().getString(houseNumberFilter.getDisplayTextId()));
        }
    }

    public List<Subject> setStreetNameFilter(String filter) {
    		this.streetNameFilter = filter;
    		if(!isLocationList){
               return listAdapter.setStreetNameFilter(filter);
    		}
			return null;
    }

    public List<Subject> setHouseNumberFilter(Walklist.HouseNumberFilter filter) {
        this.houseNumberFilter = filter;
        if(!isLocationList){
        	return listAdapter.setHouseNumberFilter(filter);
        }
		return null;
        
    }

    public List<Subject> setKnockStatusFilter(KnockStatus knockStatus) {
        this.knockStatusFilter = knockStatus;
        if(!isLocationList){
        return listAdapter.setKnockStatusFilter(knockStatus);
        }else{
        	return lastnameAdapter.setKnockStatusFilter(knockStatus);
        }
    }
    
    public List<Subject> setLastNameFilter(String lastName) {
    	this.lastNameFilter = lastName;
    	 if(lastName == null){
    	      return lastnameAdapter.setLastnameFilter("");
    	  }
    	return lastnameAdapter.setLastnameFilter(lastName);
        
    }

    public void unselectSubject() {
        int selectedPosition = listView.getCheckedItemPosition();
        if (0 <= selectedPosition) {
            listView.setItemChecked(selectedPosition, false);
        }
    }

    public void onSelectSubject(Subject subject) {
        if (null == subject) {
            LOGGER.error("Can't scroll to null subject");
            return;
        }

        this.selectedSubject = subject;

        
        if (null != listView) {
            Subject address = subject.toSubjectAddress();
            int position = 0;
            if(!isLocationList){
              position = listAdapter.getPositionOf(address);
            }else{
                position = lastnameAdapter.getPositionOf(address.getLastName());
            }
            LOGGER.debug("Scrolling to position: " + position + " for subject: " + subject.getFullAddress());
            if (position >= 0) {
                listView.setItemChecked(position, true);
                listView.setSelection(position);
            }
        }
    }

    public void onSelectLastName(String lastname) {
    	lastnameAdapter.setSelectedLastName(lastname);
        this.selectedLastName = lastname;
        if(selectedLastName == null){
        	selectedLastName = lastNamesFromDatabase.get(0);
        }
        if (null == lastname) {
            LOGGER.error("Can't scroll to null lastname");
            listView.setItemChecked(0, true);
            listView.setSelection(0);
            return;
        }
        if (null != listView) {
            int position = 0;
            position = lastnameAdapter.getPositionOf(selectedLastName);
            if (position >= 0) {
                listView.setItemChecked(position, true);
                if(listView.getLastVisiblePosition()<position){
                    listView.setSelection(position);
                }
                
            }
        }
    }



    // Helpers

    protected void hideMarketingIfHasNoMaterials() {
        if (MarketingMaterialType.NONE == MarketingMaterialType.getMarketingMaterialType(this.getActivity())) {
            View marketing = this.getView().findViewById(R.id.marketing_material_button);
            if (null != marketing) {
                //marketing.setVisibility(View.GONE);
            }
        }
    }

    /**
     * set the street filter item position (before switching view)
     * @param filterPosition
     */
	public void setFilterPosition(int filterPosition) {
		this.filterPosition = filterPosition;
	}

	/**
	 * get the street filter item position
	 * @return
	 */
	public int getfilterPosition() {
		return filterPosition;
	}
	
	/**
	 * set the knock filter position (before switching view)
	 * @param knockFilterPosition
	 */
	public void setKnockFilterPosition(int knockFilterPosition) {
		this.knockFilterPosition = knockFilterPosition;
	}

	/**
	 * get the knock filter position
	 * @return
	 */
	public int getKnockFilterPosition() {
		return knockFilterPosition;
	}

    @Override
    public void updateSuccess(Walklist walklist) {
        pd.dismiss();
        if(walklist != null && walklist.size()>0){
            refreshWalklist();
        }
    }

    @Override
    public void updateFailed(String errorMessage) {
        pd.dismiss();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onPause() {
        if(pd.isShowing()){
            pd.dismiss();
        }
        if(houseNumberFilter != null)
            ((BaseWalklistActivity)(getActivity())).getGlobalData().setHouseNumberFilter(houseNumberFilter.name());
            if(streetNameFilter != null)
            ((BaseWalklistActivity)(getActivity())).getGlobalData().setStreetFilter(streetNameFilter);
            if(knockStatusFilter != null)
            ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockStatusFilter(knockStatusFilter.name());
            ((BaseWalklistActivity)(getActivity())).getGlobalData().setStreetFilterPosition(filterPosition);
            ((BaseWalklistActivity)(getActivity())).getGlobalData().setKnockFilterPosition(knockFilterPosition);
            if(((BaseWalklistActivity)(getActivity())).getGlobalData().getLocation() != null){
                ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilter(lastNameFilter);
                ((BaseWalklistActivity)(getActivity())).getGlobalData().setLastNameFilterPosition(lastNameFilterPosition);

            }
        super.onPause();
    }
    
}
