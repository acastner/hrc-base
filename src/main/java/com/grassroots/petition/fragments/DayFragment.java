package com.grassroots.petition.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.*;
import android.widget.*;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.*;
import com.grassroots.petition.views.BusyBoxView;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * Fragment to present Day as a list of hours of day divided by busy blocks.
 * Delegates some behavior to specified listener, which implements OnDaySelectedListener interface.
 */
public class DayFragment extends ListFragment
{
    private static final String TAG = BasePetitionActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Key-name {@value} to send selected date as argument to newly created DayFragment.
     */
    public static final String SELECTED_DATE_EXTRA = "DATE_EXTRA";

    /**
     * Left margin of BusyBox's view{@value}
     */
    private static final int BUSY_BOX_LEFT_MARGIN = 100;

    /**
     * Interface of an OnDaySelectedListener listener, which has
     * #1 onAddNewAppointment() method, which is called to add new appointment
     * #2 getSubjectAppointment() method, which is called to get an created earlier appointment (if any)
     */
    public static interface OnDaySelectedListener
    {
        /**
         * Called when new appointment should be added (the user touched "Add" button for example).
         * @param dayFragment - a DayFragment object which called this method.
         * @param selectedTimeInterval - free time interval into which new appointment should be created (startTime and endTime contain actual dates).
         */
        public void onAddNewAppointment(DayFragment dayFragment, FreeTimeInterval selectedTimeInterval);
    }

    /**
     * Variable to store an OnDaySelectedListener listener
     */
    private OnDaySelectedListener listener;
    /**
     * Date which is used as date which is presented on the screen as available free tim intervals and busy boxes (the of date time doesn't matter)
     */
    Date selectedDate;

    /**
     * Parent view of DayFragment layout
     */
    ViewGroup rootView;

    /**
     * List of "Busy Boxes" which is used to represent busy/unavailable time interval on the screen.
     */
    List<BusyBox> busyBoxesArray = new ArrayList<BusyBox>();

    /**
     * Scrolled Offset of the list view with available hours of day.
     * Offset from the top of the first list view item to the top of the first visible list view item.
     */
    private int listViewYOffset;
    /**
     * Dictionary of hours of day list view items heights (need to proper calc listViewYOffset in order items has different heights)
     * key is item no inside list view
     * value is item height
     */
    private Dictionary<Integer, Integer> listViewItemHeightsArray = new Hashtable<Integer, Integer>();

    /**
     * Height of each hour of day list view item/row.
     */
    private int hourRowHeightInPixels;
    /**
     * Width of each hour of day list view item/row.
     */
    private int hourRowWidthInPixels;

    /**
     * The earliest free time interval available for the selectedDate
     */
    Date globalFreeIntervalStartTime;
    /**
     * The latest free time interval available for the selectedDate
     */
    Date globalFreeIntervalEndTime;

    /**
     * An existing Appointment object for current petition (if any)
     */
    Appointment subjectAppointment;
    /**
     * A "Busy Box" object to represent existing subject appointment (if any).
     */
    BusyBox appointmentBusyBox;

    /**
     * A flag to present if selectedDate contains any free interval (true if contains).
     */
    private boolean containsFreeInterval;

    /**
     * An abstraction of "Busy Box" entity showed on the screen in DayFragment on top of hours list.
     * Represents an event of unavailable time interval.
     * Stores "busy box" View and its coordinates, sizes, offset from the top of the parent view.
     * Height and offset from top side of parent in pixels are calculated by offset and length in minutes of a time interval or an event.
     * Delegates some behavior to specified listener, which implements OnDaySelectedListener interface.
     * "Busy box" rect are recalculated each time the new hourHeightInPixels value is set.
     */
    public class BusyBox
    {
        /**
         * Height of list view row which represent hour of day.
         * Needs to calc rect of "Busy Box" view.
         */
        int hourHeightInPixels;
        /**
         * x coordinate of view which is represent "Busy Box".
         */
        int x;
        /**
         * y coordinate of view which is represent "Busy Box".
         */
        int y;
        /**
         * width of view which is represent "Busy Box".
         */
        int width;
        /**
         * height of view which is represent "Busy Box".
         */
        int height;

        /**
         * Offset from the top of list view to the top of current "Busy Box" view.
         */
        int yOffset;

        /**
         * Offset in minutes from the beginning of the earliest free time interval till the beginning of current "Busy" event/unavailable time interval
         */
        int offsetInMinutes;
        /**
         * Length in minutes of current "Busy" event/unavailable time interval
         */
        int lengthInMinutes;

        /**
         * View which represents current "Busy Box"
         */
        View view;

        /**
         * Constructor to initialize a BusyBox object by offset in minutes and length in minutes of an time interval or event.
         * @param offsetInMinutes - offset in minutes of an time interval or event
         * @param lengthInMinutes - length in minutes of an time interval or event
         */
        public BusyBox(int offsetInMinutes, int lengthInMinutes) {
            this.offsetInMinutes = offsetInMinutes;
            this.lengthInMinutes = lengthInMinutes;
        }

        /**
         * Constructor to initialize a BusyBox object with x, y coordinates, width, height, offset and length in minutes and a view, which represents current "Busy Box".
         * @param offsetInMinutes - offset in minutes of an time interval or event
         * @param lengthInMinutes - length in minutes of an time interval or event
         */
        public BusyBox(int x, int y, int width, int height, int offsetInMinutes, int lengthInMinutes, BusyBoxView view) {
            this(offsetInMinutes, lengthInMinutes);

            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.view = view;
        }

        public void setView(View view) {
            this.view = view;
            updateViewRect();
        }

        public View getView() {
            return view;
        }

        public void setX(int x) {
            if (this.x != x) {
                this.x = x;
                updateViewRect();
            }
        }

        public void setY(int y) {
            if (this.y != y) {
                this.y = y;
                updateViewRect();
            }
        }

        public void setWidth(int width) {
            if (this.width != width) {
                this.width = width;
                updateViewRect();
            }
        }

        public int getYOffset() {
            return yOffset;
        }

        /**
         * Setter to set hour-row height in pixels.
         * Updates rect of "Busy Box" object, forces to update "Busy Box" object on the screen.
         * @param newHourHeightInPixels - new hour-row height in pixels
         */
        public void setHourHeightInPixels(int newHourHeightInPixels)
        {
            if (hourHeightInPixels != newHourHeightInPixels)
            {
                hourHeightInPixels = newHourHeightInPixels;

                yOffset = hourHeightInPixels * offsetInMinutes / 60;
                height = hourHeightInPixels * lengthInMinutes / 60;
                y = yOffset;

                updateViewRect();
            }
        }

        /**
         * Updates rect of "Busy Box" object, forces to update "Busy Box" object on the screen.
         */
        public void updateViewRect()
        {
            if (null != view)
            {
                view.setX(x);
                view.setY(y);

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
                params.height = height;
                params.width = width;
                view.setLayoutParams(params);
            }
        }
    }


    /**
     * Create a new instance of DayFragment, providing Date
     * as an argument.
     * @param date - Date which will be used as selected date (serialized as SELECTED_DATE_EXTRA)
     * @return new instance of DayFragment object.
     */
    public static DayFragment newInstance(Date date)
    {
        DayFragment f = new DayFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_DATE_EXTRA, date);

        f.setArguments( bundle );

        return f;
    }

    /**
     * Set OnDaySelectedListener listener for current instance.
     * @param listener - an OnDaySelectedListener listener for DayFragment
     */
    public void setOnDaySelectedListener( OnDaySelectedListener listener ) {
        this.listener = listener;
    }

    /**
     * onAttach event handler:
     * if attached activity is instance of OnDaySelectedListener, then set it as an OnDaySelectedListener listener
     * @param activity - new attached activity
     */
    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );
        if ( activity instanceof OnDaySelectedListener )
        {
            listener = (OnDaySelectedListener) activity;
        }
        else
        {
            throw new ClassCastException( activity.toString()
                                          + " must implement DayFragment.OnDaySelectedListener" );
        }
    }

    /**
     * onCreate handler:
     * #1 gets selected date from bundle
     * #2 gets the earliest time and the latest time of free time intervals for selected date
     */
    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);

        parseDateFromBundle();
        configureFreeTimeInterval();
    }

    /**
     * onDestroy handler: Releases listener
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        listener = null;
    }

    /**
     * Get selected date from arguments (SELECTED_DATE_EXTRA) if any
     */
    private void parseDateFromBundle()
    {
        Bundle arguments = getArguments();
        if (null != arguments)
        {
            selectedDate = (Date) arguments.getSerializable(SELECTED_DATE_EXTRA);
        }
    }

    /**
     * Creates a view to show a list of available free hours of the day and "Busy Boxes" on top of the list.
     * Adds global layout listener to the parent view of a list view, which will update rect of all "Busy Box" views on the screen.
     */
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        rootView = (ViewGroup)inflater.inflate( R.layout.day_fragment, container, false );

        final ViewGroup listViewRootView = (ViewGroup) rootView.findViewById(R.id.list_root);
        ViewTreeObserver viewTreeObserver = listViewRootView.getViewTreeObserver();
        if (viewTreeObserver.isAlive())
        {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
            {
                @Override
                public void onGlobalLayout()
                {
                    listViewRootView.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                    hourRowHeightInPixels = 0;
                    hourRowWidthInPixels = 0;

                    // get list view row height
                    ListView listView = DayFragment.this.getListView();
                    View rowView = listView.getChildAt(0);
                    if (null != rowView)
                    {
                        hourRowHeightInPixels = rowView.getHeight() + 1;
                        hourRowWidthInPixels = rowView.getWidth();

                        // update busy box rect
                        for (BusyBox busyBox : busyBoxesArray)
                        {
                            busyBox.setHourHeightInPixels(hourRowHeightInPixels);
                            busyBox.setWidth(hourRowWidthInPixels - BUSY_BOX_LEFT_MARGIN);
                        }

                        // update appointment busy box rect
                        if (null != appointmentBusyBox)
                        {
                            appointmentBusyBox.setHourHeightInPixels(hourRowHeightInPixels);
                            appointmentBusyBox.setWidth(hourRowWidthInPixels - BUSY_BOX_LEFT_MARGIN);
                        }
                    }
                }
            });
        }

        return rootView;
    }

    /**
     * Set selectedDate as title of current DayFragment.
     *
     */
    @Override
    public void onActivityCreated( Bundle savedInstanceState )
    {
        super.onActivityCreated( savedInstanceState );

        configureDate();
        configureHoursList();
        createBusyBoxes();
        updateSubjectAppointmentBox();


        // set list view scroll listener to move busy boxes
        final ListView listView = getListView();
        listView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                View childView = listView.getChildAt(0);
                if (null != childView)
                {
                    //int yOffsetPrevious = listViewYOffset;
                    listViewYOffset = -childView.getTop();

                    listViewItemHeightsArray.put(listView.getFirstVisiblePosition(), childView.getHeight());
                    for (int i = 0; i < listView.getFirstVisiblePosition(); i++)
                    {
                        Integer itemHeight = listViewItemHeightsArray.get(i);
                        if (null != itemHeight)
                        {
                            listViewYOffset += itemHeight;
                        }
                    }

                    // scroll busy boxes
                    for (BusyBox busyBox : busyBoxesArray)
                    {
                        busyBox.setY(busyBox.getYOffset() - listViewYOffset);
                    }

                    // scroll appointment busy box
                    if (null != appointmentBusyBox)
                    {
                        appointmentBusyBox.setY(appointmentBusyBox.getYOffset() - listViewYOffset);
                    }
                }
            }
        });
    }

    /**
     * Provides date format to format hours for each list view item.
     */
    private SimpleDateFormat getHoursDateFormat()
    {
        return new SimpleDateFormat(getString(R.string.SCHEDULING_DATE_SIMPLE_HOURS_DISPLAY_FORMAT));
    }

    /**
     * Provides date format to format selectedDate for DayFragment title.
     */
    private SimpleDateFormat getDateFormat()
    {
        return new SimpleDateFormat(getString(R.string.SCHEDULING_DATE_DISPLAY_FORMAT));
    }

    /**
     * Set title for current DayFragment view with formatted selectedDate.
     */
    private void configureDate()
    {
        if (null == selectedDate) return;
        TextView dateTextView = (TextView) rootView.findViewById(R.id.date_title);
        dateTextView.setText( getPrettyDate() );
    }

    public String getPrettyDate()
    {
        return getDateFormat().format( selectedDate.getTime() );
    }

    /**
     * Gets the earliest time (globalFreeIntervalStartTime) and
     * the latest time (globalFreeIntervalEndTime) of free time intervals for selected date.
     * Sets containsFreeInterval flag to indicate whether free intervals are available for selectedDate
     */
    private void configureFreeTimeInterval()
    {
        containsFreeInterval = false;

        if (null == selectedDate) return;

        AppointmentCalendar appointmentCalendar = getAppointmentCalendar();

        // get available time intervals from availableIntervalsList and divide them by existing events (NOTE: assume sorted by start time)
        List<FreeTimeInterval> freeTimeIntervalsList = appointmentCalendar.getFreeIntervalsForDate(selectedDate);
        if (null == freeTimeIntervalsList || 0 == freeTimeIntervalsList.size())
        {
            LOGGER.warn("Appointments Calendar contains empty free time intervals list for date = " + selectedDate);
            return;
        }

        // get global begin and global end time of all free time intervals
        // round up to 1 hour
        FreeTimeInterval firstFreeTimeInterval = freeTimeIntervalsList.get(0);
        globalFreeIntervalStartTime = (Date)firstFreeTimeInterval.getStartTime().clone();
        globalFreeIntervalStartTime.setMinutes(0);
        globalFreeIntervalStartTime.setSeconds(0);

        FreeTimeInterval lastFreeTimeInterval = freeTimeIntervalsList.get(freeTimeIntervalsList.size() - 1);
        globalFreeIntervalEndTime = (Date)lastFreeTimeInterval.getEndTime().clone();
        globalFreeIntervalEndTime.setMinutes(0);
        globalFreeIntervalEndTime.setSeconds(0);

        containsFreeInterval = true;
    }

    /**
     * If free time intervals are available for selectedDate, then
     * create rows for all hours between globalFreeIntervalStartTime and globalFreeIntervalEndTime
     * and set an ArrayAdapter for a hours list view.
     */
    private void configureHoursList()
    {
        if (!containsFreeInterval) return;

        // calc free time length
        long diffInMs = globalFreeIntervalEndTime.getTime() - globalFreeIntervalStartTime.getTime();
        int freeTimeInMinutes = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs) / 60;
        int freeTimeInHours = freeTimeInMinutes / 60;


        List<String> hourRowStringsArray = new ArrayList<String>();

        SimpleDateFormat hoursDateFormat = getHoursDateFormat();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(globalFreeIntervalStartTime);
        for (int i = 0; i < freeTimeInHours; i++)
        {
            String hoursRowString = hoursDateFormat.format(calendar.getTime());
            hourRowStringsArray.add(hoursRowString);

            calendar.add(Calendar.HOUR, 1);
        }

        setListAdapter( new ArrayAdapter<String>( getActivity(),
                android.R.layout.simple_list_item_1,
                hourRowStringsArray
        ) );
    }

    /**
     * #1 Removes all busy boxes, including views which represent busy boxes.
     * If free time intervals are available for selectedDate, then
     * #2 gets a list of busy/unavailable time intervals from an AppointmentCalendar by calling getBusyTimeIntervalsForDate() method (loaded from the backend)
     * #3 creates BusyBox object for each busy/unavailable time interval
     * #4 creates View for each created BusyBox and adds to list_root
     * #5 sets x coordinates for each created BusyBox's view to BUSY_BOX_LEFT_MARGIN
     */
    private void createBusyBoxes()
    {
        clearBusyBoxes();

        if (!containsFreeInterval) return;

        Context context = this.getActivity().getApplicationContext();
        final ViewGroup listViewRootView = (ViewGroup) rootView.findViewById(R.id.list_root);

        AppointmentCalendar appointmentCalendar = getAppointmentCalendar();
        List<BaseTimeInterval> busyTimeIntervals = appointmentCalendar.getBusyTimeIntervalsForDate(selectedDate);
        for (BaseTimeInterval busyTimeInterval : busyTimeIntervals)
        {
            Date busyIntervalStartTime = busyTimeInterval.getStartTime();
            Date busyIntervalEndTime = busyTimeInterval.getEndTime();

            // calc new busy box offset in minutes
            long diffInMs = busyIntervalStartTime.getTime() - globalFreeIntervalStartTime.getTime();
            int offsetInMinutes = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs) / 60;

            // calc current busy box length in minutes
            diffInMs = busyIntervalEndTime.getTime() - busyIntervalStartTime.getTime();
            int lengthInMinutes = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs) / 60;

            // add new busy box
            BusyBox busyBox = new BusyBox(offsetInMinutes, lengthInMinutes);
            busyBoxesArray.add(busyBox);

            // create & add view to represent new busy box on the screen
            BusyBoxView view = (BusyBoxView) View.inflate(context, R.layout.busy_box, null);
            listViewRootView.addView(view);
            busyBox.setX(BUSY_BOX_LEFT_MARGIN); // set x
            busyBox.setView(view);
        }
    }

    /**
     * Update BusyBox object and its view for subject Appointment.
     * #1 removes old BusyBox object and its view for subject Appointment.
     * #2 creates new BusyBox object regarding with subjectAppointment start time and end time.
     * #3 creates newly create View for BusyBox object and adds view to list_root.
     */
    protected void updateSubjectAppointmentBox()
    {
        Context context = this.getActivity().getApplicationContext();
        ViewGroup listViewRootView = (ViewGroup) rootView.findViewById(R.id.list_root);

        // remove old appointment, if any
        if (null != appointmentBusyBox)
        {
            View view = appointmentBusyBox.getView();
            if (null != view)
            {
                listViewRootView.removeView(view);
            }

            appointmentBusyBox = null;
        }

        if (!containsFreeInterval) return;


        // add new busy box, if new appointment is inside current day fragment interval
        if ( (null != subjectAppointment) &&
             (globalFreeIntervalStartTime.compareTo(subjectAppointment.getStartTime()) <= 0) &&
             (subjectAppointment.getEndTime().compareTo(globalFreeIntervalEndTime) <= 0) )
        {
            // calc subject appointment busy box offset in minutes
            Date appointmentStartTime = subjectAppointment.getStartTime();
            long diffInMs = appointmentStartTime.getTime() - globalFreeIntervalStartTime.getTime();
            int offsetInMinutes = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs) / 60;

            // calc subject appointment busy box length in minutes
            Date appointmentEndTime = subjectAppointment.getEndTime();
            diffInMs = appointmentEndTime.getTime() - appointmentStartTime.getTime();
            int lengthInMinutes = (int)TimeUnit.MILLISECONDS.toSeconds(diffInMs) / 60;


            // create busy box for subject appointment
            BusyBox busyBox = new BusyBox(offsetInMinutes, lengthInMinutes);

            // create busy box view
            BusyBoxView view = (BusyBoxView) View.inflate(context, R.layout.busy_box, null);
            listViewRootView.addView(view);
            busyBox.setView(view);
            busyBox.setX(BUSY_BOX_LEFT_MARGIN);

            // customize view look
            view.setDescription(subjectAppointment.getDescription());
            view.setBackgroundColor(Color.BLUE); // set custom background color

            if (0 != hourRowHeightInPixels)
            {
                busyBox.setHourHeightInPixels(hourRowHeightInPixels);
                busyBox.setWidth(hourRowWidthInPixels - BUSY_BOX_LEFT_MARGIN);
            }

            // add busy box as last item
            appointmentBusyBox = busyBox;
        }
    }

    /**
     * Remove all busy boxes from busyBoxesArray, including views which represent busy boxes.
     */
    protected void clearBusyBoxes()
    {
        final ViewGroup listViewRootView = (ViewGroup) rootView.findViewById(R.id.list_root);
        if (null != listViewRootView)
        {
            for (BusyBox busyBox : busyBoxesArray)
            {
                View view = busyBox.getView();
                if (null != view) {
                    listViewRootView.removeView(view);
                }
            }
        }

        busyBoxesArray.clear();
    }

    /**
     * Provides AppointmentCalendar object loaded from the backend from GlobalData singleton object.
     */
    protected AppointmentCalendar getAppointmentCalendar() {
        return getGlobalData().getAppointmentCalendar();
    }

    /**
     * Provides GlobalData singleton object.
     */
    protected GlobalData getGlobalData() {
        return (GlobalData) this.getActivity().getApplicationContext();
    }

    /**
     * Handles item clicks:
     * #1 create selected hour-row time interval
     * #2 checks if selected hour is not earlier then current time
     * #3 calls onAddNewAppointment() method in assign listener to show options dialog and add new appointment
     */
    @Override
    public void onListItemClick( ListView l, View v, int position, long id )
    {
        if (null == listener) {
            LOGGER.error("No listener is set");
            return;
        }

        Calendar calendar = Calendar.getInstance();
        Date currentTime = calendar.getTime();

        // calc begin free time for selected hour
        calendar.setTime(globalFreeIntervalStartTime);
        calendar.add(Calendar.HOUR_OF_DAY, position);
        Date selectedHourIntervalStartTime = calendar.getTime();

        // calc end free time for selected hour (+1 hour)
        calendar.add(Calendar.HOUR_OF_DAY, 1); // add 1 hour
        Date selectedHourIntervalEndTime = calendar.getTime();

        // check to not create event in past or current time: free time interval end time > current time
        if (selectedHourIntervalStartTime.compareTo(currentTime) <= 0)
        {
            notifyUser(getActivity().getApplicationContext().getString(R.string.CHOOSE_HOUR_IN_FUTURE_MESSAGE));
            return;
        }

        FreeTimeInterval selectedHourTimeInterval = new FreeTimeInterval(selectedHourIntervalStartTime, selectedHourIntervalEndTime);

        if (null != listener) {
            listener.onAddNewAppointment(this, selectedHourTimeInterval);
        }
    }

    /**
     * Show toast inside current DayFragment with a message to notify the user
     * @param message - message to show
     */
    public void notifyUser(String message) {
        Toast.makeText(this.getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
