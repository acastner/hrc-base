package com.grassroots.petition.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.BaseTimeInterval;
import com.grassroots.petition.models.EventTimeInterval;
import com.grassroots.petition.models.FreeTimeInterval;
import com.grassroots.petition.views.TimeKeyboardView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddAppointmentDialogFragment extends DialogFragment {
    // Data
    public Calendar selectedTime;
    public int minDuration;
    public int suggestedDuration;
    public int maxDuration;
    public List<FreeTimeInterval> freeTimes;
    private boolean initializing;

    // UI
    private View layout;
    private TextView appointmentStartTime;
    private TextView appointmentEndTime;
    private TextView durationIndicator;
    private SeekBar duration;
    private TextView durationMax;
    private TimeKeyboardView timeKeyboard;
    private Button addButton;

    // Appointment added listener
    ViewListener listener;

    public static interface ViewListener
    {
        public void onAddNewAppointment(EventTimeInterval event);
    }

    public void setAppointmentAddedListener(ViewListener listener)
    {
        this.listener = listener;
    }

    public AddAppointmentDialogFragment() {

    }

    public static AddAppointmentDialogFragment newInstance(Date startTIme, int minDuration, int suggestedDuration, int maxDuration) {
        AddAppointmentDialogFragment frag = new AddAppointmentDialogFragment();
        frag.selectedTime = Calendar.getInstance();
        frag.selectedTime.setTime(startTIme);
        frag.minDuration = minDuration;
        frag.suggestedDuration = suggestedDuration;
        frag.maxDuration = maxDuration;
        return frag;
    }

    /*
    public AddAppointmentDialogFragment(Date startTime,
                                        int minDuration,
                                        int suggestedDuration,
                                        int maxDuration) {
        selectedTime = Calendar.getInstance();
        selectedTime.setTime(startTime);
        this.minDuration = minDuration;
        this.suggestedDuration = suggestedDuration;
        this.maxDuration = maxDuration;
    }
    */

    /**
     * Fetches appointment calendar
     */
    protected AppointmentCalendar getAppointmentCalendar() {
        return getGlobalData().getAppointmentCalendar();
    }

    /**
     * Fetches GlobalData
     */
    protected GlobalData getGlobalData() {
        return (GlobalData) getActivity().getApplicationContext();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        layout = inflater.inflate(R.layout.calendar_add_appointment_dialog, null);

        this.initializing = true;
        initializeViewElements();
        configureTitle();
        configureTimeKeyboard();
        configureAppointmentTime();
        configureDurationSeekbar();
        configureAddButton();
        initializing = false;

        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Get schedule for the selected day
        AppointmentCalendar appointments = getAppointmentCalendar();
        freeTimes = appointments.getFreeIntervalsForDate(selectedTime.getTime());
    }

    private void initializeViewElements() {
        // Appointment
        appointmentStartTime = (TextView) layout.findViewById(R.id.scheduling_add_appointment_start_time);
        appointmentEndTime = (TextView) layout.findViewById(R.id.scheduling_add_appointment_end_time);

        // Duration
        duration = (SeekBar) layout.findViewById(R.id.scheduling_add_duration_seekbar);
        durationIndicator = (TextView) layout.findViewById(R.id.scheduling_add_duration_progress);
        durationMax = (TextView) layout.findViewById(R.id.scheduling_add_duration_max_value);

        // Other
        timeKeyboard = (TimeKeyboardView) layout.findViewById(R.id.scheduling_add_time_keyboard);
        addButton = (Button) layout.findViewById(R.id.scheduling_add_add_button);
    }

    private void configureTitle() {
        getDialog().setTitle(getPrettyDate());
    }

    public String getPrettyDate() {
        return getDateFormat().format(selectedTime.getTime());
    }

    private SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(getString(R.string.SCHEDULING_DATE_DISPLAY_FORMAT));
    }

    private void configureTimeKeyboard() {
        timeKeyboard.setInitialTime(selectedTime);
        timeKeyboard.setTimeChangedListener(createTimeChangedListener());
    }

    private TimeKeyboardView.ViewListener createTimeChangedListener() {
        return new TimeKeyboardView.ViewListener() {
            @Override
            public void onTimeChanged(Calendar time) {
                selectedTime.setTime(time.getTime());
                validateAppointment();
            }
        };
    }

    /**
     * Ensures a selected appointment time is valid. Updates user view if it is not. Updates the time
     * whether or not the selection is valid.
     */
     private void validateAppointment() {
         updateTimeView();

         if (isInvalidAppointmentTime())
         {
             disableAddButton();
         }
         else
         {
             restoreView();
         }
     }

    // Set back all text colors.
    private void restoreView() {
        setTextBackgroundToStandard();
        enableAddButton();
    }

    private void setTextBackgroundToStandard() {
        appointmentStartTime.setTextColor(Color.BLACK);
        appointmentEndTime.setTextColor(Color.BLACK);
        durationIndicator.setTextColor(Color.BLACK);
    }

    /**
     * Check if specified appointment conflicts with the current calendar.
     *
     * Appointments:
     *   - Should not begin before the work day
     *   - Should not conflict with busy times
     *   - Should not end after the work day
     *
     * @return true - if specified appointment is valid, false - otherwise.
     */
    private boolean isInvalidAppointmentTime() {
        // Determine whether the appointment times are invalid
        BaseTimeInterval proposedAppointment = calculateAppointmentInterval();
        boolean appointmentIsInConflict = getAppointmentCalendar().appointmentConflictsWithBusy(proposedAppointment);

        // Checks that starts after the first specified hour,
        // ends before the last specified hour, and isn't conflict
        if (startsBeforeWorkDay() ||
            endsAfterWorkDay() ||
            appointmentIsInConflict)
        {
            notifyAppointmentInvalid();
            return true;
        }
        // Checks if duration of appointment is valid
        else if (duration.getProgress() < minDuration)
        {
            notifyDurationTooShort();
            return true;
        }

        return false;
    }

    private boolean startsBeforeWorkDay() {
        return selectedTime.getTime().compareTo(getDayStartTime()) < 0;
    }

    private boolean endsAfterWorkDay() {
        return getEndTime().getTime().compareTo(getDayEndTime()) > 0;
    }

    private void notifyAppointmentInvalid() {
        appointmentStartTime.setTextColor(Color.RED);
        appointmentEndTime.setTextColor(Color.RED);
    }

    private void notifyDurationTooShort() {
        durationIndicator.setTextColor(Color.RED);
    }

    private EventTimeInterval calculateAppointmentInterval() {
        Calendar endTime = getEndTime();
        return new EventTimeInterval(selectedTime.getTime(), endTime.getTime());
    }

    private Calendar getEndTime() {
        Calendar endTime = Calendar.getInstance();
        endTime.setTime(selectedTime.getTime());
        endTime.add(Calendar.MINUTE, duration.getProgress());
        return endTime;
    }

    private Date getDayEndTime() {
        return freeTimes.get(freeTimes.size() - 1).getEndTime();
    }

    private Date getDayStartTime() {
        return freeTimes.get(0).getStartTime();
    }

    private void configureAppointmentTime() {
        updateTimeView();
    }

    private void configureDurationSeekbar() {
        duration.setOnSeekBarChangeListener(createDurationListener());
        duration.setMax(maxDuration);
        duration.setProgress(suggestedDuration);
        durationMax.setText("" + maxDuration);
    }

    private SeekBar.OnSeekBarChangeListener createDurationListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            private int minSeekInterval = 15;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Update seekbar
                progress = Math.round(progress / minSeekInterval) * minSeekInterval;
                seekBar.setProgress(progress);

                // TODO: Abstract to strings
                durationIndicator.setText("Duration: " + progress + " minutes");

                if (!initializing)
                {
                    validateAppointment();
                }
                else
                {
                    updateTimeView();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
    }

    private void enableAddButton() {
        addButton.setEnabled(true);
    }

    private void disableAddButton() {
        addButton.setEnabled(false);
    }

    private void updateTimeView() {
        // TODO: Abstract to strings
        appointmentStartTime.setText("Start Time: " + getPrettyTime(selectedTime.getTime()));
        appointmentEndTime.setText("End Time: " + getPrettyTime(getEndTime().getTime()));
    }

    public String getPrettyTime(Date time) {
        return getTimeFormat().format(time);
    }

    private SimpleDateFormat getTimeFormat() {
        return new SimpleDateFormat(getString(R.string.SCHEDULING_TIME_DISPLAY_FORMAT));
    }

    private void configureAddButton() {
        addButton.setOnClickListener(createValidatingAddButtonListener());
    }

    private View.OnClickListener createValidatingAddButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInvalidAppointmentTime())
                {
                    notifyAppointmentInvalid();
                    disableAddButton();
                } else {
                    listener.onAddNewAppointment(calculateAppointmentInterval());
                }
            }
        };
    }
}
