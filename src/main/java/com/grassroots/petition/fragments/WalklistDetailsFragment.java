package com.grassroots.petition.fragments;

import java.util.List;

import org.apache.log4j.Logger;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.MarketingMaterialType;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.AddressSubjectAdapter;

/**
 * Displays detailed information about selected household address from walklist object.
 * Contains a list of subjects which belong to this household.
 */
public class WalklistDetailsFragment extends Fragment
{
    private static final String TAG = WalklistDetailsFragment.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Listener interface to delegate some functionality or notify listeners about events.
     */
    public static interface ViewListener {
        /**
         * Set knock status for the whole address.
         */
        public void onSetHomeStatus();

        /**
         * Tell parent activity to refresh
         */
        public void onWalklistUpdate ();

        /**
         * Begin a new survey for the selected subject
         * @param subject - selected subject.
         */
        public void onSurveyStart (Subject subject);


        /**
         * Show marketing material
         */
        public void onMarketing(Subject subject);

        /**
         * Shows map fragment.
         */
        public void onShowMap();
        
        public void onSelectSubject(Subject subject);

		void onFilterLastName(String filter);

        void onSelectLastName(String lastname);
        
    }

    /**
     * Fragment layout
     */
    private View layout;
    /**
     * List of subjects addresses on the map.
     */
    private List<Subject> subjects;
    /**
     * Petition object which contains canvasser info, petition script, and questions to ask
     */
    Petition petition;
    /**
     * List adapter for the subjects list
     */
    private AddressSubjectAdapter subjectListAdapter = null;
    /**
     * Listener for current activity.
     */
    private ViewListener listener;
    /**
     * List of subjects which belongs to the household
     */
    private ListView subjectList;
    /**
     * List of subjects to visit
     */
    private Walklist walkList;
    /**
     * Layout inflater
     * TODO: Do I really need this?
     */
    private LayoutInflater inflater;
    /**
     * Button to set status for current household
     */
    private Button setHomeStatusButton;


    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(subjects != null)
        setSubjects(subjects);
        hideMarketingIfHasNoMaterials();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        layout = inflater.inflate( R.layout.walklist_details_fragment, container, false);
        setHasOptionsMenu(true);
        configureFont();
        subjectList = (ListView) layout.findViewById( R.id.subject_list );
        configureHomeStatusButton();
        configureShowMapButton();
        configureMarketingMaterialsButton();

        return layout;
    }

    private void configureFont ()
    {
        Context context = this.getActivity().getApplicationContext();
        TypeFaceSetter.setRobotoFont( context, layout );
    }

    /**
     * Configures home status button.
     */
    private void configureHomeStatusButton ()
    {
        // State: There are non-complete subjects
        // Configure set home status button
        setHomeStatusButton = (Button) layout.findViewById( R.id.set_home_status_button );
        if(((BasePetitionActivity)getActivity()).getGlobalData().getLocation() != null){
            setHomeStatusButton.setVisibility(View.GONE);
        }else{
            setHomeStatusButton.setVisibility(View.VISIBLE);
        }
        if ( !allSubjectsAreComplete())
        {
            setHomeStatusButton.setEnabled( true );
            setHomeStatusButton.setOnClickListener( new View.OnClickListener()
            {
                @Override
                public void onClick (View view)
                {
                    listener.onSetHomeStatus();
                }
            } );
        }
        // State: All subjects are complete
        // Disable set home status button
        else
        {
            setHomeStatusButton.setEnabled( false );
        }
    }

    private boolean allSubjectsAreComplete ()
    {
        // Too soon
        if (subjects == null) {
            return false;
        }

        // Check subject knock status
        for( Subject subject : subjects )
        {
            // First non-completed we find, exit
            if( subject.getKnockStatus() != KnockStatus.SUCCESS )
            {
                return false;
            }
        }

        // All must be completed
        return true;
    }



    /**
     * Configures show map button.
     */
    private void configureShowMapButton ()
    {
        if(walkList == null){
            walkList = getWalkList();
        }
        if(((BasePetitionActivity)getActivity()).getGlobalData().getLocation() != null){
            layout.findViewById( R.id.back_to_map ).setVisibility(View.GONE);
        }
        layout.findViewById( R.id.back_to_map ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                listener.onShowMap();
            }
        } );
    }

    /**
     * Configures marketing materials button.
     */
    private void configureMarketingMaterialsButton ()
    {
        layout.findViewById( R.id.marketing_material_button ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                listener.onMarketing(subjects.get(0));
            }
        } );
    }

    /**
     * Updates walk list
     */
    public void refreshWalklist ()
    {
        if( null != subjectListAdapter ) {
            subjectListAdapter.notifyDataSetInvalidated();
        }
        if( listener != null ) {
            listener.onWalklistUpdate();
        }
    }
    
    /**
     * Updates subject
     */
    public void updateSubject (Subject subject)
    {
        if( listener != null ) {
            listener.onSelectSubject(subject);
        }
    }

    /**
     * Starts survey
     */
    public void startSurvey (Subject subject)
    {
        listener.onSurveyStart( subject );
    }

    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Display the provided subjects in this view, unless a null is provided in which case just clear subjects list.
     * @param subjects - a list of subjects which belong to the selected household from walklist
     */
    public void setSubjects(List<Subject> subjects) {
    	this.subjects = subjects;
    	
        subjectListAdapter = new AddressSubjectAdapter(this, subjects, petition.getNonMagicQuestions(), inflater);
        subjectList.setAdapter(subjectListAdapter);
        if( subjects == null || subjects.isEmpty() ) {
            if( null != setHomeStatusButton ) {
                setHomeStatusButton.setEnabled( false );
            }
            return;
        }

        this.subjects = subjects;

        // Handle set home status button state
        if (null != setHomeStatusButton ) {
            configureHomeStatusButton();
        }

        if (null != subjectList) {
            subjectListAdapter = new AddressSubjectAdapter(this, subjects, petition.getNonMagicQuestions(), inflater);
            subjectList.setAdapter(subjectListAdapter);
        }
        subjectListAdapter.notifyDataSetChanged();
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setWalklist(Walklist walklist) {
        if (null == walklist) {
            LOGGER.error("Null walklist passed to WalklistDetailsFragment");
            return;
        }
        this.walkList = walklist;
    }

    public Walklist getWalkList()
    {
        return walkList;
    }

    public void setPetition(Petition petition) {
        if (null == petition) {
            LOGGER.error("Null petition passed to WalklistDetailsFragment");
            return;
        }
        this.petition = petition;
    }

    // Helpers

    /**
     * Checks if petition doesn't contain marketing materials, then hides marketing button.
     */
    protected void hideMarketingIfHasNoMaterials() {
        if (MarketingMaterialType.NONE == MarketingMaterialType.getMarketingMaterialType(this.getActivity())) {
            View marketing = this.getView().findViewById(R.id.marketing_material_button);
            if (null != marketing) {
                marketing.setVisibility(View.GONE);
            }
        }
    }
}
