package com.grassroots.petition.fragments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.MarketingMaterialType;
import com.grassroots.petition.models.StreetAddressSubject;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.models.Walklist.HouseNumberFilter;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.CustomMapView;



/**
 * Map fragment which displays subjects households markers on the map.
 */
public class WalklistMapFragment extends Fragment {
	private static final String TAG = WalklistMapFragment.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	/**
	 * Marker drawable which represents an address with knocked status.
	 */
	private static Drawable knockedMarkerDrawable;
	/**
	 * Marker drawable which represents an address with unknocked status.
	 */
	private static Drawable unknockedMarkerDrawable;
	/**
	 * Marker drawable which represents an address with was selected.
	 */
	private static Drawable selectedMarkerDrawable;


	/**
	 * Listener interface to delegate some functionality or notify listeners about events.
	 */
	public static interface ViewListener {
		/**
		 * Set knock status for the whole address.
		 */
		public void onSetStatus();

		/**
		 * Logout the user
		 */
		public void onLogout();

		/**
		 * Manually survey a subject (which is not in the list)
		 */
		public void onManualEntry();

		/**
		 * Show marketing material
		 */
		public void onMarketing();

		/**
		 * Select a subject from the subjects list.
		 * @param subject - a subject which was selected
		 */
		public void onSelectSubject(Subject subject);
	}


	/**
	 * Walklist object which contains subjects information
	 */
	private Walklist walklist;

	/**
	 * Index of the selected marker on the map.
	 */
	private int selectedMarkerIndex = -1;
	/**
	 * List of markers on the map.
	 */
	private ArrayList<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
	/**
	 * List of subjects addresses on the map.
	 */
	private ArrayList<Subject> subjects = new ArrayList<Subject>();

	/**
	 * Listener for current activity.
	 */
	private ViewListener listener;
	/**
	 * Layout inflater
	 */
	private LayoutInflater inflater;
	/**
	 * Map View
	 */
	private CustomMapView mapView;

	/**
	 * Overlay of my location on the map
	 */
	private MyLocationNewOverlay myLocationOverlay = null;
	/**
	 * Overlay which contains markers which represent subjects households on the map.
	 */
	private ItemizedIconOverlay<OverlayItem> locationOverlay = null;

	/**
	 * Field to hold entered street name filter
	 */
	private String streetNameFilter = "";
	/**
	 * Field to hold selected house number filter
	 */
	private Walklist.HouseNumberFilter houseNumberFilter = Walklist.HouseNumberFilter.ALL;
	/**
	 * Field to hold selected knock status filter
	 */
	private KnockStatus knockStatusFilter = null;
	private int centerLatitude,centerLongitude,zoomlevel;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.inflater = inflater;
		View rootView = inflater.inflate(R.layout.walklist_map_fragment, container, false);
		Context context = this.getActivity().getApplicationContext();

		setHasOptionsMenu(true);
		knockedMarkerDrawable = getResources().getDrawable(R.drawable.marker_knocked);
		unknockedMarkerDrawable = getResources().getDrawable(R.drawable.marker_unknocked);
		selectedMarkerDrawable = getResources().getDrawable(R.drawable.marker_selected);

		TypeFaceSetter.setRobotoFont(context, rootView);

		// init map
		mapView = (CustomMapView) rootView.findViewById(R.id.walklist_map);

		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.setMapFragment(this);
		mapView.getOverlays().clear();
		/**BING : Alternative for MAPQUESTOSM tile source
		 * 
		 */
		BingMapTileSource.retrieveBingKey(getActivity().getApplicationContext());
		BingMapTileSource bmts = new BingMapTileSource(null);
		mapView.setTileSource(bmts);

		//mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
		myLocationOverlay = new MyLocationNewOverlay(context, mapView);
		myLocationOverlay.setDrawAccuracyEnabled(true);
		myLocationOverlay.disableFollowLocation();

		mapView.getOverlays().add(myLocationOverlay);
		setWalklist(((BaseWalklistActivity)getActivity()).getWalklist());
		return rootView;
	}

	// used as onRestoreInstanceState()
	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Context context = this.getActivity().getApplicationContext();
		View rootView = this.getView();


		if (null != savedInstanceState) {
			streetNameFilter = savedInstanceState.getString("StreetFilter");
			if (null == streetNameFilter) {
				streetNameFilter = "";
			}
			String houseNumberName = savedInstanceState.getString("HouseNumberFilter");
			if (null == houseNumberName) {
				houseNumberFilter = Walklist.HouseNumberFilter.ALL;
			} else {
				houseNumberFilter = Walklist.HouseNumberFilter.valueOf(houseNumberName);
			}
			String knockStatusName = savedInstanceState.getString("KnockStatusFilter");
			if (null == knockStatusName) {
				knockStatusFilter = null;
			} else {
				knockStatusFilter = KnockStatus.valueOf(knockStatusName);
			}
		}
		updateMap();
		hideMarketingIfHasNoMaterials();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putString("StreetFilter", streetNameFilter);
		savedInstanceState.putString("HouseNumberFilter", houseNumberFilter.name());
		if (null != knockStatusFilter)
			savedInstanceState.putString("KnockStatusFilter", knockStatusFilter.displayText());
	}


	@Override
	public void onResume() {
		super.onResume();
		requireGpsEnabled();
		myLocationOverlay.enableMyLocation();
	}

	@Override
	public void onPause() {
		super.onPause();
		myLocationOverlay.disableMyLocation();
		//        if(mapView != null){
		//            mapView.getOverlayManager().onDetach(mapView);
		//        }
	}


	public void setViewListener(ViewListener listener) {
		this.listener = listener;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}


	public void setStreetNameFilter(String streetNameFilter) {
		this.streetNameFilter = streetNameFilter;
		updateMap();
	}

	public void setHouseNumberFilter(Walklist.HouseNumberFilter houseNumberFilter) {
		this.houseNumberFilter = houseNumberFilter;
		updateMap();
	}

	public void setKnockStatusFilter(KnockStatus knockStatusFilter) {
		this.knockStatusFilter = knockStatusFilter;
		updateMap();
	}

	public void setWalklist(Walklist walklist) {
		if (null == walklist) {
			LOGGER.error("Null walklist passed to set walklist, not displaying any walklist");
			return;
		}
		this.walklist = walklist;
	}


	//********************Helpers********************

	/**
	 * Checks is GPS location provider enabled, if not notifies the user about it.
	 */
	protected void requireGpsEnabled() {
		if (!LocationProcessor.isGpsLocationProviderEnabled(this.getActivity())) {
			buildAlertMessageNoGps();
		}
	}

	/**
	 * Notifies the user about disabled GPS location provider
	 */
	protected void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setMessage(R.string.gps_disabled)
		.setCancelable(false)
		.setPositiveButton(R.string.go_button, new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int id) {
				startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Checks if petition doesn't contain marketing materials, then hides marketing button.
	 */
	protected void hideMarketingIfHasNoMaterials() {
		if (MarketingMaterialType.NONE == MarketingMaterialType.getMarketingMaterialType(this.getActivity())) {
			View marketing = this.getView().findViewById(R.id.marketing_material_button);
			if (null != marketing) {
				marketing.setVisibility(View.GONE);
			}
		}
	}


	//********************A bunch of map stuff that should be in the view******************************

	/**
	 * Updates map view with all its content.
	 * Gets household addresses from walklist and display them on the map, zooms this area.
	 */
	public void updateMap() {
		List<GeoPoint> points = addWalklistLocationMarkers();
		mapView.setPoints(points);
		zoomToEncompass(points);
	}

	/**
	 * Gets the best location for the user's location.
	 * @return the best available user's location.
	 */
	private Location getBestLocation() {
		//TODO use walklist address if unable to find location?

		// Updates frequently, only when Activity is visible
		Location bestLocation = myLocationOverlay.getLastFix();
		if (null == bestLocation) {
			// Updates every 10 minutes
			bestLocation = LocationProcessor.getLastKnownLocation(this.getActivity());
		}
		return bestLocation;
	}

	/**
	 * Gets a list of GeoPoint which represents households from walklist on the map.
	 * List contains only appropriate households which are corresponded to selected house number filter, knock status filter, and street name filter.
	 * Fills subjects array with subjects addresses.
	 * Fills overlayItems with newly created markers, which represent status, first name and address.
	 * Creates new locationOverlay which contains newly created overlayItems and set onItemSingleTapUp() listeners.
	 * Set newly created locationOverlay to mapView.
	 * onItemSingleTapUp() listener for locationOverlay changes marker drawable to selected drawable and calls onSelectSubject() method in listener.
	 * @return a list of GeoPoint which represents households from walklist on the map.
	 */
	private List<GeoPoint> addWalklistLocationMarkers() {
		if (null != locationOverlay) {
			mapView.getOverlays().remove(locationOverlay);
		}

		overlayItems.clear();


		List<GeoPoint> points = new ArrayList<GeoPoint>();
		List<Subject> subjectsList = null;
		Set<StreetAddressSubject> addedAddresses = new HashSet<StreetAddressSubject>();
		if (streetNameFilter.equalsIgnoreCase("")
				&& (houseNumberFilter == null || houseNumberFilter.equals(HouseNumberFilter.ALL))
				&& knockStatusFilter == null) {
			subjectsList = walklist.getAddressSubjects();
		}else{
			subjects.clear();
			subjectsList = walklist.filterStreetAddress(streetNameFilter, houseNumberFilter, knockStatusFilter);
		}
		Drawable drawable = null;
		for (Subject subject : subjectsList) {
			StreetAddressSubject streetAddressSubject = subject.toStreetAddressSubject();
			if(addedAddresses.contains(streetAddressSubject)) continue;
			addedAddresses.add(streetAddressSubject);
			GeoPoint geoPoint = subject.getGeoPoint();

			if (!Subject.DEFAULT_MISSING_LAT_LNG.equals(geoPoint))
			{
				points.add(geoPoint);
				/*drawable = !KnockStatus.NOT_KNOCKED.equals(walklist.getHighestApartmentStatus(subject)) ?
						getResources().getDrawable(R.drawable.marker_knocked) : getResources().getDrawable(R.drawable.marker_unknocked);*/
				drawable = getResources().getDrawable(R.drawable.marker_default);
				drawable.mutate();
				OverlayItem overlayItem = new OverlayItem(subject.getFirstName(), subject.getFullAddress(), geoPoint);

				LOGGER.debug("STATUS: " + subject.getAddressLine1() + " " + walklist.getHighestApartmentStatus(subject));
				
				if(walklist.getHighestApartmentStatus(subject).toString().equals("SUCCESS"))
				{
					drawable.setColorFilter(Color.GRAY, Mode.MULTIPLY);
					overlayItem.setMarker(drawable);
				}
				else if(subject.getColor() != null)
				{
					String color = subject.getColor().replaceAll("#", "");
					boolean isHex = color.matches("^[A-Fa-f0-9]{6}$");
					if(isHex){
						LOGGER.debug("COLORTEST hex " + color);
						drawable.setColorFilter(Color.parseColor("#"+color), Mode.MULTIPLY);
						//drawable.setColorFilter(Color.parseColor("#"+subject.getColor()), Mode.SRC_ATOP);
					}

					overlayItem.setMarker(drawable);
				}else{
					drawable.setColorFilter(Color.BLUE, Mode.MULTIPLY);
					overlayItem.setMarker(drawable);
				}

				subjects.add(subject);
				overlayItems.add(overlayItem);
				drawable = null;
			}
		}
		LOGGER.debug("Had: " + subjects.size() + " subjects, added: " + addedAddresses.size() + " markers");

		locationOverlay = new ItemizedIconOverlay<OverlayItem>(overlayItems,
				new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>()
				{
			public boolean onItemSingleTapUp(final int index, final OverlayItem item)
			{

				if (selectedMarkerIndex >= 0)
				{
					Subject subject = subjects.get(selectedMarkerIndex);
					if (null != subject)
					{
						final Drawable drawable = getResources().getDrawable(R.drawable.marker_default); /*!KnockStatus.NOT_KNOCKED.equals(walklist.getHighestApartmentStatus(subject)) ?
								getResources().getDrawable(R.drawable.marker_knocked) : getResources().getDrawable(R.drawable.marker_unknocked);
						 */
						drawable.mutate();

						OverlayItem overlayItem = overlayItems.get(selectedMarkerIndex);
						Drawable drawableToMarker = drawable;

						if(walklist.getHighestApartmentStatus(subject).toString().equals("SUCCESS"))
						{
							drawable.setColorFilter(Color.GRAY, Mode.MULTIPLY);
							overlayItem.setMarker(drawable);
						}
						else if(subject.getColor() != null)
						{
							String color = subject.getColor();
							boolean isHex = color.matches("([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
							if(isHex){
								drawable.setColorFilter(Color.parseColor("#"+color), Mode.MULTIPLY);
							}
						}
						else
						{
							drawable.setColorFilter(Color.BLUE, Mode.MULTIPLY);
						}
						if (overlayItem != null) overlayItem.setMarker(drawableToMarker);
					}
				}

				Subject subject = subjects.get(index);
				if (null != subject)
				{
					OverlayItem overlayItem = overlayItems.get(index);
					if (null != overlayItem) {
						overlayItem.setMarker(selectedMarkerDrawable);
					}

					listener.onSelectSubject(subject);
				}
				selectedMarkerIndex = index;
				mapView.invalidate();

				return true;
			}
			public boolean onItemLongPress(final int index, final OverlayItem item){
				return true;
			}
				}, new DefaultResourceProxyImpl(this.getActivity().getApplicationContext()));
		mapView.getOverlays().add(locationOverlay);


		Location userLocation = getBestLocation();
		if (null != userLocation) {
			GeoPoint userLatLng = new GeoPoint(userLocation.getLatitude(), userLocation.getLongitude());
			points.add(userLatLng);
		}

		return points;
	}

	/**
	 * Zoom to encompass GeoPoints from the specified list.
	 * @param points - a list of points which are used for bounding box.
	 */
	public void zoomToEncompass(final List<GeoPoint> points) {
		final BoundingBoxE6 boundingBoxE6 = BoundingBoxE6.fromGeoPoints((ArrayList<? extends GeoPoint>) points);

		// Do nothing, when no walklist assigned
		if (null == points || 0 == points.size()) return;
		final IMapController mapController = mapView.getController();
		if (1 == points.size()) {
			GeoPoint myLocation = myLocationOverlay.getMyLocation();
			if (null != myLocation) {
				mapController.setCenter(myLocation);
			}else {
				mapController.setCenter(points.get(0));
			}
			mapController.setZoom(12);
		}


		// Wait for layout to set zoom
		if (mapView.getHeight() > 0) {
			//  zoomToBoundingBox( mapController, points );
		}  else {
			//  wait for layout
			ViewTreeObserver vto1 = mapView.getViewTreeObserver();
			vto1.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					zoomToBoundingBox( mapController, points );
					ViewTreeObserver vto2 = mapView.getViewTreeObserver();
					vto2.removeGlobalOnLayoutListener(this);
				}
			});

		}
	}

	private void zoomToBoundingBox (IMapController mapController, List<GeoPoint> points)
	{
		if(this.centerLatitude != 0 && this.centerLongitude != 0 && this.zoomlevel != 0 ){
			GeoPoint geopoint = new GeoPoint(centerLatitude, centerLongitude);
			mapController.setCenter(geopoint);
			mapController.setZoom(zoomlevel);
			mapController.animateTo(geopoint);
		}else{
			int minLat = Integer.MAX_VALUE;
			int maxLat = Integer.MIN_VALUE;
			int minLon = Integer.MAX_VALUE;
			int maxLon = Integer.MIN_VALUE;

			for (GeoPoint point : points)
			{
				int lat = point.getLatitudeE6();
				int lon = point.getLongitudeE6();

				maxLat = Math.max(lat, maxLat);
				minLat = Math.min(lat, minLat);
				maxLon = Math.max(lon, maxLon);
				minLon = Math.min(lon, minLon);
			}
			mapController.zoomToSpan(Math.abs(maxLat - minLat), Math.abs(maxLon - minLon));
			mapController.animateTo(new GeoPoint( (maxLat + minLat)/2,
					(maxLon + minLon)/2 ));
		}
	}

	public MapView getMapView(){
		return mapView;
	}

	public void restoreMapConfig(int mapCenterLatitude, int mapCenterLongitude,
			int zoomLevel) {

		this.centerLatitude = mapCenterLatitude;
		this.centerLongitude = mapCenterLongitude;
		this.zoomlevel = zoomLevel;

	}

	@Override
	public void onDestroy() {
		if(mapView != null){
			mapView.getTileProvider().clearTileCache();
		}

		knockedMarkerDrawable = null;
		unknockedMarkerDrawable = null;
		selectedMarkerDrawable = null;
		//mapView = null;
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onLowMemory() {
		Log.e("on low memory","on low memory");
		System.gc();
		super.onLowMemory();
	}
	@Override
	public void onStop() {
		if(mapView != null){
			if(mapView.getTileProvider()!= null)
				mapView.getTileProvider().clearTileCache();
		}
		super.onStop();
	}
}
