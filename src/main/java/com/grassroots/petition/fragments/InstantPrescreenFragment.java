package com.grassroots.petition.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.grassroots.petition.R;
import com.grassroots.petition.models.InstantPrescreenRequest;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.tasks.SendInstantPrescreenRequestTask;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *  Reusable fragment for conducting a TransUnion credit check at the door.
 */
public class InstantPrescreenFragment extends Fragment implements
        DatePickerDialogFragment.OnSetDateListener,
        SendInstantPrescreenRequestTask.OnClientApprovedListener
{
    private static final String TAG = InstantPrescreenFragment.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Default subject's DOB year.
     */
    private static final int DEFAULT_SUBJECT_DOB_YEAR = 1965;

    /**
     * Default date format for subject's Date Of Birth (DOB)
     */
    private static final String DEFAULT_SUBJECT_DOB_DATE_FORMAT = "MM/dd/yyyy";

    // UI
    private EditText firstNameField;
    private EditText lastNameField;
    private EditText addressField;
    private EditText cityField;
    private EditText zipCodeField;
    private Spinner USStatesSpinner;
    private EditText phoneField;
    private EditText DOBField;

    private List<String> USStatesArray = new ArrayList<String>();

    private class RequiredFieldsTextWatcher implements TextWatcher
    {
        private EditText tv;
        private RequiredFieldsTextWatcher(EditText view) {
            this.tv = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            if (0 == text.length()) {
                tv.setHintTextColor(getResources().getColor(R.color.required_edit_text));
            }
        }
    }

    private InstantPrescreenListener listener;
    public interface InstantPrescreenListener
    {
        public void onClientApproved(String message);
        public void onClientDeclined(String message);
        public void onErrorOccurred(String message);
    }

    public void setSubject(Subject subject) {
        this.subject = subject;

        restoreSubjectData();
    }

    /**
     * Subject which will be checked for credit.
     */
    private Subject subject;

    @Override
    public void onDestroy() {
        super.onDestroy();
        USStatesArray = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.instant_prescreen_fragment, container, false);

        firstNameField = (EditText) v.findViewById(R.id.first_name_field);
        firstNameField.addTextChangedListener(new RequiredFieldsTextWatcher(firstNameField));

        lastNameField = (EditText) v.findViewById(R.id.last_name_field);
        lastNameField.addTextChangedListener(new RequiredFieldsTextWatcher(lastNameField));

        addressField = (EditText) v.findViewById(R.id.address_field);
        addressField.addTextChangedListener(new RequiredFieldsTextWatcher(addressField));

        cityField = (EditText) v.findViewById(R.id.city_field);
        cityField.addTextChangedListener(new RequiredFieldsTextWatcher(cityField));

        zipCodeField = (EditText) v.findViewById(R.id.zip_code_field);
        zipCodeField.addTextChangedListener(new RequiredFieldsTextWatcher(zipCodeField));

        phoneField = (EditText) v.findViewById(R.id.phone_field);

        DOBField = (EditText) v.findViewById(R.id.dob_picker);
        DOBField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowSubjectDOBDatePicker();
            }
        });

        USStatesArray = getUSStatesShortNamesArray();
        USStatesArray.add(0, getString(R.string.state_not_selected_charset)); // the first item means no selection
        ArrayAdapter<String> usStatesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, USStatesArray);
        usStatesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        USStatesSpinner = (Spinner) v.findViewById(R.id.us_states_spinner);
        USStatesSpinner.setAdapter(usStatesAdapter);

        v.findViewById(R.id.submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });

        restoreSubjectData();

        return v;
    }

    /**
     * Provide a list of short names of US States (loaded from "us_states.xml" file from "us_states_short_names" strings array).
     * @return a list of short names of US States.
     */
    public List<String> getUSStatesShortNamesArray()
    {
        return new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.us_states_short_names)));
    }

    /**
     * Shows date picker to get subject's date of birth (DOB)
     */
    private void onShowSubjectDOBDatePicker()
    {
        Date selectedDate = getDateFromString(DOBField.toString());
        if (null == selectedDate) {
            Calendar c = GregorianCalendar.getInstance();
            c.set(Calendar.YEAR, DEFAULT_SUBJECT_DOB_YEAR);
            selectedDate = c.getTime();
        }
        DatePickerDialogFragment dpdf = new DatePickerDialogFragment(selectedDate);
        dpdf.setListener(this);
        dpdf.show(getFragmentManager(), DatePickerDialogFragment.DATE_PICKER_FRAGMENT_TAG);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof InstantPrescreenListener) {
            listener = (InstantPrescreenListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement InstantPrescreenFragment.InstantPrescreenListener interface.");
        }
    }

    /**
     * Restores subject's data to fields.
     */
    private void restoreSubjectData()
    {
        firstNameField.setText("");
        lastNameField.setText("");
        addressField.setText("");
        cityField.setText("");
        USStatesSpinner.setSelection(0);
        zipCodeField.setText("");
        phoneField.setText("");
        DOBField.setText("");

        if (null == subject) return;

        firstNameField.setText(subject.getFirstName());
        lastNameField.setText(subject.getLastName());
        addressField.setText(subject.getAddressLine1() + subject.getAddressLine2());
        cityField.setText(subject.getCity());
        setSelectedState(subject.getState());
        zipCodeField.setText(subject.getZip());
        phoneField.setText(subject.getPhone());

        if (0 != subject.getAge()) {
            Calendar c = GregorianCalendar.getInstance();
            c.add(Calendar.YEAR, -subject.getAge());
            DOBField.setText(getStringFromDate(c.getTime()));
        }
    }

    private void setSelectedState(String state)
    {
        if (TextUtils.isEmpty(state)) return;

        int i = 0;
        for (String nextState : USStatesArray) {
            if ( state.equalsIgnoreCase(nextState) ) {
                USStatesSpinner.setSelection(i);
                return;
            }
            i++;
        }
    }


    // Buttons OnClickListener handlers implementations

    /**
     * @return selected US state short name ("NO", "WA" etc), null otherwise
     */
    private String getSelectedUSState()
    {
        // the first item means no selection
        int pos = USStatesSpinner.getSelectedItemPosition();
        if (0 == pos) {
            return null;
        }

        return (String) USStatesSpinner.getSelectedItem();
    }

    /**
     * @return entered DOB Date (converted using DEFAULT_SUBJECT_DOB_DATE_FORMAT = {@value #DEFAULT_SUBJECT_DOB_DATE_FORMAT})
     */
    private Date getDOB()
    {
        return getDateFromString(DOBField.getText().toString());
    }

    /**
     * Sends InstantPrescreenRequest asynchronously to get is valid
     */
    private void onSubmit()
    {
        // #1 Create InstantPrescreenRequest object
        InstantPrescreenRequest prescreenRequest = new InstantPrescreenRequest(
                firstNameField.getText().toString(),
                lastNameField.getText().toString(),
                addressField.getText().toString(),
                cityField.getText().toString(),
                getSelectedUSState(),
                zipCodeField.getText().toString(),
                phoneField.getText().toString(),
                getDOB());

        // #2 Validate object
        if (!prescreenRequest.isValid()) {
            LOGGER.debug("Instant prescreen request is not valid: possible not all required fields are filled in.");
            notifyUser(getString(R.string.INSTANT_PRESCREEN_PLEASE_FILL_IN_ALL_REQUIRED_FIELDS_MESSAGE));
            return;
        }

        // #3 Create SendInstantPrescreenRequestTask
        showProgress(getActivity().getString(R.string.INSTANT_PRESCREEN_SENDING_INSTANT_PRESCREEN_REQUEST_IDLE_MESSAGE));
        new SendInstantPrescreenRequestTask(this).execute(prescreenRequest);
    }


    // SendInstantPrescreenRequestTask.OnClientApprovedListener implementation

    /**
     * client approved handler:
     * #1 hides progress dialog
     * #2 show message as notification
     * #3 passes message to InstantPrescreenListener listener
     * part of SendInstantPrescreenRequestTask.OnClientApprovedListener interface
     * @param message - a message with results
     */
    public void OnClientApproved(String message)
    {
        hideActiveDialog();

        if (null != listener) {
            listener.onClientApproved(message);
        }
    }

    /**
     * client declined handler:
     * #1 hides progress dialog
     * #2 show message as notification
     * #3 passes message to InstantPrescreenListener listener
     * part of SendInstantPrescreenRequestTask.OnClientApprovedListener interface
     * @param message - a message with results
     */
    public void OnClientDeclined(String message)
    {
        hideActiveDialog();

        if (null != listener) {
            listener.onClientDeclined(message);
        }
    }

    /**
     * error occurred handler:
     * #1 hides progress dialog
     * #2 show message as notification
     * #3 passes message to InstantPrescreenListener listener
     * part of SendInstantPrescreenRequestTask.OnClientApprovedListener interface
     * @param message - a message with results
     */
    public void onErrorOccurred(String message)
    {
        hideActiveDialog();

        if (null != listener) {
            listener.onErrorOccurred(message);
        }
    }


    // DatePickerDialogFragment.OnSetDateListener implementation

    public void onSetDate(Date date)
    {
        DOBField.setText(getStringFromDate(date));
    }


    // Date formatter helpers

    /**
     * Provides default date formatter which is used to format subject's Date Of Birth (DOB)
     * @return DateFormat object with {@value #DEFAULT_SUBJECT_DOB_DATE_FORMAT} format
     */
    private DateFormat getDateFormatter() {
        return new SimpleDateFormat(DEFAULT_SUBJECT_DOB_DATE_FORMAT, Locale.getDefault());
    }

    /**
     * Convert date to formatted string using {@value #DEFAULT_SUBJECT_DOB_DATE_FORMAT} date format
     * @param date - subject's date of birth
     * @return - formatted string with subject's DOB
     */
    private String getStringFromDate(Date date)
    {
        return getDateFormatter().format(date);
    }

    /**
     * Convert string with subject's DOB to Date object using {@value #DEFAULT_SUBJECT_DOB_DATE_FORMAT} date format
     * @param dateString - string with subject's DOB (should have {@value #DEFAULT_SUBJECT_DOB_DATE_FORMAT} date format)
     * @return - Date object of date which is contained in dateString string
     */
    private Date getDateFromString(String dateString)
    {
        Date parsedDate;
        try {
            parsedDate = getDateFormatter().parse(dateString);
        } catch (ParseException e) {
            parsedDate = null;
        }
        return parsedDate;
    }


    // User notification helper functions
    protected Dialog activeDialog = null;

    /**
     * Show a toast notification to the user
     *
     * @param message Message to toast
     */
    public void notifyUser(String message) {
        if (null == message) return;
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    /**
     * Show a ProgressDialog notification to the user
     * @param message Message to display
     */
    public void showProgress(String message) {
        hideActiveDialog();
        activeDialog = new ProgressDialog(getActivity());
        ((ProgressDialog)activeDialog).setMessage(message);
        activeDialog.setCancelable(false);
        activeDialog.show();
    }

    /**
     * Hides an ActiveDialog from user
     */
    public void hideActiveDialog() {
        if (null != activeDialog) {
            activeDialog.dismiss();
            activeDialog = null;
        }
    }
}
