package com.grassroots.petition.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{
    public static final String DATE_PICKER_FRAGMENT_TAG = "DATE_PICKER_FRAGMENT_TAG";
    private Date defaultDate;
    private DatePickerDialog dialog;

    private OnSetDateListener listener;
    public interface OnSetDateListener
    {
        public void onSetDate(Date date);
    }

    public void setListener(OnSetDateListener listener)
    {
        this.listener = listener;
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialogFragment(Date defaultDate)
    {
        this.defaultDate = defaultDate;
    }

    public DatePickerDialogFragment()
    {
        this(GregorianCalendar.getInstance().getTime());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the current date in 1965 as the default date in the picker
        final Calendar c = new GregorianCalendar();
        c.setTime(defaultDate);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int year = c.get(Calendar.YEAR);

        // Create a new instance of DatePickerDialog without calendar view (which is buggy)
        dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setCalendarViewShown(false);
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSetDateListener) {
            listener = (OnSetDateListener) activity;
        } else {
            //throw new ClassCastException(activity.toString() + " must implement DatePickerDialogFragment.OnSetDateListener interface.");
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day)
    {
        if (null != listener)
        {
            Calendar c = new GregorianCalendar(year, month, day);
            listener.onSetDate(c.getTime());
        }

        if (dialog != null)
        {
            dialog.dismiss();
        }
    }
}
