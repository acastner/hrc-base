package com.grassroots.petition.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.grassroots.petition.GlobalData;

public class BatteryInformationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int level = intent.getIntExtra("level", 0);
        GlobalData.BATTERYLEVEL = level;
        Log.e("battery level","here"+level);
    }

}
