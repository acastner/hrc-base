package com.grassroots.petition.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class RPSBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		  Toast.makeText(context,"Data Received from RPS", Toast.LENGTH_SHORT).show();
	      context.sendBroadcast(new Intent("302error"));
	}

}
