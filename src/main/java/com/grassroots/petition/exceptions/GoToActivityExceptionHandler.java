package com.grassroots.petition.exceptions;

import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.bugsense.trace.BugSenseHandler;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.Tracker;

import org.apache.log4j.Logger;

/**
 * Makes it so that on application crash, the user is returned to the login screen (or another screen
 * specified)
 */
public class GoToActivityExceptionHandler implements Thread.UncaughtExceptionHandler {
	private static final String TAG = GoToActivityExceptionHandler.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	public static final String KEY = "CRASHED";

	private final Context context;
	private final Class<? extends Activity> activityClass;

	public GoToActivityExceptionHandler(Context context) {
		this(context, ClassRetriever.getLoginActivity(context));
	}

	public GoToActivityExceptionHandler(Context context, Class<? extends Activity> activityClass) {
		this.context = context;
		this.activityClass = activityClass;
	}

	private static Class<? extends Activity> getLaunchActivity(Context context) {
		String launchClassName = context.getPackageManager().getLaunchIntentForPackage("com.grassroots.petition").getComponent().getClassName();
		try {
			Class<?> launchClass = Class.forName(launchClassName);
			if(launchClass != null &&
					Activity.class.isAssignableFrom(launchClass)) {
				return (Class<? extends Activity>) launchClass;
			}
		} catch (ClassNotFoundException e) {
			LOGGER.error("ClassNotFound: " + launchClassName, e);
			Tracker.appendLog("ERROR: " + "ClassNotFound: " + launchClassName);
		}
		LOGGER.error("Unable to determine launch activity, returning the default");
		Tracker.appendLog("ERROR: " + "Unable to determine launch activity, returning the default");
		return ClassRetriever.getLoginActivity(context);
	}

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {
		if(throwable instanceof Exception)
			BugSenseHandler.sendException((Exception)throwable);
		LOGGER.error("Uncaught exception on thread: " + thread + " returning to " + activityClass, throwable);

		Tracker.appendLog("ERROR: " + "Uncaught exception on thread: " + thread 
				+ " returning to " + activityClass + "\n" + throwable.getCause());

		Exception e = new Exception(throwable);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}

		Intent intent = new Intent(context, activityClass);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(KEY, true);
		intent.putExtra("username",((GlobalData)(((context.getApplicationContext())))).getLoggedInUserName());
		intent.putExtra("scriptid",((GlobalData)(((context.getApplicationContext())))).getPetition().getId());
		intent.putExtra("scriptname",((GlobalData)(((context.getApplicationContext())))).getPetition().getName());
		context.startActivity(intent);

		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(0);
	}
}
