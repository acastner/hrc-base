package com.grassroots.petition.exceptions;

import java.util.Calendar;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.utils.Tracker;

public class AAMVAException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6714189353194704545L;

    /**
     *
     */
    public AAMVAException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     * @param arg1
     */
    public AAMVAException(String arg0, Throwable arg1) {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    	Tracker.appendLog("ERROR: " + arg0);
  
		Exception e = new Exception(arg1);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}
    }

    /**
     * @param arg0
     */
    public AAMVAException(String arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub
    	Tracker.appendLog("ERROR: " + arg0);
    }

    /**
     * @param arg0
     */
    public AAMVAException(Throwable arg0) {
        super(arg0);
        // TODO Auto-generated constructor stub

		Exception e = new Exception(arg0);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}
    }


}