package com.grassroots.petition.exceptions;

import java.util.Calendar;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.utils.Tracker;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class JsonParseException extends RuntimeException {
    public JsonParseException(String msg) {
        super(msg);
        Tracker.appendLog("ERROR: " + msg);
    }

    public JsonParseException(Throwable throwable) {
        super(throwable);

		Exception e = new Exception(throwable);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}
    }
}
