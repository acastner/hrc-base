package com.grassroots.petition.exceptions;

import java.util.Calendar;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.utils.Tracker;

/**
 * Created by jeff on 11/6/13.
 */
public class DatabaseException extends Exception
{
	public DatabaseException()
	{
		super();
	}

	public DatabaseException(String detailMessage)
	{
		super(detailMessage);
		Tracker.appendLog("ERROR: " + detailMessage);
	}

	public DatabaseException(String detailMessage, Throwable throwable)
	{
		super(detailMessage, throwable);
		Tracker.appendLog("ERROR: " + detailMessage);

		Exception e = new Exception(throwable);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}
	}

	public DatabaseException(Throwable throwable)
	{
		super(throwable);

		Exception e = new Exception(throwable);

		//get first cause of exception
		while (e.getCause().getCause() != null)
		{
			e = (Exception) e.getCause();
		}

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";
		
		Tracker.appendLog(time + " ERROR: " + e.getCause());
		for (int i = 0; i < e.getCause().getStackTrace().length; i++)
		{
			Tracker.appendLog("[STACK TRACE] " + e.getCause().getStackTrace()[i]);
		}
	}
}
