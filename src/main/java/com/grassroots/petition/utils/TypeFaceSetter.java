package com.grassroots.petition.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Sets the font to Roboto on a view
 */
public class TypeFaceSetter {
    public enum FontType {
        REGULAR("Roboto-Regular.ttf"), BOLD("Roboto-Bold.ttf"), ITALIC("Roboto-Italic.ttf");
        private final String assetName;

        private FontType(String assetName) {
            this.assetName = assetName;
        }

        public String getAssetName() {
            return assetName;
        }
    }

    private static Typeface regularTypeFace;
    private static Typeface boldTypeFace;
    private static Typeface italicTypeFace;

    public static void setRobotoFont(Context context, View view) {
        try {
            if (regularTypeFace == null) {
                regularTypeFace = Typeface.createFromAsset(context.getAssets(), "fonts/" + FontType.REGULAR.getAssetName());
                boldTypeFace = Typeface.createFromAsset(context.getAssets(), "fonts/" + FontType.BOLD.getAssetName());
                italicTypeFace = Typeface.createFromAsset(context.getAssets(), "fonts/" + FontType.ITALIC.getAssetName());
            }
        } catch (Throwable t) {
            return;
        }
        setFont(view);
    }

    private static void setFont(View view) {
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setFont(((ViewGroup) view).getChildAt(i));
            }
        } else if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setTypeface(getTypeFace(textView.getTypeface()));
        }
    }

    private static Typeface getTypeFace(Typeface existingTypeFace) {
        if (existingTypeFace == null)
            return regularTypeFace;
        if (existingTypeFace.isBold()) {
            return boldTypeFace;
        } else if (existingTypeFace.isItalic()) {
            return italicTypeFace;
        } else {
            return regularTypeFace;
        }
    }

}
