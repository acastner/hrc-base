package com.grassroots.petition.utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.grassroots.petition.exceptions.JsonParseException;
import com.grassroots.petition.models.*;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Handles all the serializing and deserializing of JSON
 */
public class JsonParser {
    private static final String TAG = JsonParser.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private static final JsonParser parser = new JsonParser();

    public static JsonParser getParser() {
        return parser;
    }

    //************** Set up the GSON parser with our custom settings ******************
    private final Gson gson = new GsonBuilder()
            .disableHtmlEscaping()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .registerTypeAdapter(Question.Type.class, new QuestionTypeDeserializer())
            .registerTypeAdapter(KnockStatus.class, new KnockStatusSerializer())
            .registerTypeAdapter(SubjectAnswer.class, new SubjectAnswerSerializerDeserializer())
            .registerTypeAdapter(Cart.class, new CartSerializer())
            .registerTypeAdapter(Appointment.class, new AppointmentSerializer())
            .registerTypeAdapter(EventTimeInterval.class, new EventSerializerDeserializer())
            .registerTypeAdapter(FreeTimeInterval.class, new FreeTimeIntervalSerializerDeserializer())
            .registerTypeAdapter(PetitionAnswers.class, new PetitionAnswersSerializer())
            .registerTypeAdapter(InstantPrescreenRequest.class, new InstantPrescreenRequestSerializer())
            .create();

    private static class QuestionTypeDeserializer implements JsonDeserializer<Question.Type> {
        @Override
        public Question.Type deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws com.google.gson.JsonParseException {
            for (Question.Type questionType : Question.Type.values()) {
                String jsonType = jsonElement.getAsString();
                String questionJsonType = questionType.getJsonType();
                if (questionJsonType.equals(jsonType))
                    return questionType;
            }
            return null;
        }
    }

    public static JsonElement serializeTimeIntervalObject(BaseTimeInterval event) throws com.google.gson.JsonParseException
    {
        JsonObject serializedObject = new JsonObject();

        // serialize start time
        DateTimeFormatter dtf = event.getDateTimeFormatter();
        String startTimeString = dtf.print(new DateTime(event.getStartTime()));
        serializedObject.add("Start", new JsonPrimitive(startTimeString));


        // serialize duration
        String endTimeString = dtf.print(new DateTime(event.getEndTime()));
        serializedObject.add("End", new JsonPrimitive(endTimeString));


        // serialize title
        String titleString = event.getTitle();
        if (null != titleString) {
            serializedObject.add("Title", new JsonPrimitive(titleString));
        }

        return serializedObject;
    }

    public static BaseTimeInterval deserializeTimeIntervalObject(BaseTimeInterval event, JsonElement jsonElement) throws com.google.gson.JsonParseException
    {
        DateTimeFormatter dtf = event.getDateTimeFormatter();

        JsonObject jsonObject = jsonElement.getAsJsonObject();


        // parse "Start" field (required)
        JsonPrimitive startTimeJson = jsonObject.getAsJsonPrimitive("Start");
        if (null == startTimeJson || !startTimeJson.isString()) {
            throw new JsonParseException("Unable to parse \"Start\" field.");
        }

        DateTime startDateTime = dtf.parseDateTime(startTimeJson.getAsString());
        if (null == startDateTime) {
            throw new JsonParseException("Unable to parse \"Start\" date.");
        }
        event.setStartTime(startDateTime.toDate());


        // parse "End" field (required)
        JsonPrimitive endTimeJson = jsonObject.getAsJsonPrimitive("End");
        if (null == endTimeJson || !endTimeJson.isString()) {
            throw new JsonParseException("Unable to parse \"End\" field.");
        }

        DateTime endDateTime = dtf.parseDateTime(endTimeJson.getAsString());
        if (null == endDateTime) {
            throw new JsonParseException("Unable to parse \"End\" date.");
        }
        event.setEndTime(endDateTime.toDate());


        // parse "Title" field (optional)
        JsonPrimitive titleJson = jsonObject.getAsJsonPrimitive("Title");
        if (null == titleJson || !titleJson.isString()) {
//                throw new JsonParseException("Unable to parse \"Title\" date.");
        } else {
            event.setTitle(titleJson.toString());
        }

        return event;
    }

    private static class EventSerializerDeserializer implements JsonSerializer<EventTimeInterval>, JsonDeserializer<EventTimeInterval>
    {
        @Override
        public JsonElement serialize(EventTimeInterval event, Type type, JsonSerializationContext jsonSerializationContext) {
            return JsonParser.serializeTimeIntervalObject(event);
        }

        @Override
        public EventTimeInterval deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws com.google.gson.JsonParseException
        {
            EventTimeInterval event = new EventTimeInterval();
            JsonParser.deserializeTimeIntervalObject(event, jsonElement);

            return event;
        }
    }

    private static class FreeTimeIntervalSerializerDeserializer implements JsonSerializer<FreeTimeInterval>, JsonDeserializer<FreeTimeInterval>
    {
        @Override
        public JsonElement serialize(FreeTimeInterval event, Type type, JsonSerializationContext jsonSerializationContext) {
            return JsonParser.serializeTimeIntervalObject(event);
        }

        @Override
        public FreeTimeInterval deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws com.google.gson.JsonParseException
        {
            FreeTimeInterval event = new FreeTimeInterval();
            JsonParser.deserializeTimeIntervalObject(event, jsonElement);

            return event;
        }
    }

    private static class KnockStatusSerializer implements JsonSerializer<KnockStatus>, JsonDeserializer<KnockStatus> {
        @Override
        public JsonElement serialize(KnockStatus knockStatus, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(knockStatus.getRepresentation());
        }

        @Override
        public KnockStatus deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws com.google.gson.JsonParseException {
            try {
                int status = jsonElement.getAsInt();
                return KnockStatus.fromId(status);
            } catch (Throwable t) {
                return KnockStatus.NOT_KNOCKED;
            }
        }
    }

    private static class SubjectAnswerSerializerDeserializer implements JsonSerializer<SubjectAnswer>, JsonDeserializer<SubjectAnswer> {
        @Override
        public JsonElement serialize(SubjectAnswer subjectAnswer, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject serializedObject = new JsonObject();
            serializedObject.add("question", new JsonPrimitive(subjectAnswer.getQuestionId()));
            JsonElement answer = subjectAnswer.hasAnswerId() ?
                    new JsonPrimitive(subjectAnswer.getAnswerId()) :
                    subjectAnswer.hasPictureAnswer() ?
                            new JsonPrimitive(subjectAnswer.getPictureAsString()) :
                            new JsonPrimitive(
                                    subjectAnswer.hasStringAnswer() ?
                                            subjectAnswer.getStringAnswer() :
                                            ""
                            );
            serializedObject.add("answer", answer);
            return serializedObject;
        }

        @Override
        public SubjectAnswer deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws com.google.gson.JsonParseException {
            SubjectAnswer answer = new SubjectAnswer();
            JsonObject answerJsonObject = jsonElement.getAsJsonObject();
            JsonPrimitive questionIdJson = answerJsonObject.getAsJsonPrimitive("question");
            if (questionIdJson != null && questionIdJson.isNumber()) {
                answer.setQuestionId(questionIdJson.getAsInt());
            }

            JsonPrimitive answerJson = answerJsonObject.getAsJsonPrimitive("answer");
            if (answerJson != null) {
                if (answerJson.isNumber()) {
                    answer.setAnswerId(answerJson.getAsInt());
                } else {
                    answer.setStringAnswer(answerJson.getAsString());
                }
            }
            return answer;
        }
    }

    private static class CartSerializer implements JsonSerializer<Cart>
    {
        public static final String PAPER_CHECK_PAYMENT = "PaperCheck";
        public static final String CASH_PAYMENT = "Cash";

        @Override
        public JsonElement serialize (Cart cart, Type type, JsonSerializationContext jsonSerializationContext)
        {

            // If it's an empty cart (no products), don't send the structure.
            // The webapp will attempt to parse it and throw an error. Either it's complete or not sent at all.
            if( cart.getItems().isEmpty() )
            {
                return null;
            }

            JsonObject obj = new JsonObject();
            obj.add( "items", jsonSerializationContext.serialize( cart.getItems() ) );

            // Process electronic payments
            if( cart.getPaymentData() != null )
            {
                JsonObject childObj = jsonSerializationContext.serialize( cart.getPaymentData() ).getAsJsonObject();

                for( Map.Entry<String, JsonElement> entry : childObj.entrySet() )
                {
                    obj.add( entry.getKey(), entry.getValue() );
                }
            }

            // Process paper check payments
            if( cart.isPaperCheckPayment() )
            {
                obj.add( PAPER_CHECK_PAYMENT, new JsonPrimitive( true ) );
                cart.setMobileId(java.util.UUID.randomUUID().toString());
            }

            // Process cash payments
            if( cart.isCashPayment() )
            {
                obj.add( CASH_PAYMENT, new JsonPrimitive( true ) );
                cart.setMobileId(java.util.UUID.randomUUID().toString());
            }

            if(cart.getMobileId() != null) {
            obj.add("MobileId", new JsonPrimitive(cart.getMobileId()));
            }

            if(cart.getPaymentMethodType() != null) {
                obj.add("PaymentMethodType", new JsonPrimitive(cart.getPaymentMethodType()));
            }
            obj.add("PaymentProcessorTypeId", new JsonPrimitive(cart.getPaymentProcessorTypeId()));

            return obj;
        }
    }

    private static class AppointmentSerializer implements JsonSerializer<Appointment> {
        @Override
        public JsonElement serialize(Appointment appointment, Type type, JsonSerializationContext jsonSerializationContext) {

            if (null == appointment) return null;
            if (null == appointment.getDescription()) appointment.setDescription("");

            // serialize start time
            JsonObject serializedObject = new JsonObject();
            DateTimeFormatter dtf = appointment.getDateTimeFormatter();
            String startTimeString = dtf.print(new DateTime(appointment.getStartTime()));

            serializedObject.add("StartTime", new JsonPrimitive(startTimeString));


            // serialize duration
            long durationInMinutes = appointment.getDuration();
            serializedObject.add("Duration", new JsonPrimitive(durationInMinutes));


            // serialize description
            serializedObject.add("Description", new JsonPrimitive(appointment.getDescription()));
            
         // serialize calendar id
            serializedObject.add("CalendarId", new JsonPrimitive(appointment.getCalendarId()));

            // don't serialize

            return serializedObject;
        }
    }

    private static class InstantPrescreenRequestSerializer implements JsonSerializer<InstantPrescreenRequest>
    {
        @Override
        public JsonElement serialize(InstantPrescreenRequest prescreenRequest, Type type, JsonSerializationContext jsonSerializationContext)
        {
            if (null == prescreenRequest) return null;
            if (!prescreenRequest.isValid()) return null;

            JsonObject serializedObject = new JsonObject();

            serializedObject.add("FirstName", new JsonPrimitive(prescreenRequest.getFirstName()));
            serializedObject.add("LastName", new JsonPrimitive(prescreenRequest.getLastName()));
            serializedObject.add("Address", new JsonPrimitive(prescreenRequest.getAddress()));
            serializedObject.add("City", new JsonPrimitive(prescreenRequest.getCity()));
            serializedObject.add("State", new JsonPrimitive(prescreenRequest.getState()));
            serializedObject.add("ZipCode", new JsonPrimitive(prescreenRequest.getZipCode()));

            // optional
            if (null != prescreenRequest.getPhone()) {
                serializedObject.add("Phone", new JsonPrimitive(prescreenRequest.getPhone()));
            }


            // optional
            String DOBString = prescreenRequest.getFormattedDOB();
            if (null != DOBString) {
                serializedObject.add("DOB", new JsonPrimitive(DOBString));
            }

            return serializedObject;
        }
    }

    private static class PetitionAnswersSerializer implements JsonSerializer<PetitionAnswers> {
        @Override
        public JsonElement serialize(PetitionAnswers petitionAnswers, Type type, JsonSerializationContext jsonSerializationContext) {
            final Gson gson = new GsonBuilder()
                    .disableHtmlEscaping()
                    .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                    .registerTypeAdapter(Question.Type.class, new QuestionTypeDeserializer())
                    .registerTypeAdapter(KnockStatus.class, new KnockStatusSerializer())
                    .registerTypeAdapter(SubjectAnswer.class, new SubjectAnswerSerializerDeserializer())
                    .registerTypeAdapter(Cart.class, new CartSerializer())
                    .registerTypeAdapter(Appointment.class, new AppointmentSerializer())
                    .create();


            // Remove Empty answers
            List<SubjectAnswer> answers = petitionAnswers.getAnswers();
            List<SubjectAnswer> notEmptyAnswers = new ArrayList<SubjectAnswer>();
            for (SubjectAnswer answer: answers) {
                if (answer.hasAnswer()) {
                    notEmptyAnswers.add(answer);
                }
            }

            petitionAnswers.setAnswers(notEmptyAnswers);

            JsonObject obj = gson.toJsonTree(petitionAnswers).getAsJsonObject();

            Signature signature = petitionAnswers.getSignature();
            if (signature != null) {
                obj.add("signature", new JsonPrimitive(signature.asBase64EncodedString()));
            }

            return obj;
        }
    }

    public InstantPrescreenResponce toInstantPrescreenResponce(String jsonString) throws JsonParseException {
        return toRequestedType(jsonString, InstantPrescreenResponce.class);
    }

    public AppointmentCalendar toAppointmentCalendar(String json) throws JsonParseException {
        return toRequestedType(json, AppointmentCalendar.class);
    }

    public Petition toPetition(String json) throws JsonParseException {
        Petition petition = toRequestedType(json, Petition.class);
        
       /* LOGGER.debug("A PETITION JSON: " + json);
        Tracker.appendLog(Tracker.getTime() + " A PETITION JSON: " + json);*/
        
        Collections.sort(petition.getQuestions(), new Comparator<Question>() {
            @Override
            public int compare(Question q1, Question q2) {
                return (q1.getOrder() < q2.getOrder()) ? -1 : ((q1.getOrder() == q2.getOrder()) ? 0 : 1);
            }
        });
        return petition;
    }
    
    public LocationList toLocationList(String json) throws JsonParseException {
        LocationList locationlist = toRequestedType(json, LocationList.class);
        return locationlist;
    }

    public Tally toTally(String json) throws JsonParseException {
        Tally tally = toRequestedType(json, Tally.class);
        return tally;
    }

    public Recipient toRecipient(String json) throws JsonParseException {
        return toRequestedType(json, Recipient.class);
    }

    public Response toResponse(String json) throws JsonParseException {
        return toRequestedType(json, Response.class);
    }

    public Subject toSubject(String json) throws JsonParseException {
        return toRequestedType(json, Subject.class);
    }

    public List<Subject> toSubjectList(String json) throws JsonParseException {
        Type listType = new TypeToken<List<Subject>>() {
        }.getType();
        //String unwrappedJson = removeWrapping(json);
        return toRequestedType(json, listType);
    }

    public Walklist toWalklist(String json) throws JsonParseException {
        return toRequestedType(json, Walklist.class);
    }
    public NearestNeighborInfo toNearestNeighbor(String json) throws JsonParseException {
    	NearestNeighborInfo nearestNeighborInfo = toRequestedType(json, NearestNeighborInfo.class);
        return nearestNeighborInfo;
    }


    public Json fromJson(String json, Class<?> klass) {
        Object object = gson.fromJson(json, klass);
        return (Json) object;
    }

    public String jsonify(Object toJsonify) {
        return gson.toJson(toJsonify);
    }

    private String removeWrapping(String json) {
        if (Strings.isEmpty(json))
            return json;

        int openBracketPosition = json.indexOf("{");
        int closeBracketPosition = json.lastIndexOf("}");
        if (0 <= openBracketPosition &&
                0 < closeBracketPosition && closeBracketPosition < json.length()) {
            return json.substring(openBracketPosition + 1, closeBracketPosition - 1);
        } else {
            return json;
        }
    }

    public <T> T toRequestedType(String json, Type type) throws JsonParseException {
        long startTime = System.currentTimeMillis();
        if (Strings.isEmpty(json))
            json = "{}"; //makes this always return the object
        try {
            T returnObject = gson.fromJson(json, type);
            Performance.end(startTime, "Converting to type: " + type);
            return returnObject;
        } catch (Throwable t) {
            LOGGER.error("Failed parsing: " + json + " to: " + type, t);
            throw new JsonParseException(t);
        }
    }
}
