package com.grassroots.petition.utils;

import android.util.Log;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class Performance {
    private static final String TAG = "Performance";

    public static void end(long startTime, String messagePrepend) {
        long endTime = System.currentTimeMillis();
//        LOGGER.debug(messagePrepend + " took " + (endTime - startTime) + "ms");
        Log.d("Grassroots Performance: ", messagePrepend + " took " + (endTime - startTime) + "ms");
    }
}
