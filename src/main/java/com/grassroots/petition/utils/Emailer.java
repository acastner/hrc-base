package com.grassroots.petition.utils;



import android.util.Log;
import android.webkit.MimeTypeMap;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceAsyncClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;


/**
 * Send email via amazon ses service
 */
public class Emailer {
    private static final Logger LOGGER = Logger.getLogger(Emailer.class);

    /**
     * Amazon SES service access key
     */
    private static final String ACCESS_KEY = "AKIAJGGMZJCOQHRBTHEQ";

    /**
     * Amazon SES service secret key
     */
    private static final String SECRET_KEY = "OS1rcW335WVQKgRO+GfqGYrkmoZXyHKXXRp3IAr5";

    /**
     * Email destination address
     */
    //private static final String toAddress = "androidwildlogs@grassrootsunwired.com";
    private static final String toAddress = "androidwildlogs@grassrootsunwired.com";

    /**
     * Email from address
     */
    private static final String fromAddress = "androiddevice@grassrootsunwired.com";
    /**
     * destination address (for test purposes)
     */
    private static final String testAddress = "sharika@grassrootsunwired.com";

    // MIME types map, use this to map file extension to amazon ses supported type
    // if no value found, default android map will be used
    private final HashMap<String, String> MIME_TYPES_MAP = new HashMap<String, String>();
    {
        MIME_TYPES_MAP.put("log", "text/plain");
        MIME_TYPES_MAP.put("csv", "text/csv");
        MIME_TYPES_MAP.put("gz", null); // Amazon SES doesn't support archives
    }

    /**
     * Amazon SES client object
     */
    private AmazonSimpleEmailServiceAsyncClient sesClient;

    /**
     * Constructor
     */
    public Emailer() {
        AWSCredentials credentials = new BasicAWSCredentials( ACCESS_KEY, SECRET_KEY );
        sesClient = new AmazonSimpleEmailServiceAsyncClient( credentials );
    }

    /**
     * Send email with subject and body as strings
     * @param subject
     * @param messageBody
     */
    public void sendEmail(String subject, final String messageBody) {
        LOGGER.debug("Sending e-mail with messagebody: " + messageBody);
        Content subjectContent = new Content(subject);
        Body body = new Body(new Content(messageBody));
        Message message = new Message(subjectContent, body);
        Destination destination = new Destination().withToAddresses(toAddress);
        SendEmailRequest request = new SendEmailRequest(fromAddress,destination,message);
        sesClient.sendEmailAsync(request, new AsyncHandler<SendEmailRequest, SendEmailResult>() {
            @Override
            public void onError(Exception e) {
                LOGGER.error("Exception trying to send " + messageBody, e);
            }

            @Override
            public void onSuccess(SendEmailRequest sendEmailRequest, SendEmailResult sendEmailResult) {
                LOGGER.debug("Succesfully sent " + messageBody + " got back: " + sendEmailResult.getMessageId());
            }
        });
    }


    /**
     * Send email with Attachments
     * Only few file types are supported
     * Please refer to amazon ses client documentation
     * @param subject
     * @param messageBody
     * @param attachmentsArray
     * @return
     */
    public boolean sendEmailWithAttachments(final String subject, final String messageBody, List<String> attachmentsArray) {
        LOGGER.debug("Sending e-mail with messagebody: " + messageBody + " and attachments:" + attachmentsArray);

        Session session = Session.getInstance(new Properties(), null);
        if (session == null) {
            LOGGER.error("Couldn't get session instance");
            return false;
        }

        MimeMessage mimeMessage = new MimeMessage(session);
        InternetAddress fromInternetAddress = new InternetAddress();
        InternetAddress toInternetAddress = new InternetAddress();

        try {
            fromInternetAddress.setAddress(fromAddress);
            toInternetAddress.setAddress(toAddress);
        } catch (Exception e) {
            LOGGER.debug("Invalid internet address");
            return false;
        }

        if (fromInternetAddress == null || toInternetAddress == null) {
            LOGGER.debug("Invalid internet address");
            return false;
        }

        // Add a MIME part to the message
        MimeMultipart mimeMultipart = new MimeMultipart();

        // Sender and recipient
        try {
            mimeMessage.setFrom(fromInternetAddress);
            mimeMessage.setRecipient(javax.mail.Message.RecipientType.TO, toInternetAddress);

            //Subject
            mimeMessage.setSubject(subject);

            BodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent(messageBody, "text/plain");

            mimeMultipart.addBodyPart(bodyPart);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Unable to format message body");
            return false;
        }

        try {
            for (String fileName : attachmentsArray) {
                MimeBodyPart mimeBodyPart = new MimeBodyPart();

                String nameOfFile = fileName.substring(fileName.lastIndexOf('/') + 1, fileName.length());

                mimeBodyPart.setFileName(nameOfFile);
                mimeBodyPart.setDescription(nameOfFile, "UTF-8");

                String mimeType = getMimeType(fileName);
                if (mimeType == null) {
                    LOGGER.error("Cannot get mimeType for fileName:" + fileName);
                    continue;
                }
                Log.d("filename", "filename to email : " + fileName);

                ByteArrayDataSource dataSource = new ByteArrayDataSource(new FileInputStream(new File(fileName)), mimeType);
                mimeBodyPart.setDataHandler(new DataHandler(dataSource));

                mimeMultipart.addBodyPart(mimeBodyPart);
            }
            mimeMessage.setContent(mimeMultipart);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Unable to format message body with attachments");
            //return false;
        }

        RawMessage rawMessage = new RawMessage();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            mimeMessage.writeTo(out);
            rawMessage.setData(ByteBuffer.wrap(out.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Unable to convert MimeMessage to RawMessage");
            return false;
        }

        SendRawEmailRequest request = new SendRawEmailRequest().withRawMessage(rawMessage);

        SendRawEmailResult result = null;
        try {
            result = sesClient.sendRawEmail(request);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.debug("Unable to send email");
            return false;
        }

        if (result == null || result.getMessageId() == null) {
            LOGGER.debug("Unable to send email. result == null");
            return false;
        }

        return true;
    }

    /**
     * Convert fileType to mimeType
     * @param fileName
     * @return
     */
    private String getMimeType(String fileName) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(fileName);
        if (extension != null) {

            type = MIME_TYPES_MAP.get(extension);

            if (type == null) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                type = mime.getMimeTypeFromExtension(extension);
            }
        }
        return type;
    }
}
