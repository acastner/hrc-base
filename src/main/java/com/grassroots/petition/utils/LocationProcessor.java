package com.grassroots.petition.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.*;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.services.LocationService;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

public class LocationProcessor {

    private static final String TAG = LocationProcessor.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    public static final int LOCATION_FIX_ACCURACY = 100;            // Required Accuracy in meters
    private static final int TIMEOUT_GPS = 120 * 1000;              // Two Minutes
    private static final int TIMEOUT_NETWORK = 3 * 1000;            // Three Seconds
    public static final int LOCATION_REFRESH_TIME = 10 * 60 * 1000; // Ten minutes
    private static final int TIME_NETWORK_REFRESH = 500;
    private static final int TIME_GPS_REFRESH = 0;
    private static final int MIN_FIX_DISTANCE = 0;
    public static final int MAX_GEOCODER_RESULTS = 10;

	public static final String LOCATION_INTENT = "com.grassroots.petition.LOCATION_INTENT";
	public static final String LOCATION_BODY = "com.grassroots.petition.LOCATION_BODY";
	public static final String LOCATION_STATUS = "com.grassroots.petition.LOCATION_STATUS";
    private static final int LOCATION_MESSAGE = 99;

	public static enum LocationStatus {
		FOUND, DISABLED, TIMEOUT
	}

	private static LocationManager locManager;
	private static LocationProcessor instance;
	private static Handler handler;
	private static Context context;

	private static List<String> actions = new CopyOnWriteArrayList<String>();

	public static LocationProcessor getInstance(Context context) {
        LocationProcessor.context = context;

		if (instance == null) {
			instance = new LocationProcessor();
			locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

			handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					super.handleMessage(msg);
					if (msg.what == LOCATION_MESSAGE) {
                        LOGGER.debug("Location timer has been performed");
						locManager.removeUpdates(locationListener);

						Location lastKnownLocation = instance.getLastKnownLocation();
						broadcastLocation(lastKnownLocation, LocationStatus.TIMEOUT);
					}
				}
			};
		}
		return instance;
	}


    public boolean isGpsLocationProviderEnabled() {
        return locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isGpsLocationProviderEnabled(Context context) {
        return getInstance(context).isGpsLocationProviderEnabled();
    }

    public boolean isNetLocationProviderEnabled() {
        return locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isNetLocationProviderEnabled(Context context) {
        return getInstance(context).isGpsLocationProviderEnabled();
    }






    /**
     * Get LastKnown location based on accuracy and time since reading, from gps and cell
     * static method
     */
    public static GpsLocation getLastKnownGpsLocation(Context context) {
        return getInstance(context).getLastKnownGpsLocation();
    }

    /**
     * Get LastKnown location based on accuracy and time since reading, from gps and cell
     */
    public GpsLocation getLastKnownGpsLocation() {
        return new GpsLocation(getLastKnownLocation());
    }

    public static Location getLastKnownLocation(Context context) {
        return getInstance(context).getLastKnownLocation();
    }

    /**
     * Get LastKnown location based on accuracy and time since reading, from gps and cell
     */
    public Location getLastKnownLocation() {
        Location lastGPSLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location lastNetLocation = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        return getBetterLocation(lastGPSLocation, lastNetLocation);
    }

    /**
     * Get Better location based
     * @param location1 Location from GPS Location provider
     * @param location2 Location from Network Location provider
     */
    private Location getBetterLocation(Location location1, Location location2) {
        if (location1 == null && location2 == null) {
            return null;
        } else {
            if (isBetterLocation(location1, location2)) {
                return location1;
            } else {
                return location2;
            }
        }
    }







    private static LocationListener locationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            LOGGER.debug("Received location fix: " + ((location != null) ? location.toString() : null) + " from provider:"+ (((location != null) ? location.getProvider() : null)) );
            if (location.getAccuracy() > LOCATION_FIX_ACCURACY) {
                LOGGER.debug("Accuracy > " + LOCATION_FIX_ACCURACY + ", so waiting for something new");
                return;
            }

            handler.removeMessages(LOCATION_MESSAGE);
            locManager.removeUpdates(locationListener);

            broadcastLocation(location, LocationStatus.FOUND);
        }
    };

	public void findAccurateLocation(String action) {
		final boolean networkProviderEnabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		final boolean gpsProviderEnabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!networkProviderEnabled && !gpsProviderEnabled) {
			LOGGER.error("There is no enabled providers");
			broadcastLocation(null, LocationStatus.DISABLED);
			return;
		}

		if (!actions.contains(action)) {
			actions.add(action);
		}
		if (actions.size() > 1) {
			LOGGER.debug("findExactLocation already running");
			return;
		}

		Message msg = new Message();
		msg.what = LOCATION_MESSAGE;

        if (gpsProviderEnabled)
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_GPS_REFRESH, MIN_FIX_DISTANCE, locationListener);
		if (networkProviderEnabled)
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_NETWORK_REFRESH, MIN_FIX_DISTANCE, locationListener);


        handler.sendMessageDelayed(msg, gpsProviderEnabled ? TIMEOUT_GPS : TIMEOUT_NETWORK);

		LOGGER.debug("Location timer has been started");
	}

    private static void broadcastLocation(Location location, LocationStatus status) {
        for (String action : actions) {
            LOGGER.debug("Broadcast the location: " + ((location != null) ? location.toString() : null) + " for action:"
                    + action);
            Intent locationIntent = new Intent(action);
            locationIntent.putExtra(LOCATION_BODY, location);
            locationIntent.putExtra(LOCATION_STATUS, status);
            context.sendBroadcast(locationIntent);
        }

        actions.clear();
    }







    public static void startLocationPolling(Activity currentActivity) {
        startPollingService(currentActivity, LOCATION_REFRESH_TIME);
    }

    public static void stopLocationPolling(Activity currentActivity) {
        stopPollingService(currentActivity);
    }

    private static void startPollingService(Activity currentActivity, int repeatTimeMs) {
        AlarmManager alarmManager = (AlarmManager) currentActivity.getSystemService(Activity.ALARM_SERVICE);
        Intent startServiceIntent = new Intent(currentActivity, LocationService.class);

        /*
        startPollingIntent.putExtra(LocationPoller.EXTRA_INTENT,
                new Intent(currentActivity, receiverClass));
        startPollingIntent.putExtra(LocationPoller.EXTRA_PROVIDER, provider);
        */

        PendingIntent pendingIntent = PendingIntent.getService(currentActivity, 0, startServiceIntent, 0);
        alarmManager.setRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP, 0, repeatTimeMs,
                pendingIntent);
    }

    private static void stopPollingService(Activity currentActivity) {
        AlarmManager alarmManager = (AlarmManager) currentActivity.getSystemService(Activity.ALARM_SERVICE);

        Intent startServiceIntent = new Intent(currentActivity, LocationService.class);
        PendingIntent pendingIntent = PendingIntent.getService(currentActivity, 0, startServiceIntent, 0);

        alarmManager.cancel(pendingIntent);
    }






    public static List<Address> getNearbyAddresses(Context context) {
        return getInstance(context).getNearbyAddresses();
    }

    @SuppressLint("newApi")
    public List<Address> getNearbyAddresses() {
        Location currentLocation = getLastKnownLocation();

        if (currentLocation == null) {
            return new ArrayList<Address>();
        }

        double lat = currentLocation.getLatitude();
        double lng = currentLocation.getLongitude();

        if (Build.VERSION.SDK_INT >= 11) {
            if (!Geocoder.isPresent()) {
                LOGGER.error("No Geocoder present, returning an empty list of Addresses");
                return new ArrayList<Address>();
            }
        }

        List<Address> addressList = null;
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            addressList = geocoder.getFromLocation(lat, lng, MAX_GEOCODER_RESULTS);
        }
        catch (Exception e) {
            addressList = new ArrayList<Address>();
        }

        return addressList;
    }







    //********Code taken from http://developer.android.com/guide/topics/location/strategies.html ***********
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    protected static boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        } else if (location == null) {
            return false;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
