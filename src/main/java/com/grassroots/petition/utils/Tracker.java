package com.grassroots.petition.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import com.grassroots.petition.GlobalData;

import android.app.Application;
import android.content.Context;

/**
 * This class is for logging and giving a clear view of user activity
 * 
 * @author ciara
 *
 */

public class Tracker extends Application{

	private static Context context;
	
	private static String TRACKER_LOGFILE_NAME = "tracker";
    public static boolean newSession = false;
	
	@Override
    public void onCreate() {

        context = getApplicationContext();

        super.onCreate();
	}
	
	public static void logMemoryStatus()
	{
		Runtime info = Runtime.getRuntime();

		//LOGGER.debug("MEMORYSTUFF - " + mi.getTotalPrivateDirty()/(float)1000);

		float totalMem = info.totalMemory()/(float)1000000;
		float usedMem = totalMem - (info.freeMemory()/(float)1000000);

		//LOGGER.debug("MEMORYSTUFF - Heap Size (MB): " + totalMem + ", Allocated Memory (MB): " + usedMem);
		appendLog("[MEMORY]" + " Heap Size (MB): " + totalMem + ", Allocated Memory (MB): " + usedMem);
	}
	
	 /**
     * for writing to log file
     * @param text
     */
    public static void appendLog(String text)
    {  
    	if (newSession)
    	{
    		Calendar cl = Calendar.getInstance();
            
            int month = cl.get(Calendar.MONTH)+1;
            
            TRACKER_LOGFILE_NAME = "tracker" + "-" + month + "-" + cl.get(Calendar.DAY_OF_MONTH)
            		+ "-" + cl.get(Calendar.YEAR) + ".log";
            
            newSession = false;
    	}
    	
       File logFile = new File(GlobalData.TMP_DIRECTORY + File.separator + TRACKER_LOGFILE_NAME);
       if (!logFile.exists())
       {
          try
          {
             logFile.createNewFile();
          } 
          catch (IOException e)
          {
             // TODO Auto-generated catch block
             e.printStackTrace();
          }
       }
       try
       {
          //BufferedWriter for performance, true to set append to file flag
          BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
          buf.append(text);
          buf.newLine();
          buf.flush();
          buf.close();
       }
       catch (IOException e)
       {
          // TODO Auto-generated catch block
          e.printStackTrace();
       }
    }
    
    public static String getTime()
    {
    	Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "] ";
		
		return time;
    }
}
