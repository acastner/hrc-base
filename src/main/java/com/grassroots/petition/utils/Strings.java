package com.grassroots.petition.utils;

// TODO: Why do we have empty / not empty functions of our own? Check where this is used and replace with TextUtils
public class Strings {

    public static boolean isEmpty(String s) {
        return s == null || (s.length() == 0);
    }

    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }
}
