package com.grassroots.petition.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * BatteryPerformance returns values for several battery dependent values like
 * brightness,wifi connection,bluetooth,runing apps,etc To Test from your
 * activity : BatteryPerformance batteryPerformance = new
 * BatteryPerformance(this); //running processes
 * Log.d("running app processess","running processes : "+
 * batteryPerformance.getRunningAppsCount()); //brightness
 * Log.d("SCREEN_BRIGHTNESS"
 * ,"SCREEN_BRIGHTNESS : "+batteryPerformance.getScreenBrightness()); //blue
 * tooth
 * Log.d("is enabled : ","is enabled: "+batteryPerformance.isBluetoothEnabled
 * ()); //bonded devices
 * Log.d("bluetooth name","bt getBondedDevices : "+batteryPerformance
 * .getBondedDevices()); // wifi
 * Log.d("is wifi connected","WIFI : ?"+batteryPerformance.isWifiConnected());
 * 
 * @author sharika
 * 
 */
public class BatteryPerformance {

    private Context context = null;

    public BatteryPerformance(Context context) {
        this.context = context;
    }

    public int getRunningAppsCount() {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Activity.ACTIVITY_SERVICE);
        return am.getRunningAppProcesses().size();
    }

    public float getScreenBrightness() {
        try {
            float curBrightnessValue = android.provider.Settings.System.getInt(
                    context.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
            return curBrightnessValue;
            // between 0 & 255
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return -1;
        }

    }

    public boolean isBluetoothEnabled() {
        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
        if(bluetooth != null){
            return bluetooth.isEnabled();
        }else{
            return false;
        }
        
    }

    public int getBondedDevices() {
        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
        if(bluetooth != null){
            return bluetooth.getBondedDevices().size();
        }else{
            return 0;
        }
        
    }

    public boolean isWifiConnected() {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    // get imei
    public String getIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
        Log.e("imei","imei : "+imei);
        return imei;
    }

    public String getNetworkType() {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = telephonyManager.getNetworkType();

		if(isWifiConnected())
			return "Wifi";

        switch (networkType) {
        case TelephonyManager.NETWORK_TYPE_1xRTT:
            return ("1xRTT");
        case TelephonyManager.NETWORK_TYPE_CDMA:
            return ("CDMA");
        case TelephonyManager.NETWORK_TYPE_EDGE:
            return ("EDGE");
        case TelephonyManager.NETWORK_TYPE_EHRPD:
            return ("eHRPD");
        case TelephonyManager.NETWORK_TYPE_EVDO_0:
            return ("EVDO rev. 0");
        case TelephonyManager.NETWORK_TYPE_EVDO_A:
            return ("EVDO rev. A");
        case TelephonyManager.NETWORK_TYPE_EVDO_B:
            return ("EVDO rev. B");
        case TelephonyManager.NETWORK_TYPE_GPRS:
            return ("GPRS");
        case TelephonyManager.NETWORK_TYPE_HSDPA:
            return ("HSDPA");
        case TelephonyManager.NETWORK_TYPE_HSPA:
            return ("HSPA");
        case TelephonyManager.NETWORK_TYPE_HSPAP:
            return ("HSPA+");
        case TelephonyManager.NETWORK_TYPE_HSUPA:
            return ("HSUPA");
        case TelephonyManager.NETWORK_TYPE_IDEN:
            return ("iDen");
        case TelephonyManager.NETWORK_TYPE_LTE:
            return ("LTE");
        case TelephonyManager.NETWORK_TYPE_UMTS:
            return ("UMTS");
        case TelephonyManager.NETWORK_TYPE_UNKNOWN:
            return ("Unknown");
        }
        return ("Unknown");

    }
}
