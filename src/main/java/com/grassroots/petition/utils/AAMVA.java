/**
 *
 */
package com.grassroots.petition.utils;

import com.grassroots.petition.exceptions.AAMVAException;
import com.grassroots.petition.models.Subject;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;


/**
 * @author Jeffrey Labonski
 *
 */
public class AAMVA
{

    public String raw;

    public String firstName, middleName, lastName, suffix, address1, address2;
    public String city, state, ZIP, DLID, country;
    public String DLClass, restrictions, endorsements, sex, height, weight, hair, eye;


    final private String   MSR_DELIM_TRACK = "%?;#";
    final private String[] MSR_DELIM_FIELD = {"^", "="};
    final private String   MSR_DELIM_SUBFIELD = "$";

    final private int TRACK1 = 1;
    final private int TRACK2 = 2;
    final private int TRACK3 = 3;

    public static Subject parsePDF417ToSubject(String barcodeData) {
        AAMVA aamva = new AAMVA();
        aamva.parsePDF417(barcodeData);
        return aamva.toSubject();
    }

    public Subject toSubject() {
        return new Subject(address1, address2, city, state, ZIP, country, lastName, firstName, middleName, suffix, DLID );
    }

    public void parsePDF417(String barcodeData)
    {
        String element, data;

        // Pull out header.
        String header = barcodeData.substring(0, 19);
        barcodeData   = barcodeData.substring(19);
        
        // Parse out the record separator.
        String separator = header.substring(1,  2);

        //Parse out subfiles. We ignore them! They're 10 bytes long each and start with "DL" or "Z"
        while(true){
            if(!barcodeData.startsWith("DL") && !barcodeData.startsWith("Z"))
                break;

            barcodeData = barcodeData.substring(10);
        }
        
        String lines[] = barcodeData.split("\\r?\\n");

        //String[] lines = barcodeData.split(separator);

        // iterate over the records and assign data
        for(int i = 0; i < lines.length; i++){

        	if(lines[i].length()>3){
            element = lines[i].substring(0, 3);
            data = lines[i].substring(3);

            if(Strings.isEmpty(data))
                continue;

            if("DAA".equals(element)) {
                // This is a nonstandard PA extension maybe perhaps?
                String[] names = data.split(" ");
                this.firstName = names[0];
                if(names.length == 2)
                    this.lastName = names[1];
                else if(names.length >= 3) {
                    this.middleName = names[1];
                    this.lastName = names[2];
                }
            }
            else if("DCU".equals(element)) {
                this.suffix = data;
            }
            else if("DAG".equals(element)) {
                this.address1 = data;
            }
            else if("DAH".equals(element)) {
                this.address2 = data;
            }
            else if("DAI".equals(element)) {
                this.city = data;
            }
            else if("DAJ".equals(element)) {
                this.state = data;
            }
            else if("DAK".equals(element)) {
                this.ZIP = data;
            }
            else if("DCS".equals(element)) {
                this.lastName = data;
            }
            else if("DAC".equals(element)) {
                this.firstName = data;
            }
            else if("DCT".equals(element)) {
                this.firstName = data;
            }
            else if("DAD".equals(element)) {
                this.middleName = data;
            }
            else if("DBC".equals(element)) {
                this.sex = data;
            }
            else if("DAY".equals(element)) {
                this.eye = data;
            }
            else if("DAU".equals(element)) {
                this.height = data;
            }
            else if("DAQ".equals(element)) {
                this.DLID = data;
            }
            else if("DAZ".equals(element)) {
                this.hair = data;
            }
            else if("DCM".equals(element)) {
                this.DLClass = data;
            }
            else if("DCN".equals(element)) {
                this.endorsements = data;
            }
            else if("DCO".equals(element)) {
                this.restrictions = data;
            }
        }
        }



    }

    /**
     * Parses magnetic stripe data.
     *
     * This is ugly. There's only so many bits per inch here, and so the data is really, really
     * compacted and problematic to get at. We go slowly, parse what we can, and continuously eat at
     * the input string and its variable length fields.
     *
     *  The eating is done through the helper function 'peel', which returns a List of Strings.
     *
     *  In reality we're dealing with 7-bit strings! Think space. This spec violates ANSI D-20. Track 3
     *  violates ISO/IEC 7811-6. Proceed with caution.
     *
     *
     * @param stripeData
     * @throws AAMVAException
     */
    public void parseMagneticStripe(String stripeData) throws AAMVAException
    {
        StringTokenizer st;
        String track1, track2, track3 = null;
		
		/*
		 * Parse out tracks. Gracefully handle lacking a track3 (due to hardware).
		 */
        st = new StringTokenizer(stripeData, MSR_DELIM_TRACK);
        try{
            track1 = st.nextToken();
            track2 = st.nextToken();
            if(st.hasMoreTokens())
                track3 = st.nextToken();
        }catch(NoSuchElementException e){
            throw new AAMVAException("Unable to parse track data", e);
        }


        parseTrack1(track1);
        parseTrack2(track2);
        if(track3 != null)
            parseTrack3(track3);

    }


    private void parseTrack1(String track) throws AAMVAException
    {
        List<String> fields;

        fields = peel(track, 2, TRACK1, "State");
        this.state = fields.get(1);

        fields = peel(fields.get(0), 13, TRACK1, "City");
        this.city = fields.get(1);

        fields = peel(fields.get(0), 35, TRACK1, "Name");
        this.lastName = fields.get(1);
        try{
            this.firstName = fields.get(2);
            this.suffix = fields.get(3);
        }catch(IndexOutOfBoundsException e){
            // Ignored, may not have a first or suffix. Believe it or not.
        }

        fields = peel(fields.get(0), 29, TRACK1, "Address");
        this.address1 = fields.get(1);
        try{
            this.address2 = fields.get(2);
        }catch(IndexOutOfBoundsException e){
            // Ignored, may not have an address 2
        }
    }

    private void parseTrack2(String track) throws AAMVAException
    {
        List<String> fields;

        fields = peel(track, 6, TRACK2, "ISO IIN");
        // Ignore

        fields = peel(fields.get(0), 13, TRACK2, "DL ID");
        this.DLID = fields.get(1);

        fields = peel(fields.get(0), 4, TRACK2, "Expiration Date");
        // Ignore

        fields = peel(fields.get(0), 8, TRACK2, "Birthdate");
        // Ignore

        fields = peel(fields.get(0), 5, TRACK2, "DL ID Overflow");
        try{
            this.DLID = this.DLID + fields.get(1).trim();
        }catch(IndexOutOfBoundsException e){
            // Ignored, may not have extended DLID data
        }



    }

    private void parseTrack3(String track) throws AAMVAException
    {

        List<String> fields;

        fields = peel(track, 1, TRACK3, "CDS Version #");
        // Ignore

        fields = peel(fields.get(0), 1, TRACK3, "Jurisdiction Version #");
        // Ignore

        fields = peel(fields.get(0), 11, TRACK3, "ZIP");
        this.ZIP = fields.get(1).trim();

        fields = peel(fields.get(0), 2, TRACK3, "Class");
        this.DLClass = fields.get(1).trim();

        fields = peel(fields.get(0), 10, TRACK3, "Restrictions");
        this.restrictions = fields.get(1).trim();

        fields = peel(fields.get(0), 4, TRACK3, "Endoresements");
        this.endorsements = fields.get(1).trim();

        fields = peel(fields.get(0), 1, TRACK3, "Sex");
        this.sex = fields.get(1);

        fields = peel(fields.get(0), 3, TRACK3, "Height");
        this.height = fields.get(1).trim();

        fields = peel(fields.get(0), 3, TRACK3, "Weight");
        this.weight = fields.get(1).trim();

        fields = peel(fields.get(0), 3, TRACK3, "Hair");
        this.hair = fields.get(1).trim();

        fields = peel(fields.get(0), 3, TRACK3, "Eye");
        this.eye = fields.get(1).trim();

    }


    /**
     * Peels off a field, splits subfields, and returns the (now shortened) remaining string
     * @param input
     * @return List of Strings. Element 0 is always the remaining input string less this field,
     * with elements 1-n being the field data, depending on subfields.
     * @throws AAMVAException
     */
    private List<String> peel(String input, int maxLength, int track, String fieldName) throws AAMVAException
    {
        List<String> retval = new ArrayList<String>();
        String field;

        // Input can be shorter than maxlength.
        if(input.length() < maxLength)
            maxLength = input.length();

        try{
            // FIXME: This needs to support multiple delims for track 2.
            int index = indexOf(input.substring(0, maxLength), MSR_DELIM_FIELD);
            //int index = input.substring(0, maxLength).indexOf(MSR_DELIM_FIELD);
            if(index == -1){
                // The field is at it max, 35 characters. Peel it off
                field = input.substring(0,  maxLength);
                input = input.substring(maxLength);
            }else{
                field = input.substring(0, index);
                input = input.substring(index + 1);
            }
        }catch(IndexOutOfBoundsException e){
            throw new AAMVAException("Unable to parse track " + track + ", field " + fieldName, e);
        }


        retval.add(input);

        StringTokenizer st = new StringTokenizer(field, MSR_DELIM_SUBFIELD);
        while(st.hasMoreTokens())
            retval.add(st.nextToken());

        return retval;

    }


    private int indexOf(String s, String[] delims)
    {
        int index = -1;
        for(int i = 0; i < delims.length; i++){
            int foo = s.indexOf(delims[i]);
            if(foo == -1)
                continue;
            if(index == -1)
                index = foo;
            if(foo < index)
                index = foo;
        }

        return index;
    }


    /**
     * @param args
     */
    public static void main(String[] args) {

        String msr = "%PABELMONT HILLSLABONSKI$JEFFREY^1A MAPLE AVE$^?;63602524638882=140919780831=?#2019004      C */1       ----M71 0  UNKBLU                          F]%!(_     ?";
        String pdf = "@\n\u001e\rAAMVA6360250101DL00290189DAQ24638882\nDAAJEFFREY LABONSKI\nDAG1A MAPLE AVE\nDAIBELMONT HILLS\nDAJPA\nDAK19004\nDARC\nDAS*/1\nDAT---- \nDAU511\nDAYBLU\nDBA20140901\nDBB19780831\nDBCM\nDBD20100803\nDBF00\nDBHY";

        AAMVA parser = new AAMVA();
        try{
            parser.parseMagneticStripe(msr);
        }catch(Exception e){
            System.out.println("Exception: " + e.getMessage());
            System.exit(1);
        }

        System.out.println("MSR Parse success: " + parser.firstName + ", " + parser.lastName);

        parser = new AAMVA();
        try{
            parser.parsePDF417(pdf);
        }catch(Exception e){
            System.out.println("Exception: " + e.getMessage());
            System.exit(1);
        }

        System.out.println("PDF  Parse success: " + parser.firstName + ", " + parser.lastName);

    }

}