package com.grassroots.petition.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import com.grassroots.petition.R;

/**
 * Provides instances of the 'standard' classes, based on the resources defined in classes.xml
 */
public class ClassRetriever {

    public static Class<? extends Activity> getLoginActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.login_class));
    }

    public static Class<? extends Activity> getLogoutPromtActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.logout_promt_class));
    }


    public static Class<? extends Activity> getWalklistActivity(Context context) {
       return getActivityFromClassName(context.getResources().getString(R.string.walklist_class));
        // Support for small displays (Samsung dart)
//        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
//            // Assume Android 4.0, 7.0" tablet screen size
//            return getActivityFromClassName(context.getResources().getString(R.string.walklist_class));
//        }
//
//        // Assume Android 2.2, Samsung Dart
//        return getActivityFromClassName(context.getResources().getString(R.string.walklist_with_tabs_class));
    }

    public static Class<? extends Activity> getTerminationActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.termination_class));
    }

    public static Class<? extends Activity> getSubjectInformationActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.subject_class));
    }

    public static Class<? extends Activity> getQuestionActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.question_class));
    }

    public static Class<? extends Activity> getSignatureActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.signature_class));
    }

    public static Class<? extends Activity> getProductActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.product_class));
    }

    public static Class<? extends Activity> getDonationActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.donation_class));
    }

    public static Class<? extends Activity> getReferrerActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.referrer_class));
    }

    public static Class<? extends Activity> getSubmitActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.submit_class));
    }

    public static Class<? extends Activity> getMarketingActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.marketing_class));
    }

    public static Class<? extends Activity> getMailingActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.mailing_screen_class));
    }

    public static Class<? extends Activity> getAppointmentsActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.appointments_class));
    }

    public static Class<? extends Activity> getInstantPrescreenActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.instant_prescreen_class));
    }

    public static Class<? extends Activity> getActivityForResourceId(Context context, int id) {
        return getActivityFromClassName(context.getResources().getString(id));
    }
    
    public static Class<? extends Activity> getLocationListActivity(Context context) {
        return getActivityFromClassName(context.getResources().getString(R.string.locationlist_class));
    }
    
    public static Class<? extends Activity> getNextActivityAfterSpecifiedActivityNameId(Context context, int specifiedActivityNameId) {
        if (specifiedActivityNameId < 0) {
            return null;
        }

        TypedArray orderArray = context.getResources().obtainTypedArray(R.array.screen_order);
        if (null == orderArray || orderArray.length() < 2) {
            return null;
        }

        for (int i = 0; i < orderArray.length(); i++) {
            int activityNameId = orderArray.getResourceId(i, -1);
            if (activityNameId == specifiedActivityNameId) {
                if (i + 1 < orderArray.length()) {
                    int nextActivityNameId = orderArray.getResourceId(i, -1);
                    if (nextActivityNameId < 0) {
                        return null;
                    }
                    return ClassRetriever.getActivityForResourceId(context, nextActivityNameId);
                }
            }
        }

        return null;
    }

    // TODO: remove
    public static Class<? extends Activity> getFirstActivity(Context context) {
        TypedArray orderArray = context.getResources().obtainTypedArray(R.array.screen_order);

        if (null != orderArray && orderArray.length() > 0) {
            return ClassRetriever.getActivityForResourceId(context, orderArray.getResourceId(0, -1));
        }

        return getSubjectInformationActivity(context);
    }

    public static Class<? extends Activity> getLastActivity(Context context) {
        TypedArray orderArray = context.getResources().obtainTypedArray(R.array.screen_order);

        if (orderArray != null && orderArray.length() > 0) {
            return ClassRetriever.getActivityForResourceId(context, orderArray.getResourceId(orderArray.length()-1, -1));
        }

        return getSubmitActivity(context);
    }






    private static Class<? extends Activity> getActivityFromClassName(String className) {
        if(Strings.isEmpty(className))
            throw new RuntimeException("Null or empty class name");
        try {
            Class<? extends Activity> activityClass = (Class<? extends Activity>) Class.forName(className);
            return activityClass;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
