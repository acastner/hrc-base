package com.grassroots.petition.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class Dates {
    /**
     * Time Format used to store date-time
     */
    private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SZ";
    /**
     * Time Format used to store only date
     */
    private static final String NO_TIME_FORMAT = "yyyy-MM-dd";

    public static final String getTimeStamp() {
        return new SimpleDateFormat(TIME_FORMAT).format(new Date());
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat(TIME_FORMAT).format(date);
    }

    public static String getFormattedDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, 0, 0, 0);
        return new SimpleDateFormat(NO_TIME_FORMAT).format(calendar.getTime());
    }
}
