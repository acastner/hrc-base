package com.grassroots.petition.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.services.GrassrootsRestClient;

/**
 * UpdateLocations class
 * 
 * @author sharika
 * 
 */

public class UpdateLocationsListTask extends
		AsyncTask<String, String, LocationList> {

	private Pair<LocationList, Response> locationlistresponse;
	private String errorMessage;
	private Context context;

	public UpdateLocationsListTask(Context context) {
		this.context = context;
	}

	@Override
	protected LocationList doInBackground(String... params) {

		LocationList locationList = null;
		Pair<LocationList, Response> locationListResponse = GrassrootsRestClient
				.getClient(context).syncLoadLocationList();
		if (locationListResponse.second != null) {
			if (locationListResponse.second.getResponseErrors() != null)
				return null;
		} else {
			locationList = locationListResponse.first;
		}

		return locationList;

	}

}
