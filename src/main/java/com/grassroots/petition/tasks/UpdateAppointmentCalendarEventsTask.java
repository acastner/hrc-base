package com.grassroots.petition.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.services.GrassrootsRestClient;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: mac-202
 * Date: 12.06.13
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */


public class UpdateAppointmentCalendarEventsTask extends AsyncTask <Context, Void, String> {
    private static final String TAG = UpdateAppointmentCalendarEventsTask.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private boolean isUpdated = false;

    public static interface UpdateAppointmentCalendarEventsTaskListener {
        public void onPreExecute();
        public void onPostExecute(Boolean result, String resultString);
    }

    private UpdateAppointmentCalendarEventsTaskListener listener;

    public UpdateAppointmentCalendarEventsTask(UpdateAppointmentCalendarEventsTaskListener listener) {
        super();
        this.listener = listener;
    }

    public void setListener(UpdateAppointmentCalendarEventsTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onPreExecute();
    }

    @Override
    protected String doInBackground(Context... params) {
    	
    	/*
    	 * This line of code causes exceptions, but fixes app crash for new calendar question type
    	 */
    	//GrassrootsRestClient.getClient().syncLoadAppointmentWindows();
    	
        isUpdated = false;
        Context context = GlobalData.getContext();
        if (params == null || params.length == 0) {
            LOGGER.error("Cannot start task, context == null");
            return context.getString(R.string.APPOINTMENT_CALENDAR_CANNOT_START_TASK_MESSAGE);
        }

        Pair<AppointmentCalendar, Response> calendarResponse = GrassrootsRestClient.getClient().syncUpdateAppointmentWindows();
        if (calendarResponse.second != null) {
            LOGGER.debug("Cannot update AppointmentCalendarEvents");
            return context.getString(R.string.APPOINTMENT_CALENDAR_CANNOT_UPDATE_EVENTS_MESSAGE);
        } else {
            AppointmentCalendar calendar = calendarResponse.first;

            GlobalData globalData = (GlobalData) params[0].getApplicationContext();
            AppointmentCalendar appointmentCalendar = globalData.getAppointmentCalendar();
            appointmentCalendar.updateEvents(calendar.getEventsForDate(null));

            isUpdated = true;
        }

        LOGGER.debug("AppointmentCalendarEvents have been successfully updated");
        return context.getString(R.string.APPOINTMENT_CALENDAR_EVENTS_UPDATED_MESSAGE);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        listener.onPostExecute(isUpdated, result);
    }
}
