package com.grassroots.petition.tasks;

import java.util.Calendar;
import java.util.Date;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Pair;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.WalkListUpdateListener;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;

public class UpdateWalklistTask extends AsyncTask<String, String, Pair<Walklist, Response>> {

    private BasePetitionActivity context;
    private WalkListUpdateListener updateListener;
    private Pair<Walklist, Response> walklistResponseToRefresh;
    
    public UpdateWalklistTask(BasePetitionActivity context,Date since,WalkListUpdateListener listener,String location){
        this.context = context;
        this.updateListener = listener;
    }
    
    @Override
    protected Pair<Walklist, Response> doInBackground(String... params) {
        synchronized (GlobalData.WALKLISTUPDATING) {
            walklistResponseToRefresh = GrassrootsRestClient.getClient().syncLoadWalklistSince(context.getGlobalData().getLastWalklistSyncTime(),context.getGlobalData().getLocation());
            if(walklistResponseToRefresh != null){
                Walklist walklist = walklistResponseToRefresh.first;
                if(walklist != null && walklist.size() > 0){
                    WalklistDataSource walklistDataSource = new WalklistDataSource(context);
                    walklistDataSource.insertWalklist(walklist.getSubjects(),context.getGlobalData().getLocation());
                }
            }
            return walklistResponseToRefresh;
        }
    }
    
    @Override
    protected void onPostExecute(Pair<Walklist, Response> result) {       
        if(result == null){
            updateListener.updateFailed(context.getString(R.string.errorUploading));
        }else{
            updateListener.updateSuccess(result.first);
            context.getGlobalData().setLastWalklistSyncTime(new Date(Calendar.getInstance().getTimeInMillis()));
            Intent intent = new Intent();
            intent.setAction("WalklistUpdateReceiver");
            context.sendBroadcast(intent);
        }          
       super.onPostExecute(result);
    }

  

}
