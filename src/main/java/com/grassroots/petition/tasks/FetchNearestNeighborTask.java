package com.grassroots.petition.tasks;

import com.grassroots.petition.models.NearestNeighborInfo;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Tally;
import com.grassroots.petition.services.GrassrootsRestClient;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

public class FetchNearestNeighborTask extends AsyncTask<String, String, NearestNeighborInfo> {
	private Context context;

	@Override
	protected NearestNeighborInfo doInBackground(String... params) {
		NearestNeighborInfo nearestNeighborInfo = null;
		Pair<NearestNeighborInfo, Response> nearestNeighborResponse = GrassrootsRestClient
				.getClient(context).syncLoadNearestNeighbor();
		if (nearestNeighborResponse.second != null) {
			if (nearestNeighborResponse.second.getResponseErrors() != null)
				return null;
		} else {
			nearestNeighborInfo = nearestNeighborResponse.first;
		}

		return nearestNeighborInfo;
	}
	
	public FetchNearestNeighborTask(Context context) {
		this.context = context;
	}

}
