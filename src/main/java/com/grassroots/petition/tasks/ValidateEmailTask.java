package com.grassroots.petition.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.interfaces.EmailValidationInterface;
import com.grassroots.petition.services.GrassrootsRestClient;

/**
 * Created by Sharika on 6/22/16.
 */
public class ValidateEmailTask extends AsyncTask<String,String,String> {
    private Activity context;
    private String email;
    ProgressDialog pd;
    EmailValidationInterface mValidatorInterface;
    public ValidateEmailTask(BasePetitionActivity context, String email, EmailValidationInterface iValidator) {
        this.context = context;
        this.email = email;
        this.mValidatorInterface = iValidator;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(context);
        pd.setMessage("Validating email. Please wait... ");
        pd.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = GrassrootsRestClient
                .getClient(context).validateEmail(email);
        Log.e("validate email response","validate email "+response);
        return response;
    }

    @Override
    protected void onPostExecute(String s) {

        pd.dismiss();
        mValidatorInterface.emailValidationFinished(s,context);
    }

}
