package com.grassroots.petition.tasks;

import org.apache.log4j.Logger;

import com.grassroots.petition.activities.TallyActivity;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Tally;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

public class FetchTallyTask extends AsyncTask<String,String,Tally> {
	public static final String TAG = FetchTallyTask.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private Context context;


	public FetchTallyTask(Context context) {
		this.context = context;
	}

	@Override
	protected Tally doInBackground(String... params) {
		try{
			Tally tally = null;
			Pair<Tally, Response> tallyResponse = GrassrootsRestClient
					.getClient(context).syncLoadTally();
			if (tallyResponse.second != null) {
				if (tallyResponse.second.getResponseErrors() != null)
					return null;
			} else {
				tally = tallyResponse.first;
			}

			return tally;
		} catch (Exception e){
			e.printStackTrace();
			LOGGER.debug("TALLY EXCEPTION: " + e.toString());
			return null;
		}
	}

}
