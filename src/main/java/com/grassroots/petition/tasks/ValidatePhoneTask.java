package com.grassroots.petition.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.interfaces.EmailValidationInterface;
import com.grassroots.petition.interfaces.PhoneValidationInterface;
import com.grassroots.petition.services.GrassrootsRestClient;


public class ValidatePhoneTask extends AsyncTask<String,String,String> {
    private Activity context;
    private String phone;
    ProgressDialog pd;
    PhoneValidationInterface mValidatorInterface;
    public ValidatePhoneTask(BasePetitionActivity context, String phone, PhoneValidationInterface iValidator) {
        this.context = context;
        this.phone = phone;
        this.mValidatorInterface = iValidator;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(context);
        pd.setMessage("Validating phone number. Please wait... ");
        pd.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = GrassrootsRestClient
                .getClient(context).validatePhoneNumber(phone);
        Log.e("validate phone number response","validate phone "+response);
        return response;
    }

    @Override
    protected void onPostExecute(String s) {

        pd.dismiss();
        mValidatorInterface.phoneValidationFinished(s,context);
    }

}
