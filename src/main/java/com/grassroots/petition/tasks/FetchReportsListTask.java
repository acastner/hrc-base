package com.grassroots.petition.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.grassroots.petition.services.GrassrootsRestClient;

import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by jason on 1/10/2017.
 */

public class FetchReportsListTask extends AsyncTask<String,String,ArrayList<String>> {
    public static final String TAG = FetchReportsListTask.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private Context context;


    public FetchReportsListTask(Context context) {
        this.context = context;
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        try{
            ArrayList<String> reportList = GrassrootsRestClient
                    .getClient(context).syncLoadReportList();
            return reportList;
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.debug("REPORT LIST EXCEPTION: " + e.toString());
            return null;
        }
    }
}