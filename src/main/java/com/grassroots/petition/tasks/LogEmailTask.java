package com.grassroots.petition.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.utils.BatteryPerformance;
import com.grassroots.petition.utils.Emailer;
import com.grassroots.petition.utils.LocationProcessor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sharika on 1/28/16.
 * Send logs as attachment. EMail service used : aws
 * Libraries used : aws-core and aws-ses
 */
public class LogEmailTask extends AsyncTask<Context, Void, String> {
    /**
     * App Log filename (see Log4j configuration)
     */
    private static final String LOG_FILENAME = "/grassroots.log";
    /**
     * Extension, that will be added to the archived file name
     */
    private static final String ARCHIVE_EXTENSION = ".gz";


    // Folder for temporary files
    //use SD card if available, otherwise - documents folder
    //
    private String STORAGE_LOCATION;
    /**
     * Application context
     */
    private Context context;

    /**
     * App Log filename (see Log4j configuration)
     */
    private static final String RPS_LOG_FILENAME = "/request-service.log";


    @Override
    protected String doInBackground(Context... contexts) {
        if (contexts == null || contexts.length == 0) {
            return "Cannot start task, context == null";
        }

        File dirFile = new File(STORAGE_LOCATION);
        dirFile.mkdirs();
        ArrayList<String> attachmentsList = new ArrayList<String>();
        final String logFileName = getLogFileName();
        String trackerLogFileName = "";
        Calendar cl = Calendar.getInstance();

        int month = cl.get(Calendar.MONTH)+1;

        trackerLogFileName = "tracker" + "-" + month + "-" + cl.get(Calendar.DAY_OF_MONTH)
                + "-" + cl.get(Calendar.YEAR) + ".log";
        //trackerLogFileName = "tracker" + ".log";

        String rpsLogFileName = getRPSLogFileName();
        rpsLogFileName = rpsLogFileName.replace(".funds","");
        File logFile = new File(GlobalData.TMP_DIRECTORY + File.separator + trackerLogFileName);
        String messageBody = constructMessageBodyWithUsefulInfo();

        String result = sendEmailWithAttachments(messageBody, logFileName,logFile.getPath(),rpsLogFileName);
        return result ;
    }

    private String constructMessageBodyWithUsefulInfo() {
        String messageBody = "";
        StringBuilder message = new StringBuilder();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy-hh-mm-ss");
        String timestamp = simpleDateFormat.format(new Date());
        EventInfo eventInfo = ((GlobalData)context).getEventInfo(LocationProcessor.getLastKnownGpsLocation(context), new BatteryPerformance(context));
        String username = ((GlobalData) context).getLoggedInUserName();

        String powerSaveMode = "Not applicable on pre-lollipop devices.";

        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(powerManager.isPowerSaveMode()) {
                powerSaveMode = "ON";
            } else {
                powerSaveMode = "OFF";
            }
        }

        if( username == null || username.isEmpty()){
            username = ((GlobalData)context).getCrashedUserName();
        }
        message.append( "Server : "+ ((GlobalData)context).getBaseUrl() + "\n"+
                "Username : " + username+"\n"+
                "ScriptId : " +"("+ ((GlobalData)context).getCrashedScriptId()+ ")" +"\n"+
                "ScriptName : "+ ((GlobalData)context).getCrashedScriptName()+"\n"+
                "Timestamp : " + timestamp + "\n" +
                "IMEI : " + eventInfo.getImei() + "\n"+
                "Location : Latitude = " + eventInfo.getLatitude() + "\t"+
                "Longitude :" + eventInfo.getLongitude() +  "\n"+
                "Power Save Mode : " + powerSaveMode + "\n" +
                "Notes :"+ "\n"+ ((GlobalData)context).getUserNotes());
        messageBody = message.toString();
        if(messageBody.isEmpty()){
            messageBody = "Attaching logs - user didn't enter any notes";
        }
        return messageBody;
    }

    private String constructMessageSubjectWithUsefulInfo() {
        StringBuilder subject = new StringBuilder();
        EventInfo eventInfo = ((GlobalData)context).getEventInfo(LocationProcessor.
                getLastKnownGpsLocation(context), new BatteryPerformance(context));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy-hh-mm-ss");
        String timestamp = simpleDateFormat.format(new Date());

        subject.append("Log files emailer");
        subject.append(" " + ((GlobalData)context).getCrashedScriptName());
        subject.append("("+ ((GlobalData)context).getCrashedScriptId()+ ")");
        subject.append(" " + eventInfo.getImei());
        subject.append(" " + timestamp);
        return subject.toString();
    }

    /**
     * Return temporary filename for saving log file
     * @return - temporary filename
     */
    public String getLogFileName() {
        if (null == STORAGE_LOCATION) return null;
        return STORAGE_LOCATION + LOG_FILENAME;
    }

    /**
     * Return temporary filename for saving log file
     * @return - temporary filename
     */
    public String getRPSLogFileName() {

        if (null == STORAGE_LOCATION) return null;

        return STORAGE_LOCATION + RPS_LOG_FILENAME;
    }
    /**
     * Return archived fileName
     * @param fileName - full path and file name for current file
     * @return - archived filename
     */
    public String getCompressedFileName(String fileName) {
        return fileName + ARCHIVE_EXTENSION;
    }

    /**
     * Constructor
     * @param listener - listener that implements DataBaseEmailTaskListener interface
     * @param context - application context
     */
    public LogEmailTask(LogEmailTaskListener listener, Context context) {
        super();
        this.listener = listener;
        this.context = context;
        STORAGE_LOCATION = ((GlobalData)(context)).getTMPDirectory();
    }

    /**
     * DataBaseEmailTaskListener Interface
     * Implementation should show progressDialog onPreExecute()
     * and hide dialog and show status string onPostExecute()
     */
    public static interface LogEmailTaskListener {
        public void onPreExecute();
        public void onPostExecute(String result);
    }

    /**
     * Listener that implements DataBaseEmailTaskListener interface
     */
    private LogEmailTaskListener listener;

    /**
     * Task onPreExecute
     * call listener onPreExecute
     * show progressDialog to user to notify that task has been started
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onPreExecute();
    }

    /**
     * Send email with attachments
     * uses emailer class
     * @param body - email body string
     * @param fileNames - filename of files that should be attached to the email
     * @return  - true if email was sent successfully, otherwise - false
     */
    private String sendEmailWithAttachments(final String body, String... fileNames) {

        Log.d("LogEmailTask", "About to send logs to tech support");
        final ArrayList<String> attachmentsList = new ArrayList<String>();
        for (String fileName : fileNames) {
            attachmentsList.add(fileName);
        }

        Emailer emailer = new Emailer();
        if (emailer == null) {
            return "Cannot Instantiate Emailer";
        }

        boolean isSuccess = false;
        isSuccess = emailer.sendEmailWithAttachments(constructMessageSubjectWithUsefulInfo(), body, attachmentsList);
        return isSuccess ? "Email has been sent" : "An error occurred during sending email";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        listener.onPostExecute(s);
    }
}
