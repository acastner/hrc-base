package com.grassroots.petition.tasks;

import org.apache.log4j.Logger;

import android.content.Context;
import android.os.AsyncTask;

import com.grassroots.petition.services.GrassrootsRestClient;

public class FetchQuestionTallyTask extends AsyncTask<String, String, String> {
	private Context context;
	public static final String TAG = FetchQuestionTallyTask.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	@Override
	protected String doInBackground(String... params) {
		String tallyResults = GrassrootsRestClient.getClient().syncLoadQuestionTally(params[0]);

		return tallyResults;
	}
	
	/*public FetchQuestionTallyTask(Context context) {
		this.context = context;
	}*/

}
