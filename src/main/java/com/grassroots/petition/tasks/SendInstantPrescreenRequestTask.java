package com.grassroots.petition.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.*;
import com.grassroots.petition.services.GrassrootsRestClient;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Sends instant prescreen request.
 */
public class SendInstantPrescreenRequestTask extends AsyncTask<InstantPrescreenRequest, Void, String>
{
    private static final String TAG = SendInstantPrescreenRequestTask.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private boolean isClientApproved = false;
    private boolean isErrorOccured = false;

    public static interface OnClientApprovedListener
    {
        public void OnClientApproved(String message);
        public void OnClientDeclined(String message);
        public void onErrorOccurred(String message);
    }

    private OnClientApprovedListener listener;

    public SendInstantPrescreenRequestTask(OnClientApprovedListener listener)
    {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(InstantPrescreenRequest... params)
    {
        isClientApproved = false;
        isErrorOccured = false;

        Context context = GlobalData.getContext();

        if (null == params || 0 == params.length)
        {
            LOGGER.error("Cannot start task, params == null.");
            isErrorOccured = true;
            return context.getString(R.string.INSTANT_PRESCREEN_CLIENT_ERROR_MESSAGE);
        }

        InstantPrescreenRequest prescreenRequest = params[0];
        // validate InstantPrescreenRequest object
        if (!prescreenRequest.isValid())
        {
            LOGGER.error("Invalid prescreen request.");
            isErrorOccured = true;
            return context.getString(R.string.INSTANT_PRESCREEN_CLIENT_ERROR_MESSAGE);
        }

        Pair<InstantPrescreenResponce, Response> calendarResponse = GrassrootsRestClient.getClient().syncSendInstantRequest(prescreenRequest);
        Response response = calendarResponse.second;
        InstantPrescreenResponce prescreenResponce = calendarResponse.first;
        if (null != response)
        {
            LOGGER.debug("Cannot send instant prescreen request: ");
            isErrorOccured = true;
            List<ResponseError> responseErrors = response.getResponseErrors();
            if (null != responseErrors)
            {
                int i = 0;
                for (ResponseError responseError : responseErrors) {
                    LOGGER.debug("" + i + ") " + responseError);
                    i++;
                }
            }
            return context.getString(R.string.INSTANT_PRESCREEN_CLIENT_ERROR_MESSAGE);
        } else {
            isClientApproved = prescreenResponce.isClientApproved();
        }

        LOGGER.debug("Instant prescreen request successfully performed.");
        return prescreenResponce.getText();
    }

    @Override
    protected void onPostExecute(String message)
    {
        if (null != listener) {
            if (isErrorOccured) {
                listener.onErrorOccurred(message);
            } else {
                if (isClientApproved) {
                    listener.OnClientApproved(message);
                } else {
                    listener.OnClientDeclined(message);
                }
            }
        }
    }
}
