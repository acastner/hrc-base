package com.grassroots.petition.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import com.grassroots.petition.exceptions.DatabaseException;
import org.apache.log4j.Logger;

import java.io.*;


public class DatabaseInstaller
{
	private static final int BUFFSIZE = 65536;

	private static final String DATABASE_NAME = "grassroots.db";

	private String databaseFileName;
	private String databasePath;

	private Context context;
	private static boolean inUpgrade = false;
	private static final Object upgradeLock = new Object();

	private static final String TAG = DatabaseInstaller.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

	public DatabaseInstaller(Context context)
	{
		this.context = context;
		File file = context.getDatabasePath(DATABASE_NAME);
		databaseFileName = file.getAbsolutePath();
		databasePath = file.getPath();
	}

	/* Synchronization methods */

	public static boolean canOpen()
	{
		synchronized(upgradeLock){
			LOGGER.debug("Upgrade check: " + inUpgrade);
			return inUpgrade;
		}
	}

	public void waitUpgrade() throws InterruptedException
	{
		synchronized(upgradeLock){
			LOGGER.debug("Upgrade wait: " + inUpgrade);
			while(inUpgrade)
				upgradeLock.wait();
		}
	}

	private static void startUpgrade()
	{
		synchronized(upgradeLock){
			LOGGER.debug("Upgrade flag set to true");
			inUpgrade = true;
		}
	}

	private static void completeUpgrade()
	{
		synchronized(upgradeLock){
			LOGGER.debug("Upgrade flag set to false, notifying waiting threads.");
			inUpgrade = false;
			upgradeLock.notifyAll();
		}
	}


	public void doInstall() throws DatabaseException
	{
		LOGGER.info("Starting database installation/upgrade process");
		startUpgrade();

		if(!localDatabaseExists() || assetDBIsNewer()){
			LOGGER.info("Upgrade or install needed, starting copy");
			copyDatabaseFromAssetsToData();
			LOGGER.info("Database installation complete.");
		}else
			LOGGER.info("Database installation not needed.");


		completeUpgrade();
	}

	private boolean localDatabaseExists()
	{
		File file = new File(databaseFileName);
		if(!file.exists() || !file.canRead() || !file.canWrite()){
			LOGGER.debug("Local database does not exist or is not accessible.");
			return false;
		}

		try{
			SQLiteDatabase dataDB = SQLiteDatabase.openDatabase(databaseFileName, null, SQLiteDatabase.OPEN_READWRITE);
			if(dataDB.isOpen()){
				dataDB.close();
				LOGGER.debug("Local database exists and is readable");
				return true;
			}
		}catch(SQLiteException e){
			LOGGER.debug("Exception attempting to open local db: " + e.getMessage(), e);
			return false;
		}

		return false;
	}

	private boolean assetDBIsNewer() throws DatabaseException
	{
		SQLiteDatabase dataDB = null, assetDB = null;
		String tempFile = null;
		int dataVersion = 0, assetVersion = 0;

		try{
			tempFile = spoolAssetToTempFile();

			LOGGER.debug("Opening local database");
			dataDB  = SQLiteDatabase.openDatabase(databaseFileName, null, SQLiteDatabase.OPEN_READONLY);
			LOGGER.debug("Opening temp database");
			assetDB = SQLiteDatabase.openDatabase(tempFile, null, SQLiteDatabase.OPEN_READONLY);

			dataVersion = dataDB.getVersion();
			assetVersion = assetDB.getVersion();

			LOGGER.debug("Database versions: data is " + dataVersion + ", asset is " + assetVersion);

		}catch(Exception e){
			LOGGER.error("Unable to compare asset to data database: " + e.getMessage(), e);
			throw new DatabaseException("Unable to compare asset to data databases", e);
		}finally {
			// Cleanup
			if(dataDB != null)
				dataDB.close();
			if(assetDB != null)
				assetDB.close();

			if(tempFile != null){
				File file = new File(tempFile);
				if(file.exists()){
					LOGGER.debug("Deleting temporary database " + tempFile);
					file.delete();
				}
			}
		}

		if(assetVersion > dataVersion)
			return true;
		else
			return false;

	}

	private String spoolAssetToTempFile() throws DatabaseException
	{
		LOGGER.info("Attempting to spool asset db to temp file.");

		try{
			File outputDir = context.getCacheDir();
			File outputFile = File.createTempFile("grassroots-temp", ".db", outputDir);

			String filename = outputFile.getAbsolutePath();

			LOGGER.debug("Temp file: " + filename);

			copyDatabase(filename);

			return filename;
		}catch(Exception e){
			LOGGER.error("Unable to copy db from assets to destination: " + e.getMessage(), e);
			throw new DatabaseException("Unable to copy db from assets to destination", e);
		}

	}

	private void copyDatabaseFromAssetsToData() throws DatabaseException
	{
		copyDatabase(databaseFileName);
	}

	private void copyDatabase(String destfile) throws DatabaseException
	{

		LOGGER.info("Copying database from assets");

		try{
			// Open your local db as the input stream
			InputStream is = context.getAssets().open("db/" + DATABASE_NAME);

			// Create any missing intermediates.
			File file = new File(databasePath);
			File parent = new File(file.getParent());
			parent.mkdirs();

			// Open the empty db as the output stream
			OutputStream os = new FileOutputStream(destfile);

			// transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[BUFFSIZE];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}

			// Close the streams
			os.flush();
			os.close();
			is.close();
		}catch(IOException e){
			LOGGER.error("Unable to copy database: " + e.getMessage(), e);
			throw new DatabaseException("Unable to copy database: " + e.getMessage(), e);
		}

		LOGGER.info("Copy successful");

	}

}
