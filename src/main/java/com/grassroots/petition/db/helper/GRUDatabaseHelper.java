package com.grassroots.petition.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database  helper class that creates the database and returns an instance of application database.
 * @author sharika
 *
 */
public class GRUDatabaseHelper extends SQLiteOpenHelper {



    public static final String TABLE_WALKLIST = "Walklist";
    
   // public static final String COLUMN_ID = "_id";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_HID = "Hid";
    public static final String COLUMN_FIRSTNAME = "FirstName";
    public static final String COLUMN_LASTNAME = "LastName";
    public static final String COLUMN_SALUTATION = "Salutation";
    public static final String COLUMN_SUFFIX = "Suffix";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_CITY = "City";
    public static final String COLUMN_STATE = "State";
    public static final String COLUMN_ZIP = "ZIP";
    public static final String COLUMN_APARTMENT = "Apartment";
    public static final String COLUMN_EMAIL = "Email";
    public static final String COLUMN_PHONE = "Phone";
    public static final String COLUMN_STATUS = "Status";
    public static final String COLUMN_LOCATION = "Location";
    public static final String COLUMN_LATITUDE = "Latitude";
    public static final String COLUMN_LONGITUDE = "Longitude";
    public static final String COLUMN_OTHER = "Other";
    public static final String COLUMN_EVEN = "Even";
    public static final String COLUMN_COLOR = "Color";
    public static final String COLUMN_COLOR_NAME = "Colorname";
    public static final String CUSTOM_WALKLIST_VIEW = "ColoredWalklist";


    private static final String DATABASE_NAME = "grassroots.db";

	/**
	 * We hardcode this to 1 as actual upgrades are handled via DatabaseInstaller called from login.
	 */
	private static final int DATABASE_VERSION = 1;

	private Context context;
	private String databasePath;



    public GRUDatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
   	}

    private static GRUDatabaseHelper mDBHelper;

    public static synchronized GRUDatabaseHelper getInstance(Context ctx) {

        if (mDBHelper == null) { //this will ensure no multiple instances out there.
            mDBHelper = new GRUDatabaseHelper(ctx.getApplicationContext());
        }
        return mDBHelper;
    }


	/* For all Android cares, we're always at DB version 1.
	 *
	 * We handle upgrades in the com.grassroots.petition.db.helper.DatabaseInstaller class, not here
	 * These methods below are empty signatures to give Android that warm and fuzzy feeling
	 * that we've embedded a crapload of SQL "ALTER TABLE" commands in here like good little devs.
	 *
	 * Blech.
	 */
    
    @Override
    public void onCreate(SQLiteDatabase database)
	{
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
	}
}


