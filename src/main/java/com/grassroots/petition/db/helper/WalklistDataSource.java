package com.grassroots.petition.db.helper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;

import android.R.string;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.utils.Performance;

public class WalklistDataSource {

    // Database fields
    private SQLiteDatabase database;
    private GRUDatabaseHelper dbHelper;
    private static final String TAG = WalklistDataSource.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    public WalklistDataSource(Context context) {
        dbHelper = GRUDatabaseHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        //not closing db manually.
        //dbHelper.close();
    }

    /**
     * fetch walklist from database 
     * @return Walklist object
     */
    public Walklist getWalklist() {
        if (database == null || !database.isOpen()) {
            open();
        }
        try{
        long startTime = System.currentTimeMillis();
        database.beginTransaction();
        String fetchWalklistQuery = "Select * from "+GRUDatabaseHelper.CUSTOM_WALKLIST_VIEW+" order by "+GRUDatabaseHelper.COLUMN_STATUS+" DESC";
        Cursor cursor = database.rawQuery(fetchWalklistQuery, null);
       
       
        if(cursor != null){
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        Performance.end(startTime, "Fetching walklist from database");
        cursor.close();
        close();
        return walklist;
        }
        else{
            return null;
        }}catch (Exception e) {
            e.printStackTrace();
         }finally{
        	 database.endTransaction();
         }
      return null;
    }

    /**
     * retrns the WALKLIST represented by the cursor
     * @param cursor : cursor pointing to WALKLIST table
     * @return
     */
    private Walklist cursorToWalklist(Cursor cursor) {
        Walklist walklist = new Walklist();
        List<Subject> subjects = new ArrayList<Subject>();
		Gson gson = new Gson();
        while (!cursor.isAfterLast()) {
            Subject subject = new Subject(cursor.getInt(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_ID)));
            subject.setHid(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_HID)));
            subject.setFirstName(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_FIRSTNAME)));
            subject.setLastName(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_LASTNAME)));
            subject.setSalutation(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_SALUTATION)));
            subject.setSuffix(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_SUFFIX)));
            subject.setCity(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_CITY)));
            subject.setAddressLine1(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_ADDRESS)));
            subject.setAddressLine2(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_APARTMENT)));
            subject.setState(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_STATE)));
            subject.setZip(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_ZIP)));
            subject.setEmail(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_EMAIL)));
            subject.setPhone(cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_PHONE)));
            subject.setKnockedStatus(KnockStatus.fromId(cursor.getInt((cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_STATUS)))));
            subject.setEven(cursor.getInt(cursor.getColumnIndex(GRUDatabaseHelper.COLUMN_EVEN)));
            subject.setLocation(cursor.getString((cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_LOCATION))));
            subject.setLatitude(Double.toString(cursor.getDouble(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_LATITUDE))));
            subject.setLongitude(Double.toString(cursor.getDouble(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_LONGITUDE))));
			String other = cursor.getString(cursor.getColumnIndex(GRUDatabaseHelper.COLUMN_OTHER));
			LinkedHashMap<String, String> otherHash = gson.fromJson(other, LinkedHashMap.class);
			subject.setOtherFieldsMapping(otherHash);
			subject.setOtherJSON(other);
			try{
				int colIndex = cursor.getColumnIndex(GRUDatabaseHelper.COLUMN_COLOR_NAME);
				if(colIndex != -1){
					String color = cursor.getString(colIndex);
					if(color != null){
						subject.setColor(color);
					}
				}

			}catch(CursorIndexOutOfBoundsException e){
				e.printStackTrace();
			}
            subjects.add(subject);
            cursor.moveToNext();

        }
        walklist.setSubjects(subjects);
        return walklist;
    }

    /**
     * insert walklist
     * @param location 
     * @param walklist
     */
    public long insertWalklist (List<Subject> subjects, String location) throws SQLException{

		if(subjects == null)
			return 0;

		Gson gson = new Gson();
		try{
			if (database == null || !database.isOpen()) {
                open();
            }
			database.beginTransaction();
        for (Subject subject : subjects) {
            
            ContentValues values = new ContentValues();
            values.put(GRUDatabaseHelper.COLUMN_HID, subject.getHid());
            values.put(GRUDatabaseHelper.COLUMN_FIRSTNAME,
                    subject.getFirstName());
            values.put(GRUDatabaseHelper.COLUMN_LASTNAME, subject.getLastName());
            values.put(GRUDatabaseHelper.COLUMN_SALUTATION,
                    subject.getSalutation());
            values.put(GRUDatabaseHelper.COLUMN_SUFFIX, subject.getSuffix());
            values.put(GRUDatabaseHelper.COLUMN_CITY, subject.getCity());
            values.put(GRUDatabaseHelper.COLUMN_ADDRESS,
                    subject.getAddressLine1());
            values.put(GRUDatabaseHelper.COLUMN_APARTMENT,
                    subject.getAddressLine2());
            values.put(GRUDatabaseHelper.COLUMN_STATE, subject.getState());
            values.put(GRUDatabaseHelper.COLUMN_ZIP, subject.getZip());
            values.put(GRUDatabaseHelper.COLUMN_EMAIL, subject.getEmail());
            values.put(GRUDatabaseHelper.COLUMN_PHONE, subject.getPhone());
            values.put(GRUDatabaseHelper.COLUMN_STATUS, subject.getKnockStatus().getRepresentation());
            if(subject.isHouseNumberEven()){
                values.put(GRUDatabaseHelper.COLUMN_EVEN, 1);
            }else{
                values.put(GRUDatabaseHelper.COLUMN_EVEN, 0);
            }
            values.put(GRUDatabaseHelper.COLUMN_LOCATION, location);
            if(subject.getLatitude() != null && subject.getLongitude() != null && !subject.getLatitude().equals("0.0") && !subject.getLongitude().equals("0.0")){
            try{
            values.put(GRUDatabaseHelper.COLUMN_LATITUDE, Double.valueOf(subject.getLatitude()));
            values.put(GRUDatabaseHelper.COLUMN_LONGITUDE, Double.valueOf(subject.getLongitude()));
            }catch(NumberFormatException e){
                e.printStackTrace();
            }
            }
            LinkedHashMap<String, String> otherFieldsMapping = (LinkedHashMap<String, String>) subject.getOtherFieldsMapping();
			String other = gson.toJson(otherFieldsMapping);
			values.put(GRUDatabaseHelper.COLUMN_OTHER, other);
            values.put(GRUDatabaseHelper.COLUMN_COLOR, subject.getColor());
			if(values != null && values.size() != 0){
					long startTime = System.currentTimeMillis();
					long insertResultKey = database.insertWithOnConflict(GRUDatabaseHelper.TABLE_WALKLIST, null, values, SQLiteDatabase.CONFLICT_REPLACE);
					if(insertResultKey == -1)
						throw new SQLException("Something strange happened and this shouldn't have happened.");
				}
			
        }
        database.setTransactionSuccessful(); //end of for loop-make transaction successful
		}catch(SQLException e){

					// Throw some data on the log. There's something fishy going on here in 1.9.3.
					// 11/25/13 jl
					LOGGER.error("Unable to Update: " + e.getMessage(), e);

					Throwable cause = e.getCause();
					if(cause != null){
						LOGGER.error("Inner: " + cause.getMessage(), cause);
					}

					throw e;
				}
			finally{
				     
		        	 database.endTransaction();
		         }
        
        return 0;
    }

    /**
     * clears walklist table
     */
    public void clearWalklist() {
        if (database == null || !database.isOpen()) {
            open();
        }

        try{
          database.execSQL("delete from " + GRUDatabaseHelper.TABLE_WALKLIST);
        }catch(SQLException e){
            e.printStackTrace();
        }
        close();
    }

   public Walklist getLastNamesFromWalklist(String lastnamefilter) {
        if (database == null || !database.isOpen()) {
            open();
        }
    Cursor cursor = null;
    if(lastnamefilter == null || lastnamefilter.length()==0){
        cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null, null, null, null ,null, GRUDatabaseHelper.COLUMN_LASTNAME + " asc");

    }else{
        cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null, GRUDatabaseHelper.COLUMN_LASTNAME + " like ?", new String[]{lastnamefilter+"%"}, null ,null, GRUDatabaseHelper.COLUMN_LASTNAME + " asc");
    }
    cursor.moveToFirst();
    Walklist walklist = cursorToWalklist(cursor);
    cursor.close();
    close();
    return walklist;
        
    }

    public ArrayList<String> getAllLastNames() {
        if (database == null || !database.isOpen()) {
            open();
        }
        ArrayList<String> lastNames = new ArrayList<String>();
        Cursor cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST,
                new String[] { GRUDatabaseHelper.COLUMN_LASTNAME },
                null,
                null,
                GRUDatabaseHelper.COLUMN_LASTNAME, null,  GRUDatabaseHelper.COLUMN_LASTNAME + " asc");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String lastname = cursor.getString(cursor
                    .getColumnIndex(GRUDatabaseHelper.COLUMN_LASTNAME));
            lastNames.add(lastname);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return lastNames;

    }


	/**
	 * Gets a distinct list of last names for use in the left hand pane of the Location based walklist.
	 *
	 * @return An arraylist of last names
	 * @author jlabonski
	 */
	public ArrayList<String> getDistinctLastNames()
	{
		if (database == null || !database.isOpen()) {
			open();
		}
		ArrayList<String> lastNames = new ArrayList<String>();

		String sql = "SELECT DISTINCT LastName FROM Walklist ORDER BY LastName ASC";

		Cursor cursor = database.rawQuery(sql, null);

		cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String lastname = cursor.getString(cursor.getColumnIndex(GRUDatabaseHelper.COLUMN_LASTNAME));
            if(lastname!=null && !lastname.equals("null")){
            	lastNames.add(lastname);
            }
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return lastNames;

	}

    public Walklist getWalklistForEven() {
        if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                GRUDatabaseHelper.COLUMN_EVEN +"=?", new String[]{"1"}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
       
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist;
        
    }

    public Walklist getWalklistForOdd() {
        if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                GRUDatabaseHelper.COLUMN_EVEN +"=?", new String[]{"0"}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
       
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist;
    }

    public List<Subject> getWalklistForStreetAndHouse(String filter, int numberFilter, String status) {
        if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = null;
        if(numberFilter == 2){
            cursor = database.query(true,GRUDatabaseHelper.TABLE_WALKLIST, null,
                    GRUDatabaseHelper.COLUMN_ADDRESS +" like ? and "+GRUDatabaseHelper.COLUMN_STATUS+"=?", new String[]{"%"+filter+"%",status}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc",null);
        }else{
            cursor = database.query(true,GRUDatabaseHelper.TABLE_WALKLIST, null,
                    GRUDatabaseHelper.COLUMN_ADDRESS +" like ? and "+GRUDatabaseHelper.COLUMN_EVEN+"=? and "+GRUDatabaseHelper.COLUMN_STATUS+" =?", new String[]{"%"+filter+"%",""+numberFilter,status}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc",null);
        }
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist.getSubjects();
    }
    public List<Subject> getWalklistForStreetAndHouse(String filter, int numberFilter) {
        if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = null;
        if(numberFilter == 2){
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    GRUDatabaseHelper.COLUMN_ADDRESS +" like ? ", new String[]{"%"+filter+"%"}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
        }else{
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    GRUDatabaseHelper.COLUMN_ADDRESS +" like ? and "+GRUDatabaseHelper.COLUMN_EVEN+"=? ", new String[]{"%"+filter+"%",""+numberFilter}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
        }
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist.getSubjects();
    }

	public List<Subject> getWalklistForLastName(String status, String lastNameFilter) {
		if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = null;
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    //GRUDatabaseHelper.COLUMN_LASTNAME +" like ? and "+GRUDatabaseHelper.COLUMN_STATUS+"=?", new String[]{"%"+lastNameFilter+"%",status}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
					GRUDatabaseHelper.COLUMN_LASTNAME +" like ? and "+GRUDatabaseHelper.COLUMN_STATUS+"=?", new String[]{"%"+lastNameFilter+"%",status}, null, null, GRUDatabaseHelper.COLUMN_LASTNAME + " ASC");
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist.getSubjects();
	}

	public List<Subject> getWalklistForKnockAndHouse(String status,
			int numberFilter) {
		if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = null;
        if(numberFilter == 2){
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    //GRUDatabaseHelper.COLUMN_STATUS+"=?", new String[]{status}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
					GRUDatabaseHelper.COLUMN_STATUS+"=?", new String[]{status}, null, null, GRUDatabaseHelper.COLUMN_LASTNAME + " asc");
        }else{
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    //GRUDatabaseHelper.COLUMN_EVEN+"=? and "+GRUDatabaseHelper.COLUMN_STATUS+" =?", new String[]{""+numberFilter,status}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
					GRUDatabaseHelper.COLUMN_EVEN+"=? and "+GRUDatabaseHelper.COLUMN_STATUS+" =?", new String[]{""+numberFilter,status}, null, null, GRUDatabaseHelper.COLUMN_LASTNAME + " asc");
        }
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist.getSubjects();
	}

	public List<Subject> getWalklistForLastName(String lastNameFilter) {
		if (database == null || !database.isOpen()) {
            open();
        }
        Cursor cursor = null;
            cursor = database.query(GRUDatabaseHelper.TABLE_WALKLIST, null,
                    //GRUDatabaseHelper.COLUMN_LASTNAME +" like ? ", new String[]{"%"+lastNameFilter+"%"}, null, null, GRUDatabaseHelper.COLUMN_STATUS + " desc");
					GRUDatabaseHelper.COLUMN_LASTNAME +" like ? ", new String[]{"%"+lastNameFilter+"%"}, null, null, GRUDatabaseHelper.COLUMN_LASTNAME + " asc");
        cursor.moveToFirst();
        Walklist walklist = cursorToWalklist(cursor);
        cursor.close();
        close();
        return walklist.getSubjects();
	}

}
