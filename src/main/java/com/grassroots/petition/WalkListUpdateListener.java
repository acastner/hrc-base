package com.grassroots.petition;

import com.grassroots.petition.models.Walklist;

public interface WalkListUpdateListener {
    public void updateSuccess(Walklist walklist);
    public void updateFailed(String errorMessage);
}
