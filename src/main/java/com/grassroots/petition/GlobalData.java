package com.grassroots.petition;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

import com.grassroots.modules.DaggerApplicationComponent;

import com.bugsense.trace.BugSenseHandler;
import com.grassroots.modules.ApplicationComponent;
import com.grassroots.modules.ApplicationModule;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.MarketingSlides;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.tasks.FetchReportsListTask;
import com.grassroots.petition.utils.BatteryPerformance;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.Strings;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Base of the application, mostly used for storing 'global' data, petition,
 * walklist, petitionAnswers
 */
public class GlobalData extends Application {

    public static final String PREFERENCES_KEY = "GrassrootsPetition";
    private static final String TAG = GlobalData.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    public static final int SCREEN_TIMEOUT_SECONDS = 1;
    protected Walklist walklist;
    protected Petition petition;
    protected PetitionAnswers petitionAnswers;
    private static List<MarketingSlides> slidesList;
    private Date lastWalklistSyncTime;
    public static Boolean WALKLISTUPDATING = true;
    protected LocationList locationList;
    private String ZOOM_KEY = "zoomlevel";
    private String LATITUDE_KEY = "latitude";
    private String LONGITUDE_KEY = "longitude";
    private String StreetFilter = "StreetFilter";
    private String HouseNumberFilter = "HouseNumberFilter";
    private String KnockStatusFilter = "KnockStatusFilter";
    private String StreetFilterPosition = "StreetFilterPosition";
    private String KnockFilterPosition = "KnockFilterPosition";
    private String LastNameFilter = "LastNameFilter";
    private String LastNameFilterPostition = "LastNameFilterPosition";
    
    public static boolean shouldLocationReload = false;

    private ArrayList<String> reportList;
    private String preferredReportSetting;

    public ArrayList<String> getReportList(){
        return reportList;
    }

    public void setPreferredReportSetting(String preferredReportSetting) {
        this.preferredReportSetting = preferredReportSetting;
    }

    public String getPreferredReportSetting() {
        return preferredReportSetting;
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context)
    {
            CookieSyncManager cookieSyncMngr= CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager= CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
    }


    public void checkForReportsScriptProperties() {
        JSONObject jsonObj = petition.getPetitionJSON();
        JSONArray jsonArr = new JSONArray();
        try {
            jsonArr = jsonObj.getJSONArray("ScriptProperties");

            for (int i = 0; i < jsonArr.length(); i++) {

                if (jsonArr.getJSONObject(i).getString("Name").equals("PreferredReportName")) {
                    this.preferredReportSetting = jsonArr.getJSONObject(i).getString("Value");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void fetchAndSaveReports() {
        FetchReportsListTask fetchReportTask = new FetchReportsListTask(this);

        try {
            this.reportList = fetchReportTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    /**
     * Stores data about free and busy time intervals. Deserialized from JSON,
     * which is loaded from the backend. Can be null, if petition doesn't
     * support appointments (look at ).
     */
    protected AppointmentCalendar appointmentCalendar;

    public static final String LOG_FILENAME = "grassroots.log";
    public static String TMP_DIRECTORY = null;
    public String location;
    
    private String base_url = null;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private static Context context;
    public static int counterValue;
    public static int BATTERYLEVEL;
    public static int SIGNALSTRENGTH;
 

    public String getTMPDirectory() {
        return TMP_DIRECTORY;
    }

    private boolean configureLog4J() {
        if (TMP_DIRECTORY == null)
            return false;

        final String fileName = TMP_DIRECTORY + File.separator + LOG_FILENAME;

        if (!isLogFileWritable(fileName))
            return false;

        try {
            final LogConfigurator logConfigurator = new LogConfigurator();
            logConfigurator.setFileName(fileName);
            logConfigurator.setRootLevel(org.apache.log4j.Level.DEBUG);


            // Set log level of a specific logger
            logConfigurator.setLevel("org.apache", Level.ERROR);
            logConfigurator.configure();
            LOGGER.debug("Logging to: " + fileName);
        } catch (Exception ex) {

        }
        return true;
    }

    private boolean isLogFileWritable(String fileName) {
        File file = new File(fileName);
        if (file.exists())
            return file.canWrite();

        boolean retValue = false;

        try {
            retValue = file.createNewFile();
        } catch (IOException e) {
            LOGGER.debug("Unable to write into log file: " + fileName);
            retValue = false;
        }

        return retValue;
    }

    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {

        context = getApplicationContext();

        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        File externalDirectory = getApplicationContext().getExternalFilesDir(
                null);
        File internalDirectory = getApplicationContext().getFilesDir();

        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state))
            externalDirectory = null;

        TMP_DIRECTORY = (externalDirectory == null) ? (internalDirectory == null ? null
                : internalDirectory.getAbsolutePath())
                : externalDirectory.getAbsolutePath();

        if (!BasePetitionActivity.isDebug && BasePetitionActivity.isSilent)
            configureBugsense();

        if (!configureLog4J()) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.CANNOT_OPEN_LOG_FILE_MESSAGE),
                    Toast.LENGTH_LONG).show();
        }
        
    }
    
    public static void clearMarketingSlides(){
    	if (slidesList == null || slidesList.isEmpty()) {
    		return;
    	}
    	
    	
    	Log.e("Clearing marketing slides. current free mem = ", "Clearing"+Runtime.getRuntime().freeMemory());
    	for (MarketingSlides a:slidesList){
    		if(a == null){
    			continue;
    		}
    		List<Bitmap> slides = a.getSlides();
    		
			for (Bitmap slide:slides){
				if(slide == null){
	    			continue;
	    		}
    			slide.recycle();
    		}
    	}
    	slidesList = null;
    	Runtime.getRuntime().gc();
    	Log.e("Clearing marketing slides. free mem after GC = ", "Clearing after gc"+Runtime.getRuntime().freeMemory());
    }

    public void onMarketing(final Activity currentActivity) {
        final ProgressDialog pd = ProgressDialog.show(currentActivity,
                getString(R.string.pleasewaitmsg), getString(R.string.loadingmsg));
        if (slidesList == null) {

            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    slidesList = MarketingSlides
                            .getMarketingSlides(currentActivity);
                   
                    currentActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            pd.dismiss();
                            if (slidesList == null || slidesList.size() == 0) {
                                Toast.makeText(currentActivity, getString(R.string.marketingfetchfailedmsg), Toast.LENGTH_SHORT).show();
                                LOGGER.debug("OnMarketing called with no slides; should be disabled");
                            } else {
                                Intent intent = new Intent(
                                        currentActivity,
                                        ClassRetriever
                                                .getMarketingActivity(GlobalData.this));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    });
                }
            });
            t.start();
        } else {
			pd.dismiss();
            Intent intent = new Intent(currentActivity,
                    ClassRetriever.getMarketingActivity(GlobalData.this));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void configureBugsense() {
        initializeeBugsense(getApplicationContext().getString(R.string.bugsenseKey));
    }

    private void initializeeBugsense(String bugsenseKey) {
        if(Strings.isNotEmpty(bugsenseKey)) {
            try {
                BugSenseHandler.initAndStartSession(this, bugsenseKey);
            } catch(Exception ex) {
                LOGGER.error("BugSense Initialization error: " + ex.toString());
            }
        } else {
            LOGGER.error("Your BugSense API Key is invalid!");
        }

        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        BugSenseHandler.addCrashExtraData("IMEI",
                telephonyManager.getDeviceId());
        BugSenseHandler.addCrashExtraData("Phone",
                telephonyManager.getLine1Number());
    }


    private Petition getPetitionFromPreferences() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String petitionJson = preferences.getString(Petition.PETITION_KEY, "");
        if (Strings.isEmpty(petitionJson)) {
            return null;
        }
        try {
            return JsonParser.getParser().toPetition(petitionJson);
        } catch (Throwable t) {

        }
        return null;
    }

    public Petition getPetition() {
        if (petition == null) {
            LOGGER.error("Had null petition, loading from preferences");
            petition = getPetitionFromPreferences();
            if (petition == null) {
                LOGGER.error("Had null petition loaded from preferences");
            }
        }
        return petition;
    }

    public void setPetition(Petition petition) {
        this.petition = petition;
        if (petition != null) {
            savePreference(Petition.PETITION_KEY, petition.toJson());
        } else {
            savePreference(Petition.PETITION_KEY, "");
        }
    }
    
    public LocationList getLocationList() {
        if (locationList == null) {
            locationList = getLocationListFromPreferences();
            if (locationList == null) {
                LOGGER.error("Had null locations loaded from preferences");
            }
        }
        return locationList;
    }

    public void setLocationList(LocationList locationList) {
        this.locationList = locationList;
        if (locationList != null) {
            savePreference(LocationList.LOCATIONS_KEY, locationList.toJson());
        } else {
            savePreference(LocationList.LOCATIONS_KEY, "");
        }
    }
    
    private LocationList getLocationListFromPreferences() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String locationListJson = preferences.getString(LocationList.LOCATIONS_KEY, "");
        if (Strings.isEmpty(locationListJson)) {
            return null;
        }
        try {
            return JsonParser.getParser().toLocationList(locationListJson);
        } catch (Throwable t) {

        }
        return null;
    }

    // ------------------------------------------------------------------------------------------------
    // AppointmentCalendar handling methods
    // ------------------------------------------------------------------------------------------------
    public void setAppointmentCalendar(AppointmentCalendar calendar) {
        this.appointmentCalendar = calendar;
        if (appointmentCalendar != null) {
            savePreference(AppointmentCalendar.CALENDAR_KEY,
                    appointmentCalendar.toJson());
        } else {
            savePreference(AppointmentCalendar.CALENDAR_KEY, "");
        }
    }

    public AppointmentCalendar getAppointmentCalendar() {
        if (appointmentCalendar == null) {
            LOGGER.error("Had null appointment calendar, loading from preferences");
            appointmentCalendar = getAppointmentCalendarFromPreferences();
            if (appointmentCalendar == null) {
                LOGGER.error("Had null appointment calendar loaded from preferences");
            }
        }
        return appointmentCalendar;
    }

    private AppointmentCalendar getAppointmentCalendarFromPreferences() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String json = preferences.getString(AppointmentCalendar.CALENDAR_KEY,
                "");
        if (Strings.isEmpty(json)) {
            return null;
        }
        try {
            return JsonParser.getParser().toAppointmentCalendar(json);
        } catch (Throwable t) {

        }
        return null;
    }

    public void clearAppointmentCalendar() {
        appointmentCalendar = null;
        savePreference(AppointmentCalendar.CALENDAR_KEY, "");
    }

    private Walklist getWalklistFromPreferences() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String walklistJson = preferences.getString(Walklist.WALKLIST_KEY, "");
        if (Strings.isEmpty(walklistJson)) {
            return null;
        }
        try {
            return JsonParser.getParser().toWalklist(walklistJson);
        } catch (Throwable t) {

        }
        return null;
    }

    public Walklist getWalklist() {
        if (walklist == null) {
            LOGGER.error("Had null wakllist, loading from preferences");
            walklist = getWalklistFromPreferences();
        }
        return walklist;
    }

    public void setWalklist(Walklist walklist) {
        this.walklist = walklist;
        if (walklist != null) {
            savePreference(Walklist.WALKLIST_KEY, walklist.toJson());
        } else {
            savePreference(Walklist.WALKLIST_KEY, "");
        }
    }

    public void setPetitionAnswers(PetitionAnswers petitionAnswers) {
        this.petitionAnswers = petitionAnswers;
    }

    public PetitionAnswers getPetitionAnswers() {
        return getNeverNullPetitionAnswers();
    }

    public void clearPetitionAnswers() {
        petitionAnswers = null;
    }

    protected PetitionAnswers getNeverNullPetitionAnswers() {
        if (petitionAnswers == null) {
            LOGGER.error("PetitionAnswers are null, returning new instance. In "
                    + this.getClass());
            petitionAnswers = new PetitionAnswers();
            Petition petition1 = getPetition();
            if (petition1 != null) {
                petitionAnswers.setId(getPetition().getId());
            }
        }
        return petitionAnswers;
    }

    public void clearPetition() {
        petition = null;
        savePreference(Petition.PETITION_KEY, "");
    }

    public void clearWalklist() {
        walklist = null;
        savePreference(Walklist.WALKLIST_KEY, "");
        WalklistDataSource walklistDataSource = new WalklistDataSource(getApplicationContext());
        walklistDataSource.clearWalklist();


    }

    public void clearAllPetition() {
        clearPetitionAnswers();
        clearPetition();
        clearWalklist();
        clearAppointmentCalendar();
    }

    @SuppressLint("NewApi")
    private void savePreference(String key, String value) {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);

        if (Build.VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }
    
    private void savePreference(String key, int value) {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * setScreenTimeout(60 * SCREEN_TIMEOUT_SECONDS);
     * @param timeoutSeconds
     */

    private void setScreenTimeout(int timeoutSeconds) {
        Settings.System.putInt(getContentResolver(),
                Settings.System.SCREEN_OFF_TIMEOUT, timeoutSeconds * 1000);
    }

    private Properties loadProperties() throws IOException {
        String file = "bugsense.properties";
        Properties prop = new Properties();
        InputStream fileStream = getAssets().open(file);
        prop.load(fileStream);
        fileStream.close();
        return prop;
    }



    public static Context getContext() {
        return context;
    }

    public static List<MarketingSlides> getSlides() {
        return slidesList;
    }
    
    public Date getLastWalklistSyncTime() {
        if(lastWalklistSyncTime == null){
            return Calendar.getInstance().getTime();
        }
        return lastWalklistSyncTime;
    }

    public void setLastWalklistSyncTime(Date lastsyncedTime) {
        this.lastWalklistSyncTime = lastsyncedTime;
    }
    
    /**
     * get walklist from database
     * @return
     */
    public Walklist getWalkListFromDatabase(){
        WalklistDataSource walklistDataSource = new WalklistDataSource(getApplicationContext());
        Walklist walklist = walklistDataSource.getWalklist();
        return walklist;
    }

    public int getZoomLevel() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int zoomlevel = preferences.getInt(ZOOM_KEY, 0);
        return zoomlevel;
    }

    public void setZoomLevel(int zoomLevel) {
        savePreference(ZOOM_KEY, zoomLevel);
    }

    public void setMapCenterLatitude(int latitudeE6) {
        savePreference(LATITUDE_KEY, latitudeE6);
    }
    
    public void setMapCenterLongitude(int longitudeE6) {
        savePreference(LONGITUDE_KEY, longitudeE6);
    }
    
    public int getMapCenterLatitude(){
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int latitude = preferences.getInt(LATITUDE_KEY, 0);
        return latitude;
    }
    
    public int getMapCenterLongitude(){
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int longitude = preferences.getInt(LONGITUDE_KEY, 0);
        return longitude;
    }

    public void clearMapFactors() {
        setMapCenterLatitude(0);
        setMapCenterLongitude(0);
        setZoomLevel(0);
    }
    
    /**
     * get device model name
     * @return model
     */
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
          return capitalize(model);
        } else {
          return capitalize(manufacturer) + " " + model;
        }
      }


    /**
     * format model name
     * @param s : modelname
     * @return
     */
      private String capitalize(String s) {
        if (s == null || s.length() == 0) {
          return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
          return s;
        } else {
          return Character.toUpperCase(first) + s.substring(1);
        }
      }

    public EventInfo getEventInfo(GpsLocation gpsLocation,BatteryPerformance batteryPerformance){
        EventInfo eventInfo = new EventInfo();
        eventInfo.setBatterylevel(GlobalData.BATTERYLEVEL);
        eventInfo.setSignalstrength(GlobalData.SIGNALSTRENGTH);
        eventInfo.setRunningappscount(batteryPerformance.getRunningAppsCount());
        eventInfo.setBrightness(batteryPerformance.getScreenBrightness());
        eventInfo.setBluetooth(batteryPerformance.isBluetoothEnabled());
        eventInfo.setBondeddevices(batteryPerformance.getBondedDevices());
        eventInfo.setWifi(batteryPerformance.isWifiConnected());
        eventInfo.setImei(batteryPerformance.getIMEI());
        eventInfo.setPhonemodel(getDeviceName());
        eventInfo.setNetworktype(batteryPerformance.getNetworkType());
        eventInfo.setLatitude(gpsLocation.getLatitude());
        eventInfo.setLongitude(gpsLocation.getLongitude());
        eventInfo.setType(gpsLocation.getType());
        eventInfo.setUncertainty(gpsLocation.getUncertainty());
        eventInfo.setTime(gpsLocation.getTime());
        return eventInfo;
    }

    public String getStreetFilter() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String streetFilter = preferences.getString(StreetFilter, "");
        return streetFilter;
    }

    public void setStreetFilter(String streetFilter) {
        savePreference(StreetFilter, streetFilter);
    }

    public String getHouseNumberFilter() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String housefilter = preferences.getString(HouseNumberFilter, "");
        return housefilter;
    }

    public void setHouseNumberFilter(String houseNumberFilter) {
        savePreference(HouseNumberFilter, houseNumberFilter);
    }

    public String getKnockStatusFilter() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String knockfilter = preferences.getString(KnockStatusFilter, "");
        return knockfilter;
    }

    public void setKnockStatusFilter(String knockStatusFilter) {
        savePreference(KnockStatusFilter, knockStatusFilter);
    }

    public int getStreetFilterPosition() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int knockfilter = preferences.getInt(StreetFilterPosition, 0);
        return knockfilter;
    }

    public void setStreetFilterPosition(int streetFilterPosition) {
        savePreference(StreetFilterPosition, streetFilterPosition);
    }

    public int getKnockFilterPosition() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int knockfilter = preferences.getInt(KnockFilterPosition, 0);
        return knockfilter;
    }

    public void setKnockFilterPosition(int knockFilterPosition) {
        savePreference(KnockFilterPosition, knockFilterPosition);
    } 
    
    
    public String getLastNameFilter() {
    	SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        String lastnamefilter = preferences.getString(LastNameFilter, "");
        return lastnamefilter;
	}

	public void setLastNameFilter(String lastNameFilter) {
		savePreference(LastNameFilter, lastNameFilter);
	}
	
	public int getLastNameFilterPosition() {
    	SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        int lastnamefilterpos = preferences.getInt(LastNameFilterPostition, 0);
        return lastnamefilterpos;
	}

	public void setLastNameFilterPosition(int lastNameFilterPosition) {
		savePreference(LastNameFilterPostition, lastNameFilterPosition);
	}

	public void clearPreferences(){
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY,
                MODE_PRIVATE);
        preferences.edit().clear().commit();
    }

	public String getBaseUrl() {
		return base_url;
	}

	public void setBaseUrl(String baseUrl) {
		base_url = baseUrl;
	}

    private String crashedUsername;
    private int crashedScriptId;
    private String crashedScriptName;
    private String userNotes = "";

    private String loggedInUserName;
    public String getLoggedInUserName() {
        return loggedInUserName;
    }

    public void setLoggedInUserName(String loggedInUserName) {
        this.loggedInUserName = loggedInUserName;
    }

    public void setCrashedUserName(String username) {
        this.crashedUsername = username;
    }

    public String getCrashedUserName(){
        return crashedUsername;
    }

    public void setCrashedScriptId(int scriptid){
        this.crashedScriptId = scriptid;
    }

    public int getCrashedScriptId(){
        return crashedScriptId;
    }

    public void setCrashedScriptName(String scriptname){
        this.crashedScriptName = scriptname;
    }

    public String getCrashedScriptName(){
        return crashedScriptName;
    }

    public String getUserNotes() {
        return userNotes;
    }

    public void setUserNotes(String usernotes) {
        userNotes = usernotes;
    }
   
    
}
