package com.grassroots.petition.activities;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.fragments.WalklistDetailsFragment;
import com.grassroots.petition.fragments.WalklistListFragment;
import com.grassroots.petition.fragments.WalklistMapFragment;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.KnockEvent;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Knocks;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.petition.views.HomeStatusView;
/**
 * Walklist class replacing WalklistActivity and WalklistActivityWithTabs-avoiding duplication of code and using 
 * database operations for filtering and sorting
 * @author sharika
 *
 */
public class BaseWalklistActivity extends BasePetitionActivityWithMenu implements
TabListener {

	private FrameLayout mapFragmentHolder, detailsFragmentHolder;
	private RelativeLayout titlelayout;
	public boolean isTablet = false;
	private FragmentManager fragmentManager = null;
	private WalklistMapFragment mapFragment = null;
	private WalklistDetailsFragment detailsFragment = null;
	private WalklistListFragment listFragment = null;
	private Walklist walklist = null;
	private ActionBar.Tab listTab = null;
	private ActionBar.Tab mapTab = null;
	private Subject selectedSubject = null;
	private ActionBar actionBar = null;
	public boolean isLocationList;
	public static final String WALKLIST_EXTRA_PARAMETER_SUBJECT = "SELECTED_SUBJECT_EXTRA";
	public static final String SHOULD_SHOW_MAP_TAB = "SHOULD_SHOW_MAP_TAB";
	private static final String TAG = BaseWalklistActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private String selectedLastName = null;
	public static final String WALKLIST_EXTRA_PARAMETER_LASTNAME="SELECTED_LASTNAME_EXTRA";

	/**
	 * Receiver that listens to walklist update action(on update of walklist api from server)
	 */
	BroadcastReceiver walklistUpdateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("BaseWalklistActivity", "Received walklist update from server");
			//listFragment.setWalklist(getWalklist());
			listFragment.refreshWalklist();
			if(selectedSubject != null)
				listFragment.onSelectSubject(selectedSubject);
			if(detailsFragmentListener != null && isTablet)
				detailsFragmentListener.onSelectSubject(selectedSubject);
		}
	};

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// To change body of implemented methods use File | Settings | File
		// Templates.
		if (listTab == tab) {
			if(mapFragment != null && mapFragment.getMapView() != null){
				mapFragment.getMapView().getTileProvider().clearTileCache();
			}
			switchToList();
		} else if (mapTab == tab) {
			switchToMap();
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			// Assume Android 4.0, 7.0" tablet screen size
			isTablet = true;
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		} else {
			// dart
			isTablet = false;
		}
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.walklistscreen);
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			configurePortraitWalklistButtons();
		}
		TextView textView = (TextView)findViewById(R.id.textView);
		textView.setText("Home Screen -" + getLocationName());
		if(petition.isTraining())	{
			textView.setText("Home Screen -" + getLocationName() + "-TRAINING PAYMENTS MODE");
			textView.setTextColor(Color.RED);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		initViews();

	}
	@Override
	protected void onPostResume() {
		super.onPostResume();
		setUpViews();
	}

	private void initViews() {
		mapFragmentHolder = (FrameLayout) findViewById(R.id.map_fragment_container);
		detailsFragmentHolder = (FrameLayout) findViewById(R.id.details_fragment_container);
		fragmentManager = getSupportFragmentManager();
		if (getGlobalData().getLocation() != null) {
			isLocationList = true;
		} else {
			isLocationList = false;
		}

		walklist = getWalklist();
		registerReceiver(walklistUpdateReceiver, new IntentFilter("WalklistUpdateReceiver"));
	}

	private void setUpViews() {

		listFragment = (WalklistListFragment) fragmentManager
				.findFragmentById(R.id.walklist_list_fragment);
		listFragment.setViewListener(listFragmentListener);
		listFragment.setWalklist(walklist);
		listFragment.updateFilters();
		titlelayout = (RelativeLayout) findViewById(R.id.titlelayout);
		if(getGlobalData().getLocation() != null){
			((TextView)(titlelayout.findViewById(R.id.textView))).setText(getGlobalData().getLocation());
		}
		Subject extraSelectedSubject = (Subject) getIntent().getParcelableExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT);
		selectedLastName = getIntent().getStringExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME);
		if(isLocationList && extraSelectedSubject != null && selectedLastName == null){
			selectedLastName = extraSelectedSubject.getLastName();
		}
		Log.e("on resume :","extra lastname : "+selectedLastName);
		selectedSubject = extraSelectedSubject;
		if(selectedSubject!= null){
			Log.e("selected subject","selected subject :"+selectedSubject.getAddressLine1());
		}
		if (isTablet) {
			titlelayout.setVisibility(View.VISIBLE);
			showListFragment();

			if (selectedSubject == null && getGlobalData().getLocation() == null) {
				// load map fragment
				//loadMapFragment();

				loadDetailsFragment();
			} else if(selectedSubject == null && getGlobalData().getLocation() != null){
				//selectedSubject = walklist.getSubjects().get(0);
				selectedLastName = listFragment.lastNamesFromDatabase.get(0);
				listFragment.onSelectLastName(listFragment.lastNamesFromDatabase.get(0));
				loadDetailsFragment();
			}	else {
				if (getGlobalData().getLocation() == null) {
					if(selectedSubject.getLatitude()==null){
						selectedSubject.setLatitude("0.0");
					}
					if(selectedSubject.getLongitude()==null){
						selectedSubject.setLongitude("0.0");
					}
					listFragment.onSelectSubject(selectedSubject);
					detailsFragmentListener.onSelectSubject(selectedSubject);
				}else{
					listFragment.onSelectLastName(selectedLastName);
					detailsFragmentListener.onSelectLastName(selectedLastName);
				}

				loadDetailsFragment();
			}

		} else {
			// make mapfragmentholder and titlebar visibility gone
			mapFragmentHolder.setVisibility(View.GONE);
			titlelayout.setVisibility(View.GONE);
			if (actionBar == null || listTab == null || mapTab == null) {
				actionBar = getSupportActionBar();
				actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
				actionBar.setDisplayShowHomeEnabled(false);
				actionBar.setDisplayShowTitleEnabled(false);

				listTab = actionBar.newTab();
				listTab.setText(getString(R.string.WALKLIST_LIST_TAB_TITLE));
				listTab.setTabListener(this);
				actionBar.addTab(listTab);

				mapTab = actionBar.newTab();
				mapTab.setText(getString(R.string.WALKLIST_MAP_TAB_TITLE));
				mapTab.setTabListener(this);
				actionBar.addTab(mapTab);
			} 
		}
		if (getGlobalData().getLocation() == null) {
			listFragment.onSelectSubject(selectedSubject);
		}else{
			listFragment.onSelectLastName(selectedLastName);
		}
		boolean isShouldShowMapTab = getIntent().getBooleanExtra(SHOULD_SHOW_MAP_TAB, false);
		if (isShouldShowMapTab && !isTablet) {
			actionBar.selectTab(mapTab);
			getIntent().getExtras().clear();
		}
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			mapFragmentHolder.setVisibility(View.GONE);
			hideListFragment();
			detailsFragmentHolder.setVisibility(View.GONE);
		}
	}

	private void loadMapFragment() {

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":"
				+ cl.get(Calendar.SECOND) + "]";

		Tracker.appendLog(time + " Switching to map.");

		mapFragment = new WalklistMapFragment();
		mapFragment.setViewListener(mapFragmentListener);
		mapFragment.setWalklist(walklist);
		mapFragment.restoreMapConfig(getGlobalData().getMapCenterLatitude(),
				getGlobalData().getMapCenterLongitude(), getGlobalData()
				.getZoomLevel());
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.map_fragment_container, mapFragment);
		fragmentTransaction.commit();
		if (!isTablet) {
			hideListFragment();
		}
		fragmentManager.executePendingTransactions();
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			mapFragmentHolder.setVisibility(View.VISIBLE);
		detailsFragmentHolder.setVisibility(View.GONE);

	}

	private void switchToDetails() {
		// hidemapfragment
		hideMapFragment();
		// showdetailsfragment
		loadDetailsFragment();

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";

		Tracker.appendLog(time + " Showing subject details.");
	}
	/**
	 * show the reports for the user
	 */
	private void showTallyScreen(){
		String preferredReport = getGlobalData().getPreferredReportSetting();
		if(preferredReport == null || preferredReport.isEmpty()){
			final ArrayList<String> reports = getGlobalData().getReportList();
			CharSequence[] cs = reports.toArray(new CharSequence[reports.size()]);

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Select a report to view");
			builder.setItems(cs, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					ReportDetailWebviewActivity reportdetailActivity = new ReportDetailWebviewActivity();
					Intent intent = new Intent(BaseWalklistActivity.this,ReportDetailWebviewActivity.class);
					intent.putExtra("reportname",reports.get(item));
					startActivity(intent);
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}else{
			ReportDetailWebviewActivity reportdetailActivity = new ReportDetailWebviewActivity();
			Intent intent = new Intent(BaseWalklistActivity.this,ReportDetailWebviewActivity.class);
			intent.putExtra("reportname",preferredReport);
			startActivity(intent);
		}

		//Intent intent = new Intent(BaseWalklistActivity.this,TallyActivity.class);
		//startActivity(intent);
	}

	private void loadDetailsFragment() {

		detailsFragment = new WalklistDetailsFragment();
		detailsFragment.setViewListener(detailsFragmentListener);
		detailsFragment.setWalklist(walklist);
		detailsFragment.setPetition(petition);

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.details_fragment_container,
				detailsFragment);
		fragmentTransaction.commit();
		fragmentManager.executePendingTransactions();

		if(isLocationList){
			if(selectedLastName == null){
				//selectedLastName = listFragment.lastNamesFromDatabase.get(0);

			}
			String knockvalue = null;
			if(getGlobalData().getKnockStatusFilter() != null && getGlobalData().getKnockStatusFilter().length()>0){
				KnockStatus storedKnock = KnockStatus.valueOf(getGlobalData().getKnockStatusFilter());

				if(storedKnock!=null){
					knockvalue = ""+storedKnock.getRepresentation();
				}

				detailsFragment.setSubjects(walklist.getSubjects());
			}else{
				detailsFragment.setSubjects(walklist.getSubjects());

			}
			listFragment.onSelectLastName(selectedLastName);

		}else{
			if(selectedSubject != null)
			{
				detailsFragment.setSubjects(walklist
						.getSubjects());
			}
		}
		mapFragmentHolder.setVisibility(View.GONE);
		detailsFragmentHolder.setVisibility(View.GONE);

	}

	/**
	 * Show subject details in the new activity of class
	 * 'WalklistDetailsActivity'
	 * 
	 * @param subject
	 *            - a subject object which contains address and will be showed
	 *            in details activity.
	 */
	private void showSubjectDetails(Subject subject) {
		if (null != subject) {
			selectedSubject = subject;
			selectedLastName = subject.getLastName();
			Intent intent = new Intent();
			intent.setClass(this, WalklistDetailsActivity.class);
			if(isLocationList){
				intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, selectedLastName);
			}else{
				intent.putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
			}
			startActivity(intent);
		}
	}

	private void showSubjectDetailsForLastname(String lastname) {
		if (null != lastname) {
			Intent intent = new Intent();
			intent.setClass(this, WalklistDetailsActivity.class);
			intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, lastname);
			startActivity(intent);
		}

	}
	/**
	 * Begin survey for the specified subject
	 * 
	 * @param subject
	 *            - a subject which will be surveyed
	 */
	protected void beginProcessSubject(Subject subject) {
		if(isLocationList){
			subject.setLocation(getGlobalData().getLocation());
		}
		Calendar cl = Calendar.getInstance();
		int month = cl.get(Calendar.MONTH)+1;
		int day = cl.get(Calendar.DAY_OF_MONTH);
		int year = cl.get(Calendar.YEAR);
		int hour = cl.get(Calendar.HOUR_OF_DAY);
		int min = cl.get(Calendar.MINUTE);
		int sec = cl.get(Calendar.SECOND);
		
		petitionAnswers.setSurveyStart(month + "-" + day + "-" + year + ", "
				+ hour + ":" + min + ":" + sec);
		setSubject(subject);
		startActivity(getNextActivity());
	}

	/**
	 * Map fragment listener, used to receive events from map fragment
	 */
	private WalklistMapFragment.ViewListener mapFragmentListener = new WalklistMapFragment.ViewListener() {
		@Override
		public void onSetStatus() {
			BaseWalklistActivity.this.onSetHomeStatus();
		}

		@Override
		public void onLogout() {
			BaseWalklistActivity.this.showLogoutDialog();
		}

		@Override
		public void onManualEntry() {
			BaseWalklistActivity.this.beginProcessSubject(new Subject());
		}

		@Override
		public void onMarketing() {
			BaseWalklistActivity.this.onMarketing();
		}

		@Override
		public void onSelectSubject(Subject subject) {
			selectedSubject = subject;
			listFragment.onSelectSubject(subject);
		}
	};

	/**
	 * List fragment listener, used to receive events from the list with
	 * subjects addresses
	 */
	private WalklistListFragment.ViewListener listFragmentListener = new WalklistListFragment.ViewListener() {
		@Override
		public void onSelectSubject(Subject subject) {
			selectedSubject = subject;
			if (isTablet) {
				switchToDetails();
			} else {
				showSubjectDetails(selectedSubject);
			}

		}

		@Override
		public void onFilterStreetName(String streetNameFilter) {
			updateWalklistWithStreetName(streetNameFilter);
		}

		@Override
		public void onShowHouseNumber(
				Walklist.HouseNumberFilter houseNumberFilter) {
			updateWalklistWithHouseNumber(houseNumberFilter);
		}

		@Override
		public void onFilterKnockStatus(KnockStatus knockStatus) {
			updateWalklistWithKnockStatus(knockStatus);
		}

		@Override
		public void onNewRecord() {
			beginProcessSubject(new Subject());
		}

		@Override
		public void onFilterLastName(String filter) {
			updateWalklistWithLastName(filter);

		}

		@Override
		public void onSelectLastName(String lastname) {
			selectedLastName = lastname;
			if (isTablet) {
				switchToDetails();
			} else {
				showSubjectDetailsForLastname(lastname);
			}

		}

		@Override
		public void onTally() {
			showTallyScreen();
		}

	};

	/**
	 * Details fragment listener, used to receive events from the details
	 * fragment which contains details information about subject address
	 */
	private WalklistDetailsFragment.ViewListener detailsFragmentListener = new WalklistDetailsFragment.ViewListener() {
		@Override
		public void onSetHomeStatus() {
			BaseWalklistActivity.this.onSetHomeStatus();
		}

		@Override
		public void onWalklistUpdate() {
			listFragment.refreshWalklist();
			listFragment.onSelectSubject(selectedSubject);
		}

		@Override
		public void onSurveyStart(Subject subject) {
			BaseWalklistActivity.this.beginProcessSubject(subject);
		}

		@Override
		public void onMarketing(Subject subject) {
			Log.e("get subject","onmarketing : "+subject);
			getIntent().putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
			BaseWalklistActivity.this.onMarketing();
		}

		@Override
		public void onShowMap() {
			loadMapFragment();
		}

		@Override
		public void onSelectSubject(Subject subject) {
			listFragment.onSelectSubject(subject);
		}

		@Override
		public void onFilterLastName(String filter) {
			updateWalklistWithLastName(filter);

		}
		@Override
		public void onSelectLastName(String lastname) {
			listFragment.onSelectLastName(lastname); 
		}

	};

	private void switchToMap() {
		hideListFragment();
		loadMapFragment();
	}

	private void hideListFragment() {
		FragmentTransaction ft = fragmentManager.beginTransaction();
		ft.hide(listFragment);
		ft.commit();
	}

	private void showListFragment() {
		FragmentTransaction ft = fragmentManager.beginTransaction();
		//ft.show(listFragment);
		ft.commit();
	}

	private void hideMapFragment() {
		if(mapFragment != null){
			getGlobalData().setZoomLevel(mapFragment.getMapView().getZoomLevel());
			getGlobalData().setMapCenterLatitude(
					mapFragment.getMapView().getMapCenter().getLatitudeE6());
			getGlobalData().setMapCenterLongitude(
					mapFragment.getMapView().getMapCenter().getLongitudeE6());
			FragmentTransaction ft = fragmentManager.beginTransaction();
			ft.hide(mapFragment);
			ft.commit();
		}
	}

	private void switchToList() {
		mapFragmentHolder.setVisibility(View.GONE);
		if (mapFragment != null) {
			hideMapFragment();
		}
		if (listFragment.isHidden()) {
			showListFragment();
		}
	}
	/**
	 * Display a dialog with a list of available knock statuses where the user
	 * can select appropriate knock status for the selected subjects address.
	 */
	protected void onSetHomeStatus() {
		final Dialog statusDialog = new Dialog(this);
		statusDialog.setTitle(R.string.home_status);
		HomeStatusView view = (HomeStatusView) View.inflate(this,
				R.layout.home_status, null);

		view.setViewListener(new HomeStatusView.ViewListener() {
			@Override
			public void onStatusSelected(KnockStatus knockStatus) {
				List<Subject> knockedSubjects = detailsFragment.getSubjects();
				detailsFragment.setSubjects(knockedSubjects); 
				if (null != knockedSubjects && knockedSubjects.size() > 0) // should
					// always
					// be
					// true
				{
					Knocks knocks = new Knocks();
					GpsLocation location = LocationProcessor
							.getLastKnownGpsLocation(getApplicationContext());
					for (Subject subject : knockedSubjects) {
						// Don't update completed statuses
						if (subject.getKnockStatus() == KnockStatus.SUCCESS) {
							continue;
						}

						walklist.setKnocked(subject.getId(), knockStatus);
						WalklistDataSource walklistDataSource = new WalklistDataSource(detailsFragment.getActivity());
						walklistDataSource.insertWalklist(knockedSubjects,getGlobalData().getLocation());
						detailsFragment.getWalkList().setKnocked(subject.getId(), knockStatus);
						if(listFragment != null){
							listFragment.refreshWalklist();
							listFragment.onSelectSubject(subject);
						}

						String hid = subject.getHid();
						try {
							knocks.addKnockEvent(addBatteryInfoToKnock(new KnockEvent(Integer
									.parseInt(hid), knockStatus, location)));
						} catch (NumberFormatException e) {
							LOGGER.error("Unable to format HID for knockevent: "
									+ hid);
						}
					}

					if (knocks.hasKnocks()) {
						try {
							GrassrootsRestClient.getClient().saveKnocks(knocks);
						} catch (Exception exception) {
							logoutWithError(
									getString(R.string.error_unable_to_send_knock_status),
									exception);
						}
					}
				}

				statusDialog.cancel();
			}

			@Override
			public void onCancel() {
				statusDialog.cancel();
			}
		});

		statusDialog.setContentView(view);
		statusDialog.show();
	}

	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(walklistUpdateReceiver);
	}
	/**
	 * Filtes subjects addresses list and markers on map by specified house
	 * number.
	 * 
	 * @param houseNumberFilter
	 *            - house number to filter address list
	 */
	private void updateWalklistWithHouseNumber(
			Walklist.HouseNumberFilter houseNumberFilter) {
		List<Subject> addresses = listFragment.setHouseNumberFilter(houseNumberFilter);
		if (null != mapFragment) {
			mapFragment.setHouseNumberFilter(houseNumberFilter);
		}
		if(isLocationList){
			detailsFragment.setSubjects(addresses);
		}
	}

	/**
	 * Filters subjects addresses list and markers on map by address which
	 * should contain specified part of street name.
	 * 
	 * @param streetNameFilter
	 *            - part of street name to filter subject addresses.
	 */
	private void updateWalklistWithStreetName(String streetNameFilter) {
		List<Subject> addresses = listFragment.setStreetNameFilter(streetNameFilter);
		if (null != mapFragment) {
			mapFragment.setStreetNameFilter(streetNameFilter);
		}
		if(detailsFragment != null){
			if(addresses.size()>0 && streetNameFilter != null && streetNameFilter.length()>0){
				selectedSubject = addresses.get(0);
				loadDetailsFragment();
			}
		}
	}

	/**
	 * Filtes subjects addresses list and markers on map by specified knock
	 * status which is set to whole address.
	 * 
	 * @param knockStatus
	 *            - knock status which is used to filter address list
	 */
	private void updateWalklistWithKnockStatus(KnockStatus knockStatus) {
		List<Subject> addresses = listFragment.setKnockStatusFilter(knockStatus);
		if (null != mapFragment) {
			mapFragment.setKnockStatusFilter(knockStatus);
		}
		if(isLocationList){
			if(detailsFragment != null && detailsFragment.isVisible()){
				if(knockStatus != null){
					String knockvalue = ""+knockStatus.getRepresentation();
					detailsFragment.setSubjects(walklist.getSubjects());
					if(addresses.size()>0){
						listFragmentListener.onSelectLastName(addresses.get(0).getLastName());
					}
				}else{
					detailsFragment.setSubjects(addresses);
				}

			}
		}else{
			if(detailsFragment != null && detailsFragment.isVisible()){
				if(addresses.size()>0 && knockStatus != null){
					listFragmentListener.onSelectSubject(addresses.get(0));
				}
			}
		}
	}

	/**
	 * Filtes subjects with lastname
	 * 
	 * @param lastnamefilter
	 */
	private void updateWalklistWithLastName(String lastnamefilter) {
		if(lastnamefilter != null){
			List<Subject> addresses = listFragment.setLastNameFilter(lastnamefilter);
			if(detailsFragment != null){
				detailsFragment.setSubjects(addresses);
			}
		}


	}



	@Override
	public void onBackPressed() {
		if(petition.isLocationList()){
			getGlobalData().setKnockStatusFilter(null);
			getGlobalData().setKnockFilterPosition(0);
			getGlobalData().setLastNameFilter(null);
			getGlobalData().setLastNameFilterPosition(0);
			Intent intent = new Intent(this, LocationListActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);


		}else{
			showLogoutDialog();
		}

	}

	//adding menu for tally-event
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(!isTablet){
			menu.add(R.string.tally_button).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			return true;
		}else{
			menu.add("Privacy Policy");
			return true;
		}

	}
	/**
	 *  tally menu selection here for dart
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Resources resources = getResources();
		if (item.getTitle().toString().equals(resources.getString(R.string.tally_button))) {
			Intent intent = new Intent(BaseWalklistActivity.this,TallyActivity.class);
			startActivity(intent);
			return true;
		}
		if(item.getTitle().toString().equals("Privacy Policy")) {
			openPrivacyPolicyWebView();
		}
		return true;
	}

	public void openPrivacyPolicyWebView()  {
		Intent privacyPolicyIntent = new Intent(BaseWalklistActivity.this, PrivacyPolicy.class);
		startActivity(privacyPolicyIntent);
	}


	protected void configurePortraitWalklistButtons()	{
		Button newRecordButton = (Button) findViewById(R.id.new_record);
		Button tallyButton = (Button) findViewById(R.id.tally);
		Button marketingButton = (Button) findViewById(R.id.marketing);
		newRecordButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				beginProcessSubject(new Subject());
			}
		});

		tallyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onTally();
			}
		});

		marketingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onMarketing();
			}
		});
	}

}
