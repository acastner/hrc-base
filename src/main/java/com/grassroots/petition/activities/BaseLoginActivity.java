package com.grassroots.petition.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;

import com.grassroots.libs.updater.ApkUpdateListener;
import com.grassroots.libs.updater.AutoApkUpdater;
import com.grassroots.models.OAuthResponse;
import com.grassroots.modules.GrassrootsApiService;
import com.grassroots.petition.db.helper.DatabaseInstaller;
import com.grassroots.petition.exceptions.DatabaseException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.ActivityManager.RunningAppProcessInfo;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bugsense.trace.BugSenseHandler;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.exceptions.GoToActivityExceptionHandler;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.MarketingSlides;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.ResponseError;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.receivers.BatteryInformationReceiver;
import com.grassroots.petition.receivers.CellStateInfoReceiver;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.BatteryPerformance;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.petition.views.LoginView;
import com.grassroots.services.RequestPerformerService;
import com.grassroots.utils.ApiUtils;
import com.grassroots.utils.AuthenticationHeaderPreference;
import com.grassroots.utils.ImeiHeaderPreference;
import com.grassroots.utils.OAuthTokenPreference;
import com.grassroots.utils.StringPreference;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created with IntelliJ IDEA. Login Activity Starts from StartupActivity Starts
 * GPS polling loop to get fresh GPS coords on Login Prompts user for
 * credentials
 */
public class BaseLoginActivity extends BasePetitionActivity implements ApkUpdateListener {
	public static final boolean autofillCredentials = false || isDebug;
	private static final String TAG = BaseLoginActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	protected GrassrootsRestClient client;
	private AtomicBoolean isPingActive = new AtomicBoolean(false);
	private LoginView view;
	private WalklistDataSource walklistDataSource;
	private BatteryInformationReceiver mBatInfoReceiver = new BatteryInformationReceiver(){
		@Override
		public void onReceive(Context arg0, Intent intent) {
			int level = intent.getIntExtra("level", 0);
			GlobalData.BATTERYLEVEL = level;
		}
	};
	CellStateInfoReceiver cellstateInfoReceiver = new CellStateInfoReceiver(){

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			// TODO Auto-generated method stub
			super.onCallStateChanged(state, incomingNumber);
		} 

		public void onSignalStrengthsChanged(android.telephony.SignalStrength signalStrength) {
			GlobalData.SIGNALSTRENGTH = signalStrength.getCdmaDbm();
		}
	};

	private LoginView.ViewListener viewListener = new LoginView.ViewListener() {
		@Override
		public void onLogin(final String username, final String password) {
			BaseLoginActivity.this.onLogin(username, password);
		}
	};

	private static final Object lock = new Object();
	private static boolean upgraded = false;
	private BaseLoginActivity activity = this;

	public void loginSuccess(OAuthResponse response) {
		Log.d("AUTH", "Success!");
		oAuthTokenPreference.set("Bearer " + response.getAccessToken());
        /*
		Observable<com.grassroots.glibs.models.Petition> petitionBody = grassrootsApiService.getPetition(
				"Bearer " + response.getAccessToken(),
				DeviceInfoUtils.getImei(this));
		petitionBody.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
				.subscribe(data -> {initializePetitionData(data); }, error -> { Log.d(TAG, Log.getStackTraceString(error));});
				*/
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		boolean wasCrashed = intent.getBooleanExtra(GoToActivityExceptionHandler.KEY, false);
		if (wasCrashed) {
			LOGGER.error("Recovered from a crash");
			//notifyUser(getString(R.string.APPLICATION_SUFFER_UNRECOVERABLE_ERROR_MESSAGE));
			String username = intent.getStringExtra("username");
			int scriptid = intent.getIntExtra("scriptid", 0);
			String scriptname = intent.getStringExtra("scriptname");
			getGlobalData().setCrashedUserName(username);
			getGlobalData().setCrashedScriptId(scriptid);
			getGlobalData().setCrashedScriptName(scriptname); onEmailLogs();
		}
	}


	/**
	 * Called when the user clicks the login button. Should handle loading any
	 * needed data. Should also block further clicks of the login button until
	 * the login process has completed.
	 * 
	 * @param username
	 * @param password
	 */
	protected void onLogin(final String username, final String password) {
		showProgress(getString(R.string.LOGGIN_IN_MESSAGE));

		((GlobalData) getApplicationContext()).clearAllPetition();
		((GlobalData) getApplicationContext()).clearMapFactors();
		((GlobalData) getApplicationContext()).clearPreferences();
		getGlobalData().setPreferredReportSetting(null);

		String authHeader = ApiUtils.createBasicAuthenticationHeader(username, password);
		Observable<OAuthResponse> oauthResponse = grassrootsApiService.getOAuth("password", username, password, "MobileApp", "FhQX3R7ELRhCTXMq");

		oauthResponse.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(this::loginSuccess, error -> loginFail());


		new Thread(new Runnable() {
			@Override
			public void run() {

				/*
				 * This is extremely gross code. Make sure you recognize finally
				 * exists and will always be entered, even after return is
				 * called. Start by creating a login event, which sets the
				 * credentials for the rest of the requests. Load the petition;
				 * if there were error responses or we got a null petition, show
				 * an error message and end Load the walklist if there were
				 * error responses, show an error message and end OTHERWISE if
				 * walklist is null, no walklist - start subject info activity
				 * OTHERWISE if walklist is empty, show error message and remain
				 * at login OTHERWISE start walklist activity
				 */
				String errorMessage = null;
				boolean loadedPetition = false;
				boolean loadedWalklist = false;
				boolean loadedCalendar = false;

				try {
					client.setLoginInformation(username, password);
					String authHeader1 = ApiUtils.createBasicAuthenticationHeader(username, password);
					authenticationHeaderPreference.set(authHeader1);
					BugSenseHandler.addCrashExtraData("Username", username);
					Pair<Petition, Response> petitionResponse = client
							.syncLoadPetition();
					if (petitionResponse.second != null) {
						errorMessage = parseErrorMessage(petitionResponse.second);
						return; // this is gross... watch the finally
					} else {
						Petition petition = petitionResponse.first;
						if (petition == null) {
							errorMessage = getString(R.string.ERROR_LOADING_PETITION_MESSAGE);
							return; // this is gross... goes to finally
						}
						loadedPetition = true;
						setPetition(petition);
						getGlobalData().setLoggedInUserName(username);
						getGlobalData().fetchAndSaveReports();
						getGlobalData().checkForReportsScriptProperties();
					}
					if(petition.isLocationList()){
						Pair<LocationList, Response> locationListResponse = client
								.syncLoadLocationList();
						if (locationListResponse.second != null) {
							if (locationListResponse.second.getResponseErrors() != null)
								errorMessage = parseErrorMessage(locationListResponse.second);
							return; // this is gross... goes to finally
						} else {
							LocationList locationlist = locationListResponse.first;
							setLocationlist(locationlist);             
						}
					}else{
						if(petition.isWalklist()){
							Pair<Walklist, Response> walklistResponse = client.syncLoadWalklist();
							if (walklistResponse.second != null) {
								if (walklistResponse.second.getResponseErrors() != null)
									errorMessage = parseErrorMessage(walklistResponse.second);
								return; // this is gross... goes to finally
							} else {
								Walklist walklist = walklistResponse.first;
								walklistDataSource.clearWalklist();
								if(walklist != null)
									walklistDataSource.insertWalklist(walklist.getSubjects(),getGlobalData().getLocation());
								setWalklist(walklist);
								Date lastsyncedTime = new Date(Calendar.getInstance().getTimeInMillis());
								getGlobalData().setLastWalklistSyncTime(lastsyncedTime);
							}
						}
					}
					if (petition.isSupportReservation()) {
						Pair<AppointmentCalendar, Response> calendarResponse = client
								.syncLoadAppointmentWindows();
						if (calendarResponse.second != null) {
							if (calendarResponse.second.getResponseErrors() != null)
								errorMessage = parseErrorMessage(calendarResponse.second);
							return; // this is gross... goes to finally
						} else {
							AppointmentCalendar calendar = calendarResponse.first;
							loadedCalendar = (calendar != null);
							setAppointmentCalendar(calendar);
						}
					}					

				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.debug("LOGIN EXCEPTION: " + e.toString());
				} finally {
					final String finalErrorMessage = errorMessage;

					final boolean finalLoadedPetition = loadedPetition;
					final boolean finalLoadedWalklist = loadedWalklist;
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							hideActiveDialog();
							onFinishLoginLoading(finalErrorMessage,
									finalLoadedPetition, finalLoadedWalklist,
									username, password);
						}
					});
				}
			}
		}).start();

	}

	public void loginFail() {
		Log.d("AUTH", "Fail!"); oAuthTokenPreference.set("");
	}

	/**
	 * Called once loading of petition and walklist data has been completed.
	 * Handles starting the next activity
	 * 
	 * After login based on the isLocationList flag of petition,application will be redirected to waklist screen
	 * or locationlistscreen.
	 * 
	 * @param finalErrorMessage
	 * @param finalLoadedPetition
	 * @param finalLoadedWalklist
	 * @param username
	 * @param password
	 */
	protected void onFinishLoginLoading(String finalErrorMessage,
			boolean finalLoadedPetition, boolean finalLoadedWalklist,
			String username, String password) {
		BatteryPerformance batteryPerformance =  new BatteryPerformance(this);
		GpsLocation gpsLocation = LocationProcessor
				.getLastKnownGpsLocation(getApplicationContext());
		EventInfo eventInfo = getGlobalData().getEventInfo(gpsLocation,batteryPerformance);
		Petition petition = getPetition();
		if (finalErrorMessage != null) {
			notifyUser(finalErrorMessage);
		} else {
			if (!finalLoadedPetition || petition == null) {
				notifyUser(getString(R.string.TROUBLE_LOADING_PETITION_MESSAGE));
			} else {
				if (isPetitionValid(petition)) {

					try {
						client.login(
								username,
								password,eventInfo);
					} catch (Exception e) {
						e.printStackTrace();
						// setActivityResult(RESULT_CANCELED);
						String errorMessage = getResources().getString(
								R.string.unable_to_start_rps);
						LOGGER.error(errorMessage);
						notifyUser(errorMessage);
						return;
					}

					//grab marketing slides from incoming petition json if present
					JSONObject jsonObj = petition.getPetitionJSON();
					JSONArray jsonArr = new JSONArray();
					try {
						jsonArr = jsonObj.getJSONArray("ScriptProperties");

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//grab payment processing properties
					try {
						LOGGER.debug("JSON STUFF : " + jsonObj.getString("PaymentProcessorUser"));
						
						petition.setPaymentProcessorId(jsonObj.getInt("PaymentProcessorId"));
						petition.setPaymentProcessorMerchantId(jsonObj.getLong("PaymentProcessorMerchantId"));
						petition.setPaymentProcessorUser(jsonObj.getString("PaymentProcessorUser"));
						petition.setPaymentProcessorPassword(jsonObj.getString("PaymentProcessorPassword"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

						//by default let the report group take petition name
						petition.setReportGroup(petition.getName());
						try {
						for (int i = 0; i < jsonArr.length(); i++) {
							if (jsonArr.getJSONObject(i).getString("Name")
									.equals("VantivProperty_Campaign")) {
								JSONObject campaignObj = jsonArr.getJSONObject(i);
								Log.e("petition campaign", "campaign : "+ campaignObj.getString("Value"));
								petition.setCampaign(campaignObj.getString("Value"));
							}
							if (jsonArr.getJSONObject(i).getString("Name")
									.equals("VantivProperty_MerchantGroupingID")) {
								JSONObject merchantgroupIDObj = jsonArr.getJSONObject(i);
								Log.e("petition merchantgroupID", "merchantgroupID : "+ merchantgroupIDObj.getString("Value"));
								petition.setMerchantGroupingId(merchantgroupIDObj.getString("Value"));
							}
							if (jsonArr.getJSONObject(i).getString("Name")
									.equals("VantivProperty_reportGroup")) {
								JSONObject reportGroupIDObj = jsonArr.getJSONObject(i);
								Log.e("petition reportGroup", "reportGroup : "+ reportGroupIDObj.getString("Value"));
								petition.setReportGroup(reportGroupIDObj.getString("Value"));
							}
						}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						
					
					new DownloadImage().execute(jsonArr);

					for(int i = 0; i< jsonArr.length() ; i++){
						try {
							if(jsonArr.getJSONObject(i).getString("Name").equals("NearestNeighbor")){
								petition.setNearestNeighbor(true);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					/*MidnightPromptService.startManager(getApplicationContext());
					setActivityResult(RESULT_OK);
					startFirstActivity();*/

				} else {
					notifyUser(getString(R.string.RECEIVED_INVALID_PETITION_DATA_MESSAGE));
				}
			}
		}
	}

	protected boolean isPetitionValid(Petition petition) {
		return true;
	}

	private String parseErrorMessage(Response errors) {
		if (errors == null) {
			return getString(R.string.login_failed);
		}
		List<ResponseError> responseErrors = errors.getResponseErrors();
		String errorMessage;
		if (responseErrors == null || responseErrors.isEmpty()) {
			errorMessage = getString(R.string.login_failed);
		} else {
			// We're only going to show one of these errors
			ResponseError error = responseErrors.get(0);
			if (error == null) {
				errorMessage = getString(R.string.login_failed);
			} else {
				LOGGER.error("Error message text: " + error.getMessage());
				if (error.isImeiError()) {
					errorMessage = getString(R.string.invalid_imei);
				} else if (error.isAuthenticationError()) {
					errorMessage = getString(R.string.invalid_credentials);
				} else {
					errorMessage = getString(R.string.login_failed);
				}
			}
		}
		return errorMessage;
	}

	@Inject @ImeiHeaderPreference
	StringPreference imeiPref;
	@Inject @AuthenticationHeaderPreference StringPreference authenticationHeaderPreference;
	@Inject @OAuthTokenPreference
	StringPreference oAuthTokenPreference;
	@Inject
	GrassrootsApiService grassrootsApiService;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((GlobalData) this.getApplication()).getApplicationComponent().inject(this);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


		GrassrootsRestClient.getClient().setImeiHeader(this);
		walklistDataSource = new WalklistDataSource(BaseLoginActivity.this);
		LocationProcessor.startLocationPolling(this);

		Intent startRequestService = new Intent();
		startRequestService.setAction(RequestPerformerService.ACTION);
		sendBroadcast(startRequestService);

		boolean wasCrashed = getIntent().getBooleanExtra(
				GoToActivityExceptionHandler.KEY, false);
		if (wasCrashed) {
			LOGGER.error("Recovered from a crash");
			notifyUser(getString(R.string.APPLICATION_SUFFER_UNRECOVERABLE_ERROR_MESSAGE));
		}
		getGlobalData().clearAllPetition();
		mProgressDialog = new ProgressDialog(BaseLoginActivity.this);
		mProgressDialog.setMessage("A message");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setCancelable(true);


		client = GrassrootsRestClient.getClient();
		view = (LoginView) View.inflate(this, getLayout(), null);
		view.setViewListener(viewListener);
		setContentView(view);

		view.setIsServerAvailable(false);
		registerReceiver(mBatInfoReceiver, 
				new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		TelephonyManager mTelManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		mTelManager.listen(cellstateInfoReceiver,
				PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

		imeiPref.set(AutoApkUpdater.getImei(this));

		initializeUpdater();
		checkAppUpdates(AutoApkUpdater.APP_NAME);

	}

	/*
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		boolean wasCrashed = intent.getBooleanExtra(
				GoToActivityExceptionHandler.KEY, false);
		if (wasCrashed) {
			LOGGER.error("Recovered from a crash");
			notifyUser(getString(R.string.APPLICATION_SUFFER_UNRECOVERABLE_ERROR_MESSAGE));
		}
	}
	*/

	@Override
	public void onReload() {
		super.onReload();
		getGlobalData().clearAllPetition();
		view.clearView();
	}

	@Override
	public void onResume() {
		super.onResume();
		GlobalData.clearMarketingSlides();
		GlobalData.clearCookies(this.getApplicationContext());

		try {
			this.getApplicationContext().deleteDatabase("webview.db");
			this.getApplicationContext().deleteDatabase("webviewCache.db");
		} catch(Exception ex) {
			Log.d("DB ERROR", "Error while trying to delete webview db cache");
			ex.printStackTrace();
		}



		startPingServer();
		installDB();


		if(isScreenOn)
		{
			String versionName = "";

			try {
				versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int stringId = getApplicationInfo().labelRes;
			String appName = getString(stringId);

			Tracker.newSession = true;
			Tracker.appendLog("Start of New Session - App Name: " + appName + ", Version: " + versionName);

			/*
			final   ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
			final List<RunningTaskInfo> recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

			    for (int i = 0; i < recentTasks.size(); i++) 
			    {
			        LOGGER.debug("Executed app: " + recentTasks.get(i).baseActivity.toShortString());         
			    }
			 */

		}
		else
		{
			screenOn = true;
		}
	}

	boolean isScreenOn = true;
	@Override
	public void onPause() {
		super.onPause();
		stopPingServer();

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		isScreenOn = pm.isScreenOn();
	}

	@Override
	public void onDestroy() {
		LocationProcessor.stopLocationPolling(this);
		unregisterReceiver(mBatInfoReceiver);
		super.onDestroy();
	}

	protected int getLayout() {
		return R.layout.login;
	}



	private void installDB()
	{
		synchronized (lock){
			if(upgraded)
				return;

			upgraded = true;
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				DatabaseInstaller installer = new DatabaseInstaller(getApplicationContext());
				try{
					installer.doInstall();
				}catch(DatabaseException e){
					LOGGER.error("Unable to upgrade database: " + e.getMessage(), e);
					BugSenseHandler.sendExceptionMessage("Unable to upgrade database", null, e);
					activity.logoutWithError("Unable to upgrade database", e);
				}
			}
		}).start();
	}

	public void startPingServer() {
		// Return if it's already started
		Log.d("Pinging server","Pinging server");
		if (!isPingActive.compareAndSet(false, true))
			return;

		view.setIsServerAvailable(false);

		if (isDebug) {
			view.setIsServerAvailable(true);
			return;
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				boolean isServerUp = client.syncPing();
				while (!isServerUp && isPingActive.get()) {
					try {
						Thread.sleep(5 * 1000);
					} catch (InterruptedException e) {
					}
					LOGGER.debug("Pinging server...");
					isServerUp = client.syncPing();
				}

				isPingActive.set(false);

				final boolean isServerAvailable = isServerUp;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						view.setIsServerAvailable(isServerAvailable);
					}
				});
			}
		}).start();
	}

	private void stopPingServer() {
		isPingActive.set(false);
	}



	/*
	 * This section is for saving and deletion of marketing materials and logo packs
	 */
	ProgressDialog mProgressDialog;
	// DownloadImage AsyncTask
	private class DownloadImage extends AsyncTask<JSONArray, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(BaseLoginActivity.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Download Image Tutorial");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			//mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(JSONArray... URLS) {

			ArrayList<String> marketingURLs = new ArrayList<String>();
			ArrayList<String> marketingImgNames = new ArrayList<String>();
			String marketingFilename;

			ArrayList<String> logoURLs = new ArrayList<String>();
			ArrayList<String> logoImgNames = new ArrayList<String>();
			String logoFilename;

			
			Bitmap bitmap = null;
			for(int i = 0; i < URLS[0].length(); i++)
			{
				try {
					LOGGER.debug("JSON ARRAY: " + URLS[0].getJSONObject(i).getString("Value"));
					// Execute DownloadImage AsyncTask
					//new DownloadImage().execute(jsonArr.getJSONObject(i).getString("Value"));

					if(URLS[0].getJSONObject(i).getString("Name").equals("marketing"))
					{
						String imageURL = URLS[0].getJSONObject(i).getString("Value");

						marketingURLs.add(imageURL);

						marketingFilename = imageURL.substring(imageURL.lastIndexOf('/') + 1);

						marketingImgNames.add(marketingFilename);
					}
					else if(URLS[0].getJSONObject(i).getString("Name").equals("logos"))
					{
						String imageURL = URLS[0].getJSONObject(i).getString("Value");

						logoURLs.add(imageURL);

						logoFilename = imageURL.substring(imageURL.lastIndexOf('/') + 1);

						logoImgNames.add(logoFilename);
					}

					//saveToInternalSorage(imageURL, filename);
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}

			deleteFromInternalStorage(marketingImgNames);
			saveToInternalSorage(marketingURLs, marketingImgNames, logoURLs, logoImgNames);
			createShortcut();

			return null;
		}

		@Override
		protected void onPostExecute(Void v) {
			// Set the bitmap into ImageView

			ImageView image = (ImageView) findViewById(R.id.imageView);
			//image.setImageBitmap(result);

			setActivityResult(RESULT_OK);
			startFirstActivity();

			// Close progressdialog
			mProgressDialog.dismiss();

		}
	}

	//delete marketing file is does not exist in JSON
	private void deleteFromInternalStorage(ArrayList<String> names)
	{
		File fDir=new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
		String[] files = fDir.list();

		if(files != null)
		{
			for(String file : files)
			{
				if(!names.contains(file))
				{
					File f=new File(fDir, file);
					f.delete();
				}
			}
		}
		//fDir.delete();
	}

	private void saveToInternalSorage(ArrayList<String> marketingImgURLs, ArrayList<String> marketingFilenames,
			ArrayList<String> logoImgURLs, ArrayList<String> logoFilenames){
		//ContextWrapper cw = new ContextWrapper(getContext());
		// path to /data/data/yourapp/app_data/imageDir
		//File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

		//File directory = new File(GlobalData.TMP_DIRECTORY, "slides");

		File logoImgDir = new File(GlobalData.TMP_DIRECTORY + File.separator + "logos");
		logoImgDir.mkdirs();

		File marketingImgDir = new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
		marketingImgDir.mkdirs();

		for (int i = 0; i < marketingImgURLs.size(); i++)
		{
			// Create imageDir
			File mypath=new File(marketingImgDir,marketingFilenames.get(i));

			if(!mypath.exists())
			{
				FileOutputStream fos = null;
				try {           

					// Download Image from URL
					InputStream input = new java.net.URL(marketingImgURLs.get(i)).openStream();
					// Decode Bitmap
					Bitmap bitmapImage = BitmapFactory.decodeStream(input);   

					fos = new FileOutputStream(mypath);
					// Use the compress method on the BitMap object to write image to the OutputStream
					bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		for (int i = 0; i < logoImgURLs.size(); i++)
		{
			// Create imageDir
			File mypath=new File(logoImgDir, logoFilenames.get(i));

			if(!mypath.exists())
			{
				FileOutputStream fos = null;
				try {           

					// Download Image from URL
					InputStream input = new java.net.URL(logoImgURLs.get(i)).openStream();
					// Decode Bitmap
					Bitmap bitmapImage = BitmapFactory.decodeStream(input);   

					fos = new FileOutputStream(mypath);
					// Use the compress method on the BitMap object to write image to the OutputStream
					bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void createShortcut()
	{
		//Adding shortcut for MainActivity 
		//on Home screen

		File logo = new File(GlobalData.TMP_DIRECTORY + File.separator + "logos" + File.separator + "launcher.png");

		if(logo.exists())
		{
			SharedPreferences shortcutPrefs = getSharedPreferences("custom_shortcut_created", 0);
			SharedPreferences.Editor shortcutEditor = shortcutPrefs.edit();
			
			SharedPreferences installPrefs = getSharedPreferences("last_install", 0);
			SharedPreferences.Editor installEditor = installPrefs.edit();

			if (!(shortcutPrefs.getBoolean("custom_shortcut_created", false))) 
			{
				Intent addshortcutIntent = new Intent(getApplicationContext(),
						StartupActivity.class);

				addshortcutIntent.setAction(Intent.ACTION_MAIN);

				Intent removeIntent = new Intent();
				removeIntent
				.putExtra(Intent.EXTRA_SHORTCUT_INTENT, addshortcutIntent);
				removeIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
				/*addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
					Intent.ShortcutIconResource.fromContext(getApplicationContext(),
							R.drawable.launcher));*/
				removeIntent.putExtra("duplicate",false);
				removeIntent
				.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
				getApplicationContext().sendBroadcast(removeIntent);



				Intent shortcutIntent = new Intent(getApplicationContext(),
						StartupActivity.class);

				shortcutIntent.setAction(Intent.ACTION_MAIN);

				Intent addIntent = new Intent();
				addIntent
				.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
				addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));

				Bitmap bmp = BitmapFactory.decodeFile(logo.getPath());
				addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bmp);
				addIntent.putExtra("duplicate",false);
				addIntent
				.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
				getApplicationContext().sendBroadcast(addIntent);

				shortcutEditor.putBoolean("custom_shortcut_created", true);
				shortcutEditor.commit();
			}
		}
	}
	private AutoApkUpdater updater;
	private String updateType = AutoApkUpdater.APP_NAME;
	//ProgressDialog mProgressDialog;
	private void initializeUpdater() {
		updater = new AutoApkUpdater(this, this);
	}

	/*
	 * Check for app update for specified app.
	 */
	public void checkAppUpdates(String appName) {
		updateType = appName;
		checkAppUpdates();
	}

	/*
	 * Check if an updated apk is available for download.
	 */
	private void checkAppUpdates() {
		if(haveNetworkConnection()) {
			showProgress("Checking for " + updateType + " update...");
			updater.checkUpdate(updateType);
		} else {
			onUpdateCheckFailed("No Network Connection",
					"Cannot check for updates, no network connection.",
					"");
		}
	}

	/*
	 * Download updated apk.
	 */
	private void downloadUpdates() {
		if(haveNetworkConnection()) {
			mProgressDialog.setMessage("Downloading update for " + updateType + ". Please wait.");
			mProgressDialog.show();
			updater.downloadUpdate(updateType);
		} else {
			mProgressDialog.dismiss();
			onDownloadFailed("Download Error",
					"Cannot download update, no network connection.",
					updateType);
		}
	}

	public void onDownloadProgress(int progress) {
		Log.v("BaseLoginActivity", "setting download update progress to " + progress);
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setMax(100);
		mProgressDialog.setProgress(progress);
	}

	/*
	 * Once updated apk is done downloading, this pops up a system dialog asking user to install
	 * the update
	 */
	public void onDownloadComplete(String path, String appName) {
		Log.d(TAG, "On download complete called for " + appName + ", sending intent to install.");
		mProgressDialog.dismiss();
		hideActiveDialog();
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + path), AutoApkUpdater.ANDROID_PACKAGE);
		intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	/*
	 * If updated apk is found, show dialog to user prompting them to install. If no update is found
	 * hide the checking for updates dialog.
	 */
	public void onUpdateCheckCompleted(boolean updateAvailable, String updateVersion, String updateUrl, String appName) {
		//Always install available update. If GRUAPP does not have an available update, switch over
		//to check for an RPS update.
		if (updateAvailable) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Update Available for " + appName)
					.setMessage("Version " + updateVersion + " is available, would you like to "
							+ "download?")
					.setPositiveButton("Yes", acceptUpdateDialogClickListener)
					.setNegativeButton("No", acceptUpdateDialogClickListener)
					.show();
		} else if (!updateAvailable && appName.equals(AutoApkUpdater.APP_NAME)) {
			checkAppUpdates(AutoApkUpdater.RPS);
		}  else {
			hideActiveDialog();
		}
	}

	/*
	 * Show dialog when update apk download fails. Asks user to retry or skip download.
	 */
	public void onDownloadFailed(String title, String message, String appName) {
		hideActiveDialog();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title + appName)
				.setMessage(message)
				.setPositiveButton("Retry", retryUpdateDownloadDialogClickListener)
				.setNegativeButton("Skip Update", retryUpdateDownloadDialogClickListener)
				.show();
	}

	/*
	 * Show dialog when update apk update check fails. Asks user to retry or skip download.
	 */
	public void onUpdateCheckFailed(String title, String message, String appName) {
		hideActiveDialog();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title + appName)
				.setMessage(message)
				.setPositiveButton("Retry", retryUpdateCheckDialogClickListener)
				.setNegativeButton("Skip Check", retryUpdateCheckDialogClickListener)
				.show();
	}

	/*
     * Downloads update on Yes. If they say no to GRUAPP update, ask if they want to update RPS.
	 */
	DialogInterface.OnClickListener acceptUpdateDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					downloadUpdates();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					hideActiveDialog();
					if(updateType.equals(AutoApkUpdater.APP_NAME)) {
						checkAppUpdates(AutoApkUpdater.RPS);
					}
					break;
			}
		}
	};

	/*
     * Retry or Skip the Update Check.
	 */
	DialogInterface.OnClickListener retryUpdateCheckDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					checkAppUpdates();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					hideActiveDialog();
					break;
			}
		}
	};

	/*
     * Retry or Skip the Update Download.
	 */
	DialogInterface.OnClickListener retryUpdateDownloadDialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					downloadUpdates();
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					hideActiveDialog();
					break;
			}
		}
	};

	private boolean haveNetworkConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
