package com.grassroots.petition.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import com.grassroots.petition.R;
import com.grassroots.petition.models.*;
import com.grassroots.petition.utils.Strings;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BaseDonationActivity  extends BasePetitionActivityWithMenu {
	private static final String TAG = BaseSubjectInfoActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private TextView amountAndRecurringTextView;
	private EditText amountEditText;
	protected RadioGroup recurringRadioGroup;
	protected RadioGroup methodRadioGroup;
	private RadioGroup paymentAmountRadioGroup;
	private HashMap<String, ArrayList<Float>> recurringCostMap = new HashMap<String, ArrayList<Float>>();

	// Recurring
	public static final String RECURRING_NO = "ONE-TIME";
	public static final String RECURRING_WEEKLY = "RECURRING_WEEKLY";
	public static final String RECURRING_MONTHLY = "MONTHLY";
	protected final static HashMap<Integer, String> recurringIdsToTypesMap = new HashMap<Integer, String>();
	static {
		// don't translate - standard donation products name
		recurringIdsToTypesMap.put(R.id.radiobutton_payment_recurring_single, RECURRING_NO );
		recurringIdsToTypesMap.put(R.id.radiobutton_payment_recurring_weekly, RECURRING_WEEKLY );
		recurringIdsToTypesMap.put(R.id.radiobutton_payment_recurring_monthly, RECURRING_MONTHLY );
	}

	protected float donationAmount;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.donation_activity);

		TextView donationTitleTextView = (TextView) findViewById(R.id.donation_title);
		int donationTitleId = getResources().getIdentifier("donation_title", "string", getPackageName());
		String donationTitleString = null;
		if (donationTitleId > 0) {
			donationTitleString = getResources().getString(donationTitleId);
			if (null != donationTitleString && Strings.isNotEmpty(donationTitleString)) {
				donationTitleTextView.setVisibility(View.VISIBLE);
				donationTitleTextView.setText(donationTitleString);
			}
		}
		if (null == donationTitleString) {
			LOGGER.warn("donation_title was not found");
			donationTitleTextView.setVisibility(View.GONE);
		}

		TextView donationDescriptionTextView = (TextView) findViewById(R.id.donation_description_title);
		int donationDescriptionId = getResources().getIdentifier("donation_description", "string", getPackageName());
		String donationDescriptionString = null;
		if (donationDescriptionId > 0) {
			donationDescriptionString = getResources().getString(donationDescriptionId);
			if (null != donationDescriptionString && Strings.isNotEmpty(donationDescriptionString)) {
				donationDescriptionTextView.setVisibility(View.VISIBLE);
				donationDescriptionTextView.setText(donationDescriptionString);
			}
		}
		if (null == donationDescriptionString) {
			LOGGER.warn("donation_description was not found");
			donationDescriptionTextView.setVisibility(View.GONE);
		}

		ImageView donationLogoImageView = (ImageView) findViewById(R.id.image_view_donation_logo);
		int donationLogoId = getResources().getIdentifier("donation_logo", "drawable", getPackageName());
		Drawable donationLogoDrawable = null;
		if (donationLogoId > 0) {
			donationLogoDrawable = getResources().getDrawable(donationLogoId);
			if (null != donationLogoDrawable) {
				donationLogoImageView.setVisibility(View.VISIBLE);
				donationLogoImageView.setImageDrawable(donationLogoDrawable);
			}
		}
		if (null == donationLogoDrawable) {
			LOGGER.warn("donation_logo was not found");
			donationLogoImageView.setVisibility(View.GONE);
		}
		
		List<Product> productsList = petition.getProducts();
		recurringRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_recurring);
		populateDonationMap();
		
		/*for (Map.Entry entry : recurringIdsToTypesMap.entrySet()) {
			Integer radioButtonId = (Integer)entry.getKey();
			String recurringTypeString = (String)entry.getValue();
			View v = recurringRadioGroup.findViewById(radioButtonId);

			boolean isRecurringFound = false;
			for (Product product : productsList) {
				if (product.getRecurring().equalsIgnoreCase(recurringTypeString)) {
					isRecurringFound = true;
					break;
				}
			}
			v.setVisibility(isRecurringFound ? View.VISIBLE : View.GONE);
			v.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					//updateAmountAndRecurring();
				}
			});
		}*/
		recurringRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
		{
			public void onCheckedChanged(RadioGroup rGroup, int checkedId)
			{
				updateAmountAndRecurring();
				configureDonationDisplay(checkedId);
			}
		});		

		amountEditText = (EditText) findViewById(R.id.edit_text_payment_amount);
		amountEditText.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				updateAmountAndRecurring();
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});

		amountAndRecurringTextView = (TextView) findViewById(R.id.amount_and_recurring_title);

		methodRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_method);

		donationAmount = 0;

		paymentAmountRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_payment_amount);
		paymentAmountRadioGroup.removeAllViews();
		int[] suggestedDonationsInts = getResources().getIntArray(R.array.suggested_donations);
		if (null == suggestedDonationsInts) {
			LOGGER.error("Unable to load suggested_donations array from resources");
		}

		ArrayList<Integer> suggestedDonationsIntegersArray = new ArrayList<Integer>();
		for (int donationValInt : suggestedDonationsInts) {
			suggestedDonationsIntegersArray.add(donationValInt);
		}
		suggestedDonationsIntegersArray.add(0); // Other

		/*
		 * display payment amounts from resources
		 */
		for (Integer donationValInteger : suggestedDonationsIntegersArray) {
			RadioButton paymentRadioButton = (RadioButton) getLayoutInflater().inflate(R.layout.donation_radio_button, null);

			int leftPadding = paymentRadioButton.getPaddingLeft();
			int rightPadding = paymentRadioButton.getPaddingRight();
			int topPadding = paymentRadioButton.getPaddingTop();
			int bottomPadding = paymentRadioButton.getPaddingBottom();

			if (donationValInteger > 0) {
				paymentRadioButton.setText(getString(R.string.DONATION_DEFAULT_CURRENCY_SIGN) + String.valueOf(donationValInteger));
			} else {
				paymentRadioButton.setText(getString(R.string.DONATION_PAYMENT_AMOUNT_OTHER));
				rightPadding = 10;
			}

			paymentRadioButton.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);

			paymentRadioButton.setTag(donationValInteger);
			paymentRadioButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					updateAmountAndRecurring();
				}
			});
			paymentAmountRadioGroup.addView(paymentRadioButton);
		}


		Button nextButton = (Button) findViewById(R.id.next_button);
		if (null != nextButton) {
			nextButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					BaseDonationActivity.this.onNext();
				}
			});
		}

		Button endInterviewButton = (Button) findViewById(R.id.end_interview_button);
		if (null != endInterviewButton) {
			endInterviewButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					BaseDonationActivity.this.onEndInterview();
				}
			});
		}

		Button skipButton = (Button) findViewById(R.id.skip_button);
		if (null != skipButton) {
			skipButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					BaseDonationActivity.this.onSkip();
				}
			});
		}

		Button aboutButton = (Button) findViewById(R.id.about_card_safety_button);
		if (null != aboutButton) {
			aboutButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					BaseDonationActivity.this.onAbout();
				}
			});
		}

		Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
		if (null != marketingButton) {
			marketingButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					BaseDonationActivity.this.onMarketing();
				}
			});
		}



		RadioButton firstRecurringButton = (RadioButton) recurringRadioGroup.getChildAt(0);
		firstRecurringButton.setChecked(true);
		firstRecurringButton.performClick();
		updateAmountAndRecurring();
		
		hideMarketingIfHasNoMaterials();
	}



	private void populateDonationMap()
	{
		recurringRadioGroup.removeAllViews();
		
		List<Product> productList = petition.getProducts();
		for (Product product : productList)
		{
			String cost = product.getCost();
			
			if(!recurringCostMap.containsKey(product.getRecurring()))	//new recurring
			{	
				ArrayList<Float> costList = new ArrayList<Float>();
					
				if(cost == null || cost.equals("null"))
				{
					cost = "0";
				}
				
				costList.add(Float.parseFloat(cost));

				recurringCostMap.put(product.getRecurring(), costList);
				
				/*
				 * add recurring to donation screen
				 */
				RadioButton recurringRadioButton = 
						(RadioButton) getLayoutInflater().inflate(R.layout.donation_radio_button, null);
				recurringRadioButton.setText(product.getRecurring());
				recurringRadioButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						configureDonationDisplay(v.getId());
						updateAmountAndRecurring();
					}
				});
				
				recurringRadioGroup.addView(recurringRadioButton);
				recurringIdsToTypesMap.put(recurringRadioButton.getId(), product.getRecurring());
			}
			else
			{
				if(cost == null || cost.equals("null"))
				{
					cost = "0";
				}
				
				recurringCostMap.get(product.getRecurring()).add(Float.parseFloat(cost));				
				Collections.sort(recurringCostMap.get(product.getRecurring()));
			}
			
			
		}
	}



	private void configureDonationDisplay(int checkedId)
	{
		boolean hasOther = false;
		LOGGER.debug("PRODUCT INFO: " + petition.getProducts().get(0).toJson().toString());

		paymentAmountRadioGroup.clearCheck();
		paymentAmountRadioGroup.removeAllViews();
		donationAmount = 0;
		
		for (Float cost : recurringCostMap.get(recurringIdsToTypesMap.get( checkedId )))
		{
			RadioButton paymentRadioButton = 
					(RadioButton) getLayoutInflater().inflate(R.layout.donation_radio_button, null);

			int leftPadding = paymentRadioButton.getPaddingLeft();
			int rightPadding = paymentRadioButton.getPaddingRight();
			int topPadding = paymentRadioButton.getPaddingTop();
			int bottomPadding = paymentRadioButton.getPaddingBottom();
			
			if (cost > 0) {
				paymentRadioButton.setText(getString(R.string.DONATION_DEFAULT_CURRENCY_SIGN) + String.format( "%.2f", cost));
			} else {
				hasOther = true;
				cost = (float) 0;
				paymentRadioButton.setText(getString(R.string.DONATION_PAYMENT_AMOUNT_OTHER));
				rightPadding = 10;
				continue;
			}

			paymentRadioButton.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);

			paymentRadioButton.setTag(cost);
			paymentRadioButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					updateAmountAndRecurring();
				}
			});
			paymentAmountRadioGroup.addView(paymentRadioButton);
		}

		/*for (Product product : products)
		{
			if(product.getRecurring().equals(recurringIdsToTypesMap.get( checkedId )))
			{

				RadioButton paymentRadioButton = 
						(RadioButton) getLayoutInflater().inflate(R.layout.donation_radio_button, null);

				int leftPadding = paymentRadioButton.getPaddingLeft();
				int rightPadding = paymentRadioButton.getPaddingRight();
				int topPadding = paymentRadioButton.getPaddingTop();
				int bottomPadding = paymentRadioButton.getPaddingBottom();

				String cost = product.getCost();

				if (cost != null 
						&& !cost.equals("null") 
						&& Float.parseFloat(cost) > 0) {
					paymentRadioButton.setText(getString(R.string.DONATION_DEFAULT_CURRENCY_SIGN) + cost);
				} else {
					hasOther = true;
					cost = "0";
					paymentRadioButton.setText(getString(R.string.DONATION_PAYMENT_AMOUNT_OTHER));
					rightPadding = 10;
					continue;
				}

				paymentRadioButton.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);

				paymentRadioButton.setTag((int)Double.parseDouble(cost));
				paymentRadioButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						updateAmountAndRecurring();
					}
				});
				paymentAmountRadioGroup.addView(paymentRadioButton);
			}
		}*/
		if(hasOther)		//put other field at end
		{
			RadioButton paymentRadioButton = (RadioButton) getLayoutInflater().inflate(R.layout.donation_radio_button, null);

			int leftPadding = paymentRadioButton.getPaddingLeft();
			int rightPadding = paymentRadioButton.getPaddingRight();
			int topPadding = paymentRadioButton.getPaddingTop();
			int bottomPadding = paymentRadioButton.getPaddingBottom();

			paymentRadioButton.setText(getString(R.string.DONATION_PAYMENT_AMOUNT_OTHER));
			rightPadding = 10;

			paymentRadioButton.setPadding(leftPadding, topPadding, rightPadding, bottomPadding);

			paymentRadioButton.setTag((float)0);
			paymentRadioButton.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					updateAmountAndRecurring();
				}
			});
			paymentAmountRadioGroup.addView(paymentRadioButton);
			amountEditText.setVisibility(View.VISIBLE);
		}
		else
		{
			amountEditText.setVisibility(View.GONE);
		}
		updateAmountAndRecurring();
	}



	private void updateAmountAndRecurring() {
		// #1 get checked amount
		String donationAmountString = getString(R.string.DONATION_SELECT_AMOUNT_MESSAGE);
		int checkedAmountElementId = paymentAmountRadioGroup.getCheckedRadioButtonId();
		if (checkedAmountElementId > 0) {
			RadioButton checkedPaymentAmountRadioButton = (RadioButton)paymentAmountRadioGroup.findViewById(checkedAmountElementId);
			donationAmount = (Float)checkedPaymentAmountRadioButton.getTag();
			LOGGER.debug("RADIOBUTTON: " + String.valueOf(checkedPaymentAmountRadioButton.getTag()));
			//donationAmount = 0;

			donationAmountString = getString(R.string.DONATION_ENTER_AMOUNT_MESSAGE);
			if (0 == donationAmount) {
				amountEditText.setEnabled(true);

				try {
					donationAmount = Float.parseFloat(amountEditText.getText().toString());
				} catch(NumberFormatException nfe) {
					donationAmount = 0;
				}
			} else {
				amountEditText.setEnabled(false);
			}

			if (donationAmount > 0) {
				//donationAmountString = getString(R.string.DONATION_DEFAULT_CURRENCY_SIGN) + Float.toString(donationAmount);
				donationAmountString = getString(R.string.DONATION_DEFAULT_CURRENCY_SIGN) + String.format( "%.2f", donationAmount );
			}
		} else {
			amountEditText.setEnabled(false);
		}

		// #2 get checked recurring
		String donationRecurringString = getString(R.string.DONATION_SELECT_RECURRING_MESSAGE);
		int recurringButtonId = recurringRadioGroup.getCheckedRadioButtonId();
		RadioButton recurringButton = (RadioButton)recurringRadioGroup.findViewById(recurringButtonId);
		if (null != recurringButton) {
			donationRecurringString = recurringButton.getText().toString();
		}

		amountAndRecurringTextView.setText( donationAmountString + "             " + donationRecurringString );
	}

	@Override
	public void onReload(){
		super.onReload();

		amountEditText.setText("");
		recurringRadioGroup.clearCheck();
		methodRadioGroup.clearCheck();
		paymentAmountRadioGroup.clearCheck();

		updateAmountAndRecurring();

		hideMarketingIfHasNoMaterials();
	}

	/**
	 * Handles "next" click and verifies proper information submitted before moving on.
	 * If the person doesn't want to make a donation, they can skip instead.
	 */
	protected void onNext ()
	{
		// Validate subject information, allow anonymous donations (no subject information)
		if( isDigitalPaymentType() && missingSubjectInformation() )
		{
			notifyUser( getString( R.string.INVALID_SUBJECT_INFO_NOTIFICATION ) );
			return;
		}

		// Validate mailing information
		/* if( isDigitalPaymentType() && missingMailingInformation() )
        {
            notifyUser( getString( R.string.INVALID_MAILING_INFO_NOTIFICATION ) );
            return;
        }*/

		// Validate donation amount
		if( 0 == donationAmount )
		{
			notifyUser( getString( R.string.DONATION_ENTER_AMOUNT_MESSAGE ) );
			return;
		}

		// Validate donation frequency
		int recurringButtonId = recurringRadioGroup.getCheckedRadioButtonId();
		String recurringTypeString = recurringIdsToTypesMap.get( recurringButtonId );
		getCart().setRecurring(recurringTypeString);
		if( missingFrequency( recurringButtonId, recurringTypeString ) )
		{
			notifyUser( getString( R.string.DONATION_SELECT_RECURRING_MESSAGE ) );
			return;
		}

		// Handle payment
		handlePayment( recurringTypeString );
	}

	/**
	 * Ensures subject information is set.
	 *
	 * @return "true" when subject information is missing and "false" when we have the information
	 */
	protected boolean missingSubjectInformation ()
	{
		Subject subject = getSubject();
		return subject == null || !subject.isValid();
	}

	/**
	 * Determines whether the notification should be shown for missing mailing information.
	 *
	 * @return "true" when notification should occur and "false" when it shouldn't
	 */
	protected boolean isAnalogPaymentType ()
	{
		int selectedPaymentType = methodRadioGroup.getCheckedRadioButtonId();
		if (selectedPaymentType == R.id.radiobutton_payment_method_cash ||
				selectedPaymentType == R.id.radiobutton_payment_method_paper_check )
		{
			return true;
		}

		return false;
	}

	/**
	 * Determines whether the notification should be shown for missing mailing information.
	 *
	 * @return "true" when notification should occur and "false" when it shouldn't
	 */
	protected boolean isDigitalPaymentType ()
	{
		return !isAnalogPaymentType();
	}

	/**
	 * Ensures mailing information is set.
	 *
	 * @return "true" when mailing information is missing and "false" when we have the information
	 */
	private boolean missingMailingInformation ()
	{
		for( String questionName : Question.SHIPPING_INFORMATION_NAMES )
		{
			// Ignore "APARTMENT" field
			if( questionName.equals( Question.ADDRESS_2 + Question.SHIPPING_POSTSCRIPT ) )
			{
				continue;
			}

			if( !questionHasBeenAnswered( questionName ) )
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Validates the answer to the question
	 *
	 * @param questionName name of the question
	 *
	 * @return "true" when a question is answered and "false" when it is not
	 */
	protected boolean questionHasBeenAnswered (String questionName)
	{
		Question question = petition.getQuestionWithVarName( questionName );

		if( null == question )
		{
			LOGGER.warn( "No question in petition with varname: " + questionName );
			return false;
		}

		SubjectAnswer answer = petitionAnswers.getAnswerTo( question );
		return !( answer == null || !answer.hasAnswer() );
	}

	/**
	 * Ensures the frequency is correctly set in the view.
	 *
	 * @param recurringButtonId radio button id of the selected frequency
	 * @param recurringTypeString string representing the selected frequency
	 *
	 * @return "true" when a frequency isn't selected and "false" when a frequency is selected
	 */
	protected boolean missingFrequency (int recurringButtonId, String recurringTypeString)
	{
		if( recurringButtonId < 0 )
		{
			return true;
		}
		else if( null == recurringTypeString )
		{
			LOGGER.error( "No corresponded donation product was found for recurring type " + recurringTypeString );
			return true;
		}

		return false;
	}

	/**
	 * Handles the payment, cart creation, and starting the next activity.
	 * Notifies user when a payment type is not selected.
	 *
	 * @param recurringTypeString string representing the selected frequency, used in cart creation
	 */
	protected void handlePayment (String recurringTypeString)
	{
		// Ensure analog payments have "one time" recurring set
		if( isAnalogPaymentType() && !recurringTypeString.equals( RECURRING_NO ) )
		{
			notifyUser( getString( R.string.DONATION_RECURRING_WITH_ANALOG_PAYMENT ) );
			return;
		}

		int selectedPaymentMethod = methodRadioGroup.getCheckedRadioButtonId();
		// Electronic check
		if( selectedPaymentMethod == R.id.radiobutton_payment_method_checking )
		{
			saveProductToCart( recurringTypeString, false, false );
			startActivity( BaseACHDataActivity.class );
		}
		// Paper check
		else if( selectedPaymentMethod == R.id.radiobutton_payment_method_paper_check )
		{
			saveProductToCart( recurringTypeString, true, false );
			startActivity( getNextActivity() );
		}
		// Credit card
		else if( selectedPaymentMethod == R.id.radiobutton_payment_method_card )
		{
			saveProductToCart( recurringTypeString, false, false );
			startActivity( BaseCreditCardSwipeActivity.class );
		}
		// Cash
		else if( selectedPaymentMethod  == R.id.radiobutton_payment_method_cash )
		{
			saveProductToCart( recurringTypeString, false, true );
			startActivity( getNextActivity() );
		}
		// None selected, notify user
		else
		{
			notifyUser( getString( R.string.DONATION_SELECT_PAYMENT_METHOD_MESSAGE ) );
		}
	}

	/**
	 * Saves the selected product and payment type in the petition answers.
	 *
	 * @param recurringTypeString string representing the selected frequency
	 * @param isCheck - "true" represents a paper check payment
	 * @param isCash - "true" represents a cash payment
	 *
	 * TODO: Should this be saved to the cart before it's paid for? This should probably only happen with
	 *       analog payment types.
	 */
	protected void saveProductToCart (String recurringTypeString, boolean isCheck, boolean isCash)
	{
		LOGGER.debug("PRODUCT INFO: " + recurringTypeString);
		Product donationProduct = getProductFromString( recurringTypeString );
		petitionAnswers.setCart( configureCartWithProduct( isCheck, isCash, donationProduct ) );
	}

	/**
	 * Gets product information for the recurring type specified.
	 *
	 * @param recurringTypeString string representing the selected frequency
	 *
	 * @return a product corresponding to the reoccurring type
	 */
	protected Product getProductFromString (String recurringTypeString)
	{
		List<Product> productsList = petition.getProducts();
		for( Product product : productsList )
		{
			String cost = product.getCost();
			if(cost == null || cost.equals("null")){ cost = "0"; }

			if( product.getRecurring().equalsIgnoreCase( recurringTypeString ) 
					&& (donationAmount == (int)Double.parseDouble(cost)
					|| (cost.equals("0") && amountEditText.isEnabled())))
			{
				return product;
			}
		}
		return null;
	}

	/**
	 * Configures a cart with the donation and payment type.
	 *
	 * @param donation - selected product / donation
	 * @param isCheck - "true" represents a paper check payment
	 * @param isCash - "true" represents a cash payment
	 *
	 * @return a product corresponding to the reoccurring type
	 */
	private Cart configureCartWithProduct (boolean isCheck, boolean isCash, Product donation)
	{
		Cart cart = getCart();

		//cart.clearPaymentData();
		cart.removeAllProducts();
		cart.AddProduct( donation, Float.toString( donationAmount ) );
		cart.setCash( isCash );
		cart.setPaperCheck( isCheck );

		return cart;
	}

	protected void onSkip ()
	{
		startActivity( getNextActivity() );
	}

	protected void onAbout ()
	{
		startActivity( BaseAboutCreditCardActivity.class );
	}

	@Override
	public void onResume()
	{
		super.onResume();
		//reset cart data
		Cart cart = getCart();
		cart.clearPaymentData();
		cart.removeAllProducts();
	}
}

