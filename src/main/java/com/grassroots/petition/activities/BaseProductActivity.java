package com.grassroots.petition.activities;

import com.grassroots.petition.R;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.Product;
import com.grassroots.petition.views.ProductsView;

import android.os.Bundle;
import android.view.View;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class BaseProductActivity extends BasePetitionActivityWithMenu {
    private ProductsView.ViewListener viewListener = new ProductsView.ViewListener() {
        @Override
        public void onEndInterview() {
            BaseProductActivity.this.onEndInterview();
        }

        @Override
        public void onMarketing() {
            BaseProductActivity.this.onMarketing();
        }

        @Override
        public void onNext(Product selectedProduct) {
            BaseProductActivity.this.onNext(selectedProduct);
        }
    };

    private void onNext(Product selectedProduct) {
        if(selectedProduct == null) {
            notifyUser(getString(R.string.SELECT_PRODUCT_MESSAGE));
            return;
        }

        Cart cart = new Cart();
        cart.AddProduct(selectedProduct, 1);
        setCart(cart);

        startActivity(getNextActivity());
    }




    private ProductsView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = (ProductsView) View.inflate(this, getLayout(), null);
        view.setViewListener(viewListener);
        view.setProducts(petition.getProducts());
        setContentView(view);
    }

    protected int getLayout() {
        return R.layout.products;
    }
}
