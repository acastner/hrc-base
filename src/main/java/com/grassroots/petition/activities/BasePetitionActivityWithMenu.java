package com.grassroots.petition.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.grassroots.petition.R;
import com.grassroots.petition.models.MarketingMaterialType;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 19.06.13
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
public class BasePetitionActivityWithMenu extends BasePetitionActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Resources resources = getResources();
        menu.add(R.string.end_button).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        menu.add(R.string.logout_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        menu.add(R.string.email_logs).setShowAsAction(android.view.MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        menu.add(R.string.reports).setShowAsAction(android.view.MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        //menu.add(R.string.tally_button).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Resources resources = getResources();
        if (item.getTitle().toString().equals(resources.getString(R.string.end_button))) {
            onEndInterview(); // TODO: implement different behavior for different activity
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.marketing_button_title))) {
            onMarketing();
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.logout_button_title))) {
            showLogoutDialog();
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.tally_button))) {
            onTally();
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.reports))) {
            //show types of reports
            showReports();
        } else if (item.getTitle().toString().equals(resources.getString(R.string.email_logs))) {
            emailLogItemMenuSelected();
            return true;
        }

        return false;
    }

    public void emailLogItemMenuSelected(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.lognotes, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.userNotes);

        dialogBuilder.setTitle("User notes");
        dialogBuilder.setMessage("Please tell us about the issues you were experiencing while using the app");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                getGlobalData().setUserNotes(edt.getText().toString());
                getGlobalData().setCrashedScriptId(getPetition().getId());
                getGlobalData().setCrashedScriptName(getPetition().getName());
                onEmailLogs();
                InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                dialog.cancel();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                dialog.cancel();

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void showReports() {

        final ArrayList<String> reports = getGlobalData().getReportList();
        CharSequence[] cs = reports.toArray(new CharSequence[reports.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select a report to view");
        builder.setItems(cs, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ReportDetailWebviewActivity reportdetailActivity = new ReportDetailWebviewActivity();
                Intent intent = new Intent(BasePetitionActivityWithMenu.this,ReportDetailWebviewActivity.class);
                intent.putExtra("reportname",reports.get(item));
                startActivity(intent);

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
