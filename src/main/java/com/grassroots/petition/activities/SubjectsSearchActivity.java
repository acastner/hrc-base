package com.grassroots.petition.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.views.lists.KnownSubjectListAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import com.cloudmine.api.CMApiCredentials;
//import com.cloudmine.api.CMObject;
//import com.cloudmine.api.SimpleCMObject;
//import com.cloudmine.api.persistance.ClassNameRegistry;
//import com.cloudmine.api.rest.CMWebService;
//import com.cloudmine.api.rest.JsonUtilities;
//import com.cloudmine.api.rest.callbacks.CMObjectResponseCallback;
//import com.cloudmine.api.rest.options.CMPagingOptions;
//import com.cloudmine.api.rest.options.CMRequestOptions;
//import com.cloudmine.api.rest.response.CMObjectResponse;
//import com.grassroots.petition.models.VoterInfo;
//import com.grassroots.petition.views.lists.VoterAddressListAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 30.04.13
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
public class SubjectsSearchActivity extends BasePetitionActivity implements AdapterView.OnItemClickListener {
    public static final String SUBJECT_KEY = "SUBJECT_KEY";
    private ListView voterListView;
    private TextView lastNameFilter;
    private TextView zipFilter;
    private TextView addressFilter;
    private TextView emptyListView;
    private List<Subject> foundSubjects = new ArrayList<Subject>();
    private AsyncTask<Void, Void, Long> currentSearchTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.subject_search_activity);

        lastNameFilter = (TextView)findViewById(R.id.txtLastName);
        lastNameFilter.setOnEditorActionListener(getSearchListener());

        addressFilter = (TextView) findViewById(R.id.txtAddress);
        addressFilter.setOnEditorActionListener(getSearchListener());

        zipFilter = (TextView) findViewById(R.id.txtZip);
        zipFilter.setOnEditorActionListener(getSearchListener());

        voterListView = (ListView)findViewById(R.id.list);
        voterListView.setOnItemClickListener(this);

        emptyListView = (TextView)findViewById(R.id.empty);
        voterListView.setEmptyView(emptyListView);
    }

    private TextView.OnEditorActionListener getSearchListener() {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
                if(EditorInfo.IME_ACTION_SEARCH == keyCode){
                    onSearch(null);
                    hideSoftKeyboard(textView);
                    return true;
                }
                return false;
            }
        };
    }

    private void hideSoftKeyboard() {
        hideSoftKeyboard(zipFilter);
        hideSoftKeyboard(addressFilter);
        hideSoftKeyboard(lastNameFilter);
    }

    private void hideSoftKeyboard(TextView textView) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
    }

    public void onSearch(View view) {
        search();
    }

    private void search() {
        if(!hasSearchStrings()) {
            notifyUser(getString(R.string.ENTER_SEARCH_TERM_TO_SEARCH_MESSAGE));
            emptyListView.setText(R.string.empty_search_list);
        }
        hideSoftKeyboard();
        populateVoterInfo();
    }

    private void populateVoterInfo() {
        if(currentSearchTask != null)
            currentSearchTask.cancel(true);
        if(!hasSearchStrings()) {
            setLoadedSubjects(Collections.EMPTY_LIST);
            return;
        }

        final ProgressDialog loadingDialog = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
        loadingDialog.setMessage(getString(R.string.TOUCH_SCREEN_TO_CANCEL_MESSAGE));
        loadingDialog.setCancelable(true);
        loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                notifyUser(getString(R.string.SEARCH_CANCELLED_MESSAGE));
                if (currentSearchTask != null)
                    currentSearchTask.cancel(true);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(loadingDialog != null && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                    notifyUser(getString(R.string.SEARCH_TIMED_OUT_MESSAGE));
                }
                if(currentSearchTask != null)
                    currentSearchTask.cancel(true);
            }
        }, 30*1000);
        loadingDialog.show();
        currentSearchTask = new AsyncTask<Void, Void, Long>() {
            protected Long doInBackground(Void... params) {

                // new code
                GrassrootsRestClient client = GrassrootsRestClient.getClient();

                String lastName = getLastNameFilter();
                String zip = getZipFilter();
                Pair<List<Subject>, Response> getSubjectsResponse = client.getSubjects(lastName, zip);
                loadingDialog.dismiss();

                if (getSubjectsResponse.second != null) {
                    //notifyUser(getString(R.string.SEARCH_FAILED_MESSAGE));
                    //String errorMessage = parseErrorMessage(walklistResponse.second);
                    return 0l;
                } else {
                    if (!isCancelled() && getSubjectsResponse.first != null) {
                        setLoadedSubjects(getSubjectsResponse.first);
                    }
                    return 1l;
                }
            }

            protected void onPostExecute(Long result) {
                if (0 == result) {
                    notifyUser(getString(R.string.SEARCH_FAILED_MESSAGE));
                }
            }

        }.execute();
    }

    private void setLoadedSubjects(final List<Subject> loadedSubjects) {
        foundSubjects = loadedSubjects;

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (0 == foundSubjects.size()) {
                    emptyListView.setText(R.string.no_results_list);
                }
                voterListView.setAdapter(new KnownSubjectListAdapter(foundSubjects, getLayoutInflater()));
            }
        });
    }

    private boolean hasSearchStrings() {
        return Strings.isNotEmpty(getLastNameFilter()) &&
                Strings.isNotEmpty(getZipFilter());
    }

    private String getLastNameFilter() {
        return lastNameFilter.getText().toString();
    }
    private String getZipFilter() {
        return zipFilter.getText().toString();
    }
    private String getAddressFilter() {
        return addressFilter.getText().toString();
    }

    public void onCancel(View view) {

        Intent results = new Intent();
        if(getParent() == null) {
            setResult(RESULT_CANCELED, results);
        }else {
            getParent().setResult(RESULT_CANCELED, results);
        }
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long clickedId) {
        Subject subject = foundSubjects.get((int)clickedId);//should be a safe cast as we will limit returned results

        Intent results = new Intent();
        results.putExtra(SUBJECT_KEY,  subject);
        if(getParent() == null) {
            setResult(RESULT_OK, results);
        }else {
            getParent().setResult(RESULT_OK, results);
        }
        finish();
    }
}

