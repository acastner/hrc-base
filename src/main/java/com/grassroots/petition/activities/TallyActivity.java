package com.grassroots.petition.activities;

import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.models.Tally;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.tasks.FetchTallyTask;

public class TallyActivity extends BasePetitionActivity {
	public static final String TAG = TallyActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	ProgressDialog pd = null;
	Tally tally = null;
	private TextView knocksno,attemptsno,contactsno,nothomesno,comebacksno,refusedno,donationsno,totaldonationno,donations,totaldonation;
	private Button backButton;
	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tallyscreen);
    
        backButton = (Button) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
        pd = ProgressDialog.show(this, "Loading tally", "Please wait ...");
        FetchTallyTask tallyTask = new FetchTallyTask(this);
        try {
			tally = tallyTask.execute().get();
			pd.dismiss();
            setUpViews();			
		} catch (InterruptedException e) {
			pd.dismiss();
			e.printStackTrace();
		} catch (ExecutionException e) {
			pd.dismiss();
			e.printStackTrace();
		} catch (Exception e) {
			pd.dismiss();
			e.printStackTrace();
			LOGGER.debug("TALLY EXCEPTION: " + e.toString());
		}
        
    }
	private void setUpViews() {
		
		Log.e("Tally Model : ","Tally : "+tally);
		if(tally != null){
			knocksno = (TextView) findViewById(R.id.knocksno);
			attemptsno = (TextView) findViewById(R.id.attemptsno);
			contactsno = (TextView) findViewById(R.id.contactsno);
			nothomesno = (TextView) findViewById(R.id.nothomesno);
			comebacksno = (TextView) findViewById(R.id.comebacksno);
			refusedno = (TextView) findViewById(R.id.refusedno);
			donationsno = (TextView) findViewById(R.id.donationsno);
			totaldonationno = (TextView) findViewById(R.id.totaldonationno);
			donations = (TextView) findViewById(R.id.donations);
			totaldonation = (TextView) findViewById(R.id.totaldonation);
			
			
			knocksno.setText(""+tally.getKnocks());
			attemptsno.setText(""+tally.getAttempts());
			contactsno.setText(""+tally.getContacts());
			nothomesno.setText(""+tally.getNotHome());
			comebacksno.setText(""+tally.getComeBack());
			refusedno.setText(""+tally.getRefused());
			
			if(petition.hasDonation()){
				donationsno.setVisibility(View.VISIBLE);
				totaldonationno.setVisibility(View.VISIBLE);
				donations.setVisibility(View.VISIBLE);
				totaldonation.setVisibility(View.VISIBLE);
				
				donationsno.setText(""+tally.getDonationsCount());
				totaldonationno.setText(""+tally.getDonations());
			}else{
				donationsno.setVisibility(View.GONE);
				totaldonationno.setVisibility(View.GONE);
				donations.setVisibility(View.GONE);
				totaldonation.setVisibility(View.GONE);
			}
			
			
		}else{
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setMessage(R.string.tally_not_available)
	                .setCancelable(false)
	                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                    public void onClick(final DialogInterface dialog, final int id) {
	                       finish();
	                    }
	                });
	        final AlertDialog alert = builder.create();
	        alert.show();
		}
		
		
	}
    
	@Override
	public void onBackPressed() {
		finish();
		
	}
    

}
