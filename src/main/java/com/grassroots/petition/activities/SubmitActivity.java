package com.grassroots.petition.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.WalkListUpdateListener;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.tasks.UpdateWalklistTask;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;
import com.grassroots.petition.views.PictureQuestionView;
import com.grassroots.petition.views.QuestionView;
import com.grassroots.petition.views.SubmitView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * TODO: Allow users to edit all of their responses. (DEL-167) TODO: Make this
 * screen a lot prettier.
 */
public class SubmitActivity extends BasePetitionActivityWithMenu implements
        WalkListUpdateListener {
    private static final String TAG = SubmitActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private static final int PHOTO_ACTION_CODE = 8;
    private SubmitView view;
    private QuestionView missingQuestionView;
    private String photoPath;
    protected Subject subject;
    ProgressDialog pd = null;
    private boolean isTablet,isLocationList = false;
    
    private int spinnerCount = 2;
    private int spinnerInitializedCount = 0;
    
    public static final String WALKLIST_EXTRA_PARAMETER_LASTNAME="SELECTED_LASTNAME_EXTRA";
    public static final String WALKLIST_EXTRA_PARAMETER_SUBJECT = "SELECTED_SUBJECT_EXTRA";
    private SubmitView.ViewListener viewListener = new SubmitView.ViewListener() {
        @Override
        public void onNext() {
			Button b  = (Button) findViewById(R.id.submit_button);
			b.setVisibility(View.GONE);

            SubmitActivity.this.onNext();
        }

        @Override
        public void onMissingSubjectInfo(Subject subject) {
            SubmitActivity.this.onMissingSubjectInfo(subject);
        }

        @Override
        public void onMissingQuestionAnswer(Question question) {
            SubmitActivity.this.onMissingQuestionAnswer(question);
        }
    };

    /**
     * Constructs a dialog so a user can enter an answer for an unanswered
     * question. Shows the question title, the question text (long questions are
     * truncated), and the available answers for the question.
     * 
     * @param question
     *            The question to be displayed
     */
    protected void onMissingQuestionAnswer(Question question) {
        // Build dialog & inflate view
        AlertDialog.Builder missingQuestionAnswerDialogBuilder = ConfirmCancelDialogBuilder
                .getAlertDialogBuilder(SubmitActivity.this);
        missingQuestionView = (QuestionView) View.inflate(SubmitActivity.this,
                R.layout.question, null);
        missingQuestionView.setMaxQuestionHeight(getResources().getInteger(
                R.integer.max_question_dialog_height_in_pixels));

        // Set photo view listener
        if (question.isPicture()) {
            missingQuestionView.setPhotoViewListener(getPhotoViewListener());
        }

        // Set title & hide standard header
        missingQuestionAnswerDialogBuilder.setTitle(question.getName());
        View header = missingQuestionView.findViewById(R.id.header_title);
        header.setVisibility(View.GONE);

        // Configure buttons
        missingQuestionAnswerDialogBuilder.setPositiveButton(R.string.ok,
                getRegularQuestionListener());
        missingQuestionAnswerDialogBuilder.setNegativeButton(R.string.cancel,
                getNegativeClickListener());

        // Set view & question data
        missingQuestionView.setPetitionAnswers(petitionAnswers);
        missingQuestionView.setQuestion(question);
        missingQuestionAnswerDialogBuilder.setView(missingQuestionView);

        // Show dialog
        AlertDialog dialog = missingQuestionAnswerDialogBuilder.create();
        dialog.show();
    }

    private DialogInterface.OnClickListener getNegativeClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        };
    }

    private DialogInterface.OnClickListener getRegularQuestionListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SubjectAnswer newAnswer = missingQuestionView.getAnswer();
                petitionAnswers.overwriteAnswer(newAnswer);
                view.setPetitionAnswers(petitionAnswers, petition);
            }
        };
    }

    private PictureQuestionView.ViewListener getPhotoViewListener() {
        return new PictureQuestionView.ViewListener() {
            @Override
            public void onTakePicture() {
                Intent takePictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    File photoFile = createImageFile();
                    photoPath = photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                } catch (IOException e) {
                    LOGGER.error("Unable to get output file", e);
                }
                startActivityForResult(takePictureIntent, PHOTO_ACTION_CODE);
            }
        };
    }

    // TODO: The dialog has lots of space above and below the image.
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PHOTO_ACTION_CODE || resultCode != RESULT_OK) {
            return;
        }

        // Scale and display image
        if (Strings.isNotEmpty(photoPath)) {
            Bitmap photo = readScaledPhoto(photoPath);
            missingQuestionView.setPhoto(photo, photoPath);
        }
    }

    protected void onMissingSubjectInfo(Subject subject) {
        ConfirmCancelDialogBuilder.createMissingSubjectInfoDialog(this,
                subject, petition.getSubjectInformationQuestions(),
                new ConfirmCancelDialogBuilder.ViewListener<Subject>() {
                    @Override
                    public void onConfirm(Subject newSubject) {
                        petitionAnswers.mergeInAnswersAndSetIdFields(
                                petition.getSubjectInformationQuestions(),
                                newSubject);
                        petitionAnswers.setId(petition.getId());
                        view.setSubject(newSubject, petition);
                        petitionAnswers.setSubject(newSubject);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }

    // Override it in a subclass to do last minute changes with petitionAnswers
    protected void beforeSubmit() {

    }

    
    protected void onNext() {
    	Calendar cl = Calendar.getInstance();
		int month = cl.get(Calendar.MONTH)+1;
		int day = cl.get(Calendar.DAY_OF_MONTH);
		int year = cl.get(Calendar.YEAR);
		int hour = cl.get(Calendar.HOUR_OF_DAY);
		int min = cl.get(Calendar.MINUTE);
		int sec = cl.get(Calendar.SECOND);
		
		petitionAnswers.setSurveyEnd(month + "-" + day + "-" + year + ", "
				+ hour + ":" + min + ":" + sec);
    	pd = ProgressDialog.show(SubmitActivity.this, "Please wait", "Submiting survey ...");
       SaveAnswersTask saveAnswersTask = new SaveAnswersTask();
       saveAnswersTask.execute();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        subject = getSubject();

        view = (SubmitView) View.inflate(this, R.layout.submit, null);
        view.setViewListener(viewListener);
        view.setSubject(subject, petition);
        view.setPetitionAnswers(petitionAnswers, petition);
        setContentView(view);
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            // Assume Android 4.0, 7.0" tablet screen size
            isTablet = true;
        } else {
            // dart
            isTablet = false;
        }
        updateWalklist();
    }

    private void updateWalklist() {

        UpdateWalklistTask updateWalklistThread = new UpdateWalklistTask(this,
                getGlobalData().getLastWalklistSyncTime(), this, null );
        //updateWalklistThread.execute();
    }

    @Override
    public void onReload() {
        super.onReload();
        subject = getSubject();
        view.setSubject(subject, petition);
        view.setPetitionAnswers(petitionAnswers, petition);

		Button b  = (Button) findViewById(R.id.submit_button);
		b.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateSuccess(Walklist walklist) {
    }

    @Override
    public void updateFailed(String errorMessage) {
        Log.e("Walklist updated failed",""+errorMessage);                
    }
    
    private class SaveAnswersTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			petitionAnswers.setGpsLocation(LocationProcessor
		            .getLastKnownGpsLocation(getApplicationContext()));
		    petitionAnswers.convertPhotoFilesToAnswers(SubmitActivity.this);
		    petitionAnswers.mergeInAnswersAndSetIdFields(
		            petition.getSubjectInformationQuestions(), getSubject());

		    beforeSubmit();
		    GlobalData.counterValue++;
		    if(subject != null && ((petition.isWalklist()) || subject.getLocation()!= null)){
		      subject.setKnockedStatus(KnockStatus.SUCCESS);
		      petitionAnswers.setSubject(subject);
		      List<Subject> subjects = new ArrayList<Subject>();;
		      subjects.add(subject);
		      if(subject.getLatitude() == "0.0"){
		    	  subject.setLatitude(null);
		      }
		      if(subject.getLongitude() == "0.0"){
		    	  subject.setLongitude(null);
		      }
		      if(subject.getAddressLine2() != null && subject.getAddressLine2().length() == 0){
		    	  subject.setAddressLine2(null);
		      }
		      WalklistDataSource walklistdatasource = new WalklistDataSource(SubmitActivity.this);
		      walklistdatasource.insertWalklist(subjects,subject.getLocation());
		      getWalklist().setKnocked(subject);
		      
		    }
		    try {
		        GrassrootsRestClient.getClient().savePetitionAnswers(
		                petitionAnswers);
		        return null;
		    } catch (Exception e) {
		        String errorMessage = getResources().getString(
		                R.string.unable_to_start_rps);
		        LOGGER.error(errorMessage);
		        return errorMessage;
		        
		    }
		   
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pd.dismiss();
			if(result != null){
				notifyUser(result);
		        logout();
			}	        
	        GlobalData.shouldLocationReload = true;
		    clearAllAnswers();
		    finish();
		    if(isTablet){
		        startFirstActivity(subject);
		    }else{
		        Intent intent = new Intent();
	            intent.setClass(SubmitActivity.this, WalklistDetailsActivity.class);
	            if(getGlobalData().getLocation() != null){
	                isLocationList = true;
	            }else{
	                isLocationList = false;
	            }
	            if(isLocationList){
	                if(subject.getLastName()!= null){
	                    intent.putExtra(WALKLIST_EXTRA_PARAMETER_LASTNAME, subject.getLastName());
	                }
	            }else{
	                intent.putExtra(WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
	            }
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(intent);
		    }
	        
		}
    	
    	
    }
}
