package com.grassroots.petition.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;
import com.grassroots.petition.models.*;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.views.PictureQuestionView;
import com.grassroots.petition.views.QuestionsView;
import com.grassroots.petition.views.SignatureCanvas;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.squareup.timessquare.CalendarPickerView;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Main class that handles the arbitrary questions loop (AQOL) and branching of questions.
 */
public class QuestionsActivity extends BasePetitionActivityWithMenu {
	private static int PHOTO_ACTION_CODE = 7;
	private static Logger LOGGER = Logger.getLogger(QuestionsActivity.class);
	private boolean issignature;

	public static Subject questionSubject;
	private SubjectAnswer subjectAnswer;
	
	private final int CALENDAR_REQUEST = 0;
	

	private QuestionsView.ViewListener viewListener = new QuestionsView.ViewListener() {
		@Override
		public void onBack() {
			QuestionsActivity.this.onNext();
		}

		@Override
		public void onMarketing() {
			QuestionsActivity.this.onMarketing();
		}

		@Override
		public void onNext(SubjectAnswer answer) {
			if( requiredQuestionIsNotAnswered( answer ) )
			{
				toastRequired();
				return;
			}

			QuestionsActivity.this.onNext( answer );
		}

		@Override
		public void onTally() {
			QuestionsActivity.this.onTally();

		}

		@Override
		public void onClear() {			
			SignatureCanvas signatureView = (SignatureCanvas) findViewById(R.id.signature_canvas);
			signatureView.clearSignature();
		}
	};

	private boolean requiredQuestionIsNotAnswered (SubjectAnswer answer)
	{
		return getCurrentQuestion().isRequired() &&
				!answer.hasAnswer();
	}

	private void toastRequired ()
	{
		Toast.makeText( this, "You must answer this question", Toast.LENGTH_LONG ).show();
	}

	protected void onNext() {
		// hide keyboard
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

		if(issignature)
		{
			viewListener.onClear();
		}

		petitionAnswers.overwriteAnswers(answers.values());
		if (currentQuestionPosition > 0) {
			currentQuestionPosition--;
			setQuestionOnView(getCurrentQuestion());
		} else {
			this.onFinalBack();
		}
	}

	protected void onNext(SubjectAnswer answer) {
		if(issignature){
			petitionAnswers.setSignature(view.getSignature());
		}

		Question currentQuestion = getCurrentQuestion();

		//change to if calendar type
		if(currentQuestion.isCalendar())
		//if(true)
		{
			CalendarPickerView calendarPickerView = (CalendarPickerView) findViewById(R.id.calendar_view);
			Date selectedDate = calendarPickerView.getSelectedDate();
			if (null == selectedDate)
			{
				notifyUser(getString(R.string.SELECT_DATE_MESSAGE));
				return;
			}

			/*List<FreeTimeInterval> freeIntervals = getAppointmentCalendar().getFreeIntervalsForDate(selectedDate);

			if (freeIntervals == null || freeIntervals.isEmpty()) {
				notifyUser(getString(R.string.NO_FREE_TIME_INTERVAL_ON_THE_DATE_MESSAGE));
				return;
			}*/

			subjectAnswer = answer;

			startActivityForResult( MultiDayFragmentActivity.createIntent(QuestionsActivity.this,
					MultiDayFragmentActivity.createDayViewBundle(selectedDate), true), CALENDAR_REQUEST);
		}
		else
		{
			// hide keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

			petitionAnswers.overwriteAnswer(answer);

			answers.put(currentQuestion, answer);
			int answerId = answer == null ?
					-1 :
						answer.getAnswerId();
			QuestionAnswer questionAnswer = currentQuestion.getAnswerWithId(answerId);
			nextQuestion(questionAnswer);
		}
	}

	/**
	 * Show the next question if it exists. This could be the next by order or jump ahead if branching.
	 * If no questions are left, go to the next activity
	 * @param answer
	 */
	private void nextQuestion(QuestionAnswer answer) {
		if(answer != null && answer.hasBranch()) {
			int nextQuestionId = answer.getBranch();
			int questionIndex;
			for(questionIndex = 0; questionIndex < questionIds.size(); questionIndex++) {
				if(nextQuestionId == questionIds.get(questionIndex))
					break;
			}
			currentQuestionPosition = questionIndex;
		}else {
			currentQuestionPosition++;
		}
		if(questionIds.size() <= currentQuestionPosition) {
			onFinalNext();
		} else if(questionIds.size() < 0) {
			onFinalBack();
		} else {
			Question question = getCurrentQuestion();
			setQuestionOnView(question);
		}
	}

	protected void onFinalNext() {
		petitionAnswers.overwriteAnswers(answers.values());
		startActivity(getNextActivity());
	}

	private PictureQuestionView.ViewListener pictureViewListener = new PictureQuestionView.ViewListener() {
		@Override
		public void onTakePicture() {
			takePicture();
		}
	};

	protected void takePicture() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		try {
			File photoFile = createImageFile();
			photoPath = photoFile.getAbsolutePath();
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
		} catch (IOException e) {
			LOGGER.error("Unable to get output file", e);
		}
		startActivityForResult(takePictureIntent, PHOTO_ACTION_CODE);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode != PHOTO_ACTION_CODE && requestCode != CALENDAR_REQUEST) {
			LOGGER.debug("Received result for unknown code: " + requestCode);
			return;
		}
		if(resultCode != RESULT_OK) {
			LOGGER.debug("Received null result");
			return;
		}

		if(Strings.isNotEmpty(photoPath) && requestCode == PHOTO_ACTION_CODE) {

			Bitmap photo = readScaledPhoto(photoPath);
			view.setPhoto(photo, photoPath);
		}
		else if (requestCode == CALENDAR_REQUEST && resultCode == RESULT_OK)
		{
			// hide keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

			getAppointment().setCalendarId(Integer.parseInt(subjectAnswer.getStringAnswer()));
			
			petitionAnswers.overwriteAnswer(subjectAnswer);

			Question currentQuestion = getCurrentQuestion();
			answers.put(currentQuestion, subjectAnswer);
			int answerId = subjectAnswer == null ?
					-1 :
						subjectAnswer.getAnswerId();
			QuestionAnswer questionAnswer = currentQuestion.getAnswerWithId(answerId);
			nextQuestion(questionAnswer);
		}
	}

	protected void onFinalBack() {
		onBackPressed();
	}

	private String photoPath;
	private QuestionsView view;
	private List<Integer> questionIds;
	private LinkedHashMap<Integer, Question> idsToQuestions = new LinkedHashMap<Integer, Question>();
	private Map<Question, SubjectAnswer> answers;
	private int currentQuestionPosition = 0; //TODO I don't like this but don't want to use an iterator unless its bidi

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		answers = new HashMap<Question, SubjectAnswer>();
		photoPath = null;

		view = (QuestionsView) View.inflate(this, R.layout.questions, null);
		idsToQuestions = petition.getNonMagicQuestionMapping();
		questionIds = new ArrayList<Integer>(idsToQuestions.keySet());
		view.setPetitionAnswers(petitionAnswers);
		view.setViewListener(viewListener);
		view.setPhotoViewListener(pictureViewListener);
		setQuestionOnView(getCurrentQuestion());
		setContentView(view);
		setTitle(getCurrentQuestion().getName() + "-" + getLocationName());
		questionSubject = getSubject();

		hideMarketingIfHasNoMaterials();
	}

	@Override
	public void onReload() {
		super.onReload();

		view.setPetitionAnswers(petitionAnswers);

		answers = new HashMap<Question, SubjectAnswer>();
		photoPath = null;

		idsToQuestions = petition.getNonMagicQuestionMapping();
		questionIds = new ArrayList<Integer>(idsToQuestions.keySet());

		currentQuestionPosition = 0;
		setQuestionOnView(getCurrentQuestion());

		hideMarketingIfHasNoMaterials();
	}

	private Question getCurrentQuestion() {
		if(questionIds.size() <= currentQuestionPosition)
			currentQuestionPosition = questionIds.size() - 1;
		else if(questionIds.size() < 0)
			currentQuestionPosition = 0;

		return idsToQuestions.get(questionIds.get(currentQuestionPosition));
	}

	private void setQuestionOnView(Question question) {
		if (question == null || question.is(Question.Type.SIGNATURE)) {

			/*
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			*/
			//nextQuestion(null);
			issignature = true;
			view.setSignatureQuestion(question);

			Button clearButton = (Button) view.findViewById(R.id.clear_button);
			clearButton.setVisibility(View.VISIBLE);

		} else {

			// Support for small displays (Samsung dart)
			/*
			if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
				// Assume Android 4.0, 7.0" tablet screen size
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			} else {
				// Assume Android 2.2, Samsung Dart
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}
			*/

			issignature = false;
			view.setQuestion(question);

			Button clearButton = (Button) view.findViewById(R.id.clear_button);
			clearButton.setVisibility(View.GONE);
		}
	}


}