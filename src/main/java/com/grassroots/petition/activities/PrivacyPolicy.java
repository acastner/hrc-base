package com.grassroots.petition.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.grassroots.petition.R;


/**
 * Created by aniruddh on 2/15/2018.
 */

public class PrivacyPolicy extends Activity {

    private static String PRIVACY_POLICY_URL = "https://www.hrc.org/hrc-story/privacy-policy";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_policy);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initView();
    }

    public void initView()  {
        WebView webView = (WebView) findViewById(R.id.privacy_policy_webview);

        WebViewClient mWebClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {

                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setDomStorageEnabled(true);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                webView.getSettings().setAppCacheEnabled(true);
                webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
                webView.getSettings().setAppCacheEnabled(true);
                webView.loadUrl(url);
                return true;
            }

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
                handler.proceed();
            }
        };

        webView.setWebViewClient(mWebClient);
        webView.loadUrl(PRIVACY_POLICY_URL);


        Button backButton = (Button) findViewById(R.id.back_button_privacy_policy);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
