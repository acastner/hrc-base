package com.grassroots.petition.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 23.05.13
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
public class BaseMailingActivity extends BasePetitionActivityWithMenu {
    private static HashMap<Integer, String> idsToVarnamesMap = new HashMap<Integer, String>();

    {
        idsToVarnamesMap.put(R.id.first_name_edit, Question.FIRST_NAME);
        idsToVarnamesMap.put(R.id.last_name_edit, Question.LAST_NAME);
        idsToVarnamesMap.put(R.id.street_address_edit, Question.ADDRESS);
        idsToVarnamesMap.put(R.id.address_2_edit, Question.ADDRESS_2);
        idsToVarnamesMap.put(R.id.city_edit, Question.CITY);
        idsToVarnamesMap.put(R.id.state_edit, Question.STATE);
        idsToVarnamesMap.put(R.id.zip_edit, Question.ZIP);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mailing_activity);

        TextView header = (TextView) findViewById(R.id.header_title);
        header.setText( getString( R.string.mailing_page_title ) );

        restoreFields();

        Button nextButton = (Button) findViewById(R.id.next_button);
        Button endInterviewButton = (Button) findViewById(R.id.end_interview_button);
        Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
        Button assistButton = (Button) findViewById(R.id.autofill_fields_button);
        Button tallyButton = (Button) findViewById(R.id.tally_button);
        if(assistButton != null)
        assistButton.setVisibility(View.GONE);

        if(nextButton != null)
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseMailingActivity.this.onNext();
            }
        });

        if(endInterviewButton != null)
        endInterviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseMailingActivity.this.onEndInterview();
            }
        });

        if(marketingButton != null)
        marketingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseMailingActivity.this.onMarketing();
            }
        });
        
        if(tallyButton != null)
        	tallyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseMailingActivity.this.onTally();
                }
            });

        hideMarketingIfHasNoMaterials();
    }

    @Override
    public void onReload(){
        super.onReload();

        restoreFields();

        hideMarketingIfHasNoMaterials();
    }

    private void onNext() {
        storeFields();

        startActivity(getNextActivity());
    }


    private void storeFields() {
        for (Map.Entry entry : idsToVarnamesMap.entrySet()) {
            Integer editId = (Integer)entry.getKey();
            String questionVarname = (String)entry.getValue();
            addTextAnswer(editId, questionVarname + Question.SHIPPING_POSTSCRIPT);
        }
    }

    private void restoreFields() {
        for (Map.Entry entry : idsToVarnamesMap.entrySet()) {
            String subjectQuestionName = (String)entry.getValue();
            String shippingQuestionName = subjectQuestionName + Question.SHIPPING_POSTSCRIPT;
            if (isAnswerExist(shippingQuestionName)) {
                setTextAnswerToTextField((Integer)entry.getKey(), shippingQuestionName);
            } else {
                setTextAnswerToTextField((Integer)entry.getKey(), subjectQuestionName);
            }
        }
    }
}
