package com.grassroots.petition.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.utils.ClassRetriever;

import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * Date: 18.06.13
 * Time: 12:10
 * Fixes DEL-51 issue (If you go home or launch another app,
 * attempting to get back into delmonico via either the launcher or
 * last used application menu causes you to have to relogin again.)
 */
public class StartupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 //       android.os.Debug.waitForDebugger();
        
        addShortcut();
        // Support for small displays (Samsung dart)
        /*
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            // Assume Android 4.0, 7.0" tablet screen size
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            // Assume Android 2.2, Samsung Dart
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        */

        if (needStartApp()) {
            Intent i = new Intent(StartupActivity.this, ClassRetriever.getLoginActivity(this));
            startActivity(i);
        }

        finish();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // this prevents StartupActivity recreation on Configuration changes
        // (device orientation changes or hardware keyboard open/close).
        // just do nothing on these changes:
        super.onConfigurationChanged(null);
    }

    private boolean needStartApp() {
        final ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final List<RunningTaskInfo> tasksInfo = am.getRunningTasks(1024);

        if (!tasksInfo.isEmpty()) {
            final String ourAppPackageName = getPackageName();
            RunningTaskInfo taskInfo;
            final int size = tasksInfo.size();
            for (int i = 0; i < size; i++) {
                taskInfo = tasksInfo.get(i);
                if (ourAppPackageName.equals(taskInfo.baseActivity.getPackageName())) {
                    // continue application start only if there is the only Activity in the task
                    // (BTW in this case this is the StartupActivity)
                    return taskInfo.numActivities == 1;
                }
            }
        }

        return true;
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    private void addShortcut() {
        //Adding shortcut for MainActivity 
        //on Home screen
        SharedPreferences prefs = getSharedPreferences("shortcut_created", 0);
        SharedPreferences.Editor editor = prefs.edit();

        if (!(prefs.getBoolean("shortcut_created", false))) 
        { 
            Intent shortcutIntent = new Intent(getApplicationContext(),
                    StartupActivity.class);

            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent
                    .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                            R.drawable.launcher));
            addIntent.putExtra("duplicate",false);
            addIntent
                    .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(addIntent);
            editor.putBoolean("shortcut_created", true);
            editor.commit();
            
        } 
    }
}
