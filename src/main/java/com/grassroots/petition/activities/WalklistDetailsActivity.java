package com.grassroots.petition.activities;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.fragments.WalklistDetailsFragment;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.KnockEvent;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Knocks;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.HomeStatusView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.util.Pair;
import android.view.View;

/**
 * Present detailed information about selected household address from walklist object.
 * Contains a list of subjects which belong to this household.
 * The user is able to select a subject from a list to begin a new survey.
 * The user is able to begin manual survey (without selected subject).
 * The user is able to set knock status for the whole household.
 * Activity contains options menu with Logout, Marketing and Manual items.
 */
public class WalklistDetailsActivity extends BasePetitionActivityWithMenu {
    private static final String TAG = WalklistDetailsActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Fragment which displays detailed information about selected household address from walklist object.
     */
    private WalklistDetailsFragment detailsFragment;

    /**
     * Walklist object which contains subjects information
     */
    private Walklist walklist;
    Subject subject = null;


    /**
     * Details fragment listener, used to receive events from the details fragment which contains details information about subject address
     */
    private WalklistDetailsFragment.ViewListener detailsFragmentListener = new WalklistDetailsFragment.ViewListener() {
        @Override
        public void onSetHomeStatus () {
            WalklistDetailsActivity.this.onSetHomeStatus();
        }

        @Override
        public void onWalklistUpdate ()
        {
        }

        @Override
        public void onSurveyStart (Subject subject) {
            WalklistDetailsActivity.this.beginProcessSubject(subject);
        }

        @Override
        public void onMarketing(Subject subject) {
            WalklistDetailsActivity.this.onMarketing();
        }

        @Override
        public void onShowMap() {
            WalklistDetailsActivity.this.switchToMap();
        }

        @Override
        public void onSelectSubject(Subject subject) {
            // TODO Auto-generated method stub
            
        }

		@Override
		public void onFilterLastName(String filter) {
			// TODO Auto-generated method stub
			
		}

        @Override
        public void onSelectLastName(String lastname) {
            // TODO Auto-generated method stub
            
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        walklist = getWalklist();
        if (null == walklist) {
            throw new NullPointerException("WalklistActivity started with null walklist");
        }

        Petition petition = getPetition();
        if (null == petition ) {
            throw new NullPointerException("WalklistActivity started with null petition");
        }

        setContentView(R.layout.walklist_details_activity);


        FragmentManager fragmentManager = getSupportFragmentManager();

        detailsFragment = (WalklistDetailsFragment) fragmentManager.findFragmentById(R.id.walklist_details_fragment);
        detailsFragment.setViewListener(detailsFragmentListener);
        detailsFragment.setWalklist(walklist);
        detailsFragment.setPetition(petition);

        subject = getIntent().getExtras().getParcelable(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT);
        setSubject(subject);
        if(getGlobalData().getLocation() == null){
        if (null == subject) {
            LOGGER.error("walklist doesn't contain \"subject\"");
            finish();
            return;
        }
            detailsFragment.setSubjects(walklist.getSubjects());
        }else{
            String lastname = getIntent().getExtras().getString(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_LASTNAME);
            detailsFragment.setSubjects(walklist.getSubjects());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();
        intent.removeExtra(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT);
    }


    /**
     * Display a dialog with a list of available knock statuses where the user can select appropriate knock status
     * for the selected subjects address.
     */
    protected void onSetHomeStatus () {
        final Dialog statusDialog = new Dialog(this);
        statusDialog.setTitle(R.string.home_status);
        HomeStatusView view = (HomeStatusView) View.inflate(this,
                R.layout.home_status, null);

        view.setViewListener(new HomeStatusView.ViewListener() {
            @Override
            public void onStatusSelected(KnockStatus knockStatus) {
                List<Subject> knockedSubjects = detailsFragment.getSubjects();
                detailsFragment.setSubjects(knockedSubjects);
                if (null != knockedSubjects && knockedSubjects.size() > 0) // should
                                                                           // always
                                                                           // be
                                                                           // true
                {
                    Knocks knocks = new Knocks();
                    GpsLocation location = LocationProcessor
                            .getLastKnownGpsLocation(getApplicationContext());
                    for (Subject subject : knockedSubjects) {
                        // Don't update completed statuses
                        if (subject.getKnockStatus() == KnockStatus.SUCCESS) {
                            continue;
                        }

                        walklist.setKnocked(subject.getId(), knockStatus);
                        WalklistDataSource walklistDataSource = new WalklistDataSource(detailsFragment.getActivity());
                        walklistDataSource.insertWalklist(knockedSubjects,getGlobalData().getLocation());
                        detailsFragment.getWalkList().setKnocked(subject.getId(), knockStatus);
                        setSubject(subject);
                        String hid = subject.getHid();
                        try {
                            knocks.addKnockEvent(addBatteryInfoToKnock(new KnockEvent(Integer
                                    .parseInt(hid), knockStatus, location)));
                        } catch (NumberFormatException e) {
                            LOGGER.error("Unable to format HID for knockevent: "
                                    + hid);
                        }
                    }

                    if (knocks.hasKnocks()) {
                        try {
                            GrassrootsRestClient.getClient().saveKnocks(knocks);
                        } catch (Exception exception) {
                            logoutWithError(
                                    getString(R.string.error_unable_to_send_knock_status),
                                    exception);
                        }
                    }
                }

                statusDialog.cancel();
                onBackPressed();
            }

            @Override
            public void onCancel() {
                statusDialog.cancel();
            }
        });

        statusDialog.setContentView(view);
        statusDialog.show();
    }

    /**
     * Begin survey for the specified subject
     * @param subject - a subject which will be surveyed
     */
    protected void beginProcessSubject(Subject subject) {
    	Calendar cl = Calendar.getInstance();
		int month = cl.get(Calendar.MONTH)+1;
		int day = cl.get(Calendar.DAY_OF_MONTH);
		int year = cl.get(Calendar.YEAR);
		int hour = cl.get(Calendar.HOUR_OF_DAY);
		int min = cl.get(Calendar.MINUTE);
		int sec = cl.get(Calendar.SECOND);
		
		petitionAnswers.setSurveyStart(month + "-" + day + "-" + year + ", "
				+ hour + ":" + min + ":" + sec);
		
		LOGGER.debug("SURVEY START: " + petitionAnswers.getSurveyStart());
        setSubject(subject);
        startActivity(getNextActivity());
    }

    /**
     * Display WalklistActivityWithTabs with selected Map tab.
     */
    private void switchToMap() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), BaseWalklistActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(BaseWalklistActivity.SHOULD_SHOW_MAP_TAB, true);
        startActivity(intent);
    }


    // ********************************* Options Menu Staff *********************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(R.string.logout_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        //commenting out this in assumption that marketing materials wont work in dart
        //menu.add(R.string.marketing_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        menu.add(R.string.manual_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Resources resources = getResources();
        if (item.getTitle().toString().equals(resources.getString(R.string.logout_button_title))) {
            showLogoutDialog();
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.marketing_button_title))) {
            onMarketing();
            return true;
        } else if (item.getTitle().toString().equals(resources.getString(R.string.manual_button_title))) {
            beginProcessSubject(new Subject());
            return true;
        }

        return false;
    }
    @Override
    public void onBackPressed() {
       
        clearAllAnswers();
        if(isWalklistAssigned()) {
            Class<? extends Activity> nextActivity = ClassRetriever.getWalklistActivity(getApplicationContext());
            if (null != subject && subject.isValid()) {
                Pair<String, Parcelable> extraParameterForTheFirstActivity = new Pair<String, Parcelable>(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT, subject);
                startActivity(nextActivity, extraParameterForTheFirstActivity);
            } else {
                startActivity(nextActivity);
            }
        } else {
            showLogoutDialog();
        }
    }
}
