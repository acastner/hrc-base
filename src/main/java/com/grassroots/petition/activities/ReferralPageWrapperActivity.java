package com.grassroots.petition.activities;

import android.os.Bundle;
import android.view.View;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.views.ReferralPageWrapperView;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class ReferralPageWrapperActivity extends BasePetitionActivityWithMenu {
    public static final String TAG = ReferralPageWrapperActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    public static final String CURRENT_QUESTION = "CURRENT_QUESTION";
    public static final String DISPLAYED_QUESTION_NUMBER = "DISPLAYED_QUESTION_NUMBER";
    private ReferralPageWrapperView.ViewListener viewListener = new ReferralPageWrapperView.ViewListener() {
        @Override
        public void onNext(Map<String, String> referralMapping) {
            ReferralPageWrapperActivity.this.onNext(referralMapping);
        }

        @Override
        public void onEndInterview() {
            ReferralPageWrapperActivity.this.onEndInterview();
        }

		@Override
		public void onBack() {
			ReferralPageWrapperActivity.this.onBackPressed();
			
		}
    };

    protected void onNext(Map<String, String> referralMapping) {
        boolean anyQuestionsAnswered = addReferralAnswers(referralMapping);
        LOGGER.debug("Finished question: " + currentQuestion);
        currentQuestion++;
        displayedQuestionNumber++;
        boolean hasMoreQuestions = referrerIdsToQuestions.containsKey(currentQuestion);
        if (hasMoreQuestions && anyQuestionsAnswered) {
            view.clearReferralData(displayedQuestionNumber);
        } else {
            //so onback we still are at the previous question number
            currentQuestion--;
            displayedQuestionNumber--;
            goToNextActivity();
        }
    }

    protected void goToNextActivity() {
        startActivity(getNextActivity());
    }

    protected void onEndInterview() {
        startActivity(ClassRetriever.getTerminationActivity(getApplicationContext()));
    }

    private boolean addReferralAnswers(Map<String, String> referralMapping) {
        boolean anyQuestionsAnswered = false;
        List<Question> referralQuestions = referrerIdsToQuestions.get(currentQuestion);
        for (Question referralQuestion : referralQuestions) {
            String referralPostScript = referralQuestion.getReferrerPostScript();
            String questionAnswer = referralMapping.get(referralPostScript);
            if (Strings.isEmpty(questionAnswer)) {
                continue;
            }
            anyQuestionsAnswered = true;
            SubjectAnswer referralAnswer = new SubjectAnswer();
            referralAnswer.setQuestionId(referralQuestion.getId());
            referralAnswer.setStringAnswer(questionAnswer);
            petitionAnswers.addAnswer(referralAnswer);
        }
        return anyQuestionsAnswered;
    }

    private ReferralPageWrapperView view;
    private Map<Integer, List<Question>> referrerIdsToQuestions;
    private int currentQuestion = -1;
    private int displayedQuestionNumber = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        referrerIdsToQuestions = petition.getReferrerQuestions();
        findStartingQuestionId();
        onRestoreInstanceState(savedInstanceState);

        view = (ReferralPageWrapperView) View.inflate(this, R.layout.referral_page_wrapper, null);
        view.setViewListener(viewListener);
        view.clearReferralData(displayedQuestionNumber);
        setContentView(view);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(CURRENT_QUESTION))
                currentQuestion = savedInstanceState.getInt(CURRENT_QUESTION);
            if (savedInstanceState.containsKey(DISPLAYED_QUESTION_NUMBER))
                displayedQuestionNumber = savedInstanceState.getInt(DISPLAYED_QUESTION_NUMBER);
            // We don't need to save answers anymore
            //if (savedInstanceState.containsKey(PetitionAnswers.PETITION_ANSWER_KEY))
            //    petitionAnswers = savedInstanceState.getParcelable(PetitionAnswers.PETITION_ANSWER_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle instanceState) {
        instanceState.putInt(CURRENT_QUESTION, currentQuestion);
        instanceState.putInt(DISPLAYED_QUESTION_NUMBER, displayedQuestionNumber);
        // We don't need to restore answers anymore
        //instanceState.putParcelable(PetitionAnswers.PETITION_ANSWER_KEY, petitionAnswers);
    }

    private void findStartingQuestionId() {
        for (int i = 0; i < Question.NUMBER_OF_REFERRERS; i++) {
            if (referrerIdsToQuestions.containsKey(i)) {
                currentQuestion = i;
                break;
            }
        }
        if (currentQuestion == -1) {
            LOGGER.error("Couldn't find current question id, going to next activity");
            goToNextActivity();
        }
    }

    @Override
    public void onReload() {
        super.onReload();
        view.clearReferralData(displayedQuestionNumber);
    }
}
