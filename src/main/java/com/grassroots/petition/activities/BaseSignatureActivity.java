package com.grassroots.petition.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.views.SignatureView;
import com.grassroots.petition.R;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class BaseSignatureActivity extends BasePetitionActivityWithMenu {
    private SignatureView.ViewListener viewListener = new SignatureView.ViewListener() {
        @Override
        public void onEndInterview() {
            BaseSignatureActivity.this.onEndInterview();
        }

        @Override
        public void onNext(Signature signature) {
            BaseSignatureActivity.this.onNext(signature);
        }

        @Override
        public void onMarketing() {
            BaseSignatureActivity.this.onMarketing();
        }
    };

    @Override
    public void onReload() {
        super.onReload();
        view.clearSignature();
    }

    protected void onNext(Signature signature) {
        petitionAnswers.setSignature(signature);
        startActivity(getNextActivity());
    }

    private SignatureView view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = (SignatureView) View.inflate(this, getLayout(), null);
        view.setViewListener(viewListener);
        setContentView(view);

        TextView header = (TextView) findViewById(R.id.header_title);
        header.setText( getString( R.string.signature_header ) );
    }

    protected int getLayout() {
        return R.layout.signature;
    }
}
