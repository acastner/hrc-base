package com.grassroots.petition.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.DeviceUtils;
import com.grassroots.utils.StringPreference;
import com.grassroots.utils.SaleTokenPreference;


import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import static com.grassroots.petition.activities.BaseDonationActivity.RECURRING_NO;


/**
 * Created by jason on 5/11/2017.
 */

public class SpreedlyPaymentActivity extends BaseCreditCardSwipeActivity {

    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!DeviceUtils.isNetworkAvailable(context)) {
                notifyUser("Error: Network connection lost or unstable. Please press \"Back\"");
            }
        }
    };
    WebView webview;
    ProgressDialog progressBar;
    private boolean isPageLoadedComplete = false;
    String paymentUrl = null;
    private static final String SPREEDLY_URL_KEY = "SPREEDLY_URL_KEY";
    private static final String defaultUrl = "http://staging.grassrootsunwired.com/PaymentRequest.aspx?amount=50&currency=USD&scriptId=476&paymentMethodType=1&paymentToken=blablablatoken&title=Title&name=JuanDiego";
    public static void start(Context context) {
        Intent start = new Intent(context, SpreedlyPaymentActivity.class);
        context.startActivity(start);
    }


    @Override
    public void onResume() {
        super.onResume();
        if(!DeviceUtils.isNetworkAvailable(this)) {
            onPaymentResults(SPREEDLY_NO_CONNECTION);
        }
    }

    /*
    PaymentRequest.aspx?scriptId={Script ID}&localToken={Local Token}&amount={Amount}&currency={Currency}&name={Name}
    */

    String getPaymentUrl() {

        String paymentUrll = getResources().getString(R.string.base_url_canvass);
        paymentUrll = paymentUrll.substring(0, paymentUrll.lastIndexOf('/') + 1);

        Double cost=0.0;

        cost = Double.parseDouble(getCart().getItems().get(0).getCost());



            Log.i("COST URL",String.valueOf(cost));
            paymentUrll = paymentUrll +
                    String.format("PaymentRequest.aspx?scriptId=%d&localToken=%s&amount=%s&currency=USD&imei=%s&recurringPattern=%s&PaymentProcessorTypeId=%d&name=%s",
                            petition.getId(), saleTokenPreference.get(),cost.toString(),
                            DeviceUtils.getIMEI(this),getCart().getRecurring(),getCart().getPaymentProcessorTypeId(),getSubject().getFirstName()+ " " +getSubject().getLastName());
            Log.i("paymenturl", paymentUrll);
            return paymentUrll;

    }

    String recurring = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((GlobalData) this.getApplication()).getApplicationComponent().inject(this);
        setContentView(R.layout.report_detail_webview);

        try {
            recurring = getIntent().getStringExtra("RECURRING_TYPE");
            if(recurring.equalsIgnoreCase(RECURRING_NO)) {
                //recurring = "ONE-TIME";
            }

        } catch(Exception ex) {
        }

        //saleTokenPreference.set(java.util.UUID.randomUUID().toString());

        paymentUrl = getPaymentUrl();

        Log.e("report url", "sk payment urll: " + paymentUrl);
        webview = (WebView) findViewById(R.id.webview);


        Map<String, String> extraHeaders = new HashMap<>();

        extraHeaders.put("IsMobileApp", "true");
        String username = GrassrootsRestClient.getClient().getUsername();
        String password = GrassrootsRestClient.getClient().getPassword();
        String encoded = Base64.encodeToString((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);

        extraHeaders.put("Authorization","Basic " + encoded);


        webview.getSettings().setJavaScriptEnabled(true);
        progressBar = ProgressDialog.show(this, "Please wait", "Loading...");
        Timer myTimer = new Timer();
        //Start this timer when you create you task
        myTimer.schedule(new SpreedlyPaymentActivity.LoaderTask(), 60000);

        webview.setWebViewClient(new SpreedlyPaymentActivity.AppWebViewClients(progressBar));

        webview.loadUrl(paymentUrl, extraHeaders);

        //webview.loadUrl("https://theberrics.com");




        TextView headerTitle = (TextView) findViewById(R.id.header_title);
        if (null != headerTitle) {
            if(petition.isTraining())   {
                headerTitle.setText("Credit/Debit - Manual Entry TRAINING PAYMENT");
                headerTitle.setTextColor(Color.RED);
            }
            else {
                headerTitle.setText("Spreedly");
            }
        }
        Button backButton = (Button) findViewById(R.id.back_button);
        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(v -> finish());
        backButton.setOnClickListener(v -> finish());

        setRequestedOrientation(android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //This puts the screen in portrait mode. BTM Customization
        nextButton.setVisibility(android.view.View.INVISIBLE);//BTM Customization

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(networkStateReceiver, filter);
    }

    private Double getAdditionalContributionInt() {

        Double additionalContribution=0.0;

        try {
            additionalContribution = Double.parseDouble(getIntent().getStringExtra("ADDITIONAL_CONTRIBUTION"));
            Log.i("Get additional ", String.valueOf(additionalContribution));
        }
        catch (Exception ex){}

        return additionalContribution;
    }

    private Double getDiscount() {
        Double discount = 0.0;
        try {
            discount = Double.parseDouble(getIntent().getStringExtra("DISCOUNTCODE"));
        }   catch (Exception e) {
            e.printStackTrace();
        }
        return discount;
    }
    private class LoaderTask extends TimerTask {
        public void run() {
            System.out.println("Times Up");
            if(isPageLoadedComplete){
            }else{
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                    progressBar = null;
                    webview.setEnabled(true);
                }

                SpreedlyPaymentActivity.this.runOnUiThread(() -> Toast.makeText(SpreedlyPaymentActivity.this, "The request is timed out", Toast.LENGTH_LONG).show());

            }
        }
    }

    public static final String SPREEDLY_CANCEL = "PaymentResult.aspx?action=cancel";
    public static final String SPREEDLY_ERROR = "PaymentResult.aspx?error=true";
    public static final String SPREEDLY_SUCCESS = "PaymentResult.aspx?localToken=";
    public static final String SPREEDLY_NO_CONNECTION = "NO_CONNECTION";

    public static final String SPREEDLY_RESULT = "SPREEDLY_RESULT";

    public static final int SPREEDLY_CANCEL_CODE = 62;
    public static final int SPREEDLY_ERROR_CODE = 63;
    public static final int SPREEDLY_SUCCESS_CODE = 64;
    public static final int SPREEDLY_NO_CONNECTION_CODE = 65;

    public void onPaymentResults(String newUrl) {
        //Toast.makeText(this, String.format("New url requested: %s", newUrl), Toast.LENGTH_LONG).show();

        Intent resultIntent = getIntent();

        /*
        if(!getIntent().getBooleanExtra("requestCode", false))
        {
            if(SubmitActivity.paymentRedoStatus == 1)
            {
                SubmitActivity.paymentRedoStatus = 0;
                startActivity(new Intent(this,SubmitActivity.class).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
            }
        } else {
        */


        if (newUrl.contains(SPREEDLY_CANCEL)) {
            resultIntent.putExtra(SPREEDLY_RESULT, SPREEDLY_CANCEL_CODE);
            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        } else if (newUrl.contains(SPREEDLY_ERROR)) {
            resultIntent.putExtra(SPREEDLY_RESULT, SPREEDLY_ERROR_CODE);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        } else if (newUrl.contains(SPREEDLY_SUCCESS)) {
            getCart().setPaymentDataToCard();
            petition.setPaymentProcessorId(5);
            getCart().setSpreedly(true);
            resultIntent.putExtra(SPREEDLY_RESULT, SPREEDLY_SUCCESS_CODE);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        } else if (newUrl.contains(SPREEDLY_NO_CONNECTION)) {
            resultIntent.putExtra(SPREEDLY_RESULT, SPREEDLY_NO_CONNECTION_CODE);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }



    }

    private class AppWebViewClients extends WebViewClient {
        private ProgressDialog progressBar;
        private boolean isSuccess = true;
        public AppWebViewClients(ProgressDialog progressBar) {
            this.progressBar = progressBar;
            this.progressBar.show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            Log.e("URL", url);
            if(url.contains("sprr")) {
                SpreedlyPaymentActivity.this.onPaymentResults(url);
            } else {
                SpreedlyPaymentActivity.this.onPaymentResults(url);
                view.loadUrl(url);
            }

            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
        if(isSuccess) {
            super.onPageFinished(view, url);
            progressBar.hide();
            isPageLoadedComplete = true;
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            progressBar.dismiss();
            isSuccess = false;
            Log.i("weberror", String.valueOf(errorCode));
            notifyUser("Error: Network connection lost/unstable");
            SpreedlyPaymentActivity.this.finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(networkStateReceiver);
    }
}
