package com.grassroots.petition.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.services.GrassrootsRestClient;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ReportDetailWebviewActivity extends BasePetitionActivity {
    WebView webview;
    ProgressDialog progressBar;
    private boolean isPageLoadedComplete = false;
    //String reportDetailUrl = "http://testing.grassrootsunwired.com/reports/MobileReport.aspx?name=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String reportDetailUrl = getGlobalData().getBaseUrl();
        reportDetailUrl = reportDetailUrl.replace("/api", "");
        reportDetailUrl = reportDetailUrl+"/reports/MobileReport.aspx?name=";

        Log.e("report url", "sk report urll: "+reportDetailUrl);
        setContentView(R.layout.report_detail_webview);
        String reportname = getIntent().getStringExtra("reportname");

        reportDetailUrl = reportDetailUrl.concat(reportname);
        Log.e("report url", "sk report urll: "+reportDetailUrl);
        webview = (WebView) findViewById(R.id.webview);
        webview.clearCache(true);
        webview.clearHistory();
        GlobalData.clearCookies(this.getApplicationContext());


        Map<String, String> extraHeaders = new HashMap<String, String>();

        String username = GrassrootsRestClient.getClient().getUsername();
        String password = GrassrootsRestClient.getClient().getPassword();
        String encoded = Base64.encodeToString((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);

        extraHeaders.put("Authorization","Basic " + encoded);
        reportDetailUrl = reportDetailUrl.replace(" ", "%20");
        webview.getSettings().setJavaScriptEnabled(true);
        progressBar = ProgressDialog.show(this, "Please wait", "Loading...");
        Timer myTimer = new Timer();
        //Start this timer when you create you task
        myTimer.schedule(new LoaderTask(), 60000);

        webview.setWebViewClient(new AppWebViewClients(progressBar));

        webview.loadUrl(reportDetailUrl, extraHeaders);


        TextView headerTitle = (TextView) findViewById(R.id.header_title);
        if (null != headerTitle) {
            headerTitle.setText(reportname + " " + getLocationName());
        }
        Button backButton = (Button) findViewById(R.id.back_button);
        Button nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private class LoaderTask extends TimerTask {
        public void run() {
            System.out.println("Times Up");
            if(isPageLoadedComplete){
            }else{
                if ((progressBar != null) && progressBar.isShowing()) {
                    progressBar.dismiss();
                    progressBar = null;
                    webview.setEnabled(true);
                }

                ReportDetailWebviewActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ReportDetailWebviewActivity.this, "The request is timed out", Toast.LENGTH_LONG).show();
                    }
                });

            }
        }
    }

    private class AppWebViewClients extends WebViewClient {
        private ProgressDialog progressBar;

        public AppWebViewClients(ProgressDialog progressBar) {
            this.progressBar = progressBar;
            this.progressBar.show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.hide();
            isPageLoadedComplete = true;
        }
    }




}



