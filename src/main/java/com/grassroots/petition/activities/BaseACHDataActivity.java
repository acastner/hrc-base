package com.grassroots.petition.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.ACHData;
import com.grassroots.petition.models.Cart;
import com.grassroots.utils.SaleTokenPreference;
import com.grassroots.utils.StringPreference;

import javax.inject.Inject;

public class BaseACHDataActivity extends BasePetitionActivityWithMenu {
    protected EditText routingNumberEditText;
    protected EditText accountNumberEditText;

    protected Button nextButton;
    protected Button endInterviewButton;

    protected ImageView photoImageView;

    protected @Inject
    @SaleTokenPreference
    StringPreference saleTokenPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((GlobalData) getApplication()).getApplicationComponent().inject(this);
        setContentView(getLayout());

        saleTokenPreference.set(java.util.UUID.randomUUID().toString());

        ((TextView) findViewById(R.id.question_page_header_title)).setText(R.string.ach_data_header_title);

        routingNumberEditText = (EditText) findViewById(R.id.ach_data_routing_number_editText);
        accountNumberEditText = (EditText) findViewById(R.id.ach_data_account_number_editText);
        routingNumberEditText.setNextFocusDownId( accountNumberEditText.getId() );

        nextButton = (Button) findViewById(R.id.next_button);
        endInterviewButton = (Button) findViewById(R.id.terminate_button);
        if(endInterviewButton != null)
        endInterviewButton.setVisibility(View.VISIBLE);
        findViewById(R.id.back_button).setVisibility( View.GONE );

        photoImageView = (ImageView) findViewById(R.id.sample_check_imageView);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseACHDataActivity.this.onNext();
            }
        });
        if(endInterviewButton != null){
        endInterviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseACHDataActivity.this.onEndInterview();
            }
        });
        }
    }

    protected int getLayout() {
        return R.layout.default_ach_data;
    }

    @Override
    public void onReload()
    {
        super.onReload();

        routingNumberEditText.setText("");
        accountNumberEditText.setText("");

        photoImageView = (ImageView) findViewById(R.id.sample_check_imageView);
    }

    @Override
    public void onResume() {
        super.onResume();
        saleTokenPreference.set(java.util.UUID.randomUUID().toString());
    }

    public void onNext() {
        // validate fields
        String routingNumber = routingNumberEditText.getText().toString();
        if (0 == routingNumber.length()|| 9 > routingNumber.length()) {
            notifyUser(getString(R.string.ACH_DATA_INVALID_ROUTING_NUMBER_MESSAGE));
            return;
        }

        String accountNumber = accountNumberEditText.getText().toString();
        if (0 == accountNumber.length()) {
            notifyUser(getString(R.string.ACH_DATA_INVALID_ACCOUNT_NUMBER_MESSAGE));
            return;
        }

        try {
            // FIXME: wrong way to check if all chars are numeric.
            //Integer routingNumberInteger = Integer.parseInt(routingNumber);
        } catch (NumberFormatException e) {
            notifyUser(getString(R.string.INVALID_ROUTING_VALUE_MESSAGE));
            return;
        }

        Cart cart = getCart();
        if (null == cart || 0 == cart.getItems().size()) {
            notifyUser(getString(R.string.NO_PRODUCT_WAS_SELECTED_MESSAGE));
            return;
        }

        cart.setPaymentData(new ACHData(routingNumber, accountNumber, "Checking"));

        startActivity(getNextActivity());
    }
}
