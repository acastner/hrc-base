package com.grassroots.petition.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.grassroots.petition.models.*;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.TerminateQuestionsView;
import com.grassroots.petition.R;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class TerminateQuestionsActivity extends BasePetitionActivity {
    private static final String TAG = BaseLoginActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private TerminateQuestionsView view;
    private Subject subject;
    private final TerminateQuestionsView.ViewListener viewListener = new TerminateQuestionsView.ViewListener() {
        @Override
        public void onReview(List<SubjectAnswer> answers) {
            TerminateQuestionsActivity.this.onReview(answers);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = (TerminateQuestionsView) View.inflate(this, R.layout.terminate_questions, null);
        view.setViewListener(viewListener);
        subject = getSubject();
        view.setQuestions(petition.getTerminatedQuestions(), petitionAnswers);
        setContentView(view);
    }

    @Override
    public void onReload() {
        super.onReload();
        subject = getSubject();
        view.setQuestions(petition.getTerminatedQuestions(), petitionAnswers);
    }

    public void onReview(List<SubjectAnswer> answers) {

    	getCart().removeAllProducts();
    	getCart().clearPaymentData();
    	LOGGER.debug("PAYMENT DATA erased products");
    	
        petitionAnswers.setGpsLocation(LocationProcessor.getLastKnownGpsLocation(getApplicationContext()));
        petitionAnswers.convertPhotoFilesToAnswers(this);
        petitionAnswers.mergeInAnswersAndSetIdFields(petition.getSubjectInformationQuestions(), getSubject());
        petitionAnswers.addAnswers(answers);

        Intent subjectreview = new Intent(TerminateQuestionsActivity.this,SubmitActivity.class);
        startActivity(subjectreview);
    }

    
    @Override
    public void onBackPressed()
    {  
    	super.onBackPressed();
    	Intent subjectIntent = new Intent(TerminateQuestionsActivity.this,ClassRetriever.getSubjectInformationActivity(this));
    	subjectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(subjectIntent);
    }
}
