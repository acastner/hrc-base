package com.grassroots.petition.activities;

import android.os.Bundle;
import com.grassroots.petition.R;
import android.widget.TextView;

/**
 *  Activity to show information about credit card safety.
 */
public class BaseAboutCreditCardActivity extends BasePetitionActivityWithMenu {

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.about_card_safety);

        TextView header = (TextView) findViewById(R.id.header_title);
        header.setText( getString( R.string.about_credit_card_safety_title ) );
    }
}
