package com.grassroots.petition.activities;

import org.apache.log4j.Logger;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.CardData;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.views.SwipeView;
import com.grassroots.utils.SaleTokenPreference;
import com.grassroots.utils.StringPreference;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: mac-202
 * Date: 16.05.13
 * Time: 12:11
 * To change this template use File | Settings | File Templates.
 */
public class BaseCreditCardSwipeActivity extends BasePetitionActivityWithMenu {
    private static final String TAG = BaseCreditCardSwipeActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    protected Button nextButton;
    protected Button endInterviewButton;
    protected Button aboutButton;
    protected Button resetButton;
    protected ImageView creditCardImageView;
    protected SwipeView swipeView;
    protected CardData cardData;
    //	check headset status plugged or not 
	final headSetBroadCastReceiver mHeadsetReceiver = new headSetBroadCastReceiver();
	
	//	check audio noisy
	final NoisyAudioStreamReceiver mNoisyAudioStreamReceiver = new NoisyAudioStreamReceiver();

    protected SwipeView.ViewListener swipeViewListener = new SwipeView.ViewListener() {
        @Override
        public void onReceiveCardData(CardData cardData) {
            BaseCreditCardSwipeActivity.this.onReceiveCardData(cardData);
            BaseCreditCardSwipeActivity.this.onNext();
        }

        @Override
        public void onEndInterview() {
            BaseCreditCardSwipeActivity.this.onEndInterview();
        }

        @Override
        public void onMarketing() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
       
    };

    protected @Inject @SaleTokenPreference StringPreference saleTokenPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((GlobalData) getApplication()).getApplicationComponent().inject(this);
        GlobalData.clearMarketingSlides();
        setContentView(getLayout());

    	LOGGER.debug( "onCreate()");

        cardData = null;

        saleTokenPreference.set(java.util.UUID.randomUUID().toString());

        ((TextView) findViewById(R.id.question_page_header_title)).setText(R.string.credit_card_data_header_title);

        nextButton = (Button) findViewById(R.id.next_button);
        endInterviewButton = (Button) findViewById(R.id.terminate_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseCreditCardSwipeActivity.this.onNext();
            }
        });
        endInterviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseCreditCardSwipeActivity.this.onEndInterview();
            }
        });

        aboutButton = (Button) findViewById(R.id.about_card_safety_button);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseCreditCardSwipeActivity.this.onAbout();
            }
        });

        resetButton = (Button) findViewById(R.id.card_reset_swipe_button);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseCreditCardSwipeActivity.this.onResetSwipe();
            }
        });

        swipeView = (SwipeView) findViewById(R.id.swipe_view);
        swipeView.setViewListener(swipeViewListener);


        creditCardImageView = (ImageView) findViewById(R.id.credit_card_instructions_imageView);
        //creditCardImageView.setImageResource(R.drawable.logo_small);// TODO change to Credit Card image
    }

    protected int getLayout() {
        return R.layout.default_credit_card_swipe;
    }
    
    @Override
    public void onPause() {
    	super.onPause();
        swipeView.closeReader();
    }

    @Override
    public void onReload(){
        super.onReload();

        cardData = null;
        swipeView.openReader();
    }
    @Override
    public void onResume() {
        super.onResume();

        //saleTokenPreference.set(java.util.UUID.randomUUID().toString());

		// based on MagTekDemo.java
		registerReceiver(mHeadsetReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
		registerReceiver(mNoisyAudioStreamReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
        swipeView.openReader();

        Cart cart = getCart();
        LOGGER.debug("CART INFO: " + cart.getPaymentData());
    }


    public void onReceiveCardData(CardData cardData){
        this.cardData = cardData;
    }

    @Override
    public void onDestroy()
    {
    	LOGGER.debug("onDestroy()");
    	
		// based on MagTekDemo.java
		unregisterReceiver(mHeadsetReceiver);
		unregisterReceiver(mNoisyAudioStreamReceiver);
		
		swipeView.closeReader();
    	
        super.onDestroy();
    }



    public void onNext() {

        if (cardData == null) {
            notifyUser(getString(R.string.SWIPE_YOUR_CREDIT_CARD_MESSAGE));
            return;
        }

        Cart cart = getCart();
        if (null == cart || 0 == cart.getItems().size()) {
            notifyUser(getString(R.string.NO_PRODUCT_WAS_SELECTED_MESSAGE));
            return;
        }

        swipeView.closeReader();

        cart.setPaymentData(cardData);

        startActivity(getNextActivity());
    }

    @Override
    public void onEndInterview() {
        swipeView.closeReader();
        super.onEndInterview();
    }

    protected void onAbout() {
        swipeView.closeReader();
        //createAboutCardSafetyDialog(this, null).show();
        startActivity(BaseAboutCreditCardActivity.class);
    }

    protected void onResetSwipe() {
        cardData = null;
        swipeView.resetReader();
    }

    @Override
    public void onBackPressed()
    {       
        //super.onBackPressed();
        new AlertDialog.Builder(this)
        .setTitle("Go back again?")
        .setMessage("If “Yes”, your card information will be erased.  To complete any donation, you’ll need to reswipe.")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            	swipeView.closeReader();
            	BaseCreditCardSwipeActivity.super.onBackPressed();
            }
         })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // do nothing
            }
         })
        .setIcon(android.R.drawable.ic_dialog_alert)
         .show();
    }

    /**
 	 * Check audio become noisy or not
	 *  Once received noisy it closes MagTek Reader via SwipeView/CardMag  
	 * copied from MagTekDemo.java
	 */
	private class NoisyAudioStreamReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
//			 If the device is unplugged, this will immediately detect that action,
//			 * and close the device.

			LOGGER.warn("Reader unplugged");
			 
			if(AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction()))
			{
                LOGGER.warn("Noisy audio detected");

                try {
                    if (swipeView.isMagtek) {
                        swipeView.cardSwipeReaderMagtek.setHeadsetPluggedIn(false);
                    }
                    swipeView.closeReader();
                    swipeView.setStatus(getString(R.string.CARD_READER_IS_UNPLUGGED_MESSAGE));
                } catch(Exception ex) {
                    LOGGER.error("Exception from noisy audio call!", ex);
                    swipeView.setStatus("Card reader is unplugged, please connect!");
                }
            }
		}
	}
	
	/**
	 * 
	 * Check headset/microphone plugged or not
	 *	once received, it will open(plugged) or close(unplugged) MagTek reader via SwipeView/CardMag 
	 * copied from MagTekDemo.java
	 */
	private class headSetBroadCastReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {

			LOGGER.debug("headSetBroadCastReceiver");

			try
			{
				String action = intent.getAction();
				LOGGER.info("action=" + action);
				if( (action.compareTo(Intent.ACTION_HEADSET_PLUG))  == 0)   //if the action match a headset one
				{
					
					int headSetState = intent.getIntExtra("state", 0);      //get the headset state property
					int hasMicrophone = intent.getIntExtra("microphone", 0);//get the headset microphone property

					if( (headSetState == 1) && (hasMicrophone == 1))        //headset was unplugged & has no microphone
					{
                        LOGGER.info("Headset is plugged in");
                        try {
                            if (swipeView.cardSwipeReaderMagtek != null) {
                                swipeView.cardSwipeReaderMagtek.setHeadsetPluggedIn(true);
                            }
                            swipeView.openReader();
                        } catch(Exception ex) {
                            LOGGER.error("Error in headSetBroadCastReceiver", ex);
                        }
					}
					else 
					{
                        LOGGER.info("Nothing is plugged into the headset");
                        try {
                            swipeView.cardSwipeReaderMagtek.setHeadsetPluggedIn(false);
                            swipeView.closeReader();
                            swipeView.setStatus(getString(R.string.PLUG_IN_READER));
                        } catch (Exception ex) {
                            LOGGER.error("Error in headSetBroadCastReceiver ", ex);
                            swipeView.setStatus("Please plug in a reader");
                        }
					}

				}           

			}
			catch(Exception ex)
			{
                LOGGER.error("Unable to receive something something", ex);
			}

		}

	}	
	
	
	
}
