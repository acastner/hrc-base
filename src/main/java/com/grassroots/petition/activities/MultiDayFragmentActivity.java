package com.grassroots.petition.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import com.grassroots.petition.R;
import com.grassroots.petition.fragments.AddAppointmentDialogFragment;
import com.grassroots.petition.fragments.DayFragment;
import com.grassroots.petition.models.*;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;

/**
 * Parent activity to present a scrollable list of fragments with Day divided by hours and by busy blocks.
 * Implements DayFragment.OnDaySelectedListener interface to create new appointments (using free time interval) and
 * to provide subject appointment created earlier.
 *
 * TODO: Move to strings.xml for user error messages
 * TODO: Consider using calendar instead of Date. Date is deprecated, Calendar is not.
 */
public class MultiDayFragmentActivity extends BasePetitionActivity implements DayFragment.OnDaySelectedListener
{
    private static final String TAG = MultiDayFragmentActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private final String ADD_APPOINTMENT_FRAGMENT_TAG = "AddAppointmentDialogFragment";

    /**
     * Key-name {@value} to send selected date as argument to newly created MultiDayFragmentActivity.
     */
    private static final String SELECTED_DATE_EXTRA = "DATE_EXTRA";

    /**
     * Number of pages to cache
     */
    public static final int NUMBER_OF_PAGES_TO_CACHE = 3;

    /**
     * Date which is used as date which is presented on the screen as available free time
     * intervals and busy boxes (the of date time doesn't matter)
     */
    private Date selectedDate;

    /**
     * An existing Appointment object for current petition (if any)
     */
    private Appointment subjectAppointment;

    /**
     * Adapter to provide DayFragments for each day of month from selectedDate
     */
    DayAdapter dayAdapter;
    /**
     * View pager to scroll between DayFragments (Fragments which represent days of month)
     */
    ViewPager dayViewsPager;
    private AddAppointmentDialogFragment dialog;
    
    private static boolean isCalendarType = false;

    /**
     * #1 Gets selectedDate from bundle by SELECTED_DATE_EXTRA name, otherwise uses current date
     * #2 Creates new DayAdapter to provide DayFragments
     * #3 Sets required fields of newly created DayAdapter
     * #4 Sets newly created DayAdapter for Day ViewPager
     * #5 Switch to DayFragment which represents date from selectedDay
     * #6 Configure "navigation" buttons for current activity
     */
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.day_pager);

        Bundle bundle = getIntent().getExtras();
        if (null != bundle)
        {
            selectedDate = (Date)bundle.getSerializable(SELECTED_DATE_EXTRA);
        } else {
            selectedDate = new Date();
        }

        dayAdapter = new DayAdapter( getSupportFragmentManager() );
        dayAdapter.setDate(selectedDate);

        dayViewsPager = (ViewPager) findViewById( R.id.pager );
        dayViewsPager.setAdapter(dayAdapter);
        dayViewsPager.setPageMargin(NUMBER_OF_PAGES_TO_CACHE);

        subjectAppointment = getAppointment();

        findViewById(R.id.today_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToTodaysDate();
            }
        });

        switchToSelectedDate();
        configureButtons();
    }

    /**
     * Switch Day Views pager to selectedDate
     */
    protected void switchToSelectedDate()
    {
        switchToDate( selectedDate );
    }

    /**
     * Show today's date
     */
    protected void switchToTodaysDate()
    {
        switchToDate( new Date() );
    }

    /**
     * Switch day view pager to specified date
     */
    protected void switchToDate(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );
        int selectedDayOfMonth = calendar.get( Calendar.DAY_OF_MONTH ) - 1;
        dayViewsPager.setCurrentItem(selectedDayOfMonth, false);
    }

    /**
     * Creates new Intent inside specified Context and save specified Bundle inside it.
     * @param context -
     * @param bundle -
     * @param calendarQuestion - describes if part of calendar question type
     */
    public static Intent createIntent( Context context, Bundle bundle , boolean calendarType )
    {
    	isCalendarType = calendarType;
    	
        Intent intent = new Intent( context, MultiDayFragmentActivity.class );
        intent.putExtras(bundle);
        return intent;
    }

    /**
     * Creates new Bundle and stores inside it specified Date object as SELECTED_DATE_EXTRA
     * @param date - Date to store inside Bundle object
     */
    public static Bundle createDayViewBundle( java.util.Date date )
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_DATE_EXTRA, date);
        return bundle;
    }

    /**
     * Implementation of OnDaySelectedListener interface.
     * This method is called when new appointment should be added (the user touched "Add" button for example).
     * Create & show a dialog of AddAppointmentView view, where the user is able to select start time/end time (or duration), add description.
     * If the User creates new appointment, then store this appointment to subjectAppointment, which will be used later in onNext() to save appointment in current Petition object.
     * @param dayFragment - a DayFragment object which called this method.
     * @param selectedTimeInterval - free time interval into which new appointment should be created (startTime and endTime contain actual dates).
     */
    @Override
    public void onAddNewAppointment( DayFragment dayFragment, FreeTimeInterval selectedTimeInterval )
    {
        FreeTimeInterval selectedFreeTimeInterval = appointmentCalendar.getAvailableFreeIntervalForTimeInterval(selectedTimeInterval);
        if (null == selectedFreeTimeInterval)
        {
            notifyUser(getString(R.string.HOUR_IS_BUSY_MESSAGE));
            return;
        }

        long freeTimeIntervalInMinutes = selectedFreeTimeInterval.getTimeIntervalInMinutes();
        if (freeTimeIntervalInMinutes < petition.getMinimumReservationTime())
        {
            notifyUser(String.format(getString(R.string.SELECTED_FREE_TIME_LESS_THAN_MINIMUM_APPOINTMENT_LENGTH_MESSAGE), freeTimeIntervalInMinutes, petition.getMinimumReservationTime()));
            return;
        }

        // Configure and show add appointment dialog
        dialog = AddAppointmentDialogFragment.newInstance(selectedTimeInterval.getStartTime(),
                                                  petition.getMinimumReservationTime(),
                                                  petition.getSuggestedReservationTime(),
                                                  petition.getMaximumReservationTime());
        dialog.setAppointmentAddedListener(createTimeChangedListener());
        dialog.show(getSupportFragmentManager(), ADD_APPOINTMENT_FRAGMENT_TAG);
    }

    private AddAppointmentDialogFragment.ViewListener createTimeChangedListener() {
        return new AddAppointmentDialogFragment.ViewListener() {
            @Override
            public void onAddNewAppointment(EventTimeInterval event) {
                subjectAppointment = new Appointment(event);
                dialog.dismiss();
                MultiDayFragmentActivity.this.onNext();
            }
        };
    }

    /**
     * Adapter to provide DayFragments for each day of month from selectedDate
     */
    public static class DayAdapter extends FragmentPagerAdapter
    {
        /**
         * Number of day pages to show at the same time
         */
        public static final float NUMBER_OF_PAGES_TO_SHOW_SIMULTANEOUSLY = 0.33f;

        /**
         * Date which is used to get month, for each day of which generates DayFragment
         */

        protected Date selectedDate;
        /**
         * Number of month from selectedDate
         */

        protected int monthNumber;
        /**
         * Number of days in month from selectedDate
         */

        protected int numDaysInMonth;
        /**
         * an OnDaySelectedListener listener which should be set for each DayFragment
         */

        protected DayFragment.OnDaySelectedListener fragmentListener;
        /**
         * Constructor for DayAdapter, which sets selectedDate as current date
         */
        public DayAdapter(FragmentManager fragmentManager)
        {
            super( fragmentManager );

            Calendar calendar = Calendar.getInstance();
            selectedDate = calendar.getTime();
        }

        public void setDate(Date selectedDate)
        {
            this.selectedDate = selectedDate;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(selectedDate);
            monthNumber = calendar.get(Calendar.MONTH);
            this.numDaysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        @Override
        public int getCount()
        {
            return numDaysInMonth;
        }

        /**
         * Returns a float that represents how many pages to show at once.
         */
        @Override
        public float getPageWidth( int position )
        {
            return NUMBER_OF_PAGES_TO_SHOW_SIMULTANEOUSLY;
        }

        /**
         * Creates DayFragment for specified day of month (from 0 to numDaysInMonth) for month from provided selectedDate
         * Sets provided OnDaySelectedListener for newly created DayFragment object
         * Sets provided Appointment object for newly created DayFragment object
         */
        @Override
        public Fragment getItem( int dayOfMonth )
        {
            if (null != selectedDate)
            {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(selectedDate);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.add(Calendar.DAY_OF_MONTH, dayOfMonth);

                DayFragment fragment = DayFragment.newInstance( calendar.getTime() );
                if (null != fragmentListener) {
                    fragment.setOnDaySelectedListener(fragmentListener);
                }

                return fragment;
            }

            return null;
        }
    }

    /**
     * Sets handlers for general "navigation" buttons:
     * back_button
     * next_button
     * skip_button
     * marketing_material_button
     */
    private void configureButtons()
    {
        Button endInterviewButton = (Button) findViewById(R.id.back_button);
        if (null != endInterviewButton) {
            endInterviewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBack();
                }
            });
        }

        Button nextButton = (Button) findViewById(R.id.next_button);
        if (null != nextButton) {
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNext();
                }
            });
        }

        Button skipButton = (Button) findViewById(R.id.skip_button);
        if (null != skipButton) {
        	if(isCalendarType)
        	{
        		skipButton.setVisibility(View.GONE);
        	}
        	
            skipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSkip();
                }
            });
        }

        Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
        if (null != marketingButton) {
            marketingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onMarketing();
                }
            });
        }
    }

    private void onBack()
    {
        onBackPressed();
    }

    /**
     * Handler for next_button
     * if no Appointment object exists for current subject/petition, then notify the user
     * otherwise save Appointment object (can be only one for subject/petition) in current Petition
     * and start next activity
     */
    private void onNext()
    {
        if (null == subjectAppointment)
        {
            notifyUser(getString(R.string.ADD_NEW_APPOINTMENT_MESSAGE));
            return;
        }

        // save new appointment to petition
        setAppointment(subjectAppointment);

        
        if(isCalendarType)
        {
        	isCalendarType = false; //reset
        	setResult(RESULT_OK, getIntent());
            finish();
        }
        else
        {
            startActivity(getNextActivity());
        }
  
    }
}