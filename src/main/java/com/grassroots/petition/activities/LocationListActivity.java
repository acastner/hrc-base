package com.grassroots.petition.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.adapters.LocationListAdapter;
import com.grassroots.petition.models.Location;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.tasks.UpdateLocationsListTask;

public class LocationListActivity extends BasePetitionActivity {

    private ListView locationListView;
    private LocationListAdapter locationlistAdapter;
    List<Location> locationlist;
    EditText inputSearch = null;
    ArrayAdapter<String> adapter = null;
    private boolean walklistEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_list);
        locationlist = getLocationlist().getLocations();
        
    }
    
    @Override
    public void onResume() {
    	if(GlobalData.shouldLocationReload){
    		UpdateLocationsListTask updatelocationtask = new UpdateLocationsListTask(this);
    		ProgressDialog pd = ProgressDialog.show(this, "Loading..", "Please wait");
    		updatelocationtask.execute();
    		try {
				LocationList locationlistresult = updatelocationtask.get();
				locationlist = locationlistresult.getLocations();
				pd.dismiss();
				GlobalData.shouldLocationReload = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	initViews();

    	super.onResume();
    }

    private void initViews() {
        View headerView = findViewById(R.id.header_view);
        TextView headerTitle = (TextView) headerView
                .findViewById(R.id.header_title);
        if (null != headerTitle) {
            headerTitle.setText(getString(R.string.LOCATIONLIST_TITLE)+ " " + getLocationName());
        }
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        walklistEnabled = petition.isWalklist();
        // set location adapter in listview
        locationListView = (ListView) findViewById(R.id.location_list);
        locationlistAdapter = new LocationListAdapter(this, locationlist,walklistEnabled);
        locationListView.setAdapter(locationlistAdapter);
        locationlistAdapter.notifyDataSetChanged();
        if(locationlist == null){
        	inputSearch.setVisibility(View.GONE);
        }
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                    int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                String flterString = arg0.toString();
                if (flterString == null || flterString.length() == 0) {
                    locationlistAdapter = new LocationListAdapter(
                            LocationListActivity.this, locationlist,walklistEnabled);
                    locationListView.setAdapter(locationlistAdapter);
                    locationlistAdapter.notifyDataSetChanged();
                } else {
                    List<Location> locationsFiltered = new ArrayList<Location>();
                    for (Location location : locationlist) {
                        if (location.getName() != null) {
                            if (location.getName().toLowerCase()
                                    .contains(flterString.toLowerCase())) {
                                Log.d("Search : ",
                                        "matching text changed"
                                                + arg0.toString() + "location"
                                                + location.getName());
                                locationsFiltered.add(location);
                            }
                        }
                    }
                    if (locationsFiltered.size() > 0) {
                        locationlistAdapter = new LocationListAdapter(
                                LocationListActivity.this, locationsFiltered,walklistEnabled);
                        locationListView.setAdapter(locationlistAdapter);
                        locationlistAdapter.notifyDataSetChanged();
                    } else {
                        locationlistAdapter = new LocationListAdapter(
                                LocationListActivity.this, locationlist,walklistEnabled);
                        locationListView.setAdapter(locationlistAdapter);
                        locationlistAdapter.notifyDataSetChanged();
                    }
                }

            }
        });

    }

}
