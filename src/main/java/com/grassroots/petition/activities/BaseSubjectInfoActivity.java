package com.grassroots.petition.activities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.grassroots.modules.GrassrootsApiService;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.R2;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.KnockEvent;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Knocks;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.AAMVA;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;
import com.grassroots.petition.views.HomeStatusView;
import com.grassroots.petition.views.SubjectInfoViewWithButtonsInterface;
import com.grassroots.petition.views.SubjectInformationButtons;
import com.grassroots.utils.AuthenticationHeaderPreference;
import com.grassroots.utils.ImeiHeaderPreference;
import com.grassroots.utils.OAuthTokenPreference;
import com.grassroots.utils.StringPreference;
import com.manateeworks.cameraDemo.ActivityCapture;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */

public class BaseSubjectInfoActivity extends BasePetitionActivityWithMenu {
	private static final String TAG = BaseSubjectInfoActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private static final int PHOTO_ACTION_CODE = 9;
	private static final int VOTER_ACTION_CODE = 3;

	private SubjectInfoViewWithButtonsInterface view;
	protected Subject subject;
	private Button knockButton;
	protected List<Question> questionstoBeSet = null;

	private SubjectInformationButtons.ViewListener viewListener = new SubjectInformationButtons.ViewListener() {

		@Override
		public void onEndInterview(Subject subject) {
			BaseSubjectInfoActivity.this.onEndInterview(subject);
		}

		@Override
		public void onMarketing() {
			BaseSubjectInfoActivity.this.onMarketing();
		}

		@Override
		public void onAssist() {
			reverseGeocode();
		}

		@Override
		public void onNext(Subject subject) {
			goToNext(subject);
		}

		@Override
		public void onMoreOptionsSelected() {
			showAssistDialog();
		}

		@Override
		public void onTally() {
			BaseSubjectInfoActivity.this.onTally();
		}
	};

	protected int getLayout() {
		return R.layout.default_subject_info;
	}

	@Inject @ImeiHeaderPreference public StringPreference imeiPreference;
	@Inject @AuthenticationHeaderPreference public StringPreference authenticationHeaderPreference;
	@Inject @OAuthTokenPreference public StringPreference oAuthTokenPref;
	@Inject public GrassrootsApiService grassrootsApiService;

	@BindView(R2.id.zip_edit) protected EditText zipEdit;
    @BindView(R2.id.street_address_edit) protected EditText address1Edit;
	@BindView(R2.id.city_edit) protected EditText cityEdit;
	@BindView(R2.id.address_2_edit) protected EditText address2Edit;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		((GlobalData) this.getApplication()).getApplicationComponent().inject(this);

		subject = getSubject();
		questionstoBeSet = getPetition().getSubjectInformationQuestions();
		view = (SubjectInfoViewWithButtonsInterface) View.inflate(this, getLayout(), null);
		subject.setOccupation(" ");
		view.setSubject(subject);
		view.setQuestionNameTranslationMap(getTranslateMap());
		for(Question question : questionstoBeSet){
			if(question.getVarNameNo$().equalsIgnoreCase("Location") && getGlobalData().getLocation()==null){
				questionstoBeSet.remove(question);
			}
		}
		Question email = getPetition().getEmailQuestionIfMagic();
		if(email != null){
			questionstoBeSet.add(email);
		}
		view.setSubjectInformationQuestions(questionstoBeSet);
		view.setViewListener(viewListener);
		setContentView((View)view);
		ButterKnife.bind(this);
		hideMarketingIfHasNoMaterials();
		hideTerminateIfNoTermQuestions();


		TextView headerTitle = (TextView) ((View)view).findViewById(R.id.header_title);
		if (null != headerTitle) {
			headerTitle.setText(getString(R.string.subject_info_header) + "-" + getLocationName());
		}
		if(petition.isTraining())	{
			headerTitle.setText(getString(R.string.subject_info_header) + "-" + getLocationName() + "-TRAINING PAYMENTS MODE");
			headerTitle.setTextColor(Color.RED);
		}
		knockButton = (Button) ((View)view).findViewById(R.id.knock_button);
		knockButton.setVisibility(View.GONE);
		updateKnockIfBidirectional();
		updateNearestNeighborIfPresent();


		WebView webViewTitle = (WebView) findViewById(R.id.webView_title);
		WebView webViewQuestion = (WebView) findViewById(R.id.webView_question);

		String titleHTML = "", questionHTML = "";
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");

			for(int i = 0; i< jsonArr.length() ; i++){

				if(jsonArr.getJSONObject(i).getString("Name").equals("SubjectInfo_TitleHTML")){
					titleHTML = jsonArr.getJSONObject(i).getString("Value");

					webViewTitle.setVisibility(View.VISIBLE);					
					webViewTitle.getSettings().setJavaScriptEnabled(true);
					webViewTitle.loadData(titleHTML, "text/html", "UTF-8");
					
					headerTitle.setVisibility(View.GONE);
				}

				if(jsonArr.getJSONObject(i).getString("Name").equals("SubjectInfo_QuestionHTML")){
					questionHTML = jsonArr.getJSONObject(i).getString("Value");

					webViewQuestion.setVisibility(View.VISIBLE);			
					webViewQuestion.getSettings().setJavaScriptEnabled(true);
					webViewQuestion.loadData(questionHTML, "text/html", "UTF-8");
					
					ImageView headerLogo = (ImageView) ((View)view).findViewById(R.id.header_logo);
					headerLogo.setVisibility(View.GONE);
				}

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		displayTrainingMode((View)view);
	}

	private void displayTrainingMode(View view) {
		final LinearLayout warningLayout = (LinearLayout) view.findViewById(R.id.testPaymentsWarningLayout);
		Button button = (Button) view.findViewById(R.id.buttonHideWarning);

		if(petition.isTraining()) {
			warningLayout.setVisibility(View.VISIBLE);
		}
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				warningLayout.setVisibility(View.GONE);
			}
		});
	}

	private void hideTerminateIfNoTermQuestions()
	{
		if(getPetition().getTerminatedQuestions().size() == 0){
			Button termButton = (Button) ((View)view).findViewById(R.id.end_interview_button);

			termButton.setVisibility(View.INVISIBLE);
		}
	}

	private void updateKnockIfBidirectional() {
		if (getPetition().isBidirectional() && (petition.isWalklist() || petition.isLocationList()))
			knockButton.setVisibility(View.GONE);
		else
			knockButton.setVisibility(View.GONE);


		knockButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setSubject(view.getSubject());
				//show the dialog to select status and send the knock event from there.
				if(validateFields()){
					showStatusSelectionDialog();
				}
			}
		});
	}

	private void updateNearestNeighborIfPresent(){
		if(petition.hasNearestNeighborInfo()){
			Button nearestNeighborButton = (Button) ((View)view).findViewById(R.id.nearestneighbor_button);
			nearestNeighborButton.setVisibility(View.VISIBLE);
			nearestNeighborButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(BaseSubjectInfoActivity.this, NearestNeighborActivity.class);
					startActivity(intent);
				}
			});


		}
	}


	@Override
	public void onReload() {
		super.onReload();
		subject = getSubject();
		view.setSubject(subject);
	}

	@Override
	public void onBackPressed() {
		Subject aSubject = getSubject();
		clearAllAnswers();
		if(isWalklistAssigned() && petition.isWalklist()) {
			Class<? extends Activity> nextActivity = ClassRetriever.getWalklistActivity(getApplicationContext());
			if (null != aSubject && aSubject.isValid() && petition.isWalklist()) {
				Pair<String, Parcelable> extraParameterForTheFirstActivity = new Pair<String, Parcelable>(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT, aSubject);
				//startActivity(nextActivity, extraParameterForTheFirstActivity);
				Intent intent = new Intent(this,nextActivity);
				intent.putExtra(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT,aSubject);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);

				finish();
			} else {

				Intent intent = new Intent(this,nextActivity);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		} else {
			showLogoutDialog();
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_CANCELED) {
			LOGGER.debug("Was cancelled");
			return;
		}
		switch (requestCode) {
		case PHOTO_ACTION_CODE:
			String barcode = data.getStringExtra(ActivityCapture.BARCODE_KEY);
			Subject subject = AAMVA.parsePDF417ToSubject(barcode);
			view.setSubject(subject);
			hideActiveDialog();
			break;

		case VOTER_ACTION_CODE:
			// TODO Implement Address search Activity
			Subject subject1 = data.getParcelableExtra(SubjectsSearchActivity.SUBJECT_KEY);
			view.setSubject(subject1);
			hideActiveDialog();
			break;
		}
	}

	protected void onEndInterview(Subject subject) {
		startActivity(ClassRetriever.getTerminationActivity(getApplicationContext()));
	}

	protected void goToNext(Subject subject) {
		setSubject(subject);

		if(validateFields()){
			startActivity(getNextActivity());
		}
	}

	/**
	 * User is required to enter at least Address,City,State and ZIP. If he tries to proceed without entering any of these,
	 * he should be shown a message
	 */
	protected boolean validateFields() {

		List<Question> requiredQuestions = getRequiredQuestions();
		for(Question question : requiredQuestions){
			if(TextUtils.isEmpty(getPetitionAnswers().getStringAnswerTo(question))){
				showToast("Please enter "+question.getName());
				return false;
			}
		}
		if(getGlobalData().getLocation() != null ){
			if(TextUtils.isEmpty(getPetitionAnswers().getStringAnswerTo(petition.getQuestionWithVarName(Question.LAST_NAME)))){
				showToast("Please enter lastname");
				return false;
			}
		}
		return true;
	}

	protected boolean validateFields(boolean isAnonymousChecked) {

		if(isAnonymousChecked) {
			return true;
		}

		List<Question> requiredQuestions = getRequiredQuestions();
		for(Question question : requiredQuestions){
			if(TextUtils.isEmpty(getPetitionAnswers().getStringAnswerTo(question))){
				showToast("Please enter "+question.getName());
				return false;
			}
		}
		if(getGlobalData().getLocation() != null ){
			if(TextUtils.isEmpty(getPetitionAnswers().getStringAnswerTo(petition.getQuestionWithVarName(Question.LAST_NAME)))){
				showToast("Please enter lastname");
				return false;
			}
		}

		return true;

	}

	private void showToast(String string) {
		Toast.makeText(this, string, Toast.LENGTH_LONG).show();
	}

	/**
	 * This map defines how to go from custom defined question names to the default SubjectInformationQuestions
	 * @return
	 */
	protected Map<String, String> getTranslateMap() {
		return null;
	}

	// Shows Assist view, where you can select:
	// * Driver license assist
	// * GPS address assist
	// * GRU service assist
	protected void showAssistDialog() {
		activeDialog = ConfirmCancelDialogBuilder.createSingleChoiceDialog(this, getString(R.string.SEARCH_FOR_SUBJECT_DIALOG_TITLE), new ConfirmCancelDialogBuilder.ViewListener<Integer>() {
			@Override
			public void onConfirm(Integer value) {
				switch (value) {
				case 0:
					Intent takePictureIntent = new Intent(BaseSubjectInfoActivity.this, ActivityCapture.class);
					startActivityForResult(takePictureIntent, PHOTO_ACTION_CODE);
					break;
				case 1:
					Intent voterLookupIntent = new Intent(BaseSubjectInfoActivity.this, SubjectsSearchActivity.class);
					startActivityForResult(voterLookupIntent, VOTER_ACTION_CODE);
					break;
				default:
					break;
				}
			}

			@Override
			public void onCancel() {
				hideActiveDialog();
			}
		}, getString(R.string.SEARCH_FOR_SUBJECT_USING_LICENSE_BARCODE),
		getString(R.string.SEARCH_FOR_SUBJECT_USING_INFO_LOOKUP));

		activeDialog.show();
	}

	private void reverseGeocode() {
		Location currentLocation = LocationProcessor.getLastKnownLocation(getApplicationContext());
		if (null == currentLocation) {
			notifyUser(getString(R.string.UNABLE_TO_DETERMINE_CURRENT_LOCATION_MESSAGE));
			return ;
		}
		GrassrootsRestClient.getClient().reverseGeocodeLocation(currentLocation, new GrassrootsRestClient.Callback<Set<Subject>>() {
			@Override
			public void onCompletion(Set<Subject> addresses) {
				Subject address = null;
				if (addresses == null || addresses.isEmpty()) {
					notifyUser(getString(R.string.NO_NEARBY_ADDRESSES_FOUND_MESSAGE));
					return;
				}

				if(isNotRunning(BaseSubjectInfoActivity.this)) {
					LOGGER.error("Reverse geocoding took too long, activity has already stopped running");
					return;
				}
				Iterator<Subject> iterator = addresses.iterator();
				while(iterator.hasNext()){
					address = iterator.next();
				}
				addSubjectInfo(address);
			}

			@Override
			public void onError(Response errors) {
				notifyUser(getString(R.string.UNABLE_TO_CONTACT_GEOCODING_SERVICE_MESSAGE));
			}
		});
	}


	protected void addSubjectInfo(Subject subject) {
		Subject viewSubject = view.getSubject();
		if (viewSubject == null) {
			view.setSubject(subject);
			return;
		}

		viewSubject.merge(subject);
		view.setSubject(viewSubject);
	}


	// ********************************* Options Menu Staff *********************************

	/**
	 * fix for S3-knock button can be selected from menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		Resources resources = getResources();
		menu.add(R.string.autofill_fields_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(R.string.more_options_button).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		if(getPetition().isBidirectional()){
			//menu.add(R.string.knock_button_title).setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Resources resources = getResources();
		if (item.getTitle().toString().equals(resources.getString(R.string.end_button))) {
			onEndInterview(view.getSubject());
			return true;
		} else if (item.getTitle().toString().equals(resources.getString(R.string.autofill_fields_button_title))) {
			reverseGeocode();
			return true;
		}else if (item.getTitle().toString().equals(resources.getString(R.string.more_options_button))) {
			showAssistDialog();
			return true;
		}else if (item.getTitle().toString().equals(resources.getString(R.string.knock_button_title))) {
			if(validateFields()){
				showStatusSelectionDialog();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Display a dialog with a list of available knock statuses where the user can select appropriate knock status for the selected subjects address.
	 */
	protected void showStatusSelectionDialog () {
		final Dialog statusDialog = new Dialog(this);
		statusDialog.setTitle(R.string.home_status);
		HomeStatusView view = (HomeStatusView) View.inflate(this, R.layout.home_status, null);

		view.setViewListener(new HomeStatusView.ViewListener() {
			@Override
			public void onStatusSelected(KnockStatus knockStatus) {

				//Once status is selected intiate a knock event
				GpsLocation location = LocationProcessor.getLastKnownGpsLocation(getApplicationContext());

				KnockEvent knockEvent = null;
				if(subject.getHid() == null){
					subject = getSubject();
					knockEvent = new KnockEvent(knockStatus, location, subject.getFirstName(), subject.getLastName(),
							subject.getAddressLine1(), subject.getAddressLine2(), subject.getCity(), subject.getState(), 
							subject.getZip(), subject.getPhone(), subject.getEmail(),getGlobalData().getLocation());
				}else{
					try{
						location.setLatitude(Double.parseDouble(subject.getLatitude()));
						location.setLongitude(Double.parseDouble(subject.getLongitude()));
					}catch(NumberFormatException e){
						e.printStackTrace();
					}
					knockEvent = new KnockEvent(Integer.parseInt(subject.getHid()), knockStatus, location);
				}

				Knocks knocks = new Knocks();
				knockEvent = addBatteryInfoToKnock(knockEvent);
				knocks.addKnockEvent(knockEvent);
				try {
					WalklistDataSource walklistDataSource = new WalklistDataSource(BaseSubjectInfoActivity.this);
					List<Subject> subjects = new ArrayList<Subject>();
					subject.setKnockedStatus(knockStatus);
					subject.setLatitude(Double.toString(location.getLatitude()));
					subject.setLongitude(Double.toString(location.getLongitude()));
					subjects.add(subject);
					walklistDataSource.insertWalklist(subjects,subject.getLocation());
					setSubject(subject);
					GrassrootsRestClient.getClient().saveKnocks(knocks);
					onBackPressed();
				} catch (Exception exception) {
					logoutWithError( getString( R.string.error_unable_to_send_knock_status ), exception );   
				}


				statusDialog.cancel();
			}



			@Override
			public void onCancel() {
				statusDialog.cancel();
			}
		});

		statusDialog.setContentView(view);
		statusDialog.show();
	}

	private List<Question> getRequiredQuestions(){

		List<Question> requiredQuestions = new ArrayList<Question>();
		for (Question question : questionstoBeSet) {
			if(question.isRequired()){
				requiredQuestions.add(question);
			}
		}

		return requiredQuestions;
	}

	@Override
	public void onResume() {
		updateKnockIfBidirectional();
		super.onResume();
	}
	
	public SubjectInfoViewWithButtonsInterface getView(){
		return view;
	}
}
