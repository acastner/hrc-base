package com.grassroots.petition.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.grassroots.petition.fragments.InstantPrescreenFragment;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;
import org.apache.log4j.Logger;
import com.grassroots.petition.R;

/**
 * Description:
 *   Provide an intuitive experience to conduct a pre-screening at the door.
 *   Advances business vertical by enabling sales people to verify that customers are approved for large purchase at
 *       the door (e.g. Solar Panels).
 *   Using InstantPrescreenFragment to show instant prescreen screen & provide this functionality to the user.
 *
 * Assumption:
 * The company has configured their TransUnion credit checking criteria with their agent.
 */
public class BaseInstantPrescreenActivity extends BasePetitionActivity implements InstantPrescreenFragment.InstantPrescreenListener
{
    private static final String TAG = BaseInstantPrescreenActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private boolean isClientApproved;

    private InstantPrescreenFragment prescreenFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.instant_prescreen_activity);

        TextView headerTitle = (TextView)findViewById(R.id.question_page_header_title);
        headerTitle.setText(getString(R.string.INSTANT_PRESCREEN_TITLE) + " " + getLocationName());

        findViewById(R.id.terminate_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEndInterview();
            }
        });
        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNext();
            }
        });
        findViewById(R.id.skip_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSkip();
            }
        });

        prescreenFragment = (InstantPrescreenFragment) getSupportFragmentManager().findFragmentById(R.id.instant_prescreen_fragment);
        if (null == prescreenFragment) {
            LOGGER.error("Unable to find InstantPrescreenFragment.");
            return;
        }

        Subject subject = getSubject();
        if (null == subject) {
            LOGGER.error("No subject found for current petition.");
            notifyUser(getString(R.string.INVALID_SUBJECT_INFO_NOTIFICATION ));
            return;
        }

        prescreenFragment.setSubject(subject);
    }

    // InstantPrescreenFragment.InstantPrescreenListener implementation
    public void onClientApproved(String message)
    {
        activeDialog = buildDialog(R.string.INSTANT_PRESCREEN_CLIENT_APPROVED_MESSAGE);
        activeDialog.show();

        isClientApproved = true;
        saveAnswer();
    }

    public void onClientDeclined(String message)
    {
        activeDialog = buildDialog(R.string.INSTANT_PRESCREEN_CLIENT_DECLINED_MESSAGE);
        activeDialog.show();

        isClientApproved = false;
        saveAnswer();
    }

    private AlertDialog buildDialog(int messageId) {
        AlertDialog.Builder builder = ConfirmCancelDialogBuilder.getAlertDialogBuilder(this);
        builder.setTitle(getString(R.string.INSTANT_PRESCREEN_DIALOG_TITLE));
        builder.setMessage(getString(messageId));
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                hideActiveDialog();
            }
        });
        return builder.create();
    }

    public void onErrorOccurred(String message)
    {
        notifyUser(getString(R.string.INSTANT_PRESCREEN_CLIENT_ERROR_MESSAGE));
        isClientApproved = false;
    }


    // OnClick listeners implementation

    public void onNext()
    {
        // Just start next activity
        startActivity(getNextActivity());
    }

    /**
     * Saves isClientApproved flag as an answer to the Question.CREDIT_CHECK Magic question.
     */
    protected void saveAnswer() {
        addYesNoAnswer(isClientApproved, Question.CREDIT_CHECK);
    }
}
