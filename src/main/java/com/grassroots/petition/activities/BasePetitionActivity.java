package com.grassroots.petition.activities;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.grassroots.models.PaymentProcessor;
import com.grassroots.modules.GrassrootsApiService;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Appointment;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.models.KnockEvent;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.MarketingMaterialType;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.QuestionAnswer;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.tasks.LogEmailTask;
import com.grassroots.petition.utils.BatteryPerformance;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.Dates;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.utils.AuthenticationHeaderPreference;
import com.grassroots.utils.ImeiHeaderPreference;
import com.grassroots.utils.OAuthTokenPreference;
import com.grassroots.utils.StringPreference;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

/**
 * Contains shared logic for all Activities, mostly around starting activities and sending/receiving data
 * between activities
 */
public class BasePetitionActivity extends BaseActivity {
	private static final String TAG = BasePetitionActivity.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	//track number of instances of this class
	private static int NUM_INSTANCES = 0;

	/**
	 * Loaded petition, which includes canvasser info, petition script, and questions to ask.
	 */
	protected Petition petition;
	/**
	 * Canvassed subject answers to a petition
	 */
	protected PetitionAnswers petitionAnswers;
	/**
	 * Entity to store data about assigned appointments, free and busy time intervals
	 */
	protected AppointmentCalendar appointmentCalendar;

	@Inject
	@ImeiHeaderPreference
	public StringPreference imeiPreference;
	@Inject @AuthenticationHeaderPreference
	public StringPreference authenticationHeaderPreference;
	@Inject @OAuthTokenPreference
	public StringPreference oAuthTokenPref;
	@Inject public GrassrootsApiService grassrootsApiService;
	
	BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.e("intent action:",intent.getAction());
			Toast.makeText(context,"Data Received from RPS",Toast.LENGTH_SHORT).show();
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage("Unauthorized Access! You will be logged out now")
					.setCancelable(false)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							((GlobalData) getApplicationContext()).clearPreferences();
							((GlobalData) getApplicationContext()).clearPetition();
							startActivity(ClassRetriever.getLoginActivity(getApplicationContext()));
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((GlobalData) this.getApplication()).getApplicationComponent().inject(this);
		petition = getPetition();
		petitionAnswers = getPetitionAnswers();
		appointmentCalendar = getAppointmentCalendar();

		NUM_INSTANCES++;

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";

		Tracker.appendLog("\n" + time + " {CLASS CREATED} " + this.getClass() 
				+ " Number of activities: " + NUM_INSTANCES);

	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}


	/**
	 * Email Logs button tap handler
	 */
	public void onEmailLogs() {
		LOGGER.debug("Email Logs Button Clicked");

		if(!isNetworkAvailable()){

			AlertDialog alertDialog = new AlertDialog.Builder(BasePetitionActivity.this).create();
			alertDialog.setTitle("No Internet");
			alertDialog.setMessage("Internet connection unavailable. Cannot send log files at this time.");
			alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			alertDialog.show();
		}else{
			new LogEmailTask(new LogEmailTask.LogEmailTaskListener() {
				@Override
				public void onPreExecute() {
					showProgress("Sending logs to tech support");
				}

				@Override
				public void onPostExecute(String result) {
					hideActiveDialog();
				}
			}, getApplicationContext()).execute(getApplicationContext());
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";

		NUM_INSTANCES--;

		Tracker.appendLog(time + " {CLASS DESTROYED} " + this.getClass() 
				+ " Number of activities: " + NUM_INSTANCES);
	}

	/**
	 * Restarting activity
	 * Clear activity state
	 */
	@Override
	public void onReload() {
		super.onReload();
		petition = getPetition();
		petitionAnswers = getPetitionAnswers();
		appointmentCalendar = getAppointmentCalendar();
	}

	boolean screenOn = true;
	@Override
	public void onStop() {
		super.onStop();

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		screenOn = pm.isScreenOn();

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) + "]";

		if(!screenOn)
		{
			Tracker.appendLog(time + " Device is sleep.");
			LOGGER.debug("Device sleep");
		}
	}

	ImageView headerLogo, loginLogo, donationLogo;

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(broadcastReceiver, new IntentFilter("302error"));
		requireGpsEnabled();

		//LOGGER.debug("Entered class: " + this.getClass());

		Calendar cl = Calendar.getInstance();

		String time = "[" + cl.get(Calendar.HOUR_OF_DAY) + ":" + cl.get(Calendar.MINUTE) + ":" 
				+ cl.get(Calendar.SECOND) +"]";

		if(!screenOn)
		{
			Tracker.appendLog(time + " Device no longer sleep.");
			screenOn = true;

			LOGGER.debug("Device not sleep");
		}
		else
		{

			Tracker.appendLog("\n" + time + " - Entered class: " + this.getClass());
		}

		Tracker.logMemoryStatus();

		hideMarketingIfHasNoMaterials();

		headerLogo = (ImageView) findViewById(R.id.header_logo);
		loginLogo = (ImageView) findViewById(R.id.imageView);	
		donationLogo = (ImageView) findViewById(R.id.image_view_donation_logo);

		if(loginLogo != null)
		{
			new SetLogoImgTask().execute("login");
		}
		else if(headerLogo != null)
		{
			new SetLogoImgTask().execute("header");
		}
		else if(donationLogo != null)
		{
			new SetLogoImgTask().execute("donation");
		}

	}

	File logo = null;
	private class SetLogoImgTask extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... type) {
			
			if (type[0].equals("header"))
			{
				logo = new File(GlobalData.TMP_DIRECTORY + File.separator + "logos" + File.separator + "icon.png");
			}
			else if (type[0].equals("login"))
			{
				logo = new File(GlobalData.TMP_DIRECTORY + File.separator + "logos" + File.separator + "logo_huge.png");
				
			}
			else if (type[0].equals("donation"))
			{
				logo = new File(GlobalData.TMP_DIRECTORY + File.separator + "logos" + File.separator + "donation_logo.png");
			}

			return type[0];
		}
		
		@Override
		protected void onPostExecute(String type) {
			if (type.equals("header"))
			{
				if(logo.exists())
				{
					headerLogo.setImageURI(Uri.fromFile(logo));
				}
				else
				{
					headerLogo.setImageResource(R.drawable.icon);
				}
			}
			else if (type.equals("login"))
			{
				if(logo.exists())
				{
					loginLogo.setImageURI(Uri.fromFile(logo));
					Drawable d = Drawable.createFromPath(logo.getPath());
					//loginLogo.setBackgroundDrawable(d);
				}
				else
				{
					loginLogo.setImageResource(R.drawable.logo_huge);
				}
			}
			else if (type.equals("donation"))
			{
				if(logo.exists())
				{
					donationLogo.setImageURI(Uri.fromFile(logo));
				}
				else
				{
					donationLogo.setImageResource(R.drawable.donation_logo);
				}
			}
		}
	}


	@Override
	public void onBackPressed() {
		Class thisClass = this.getClass();
		// logout user if we are on a firstActivity (Walklist, Terminate, Subject activities)
		if (thisClass.equals(getFirstActivity(null))) {
			showLogoutDialog();
		} else {
			super.onBackPressed();
		}
	}

	/**
	 * Get Location Name for this Script
	 * @return Location Name
	 */

	public String getLocationName() {
		if(petition != null) {
			JSONObject jsonObj = petition.getPetitionJSON();
			String locationName = "";
			try {
				locationName = jsonObj.getString("LocationName");
				return locationName;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return " ";
	}

	/**
	 * Get payment processors
	 */
	public PaymentProcessor getPaymentProcessor()	{
		JSONObject jsonObj = petition.getPetitionJSON();

		try {
			JSONArray paymentprocessors = new JSONArray(jsonObj.getString("PaymentProcessors"));
			for(int i=0; i<paymentprocessors.length(); i++)	{
				JSONObject backupPaymentProcessor = paymentprocessors.getJSONObject(i);
				PaymentProcessor backupProcessor = new PaymentProcessor(backupPaymentProcessor.getInt("ProcessorTypeId"), backupPaymentProcessor.getBoolean("BackupOnly"));
				return backupProcessor;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Check if Back-Up Payment Processing is enabled/disabled
	 */

	protected boolean isBackupPaymentProcessingEnabled()	{
		JSONObject jsonObj = petition.getPetitionJSON();
		JSONArray jsonArr = new JSONArray();
		try {
			jsonArr = jsonObj.getJSONArray("ScriptProperties");
			for(int i=0; i<jsonArr.length(); i++)	{
				if(jsonArr.getJSONObject(i).get("Name").equals("BackupPaymentProcessingEnabled"))	{
					if(jsonArr.getJSONObject(i).get("Value").equals("True"))	{
						return true;
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Hide marketing button if No Marketing Materials are provided
	 */
	protected void hideMarketingIfHasNoMaterials() {
		if(MarketingMaterialType.NONE == MarketingMaterialType.getMarketingMaterialType(this)) {
			View marketing = findViewById(R.id.marketing_material_button);
			if(marketing != null)
				marketing.setVisibility(View.GONE);
		}


		File imgDir = new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
		if(imgDir.list() != null && imgDir.list().length > 0)
		{
			View marketing = findViewById(R.id.marketing_material_button);
			if (null != marketing) {
				marketing.setVisibility(View.VISIBLE);
			}
		}
	}

	/**
	 * Determines if activity should be shown
	 * Should Be overriden in subclass
	 * @param petition
	 * @return
	 */
	public static boolean shouldProceedWithPetition(Petition petition) {
		return false;
	}

	/**
	 * Figures out the next activity to start, based on the contents of the petition and the order of
	 * the screen_order array defined in classes.xml. If there is no next activity, returns the submit
	 * activity
	 * @return next activity class depending on current activity
	 */
	protected Class<? extends Activity> getNextActivity() {
		Class thisClass = this.getClass();
		LOGGER.debug("This class is: " + this.getClass() + ", looking for next activity to start");

		return getNextActivityAfterActivityClass(thisClass);
	}


	/**
	 * Figures out the next activity to start, based on the contents of the petition and the order of
	 * the screen_order array defined in classes.xml. If there is no next activity, returns the submit
	 * activity
	 * @param currentActivityClass - activity class after which need to get activity class
	 * @return next activity class depending on specified activity class
	 */
	protected Class<? extends Activity> getNextActivityAfterActivityClass(Class currentActivityClass) {
		if (null == currentActivityClass) {
			LOGGER.error("Current activity class is null.");
			return null;
		}

		TypedArray orderArray = getResources().obtainTypedArray(R.array.screen_order);
		if (null == orderArray || 0 == orderArray.length()) {
			LOGGER.error("screen_order from classes.xml is empty!");
			return null;
		}


		int currentActivityIndex = -1;


		// Code to support Payment activities (2nd activity for the "product_class" or "donation_class")
		// Check if current activity is assignable from BaseACHDataActivity or BaseCreditCardSwipeActivity,
		// which are payment activities for Product or Donation
		if (currentActivityClass.isAssignableFrom(BaseACHDataActivity.class) ||
				currentActivityClass.isAssignableFrom(BaseCreditCardSwipeActivity.class))
		{
			// Search for Donation ("donation_class") or Product ("product_class") activity class in "screen_order" array
			// NOTE: will work only for one of specified classes: BaseDonationActivity or BaseProductActivity
			// TODO: need to be implemented correctly to support both BaseDonationActivity and BaseProductActivity simultaneously
			for (int i = 0; i < orderArray.length(); i++)
			{
				int classNameId = orderArray.getResourceId(i, -1);
				Class cls = ClassRetriever.getActivityForResourceId(this, classNameId);
				if (cls.isAssignableFrom(BaseDonationActivity.class))
				{
					currentActivityClass = BaseDonationActivity.class;
					break;
				}
				else if (cls.isAssignableFrom(BaseProductActivity.class))
				{
					currentActivityClass = BaseProductActivity.class;
					break;
				}
			}
		}


		// Code to support DayViewActivity (2nd activity for "appointments_class")
		// Check if current activity is assignable from MultiDayFragmentActivity
		// which is add appointment activity for "appointments_class"
		if (currentActivityClass.isAssignableFrom(MultiDayFragmentActivity.class))
		{
			currentActivityClass = ClassRetriever.getAppointmentsActivity(this);
		}


		// Search for current activity class in "screen_order" array to get the index of the current activity class
		for(int counter = 0; counter < orderArray.length(); counter++)
		{
			int classNameId = orderArray.getResourceId(counter, -1);
			Class cls = ClassRetriever.getActivityForResourceId(this, classNameId);
			LOGGER.debug("   Checking: " + cls);
			if (currentActivityClass.isAssignableFrom(cls))
			{
				LOGGER.debug("      It is assignable");
				currentActivityIndex = counter;
			}
		}


		// Try to get next activity class from "screen_order" array by the index of the current activity class.
		// NOTE: depending on activity class we have to check for specified conditions or constraints,
		// if condition is false then try to get next activity from "screen_order" array and so on;
		// if there is no appropriate and valid activity, then return SubmitActivity in the end.
		int nextActivityIndex = currentActivityIndex + 1;
		while (nextActivityIndex < orderArray.length())
		{
			Class<? extends Activity> nextActivity = ClassRetriever.getActivityForResourceId(this, orderArray.getResourceId(nextActivityIndex, -1));
			LOGGER.debug("Next class is: " + nextActivity + " index is: " + nextActivityIndex);
			if (
					nextActivity != null &&
					(
							(nextActivity.equals(ClassRetriever.getInstantPrescreenActivity(this)) && petition.isSupportCreditCheck()) ||
							(nextActivity.equals(ClassRetriever.getAppointmentsActivity(this)) && petition.isSupportReservation()) ||
							(nextActivity.equals(ClassRetriever.getSubjectInformationActivity(this)) && petition.getSubjectInformationQuestions().size() > 0) ||
							(nextActivity.equals(ClassRetriever.getMarketingActivity(this)) && MarketingMaterialType.getMarketingMaterialType(this) != MarketingMaterialType.NONE) ||
							(nextActivity.equals(ClassRetriever.getProductActivity(this)) && petition.hasProducts()) ||
							(nextActivity.equals(ClassRetriever.getDonationActivity(this)) && petition.hasDonation()) ||
							(nextActivity.equals(ClassRetriever.getQuestionActivity(this)) && petition.hasNonMagicQuestions()) ||
							(nextActivity.equals(ClassRetriever.getReferrerActivity(this)) && petition.hasReferrerQuestions()) ||
							(nextActivity.equals(ClassRetriever.getSignatureActivity(this)) && petition.isSignature()) ||
							(nextActivity.equals(ClassRetriever.getMailingActivity(this)) && petition.hasShippingQuestions())
							)
					)
			{
				return nextActivity;
			}
			else if (nextActivity != null && BasePetitionActivity.class.isAssignableFrom(nextActivity))
			{
				try {
					Method m = nextActivity.getMethod("shouldProceedWithPetition", Petition.class);
					if (((Boolean)m.invoke(null, petition)).booleanValue())
					{
						LOGGER.debug("Proceed is set, using " + nextActivity);
						return nextActivity;
					}
				} catch (Exception exception)
				{
				}
			}

			nextActivityIndex++;
		}

		LOGGER.debug("Didn't find any suitable next class, proceeding with SubmitActivity");
		return ClassRetriever.getSubmitActivity(this);
	}



	/**
	 * Figures out the previous activity to start, based on the contents of the petition and the order of
	 * the screen_order array defined in classes.xml. If there is no previous activity, returns the first
	 * activity
	 * @return a class of previous activity depending on current activity
	 */
	protected Class<? extends Activity> getPreviousActivity() {
		TypedArray orderArray = getResources().obtainTypedArray(R.array.screen_order);

		int currentActivityIndex = -1;
		Class thisClass = this.getClass();
		LOGGER.debug("This class is: " + this.getClass());

		for(int counter = 0; counter < orderArray.length(); counter++) {
			int classNameId = orderArray.getResourceId(counter, -1);
			Class cls = ClassRetriever.getActivityForResourceId(this, classNameId);
			LOGGER.debug("Checking: " + cls);
			if (thisClass.isAssignableFrom(cls)) {
				LOGGER.debug("Is assignable");
				currentActivityIndex = counter;
			}
		}
		int nextActivityIndex = currentActivityIndex - 1;
		while(nextActivityIndex < orderArray.length()) {
			Class<? extends Activity> nextActivity = ClassRetriever.getActivityForResourceId(this, orderArray.getResourceId(nextActivityIndex, -1));
			LOGGER.debug("Next class is: " + nextActivity + " index is: " + nextActivityIndex);
			if (    nextActivity != null &&
					(
							(nextActivity.equals(ClassRetriever.getSubjectInformationActivity(this)) && petition.getSubjectInformationQuestions().size() > 0) ||
							(nextActivity.equals(ClassRetriever.getMarketingActivity(this)) && MarketingMaterialType.getMarketingMaterialType(this) != MarketingMaterialType.NONE) ||
							(nextActivity.equals(ClassRetriever.getProductActivity(this)) && petition.hasProducts()) ||
							(nextActivity.equals(ClassRetriever.getDonationActivity(this)) && petition.hasProducts()) ||
							(nextActivity.equals(ClassRetriever.getQuestionActivity(this)) && petition.hasNonMagicQuestions()) ||
							(nextActivity.equals(ClassRetriever.getReferrerActivity(this)) && petition.hasReferrerQuestions()) ||
							(nextActivity.equals(ClassRetriever.getSignatureActivity(this)) && petition.isSignature())
							)
					)
			{
				return nextActivity;
			} else if (nextActivity != null && BasePetitionActivity.class.isAssignableFrom(nextActivity))
			{
				try {
					Method m = nextActivity.getMethod("shouldProceedWithPetition", Petition.class);
					if (((Boolean)m.invoke(null, petition)).booleanValue()) {
						return nextActivity;
					}
				} catch (Exception exception) {
				}
			}

			nextActivityIndex--;
		}

		//return ClassRetriever.getFirstActivity(getApplicationContext()); // TODO: remove
		return getFirstActivity(null);
	}

	/**
	 * Check if an activity is running. Should be used when making async calls that on return try to update
	 * the activity UI
	 * @param activity - an activity which need to check if it's running
	 * @return true if specified activity is running, false otherwise
	 */
	protected boolean isRunning(Context activity) {
		if(activity == null)
			return false;
		ActivityManager activityManager = (ActivityManager)activity.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
		for(ActivityManager.RunningTaskInfo task : tasks) {
			if(activity.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
				return true;
		}
		return false;
	}

	/**
	 * Check if specified activity is not running
	 * @param activity - an activity which should be checked if it's running
	 * @return true if the specified activity is not running
	 */
	protected boolean isNotRunning(Context activity) {
		return !isRunning(activity);
	}

	/**
	 * Ask user to enable GPS in settings, if GPS is disabled
	 */
	protected void requireGpsEnabled() {
		if (!LocationProcessor.isGpsLocationProviderEnabled(getApplicationContext())) {
			buildAlertMessageNoGps();
		}
	}

	/**
	 * Create Alert message: "GPS needs to be enabled"
	 */
	protected void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.gps_disabled)
		.setCancelable(false)
		.setPositiveButton(R.string.go_button, new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int id) {
				startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * EndInterview button tap handler
	 */
	protected void onEndInterview() {
		startActivity(ClassRetriever.getTerminationActivity(getApplicationContext()));
	}

	/**
	 * Skip button tap handler
	 */
	protected void onSkip() {
		startActivity(getNextActivity());
	}



	//********************************************************************************
	// Global data methods (returns/store petition, walkilist, petitionAnswers...
	//********************************************************************************

	/**
	 * Gets GlobalData singleton object
	 * @return GlobalData singleton object
	 */
	public GlobalData getGlobalData() {
		return (GlobalData) getApplicationContext();
	}

	/**
	 * Marketing button tap handler
	 */
	protected void onMarketing() {
		getGlobalData().onMarketing(this);
	}

	/**
	 * Tally Report button tap handler
	 */
	protected void onTally() {
		String preferredReport = getGlobalData().getPreferredReportSetting();
		if(preferredReport!=null && preferredReport == 	"HRC tally report") {
			Intent intent = new Intent(BasePetitionActivity.this, ReportDetailWebviewActivity.class);
			intent.putExtra("reportname","HRC tally report");
			startActivity(intent);
		}
		else{
			Intent intent = new Intent(BasePetitionActivity.this, ReportDetailWebviewActivity.class);
			intent.putExtra("reportname","HRC tally report");
			startActivity(intent);
		}
	}

	/**
	 * Return saved walklist object
	 * @return
	 */
	public Walklist getWalklist() {
		return getGlobalData().getWalkListFromDatabase();
	}

	/**
	 * Save specified walklist
	 * @param walklist - specified walklist to save
	 */
	public synchronized void setWalklist(Walklist walklist) {
		//getGlobalData().setWalklist(walklist);
	}

	/**
	 * Return saved Petition object
	 * @return saved petition object
	 */
	public Petition getPetition() {
		return getGlobalData().getPetition();
	}

	/**
	 * Save specified Petition
	 * @param petition - a specified petition to save
	 */
	public synchronized void setPetition(Petition petition) {
		this.petition = petition;
		getGlobalData().setPetition(petition);
	}

	/**
	 * Return saved PetitionAnswers
	 * @return saved PetitionAnswers
	 */
	public PetitionAnswers getPetitionAnswers() {
		return getGlobalData().getPetitionAnswers();
	}

	/**
	 * Save specified petitionAnswers object
	 * @param petitionAnswers - petitionAnswers which will be saved
	 */
	public void setPetitionAnswers(PetitionAnswers petitionAnswers) {
		getGlobalData().setPetitionAnswers(petitionAnswers);
	}

	/**
	 * Return saved Subject
	 * @return previously saved Subject
	 */
	public Subject getSubject() {
		return getPetitionAnswers().getSubject();
	}

	/**
	 * Save specified Subject object
	 * @param subject specified subject object
	 */
	public void setSubject(Subject subject) {
		getPetitionAnswers().setSubject(subject);
		/*if(subject.getFirstName() == null){
			subject.setFirstName("");
		}
		if(subject.getLastName() == null){
			subject.setLastName("");
		}
		if(subject.getAddressLine1() == null){
			subject.setAddressLine1("");
		}
		if(subject.getAddressLine2() == null){
			subject.setAddressLine2("");
		}
		if(subject.getCity() == null){
			subject.setCity("");
		}
		if(subject.getState() == null){
			subject.setState("");
		}
		if(subject.getZip() == null){
			subject.setZip("");
		}*/

		getPetitionAnswers().mergeInAnswersAndSetIdFields(petition.getSubjectInformationQuestions(), subject);

		for (Question question : petition.getQuestions()) {
			SubjectAnswer subjectAnswer = subject.getOtherAutofillAnswerTo(question);
			if (null != subjectAnswer) {
				petitionAnswers.overwriteAnswer(subjectAnswer);
			}
		}
	}

	/**
	 * Return saved Cart (products container)
	 * @return previously saved cart object (products container)
	 */
	public Cart getCart() {
		return getPetitionAnswers().getCart();
	}

	/**
	 * Save specified Cart object
	 * @param cart - a cart object to save
	 */
	public void setCart(Cart cart) {
		getPetitionAnswers().setCart(cart);
	}

	/**
	 * Return saved Appointment object
	 * @return previously saved Appointment object
	 */
	public Appointment getAppointment() {
		return getPetitionAnswers().getAppointment();
	}

	/**
	 * Save specified Appointment object
	 * @param appointment - an appointment object to save
	 */
	public void setAppointment(Appointment appointment) {
		getPetitionAnswers().setAppointment(appointment);
	}

	/**
	 * Save specified AppointmentCalendar object
	 * @param calendar - an AppointmentCalendar object to save
	 */
	public synchronized void setAppointmentCalendar(AppointmentCalendar calendar) {
		this.appointmentCalendar = calendar;
		getGlobalData().setAppointmentCalendar(calendar);
	}

	/**
	 * Return saved AppointmentCalendar object
	 * @return previously saved AppointmentCalendar object
	 */
	public AppointmentCalendar getAppointmentCalendar() {
		return getGlobalData().getAppointmentCalendar();
	}

	/**
	 * Clear PetitionAnswers
	 */
	public void clearAllAnswers() {
		getGlobalData().clearPetitionAnswers();
	}

	/**
	 * Check if walklist was assigned
	 * @return true if walklist was assigned, false otherwise
	 */
	public boolean isWalklistAssigned(){
		Walklist walklist = getWalklist();
		return !(null == walklist || walklist.isEmpty());
	}

	/**
	 * Return saved locationlist object
	 * @return LocationList
	 */
	public LocationList getLocationlist() {
		return getGlobalData().getLocationList();
	}

	/**
	 * Save specified walklist
	 * @param walklist - specified walklist to save
	 */
	public synchronized void setLocationlist(LocationList locationlist) {
		getGlobalData().setLocationList(locationlist);
	}


	/**
	 * Start First activity defined in classes.xml screen_order array
	 * @return true if the first activity was successfully started
	 */
	public boolean startFirstActivity() {
		return startFirstActivity(null);
	}

	/**
	 * Start First activity which class is defined in classes.xml screen_order array
	 * Attach specified subject to the intent
	 * @param aSubject - a subject which will be attached to the intent
	 * @return true if the first activity was successfully started
	 */
	public boolean startFirstActivity(Subject aSubject) {
		Class<? extends Activity> nextActivity = getFirstActivity(aSubject);
		if (null != nextActivity) {
			if (null != aSubject && aSubject.isValid()) {
				if(petition.isLocationList() && aSubject.getLocation() != null){
					Pair<String, Parcelable> extraParameterForTheFirstActivity = new Pair<String, Parcelable>(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT, aSubject);
					Intent intent = new Intent(this,nextActivity);
					intent.putExtra(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT,aSubject);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}else if(petition.isWalklist() && isWalklistAssigned()){
						Pair<String, Parcelable> extraParameterForTheFirstActivity = new Pair<String, Parcelable>(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT, aSubject);

						Intent intent = new Intent(this,nextActivity);
						intent.putExtra(BaseWalklistActivity.WALKLIST_EXTRA_PARAMETER_SUBJECT,aSubject);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
				}
				else	// no walklist or location list
				{
					Intent intent = new Intent(this,nextActivity);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}


			} else {
				Intent intent = new Intent(this,nextActivity);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
			return true;
		}
		return false;
	}

	/**
	 * Return first activity class defined in classes.xml screen_order array
	 * @return first activity class defined in classes.xml screen_order array
	 */
	public Class<? extends Activity>  getFirstActivity(Subject subject) {

		Context context = getApplicationContext();
		if (null == petition) {
			// Petition may be null if we're calling this from login. Try the Context first.
			petition = getPetition();
			if(petition == null){
				LOGGER.error("Petition is empty: unable to get questions!");
				return null;
			}

		}

		if(petition.isLocationList() && getGlobalData().getLocationList() != null){
			if(subject != null){
				return ClassRetriever.getWalklistActivity(context);
			}
			return ClassRetriever.getLocationListActivity(context);
		}

		// #1 if walklist is exist and not empty - then start walklist activity
		if(petition.isWalklist() && isWalklistAssigned() ){
			return ClassRetriever.getWalklistActivity(context);
		}

		List<Question> subjectQuestions = petition.getSubjectInformationQuestions();
		// #2 if no subject information questions - then start next activity after subject information activity form screen_order array from classes.xml
		if (null == subjectQuestions || 0 == subjectQuestions.size())
		{
			Class<? extends Activity> nextActivityAfterSubjectActivity = getNextActivityAfterActivityClass(ClassRetriever.getSubjectInformationActivity(context));
			if (null == nextActivityAfterSubjectActivity) {
				LOGGER.error("No next activity after subject information activity.");
				return null;
			}

			return nextActivityAfterSubjectActivity;
		}



		// #3 if subject information questions exist - then start subject information activity ("subject_class");
		return ClassRetriever.getSubjectInformationActivity(context);
	}

	//TODO this does not belong here

	/**
	 * Read the bitmap stored at the specified location, then scale it to a standard size
	 * @param photoPath
	 * @return
	 */
	public Bitmap readScaledPhoto(String photoPath) {
		if(Strings.isEmpty(photoPath)) {
			return null;
		}
		int targetW = 1296;
		int targetH = 968;

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(photoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		return BitmapFactory.decodeFile(photoPath, bmOptions);
	}

	/**
	 * Delete all the photos
	 */
	protected void deleteOldFiles() {
		File pictureDirectory = getOutputDirectory();
		for(File file : pictureDirectory.listFiles()) {
			file.delete();
		}
	}

	/**
	 * Create an image file which can be written to by the camera
	 * @return a File object which can be written to by the camera
	 * @throws IOException
	 */
	protected File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp =
				new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = timeStamp + "_";
		//        File image = File.createTempFile(
		//                imageFileName,
		//                ".jpg"
		//        );
		File directory = getOutputDirectory();
		File image = new File(directory.getAbsolutePath(), imageFileName + ".jpg");
		image.createNewFile();
		return image;
	}

	/**
	 * Return output directory for TEMP files
	 * @return a File object which is output directory for TEMP files
	 */
	private File getOutputDirectory() {
		String dirPath = getGlobalData().getTMPDirectory() + File.separator;
		File directory = new File(dirPath);
		if(!directory.exists())
			directory.mkdirs();
		return directory;
	}


	//********************************************************************************
	// Magic Questions helper methods
	//********************************************************************************

	/**
	 * Gets date from text filed specified by viewId (look at getDateFromView() method for detailed informaiton).
	 * Then save date as answer to question with specified name
	 * @param viewId - a view id of the text field, which should contain date in format: 'year'-'month'-'day of month', where each part is represented by integers.
	 * @param questionName - a question varname where to save date as answer.
	 */
	protected void addDateAnswer(int viewId, String questionName) {
		int[] dates = getDateFromView(findViewById(viewId));
		addDateAnswer(dates[0], dates[1], dates[2], questionName);
	}

	/**
	 * Add or overwrite date answer for quesiton with specified varname.
	 * @param year - an year of date as integer
	 * @param month - a month of date as integer
	 * @param day - a day of month of date as integer
	 * @param varName - a question varname, where to save date answer.
	 */
	protected void addDateAnswer(int year, int month, int day, String varName) {
		Question question = petition.getQuestionWithVarName(varName);
		if(question == null) {
			LOGGER.warn("No question in petition with varname: " + varName);
			return;
		}

		SubjectAnswer answer = new SubjectAnswer(question.getId(), Dates.getFormattedDate(year, month, day));
		petitionAnswers.overwriteAnswer(answer);
	}

	/**
	 * Gets a string from the specified TextView,
	 * parse it by splitting on parts separated by '-' symbol,
	 * convert each part to the integer.
	 * I.e. specified TextView should contain date in format: year-month-day of month, where each part is represented by integers.
	 * returns an array of integers which represents a date: year, month, day of month.
	 * @param view - a TextView field, which should contain date in format: year-month-day of month, where each part is represented by integers.
	 * @return an array of integers which represents a date: year, month, day of month.
	 */
	protected int[] getDateFromView(View view){
		String displayed = ((TextView)view).getText().toString();
		String[] split = displayed.split("-");
		if(split.length == 3) {
			return new int[]{
					Integer.parseInt(split[0]),
					(Integer.parseInt(split[1])-1),
					Integer.parseInt(split[2])
			};
		}
		return new int[] {1995, 0, 1};
	}

	/**
	 * Add or overwrite a text answer from a text field with specified id, to a question with a specified varname.
	 * @param textId - a TextView field id, with the text which should be saved as an answer.
	 * @param questionName - a question varname where to save date as answer.
	 * @return true if answer successfully saved, false otherwise.
	 */
	protected boolean addTextAnswer(int textId, String questionName) {
		TextView textView = (TextView) findViewById(textId);
		//        if (null == textView) {
		//            LOGGER.error("TextView not found for id: " + textId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		String stringAnswer = textView.getText().toString();
		if (null == stringAnswer) {
			LOGGER.warn("Text view is empty for question with varname: " + questionName);
			return false;
		}

		if (Strings.isEmpty(stringAnswer)) {
			LOGGER.warn("Text view is empty for question with varname: " + questionName + " removing existing answer if any");
			petitionAnswers.removeAnswer(question);
			return true; // return true because user set empty field
		}

		SubjectAnswer answer = new SubjectAnswer(question.getId(), stringAnswer);
		petitionAnswers.overwriteAnswer(answer);

		return true;
	}

	/**
	 * Set text from subject's answer to a question with specified varname for text field with specified id.
	 * @param fieldId - an id of a text field, where to place answer to the specified question.
	 * @param questionName - a question varname where from get text answer.
	 * @return true if answer was successfully gotten from the question and set to the field with specified id, false otherwise.
	 */
	protected boolean setTextAnswerToTextField(int fieldId, String questionName) {
		TextView textView = (TextView)findViewById(fieldId);
		//        if (null == textView) {
		//            LOGGER.error("No text view found for id = " + fieldId);
		//            return false;
		//        }
		textView.setText("");

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		String stringAnswer = subjectAnswer.getStringAnswer();
		if (null == stringAnswer || Strings.isEmpty(stringAnswer)) {
			LOGGER.warn("Answer is empty for question with varname: " + questionName);
			return false;
		}

		textView.setText(stringAnswer);
		return true;
	}


	/**
	 * Adds/overwrites YesNo (TRUEFALSE) answer which has Boolean type and passed directly to the method.
	 * @param answer - answer (true/false),
	 * @param questionVarName - a question varname where to save answer
	 * @return true if successfully get question with specified name and add/overwrite answer.
	 */
	protected boolean addYesNoAnswer(Boolean answer, String questionVarName) {
		Question question = petition.getQuestionWithVarName(questionVarName);
		if (null == question) {
			LOGGER.error("Question with varname " + questionVarName + " was not found.");
			return false;
		}

		if (Question.Type.TRUEFALSE != question.getType()) {
			LOGGER.error("Question with varname " + questionVarName + " is not YesNo type.");
			return false;
		}


		int questionId = question.getId();
		if (questionId < 0) {
			LOGGER.error("Question with varname " + questionVarName + " has wrong Id = " + questionId);
			return false;
		}

		SubjectAnswer subjectAnswer = new SubjectAnswer(questionId, answer);
		petitionAnswers.overwriteAnswer(subjectAnswer);

		return true;
	}

	/**
	 * Adds/overwrites YesNo (TRUEFALSE) answer from checkbox, for question with specified varname.
	 * Adds/overwrites answer depending on checkbox state checked = true / unchecked = false.
	 * Checkbox is selected from ViewGroup by it's TAG, which is question Id.
	 * @param viewGroup - ViewGroup where to find required checkbox;
	 * @param questionVarName - a question varname where to save answer
	 * @return true if successfully get question with specified name, find checkbox with tag equal to question Id and add/overwrite answer.
	 */
	protected boolean addYesNoAnswerFromCheckBoxWithTagFromGroupView(ViewGroup viewGroup, String questionVarName) {
		Question question = petition.getQuestionWithVarName(questionVarName);
		if (null == question) {
			LOGGER.error("Question with varname " + questionVarName + " was not found.");
			return false;
		}

		if (Question.Type.TRUEFALSE != question.getType()) {
			LOGGER.error("Question with varname " + questionVarName + " is not YesNo type.");
			return false;
		}


		int questionId = question.getId();
		if (questionId < 0) {
			LOGGER.error("Question with varname " + questionVarName + " has wrong Id = " + questionId);
			return false;
		}

		CheckBox checkBox = (CheckBox) viewGroup.findViewWithTag(questionId);
		if (null == checkBox) {
			LOGGER.error("View with tag " + questionId + " was not found.");
			return false;
		}

		if (!CheckBox.class.isInstance(checkBox)) {
			LOGGER.error("View with tag " + questionId + " is not checkbox.");
			return false;
		}

		SubjectAnswer subjectAnswer = new SubjectAnswer(questionId, checkBox.isChecked());
		petitionAnswers.overwriteAnswer(subjectAnswer);

		return true;
	}


	/**
	 * Sets checkbox checked / unchecked using YesNo / TRUEFALSE answer for question with specified varname.
	 * Checkbox is selected from ViewGroup by it's TAG, which is question Id.
	 * Also reset checkbox, if there is no subject answer for specified question.
	 * @param viewGroup - ViewGroup where to find required checkbox;
	 * @param questionVarName - a question varname where to save answer
	 * @return true if successfully get question with specified name, find checkbox with tag equal to question Id, find answer and make checkbox checked or unchecked.
	 */
	protected boolean setYesNoAnswerToCheckBoxWithTagFromGroupView(ViewGroup viewGroup, String questionVarName) {
		Question question = petition.getQuestionWithVarName(questionVarName);
		if (null == question) {
			LOGGER.warn("Question with varname " + questionVarName + " was not found");
			return false;
		}

		if (Question.Type.TRUEFALSE != question.getType()) {
			LOGGER.warn("Question with varname " + questionVarName + " is not YesNo type.");
			return false;
		}

		int questionId = question.getId();
		if (questionId < 0) {
			LOGGER.warn("Question with varname " + questionVarName + " has wrong Id = " + questionId);
			return false;
		}

		CheckBox checkBox = (CheckBox) viewGroup.findViewWithTag(questionId);
		if (null == checkBox) {
			LOGGER.warn("View with tag " + questionId + " was not found.");
			return false;
		}

		if (!CheckBox.class.isInstance(checkBox)) {
			LOGGER.warn("View with tag " + questionId + " is not checkbox.");
			return false;
		}

		checkBox.setChecked(false); // reset checkbox

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("Question with varname " + questionVarName + " has no answer.");
			return false;
		}

		int answerId = subjectAnswer.getAnswerId();
		if (Question.TRUE_ANSWER_ID == answerId) {
			checkBox.setChecked(true);
			return true;
		} else if (Question.FALSE_ANSWER_ID == answerId) {
			checkBox.setChecked(false);
			return true;
		}

		LOGGER.error("Subject answer contains wrong id = " + answerId + " for quesiton with varname " + questionVarName);
		return false;
	}

	/**
	 * Adds/overwrites YesNo (TRUEFALSE type) answer from RadioGroup, for question with specified varname.
	 * Gets selected answer depending on the selected RadioGroup's item:
	 * if selected item with id = true_radio, then answer is set to true (TRUE_ANSWER_ID)
	 * if selected item with id = false_radio, then answer is set to false (FALSE_ANSWER_ID)
	 * @param radioGroupId - id of RadioGroup where to find items with id = true_radio and id = false_radio;
	 * @param questionName - a question varname where to save answer;
	 * @return true if successfully get question with specified name, find RadioGroup with specified id, get selection from items with required id and add/overwrite answer.
	 */
	protected boolean addYesNoAnswerFromRadioGroup(int radioGroupId, String questionName) {
		SubjectAnswer yesNoAnswer = getYesNoAnswer(radioGroupId, questionName);
		if (null == yesNoAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		petitionAnswers.overwriteAnswer(yesNoAnswer);
		return true;
	}

	/**
	 * Select RadioGroup item depending on subject answer from a question with the specified varname.
	 * The question should have a TRUEFALSE type.
	 * Also reset RadioGroup selection, if there is no subject answer for specified question.
	 * If subject answer is true (TRUE_ANSWER_ID), then RadioGroup item with id = true_radio will be selected;
	 * If subject answer is false (FALSE_ANSWER_ID), then RadioGroup item with id = false_radio will be selected;
	 * @param radioGroupId - id of RadioGroup where to select items with id = true_radio and id = false_radio;
	 * @param questionName - a question varname where from to get subject answer;
	 * @return true if successfully get question with specified varname, get subject answer for this question, find RadioGroup with specified id, and select item with required id depending on subject answer.
	 */
	protected boolean setSelectedRadioButtonByYesNoAnswer(int radioGroupId, String questionName) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.clearCheck(); // Clear selected radio button first

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		int answerId = subjectAnswer.getAnswerId();
		if (Question.TRUE_ANSWER_ID == answerId) {
			radioGroup.check(R.id.true_radio);
			return true;
		} else if (Question.FALSE_ANSWER_ID == answerId) {
			radioGroup.check(R.id.false_radio);
			return true;
		}

		LOGGER.error("Subject answer contains wrong id = " + answerId + " for quesiton with varname " + questionName);
		return false;
	}

	/**
	 * Adds/overwrites YesNo (TRUEFALSE type) answer from checkbox with specified id, for question with specified varname.
	 * Gets selected answer depending on the state of checkbox:
	 * if checkbox is checked, then answer will be set to true (TRUE_ANSWER_ID)
	 * if checkbox is not checked, then answer will be set to false (FALSE_ANSWER_ID)
	 * @param checkboxId - id of CheckBox where to get subject answer from;
	 * @param questionName - a question varname where to save answer;
	 * @return true if successfully get question with specified varname, find CheckBox with specified id, and add/overwrite answer.
	 */
	protected boolean addCheckboxAnswer(int checkboxId, String questionName) {
		CheckBox checkBox = (CheckBox) findViewById(checkboxId);
		//        if (null == checkBox) {
		//            LOGGER.error("No radio group found for id = " + checkboxId);
		//            return false;
		//        }
		boolean isChecked = checkBox.isChecked();

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer answer = new SubjectAnswer(question.getId(), isChecked);
		petitionAnswers.overwriteAnswer(answer);
		return true;
	}

	/**
	 * Gets subject answer of TRUEFALSE type depending on the selected RadioGroup's item and bind this answer to a question with specified varname.
	 * if selected item with id = true_radio, then answer is set to true (TRUE_ANSWER_ID).
	 * if selected item with id = false_radio, then answer is set to false (FALSE_ANSWER_ID).
	 * @param radioGroupId - id of RadioGroup where to find items with id = true_radio and id = false_radio;
	 * @param questionName - a question varname where to save answer;
	 * @return true if successfully get question with specified name, find RadioGroup with specified id, get selection from items with required id.
	 */
	protected SubjectAnswer getYesNoAnswer(int radioGroupId, String questionName) {
		return getYesNoAnswer((RadioGroup) findViewById(radioGroupId), questionName);
	}

	/**
	 * Gets subject answer of TRUEFALSE type depending on the selected RadioGroup's item and bind this answer to a question with specified varname.
	 * if selected item with id = true_radio, then answer is set to true (TRUE_ANSWER_ID).
	 * if selected item with id = false_radio, then answer is set to false (FALSE_ANSWER_ID).
	 * @param radioGroupId - id of RadioGroup where to find items with id = true_radio and id = false_radio;
	 * @param questionName - a question varname where to save answer;
	 * @return true if successfully get question with specified varname, find RadioGroup with specified id, get selection from items with required id.
	 */
	protected SubjectAnswer getYesNoAnswer(RadioGroup group, String questionName) {
		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return null;
		}

		SubjectAnswer answer = new SubjectAnswer();
		answer.setQuestionId(question.getId());
		int groupID = group.getCheckedRadioButtonId();
		if (groupID == R.id.true_radio) {
			answer.setAnswerId(Question.TRUE_ANSWER_ID);
		} else if (groupID == R.id.false_radio) {
			answer.setAnswerId(Question.FALSE_ANSWER_ID);
		} else {
			return null;
		}

		return answer;
	}

	/**
	 * Get a picture from the specified path and save it as subject answer to a question with the specified varname.
	 * @param photoPath - a path of picture which should be saved to the subject answer;
	 * @param questionName - a question varname where to save answer;
	 * @return true get a picture from the specified path and save it to the question with specified varname, false otherwise.
	 */
	protected boolean addPictureAnswer(String photoPath, String questionName) {
		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		if (null == photoPath || Strings.isEmpty(photoPath)) {
			LOGGER.warn("photoPath is empty for question with varname: " + questionName);
			return false;
		}

		SubjectAnswer answer = new SubjectAnswer();
		answer.setQuestionId(question.getId());
		answer.setPictureAnswerPlaceholder(photoPath);
		petitionAnswers.overwriteAnswer(answer);

		return true;
	}

	/**
	 * Adds/overwrites string answer, using hash map to convert selected item title to string answer.
	 * @param spinnerId - id of spinner control
	 * @param questionName - question name
	 * @param titlesToAnswersMap - Map<String, String> to convert selected item title to answer string
	 * @return true if successfully get answer title from spinner and convert it to answer string, false otherwise
	 */
	protected boolean addTextAnswerFromSpinner(int spinnerId, String questionName, Map<String, String> titlesToAnswersMap) {
		Spinner spinner = (Spinner) findViewById(spinnerId);
		//        if (null == spinner) {
		//            LOGGER.error("No spinner found for id = " + spinnerId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (question == null) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		String answerTitle = spinner.getSelectedItem().toString(); // Spinner always has selected item
		String answerString = titlesToAnswersMap.get(answerTitle);
		if (null == answerString || Strings.isEmpty(answerString)) {
			LOGGER.error("No string answer in titlesToAnswersMap for item title: " + answerTitle);
			return false;
		}

		SubjectAnswer answer = new SubjectAnswer(question.getId(), answerString);
		petitionAnswers.overwriteAnswer(answer);

		return true;
	}

	/**
	 * Sets spinner selected item by text answer from petitionAnswers, using hash map to convert string answer to item title.
	 * @param spinnerId - id of spinner control
	 * @param questionName - question name
	 * @param titlesToAnswersMap - Map<String, String> to convert selected item title to answer string (the same map as for addSpinnerTextAnswer() method)
	 * @return true if successfully get answer and convert it to title and select appropriate spinner item, otherwise false
	 */
	protected boolean setSpinnerSelectedItemByTextAnswer(int spinnerId, String questionName, Map<String, String> titlesToAnswersMap) {
		Spinner spinner = (Spinner) findViewById(spinnerId);
		//        if (null == spinner) {
		//            LOGGER.error("No spinner found for id = " + spinnerId);
		//            return false;
		//        }
		spinner.setSelection(0); // clear selection first: the 1st item defines no selection

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		String answerString = subjectAnswer.getStringAnswer();
		if (null == answerString || Strings.isEmpty(answerString)) {
			LOGGER.warn("No text answer found for question with varname: " + questionName);
			return false;
		}

		String answerTitle = null;
		for (Map.Entry entry : titlesToAnswersMap.entrySet()) {
			String entryAnswerString = (String)entry.getValue();
			if (entryAnswerString.equals(answerString)) {
				answerTitle = (String)entry.getKey();
				break;
			}
		}

		if (null == answerTitle || Strings.isEmpty(answerTitle)) {
			LOGGER.warn("No title found for answer: " + answerString);
			return false;
		}

		int answerItemIndex = -1;
		SpinnerAdapter spinnerAdapter = spinner.getAdapter();
		for (int i = 0; i < spinnerAdapter.getCount(); i++) {
			String itemTitle = spinnerAdapter.getItem(i).toString();
			if (answerTitle.equals(itemTitle)) {
				answerItemIndex = i;
				break;
			}
		}

		if (answerItemIndex < 0) {
			LOGGER.error("No item found for answer with title: " + answerTitle);
			return false;
		}

		spinner.setSelection(answerItemIndex);

		return true;
	}

	/**
	 * Adds/overwrites string answer, using hash map to convert selected radio button id to string answer.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToAnswersMap - Map<String, String> to convert selected radio button id to string answer
	 * @return true if successfully get selected radio button id and convert it to answer string, false otherwise
	 */
	protected boolean addTextAnswerFromRadioGroup(int radioGroupId, String questionName, Map<Integer, String> idsToAnswersMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
		if (selectedRadioButtonId < 0) {
			LOGGER.warn("No selected radio button for question with varname: " + questionName);
			return false;
		}

		String answerString = idsToAnswersMap.get(selectedRadioButtonId);
		if (null == answerString || Strings.isEmpty(answerString)) {
			LOGGER.error("No string answer in idsToAnswersMap for RadioButton id = " + selectedRadioButtonId);
			return false;
		}

		SubjectAnswer answer = new SubjectAnswer(question.getId(), answerString);
		petitionAnswers.overwriteAnswer(answer);

		return true;
	}

	/**
	 * Select radio button by text answer from petitionAnswers, using hash map to convert string answer to radio button id.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToAnswersMap - Map<String, String> to convert selected radio button id (key) to string answer (value), the same as for addTextAnswerFromRadioGroup() method
	 * @return true if successfully get answer and convert it to radio button id and select appropriate radio button, otherwise false
	 */
	protected boolean setSelectedRadioButtonByTextAnswer(int radioGroupId, String questionName, Map<Integer, String> idsToAnswersMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.clearCheck(); // clear selection first

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		String answerString = subjectAnswer.getStringAnswer();
		if (null == answerString || Strings.isEmpty(answerString)) {
			LOGGER.warn("No text answer found for question with varname: " + questionName);
			return false;
		}

		int radioButtonId = -1;
		for (Map.Entry entry : idsToAnswersMap.entrySet()) {
			String entryAnswerString = (String)entry.getValue();
			if (entryAnswerString.equals(answerString)) {
				radioButtonId = (Integer)entry.getKey();
				break;
			}
		}

		if (radioButtonId < 0) {
			LOGGER.error("No radio button id found for answer: " + answerString);
			return false;
		}

		RadioButton radioButton = (RadioButton)radioGroup.findViewById(radioButtonId);
		if (null == radioButton) {
			LOGGER.error("No radio button found for item id = " + radioButtonId);
			return false;
		}

		radioButton.setChecked(true);

		return true;
	}


	/**
	 * Adds/overwrites standard answer, using hash map to convert selected radio button id to string answer and then find suitable question answer.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToStringAnswersMap - Map<Integer, String> to convert selected radio button id to string answer (from question answers)
	 * @return true if successfully get selected radio button id, convert it to string answer and found suitable question answer, false otherwise
	 */
	protected boolean addStandardAnswerFromRadioGroupByAnswerString(int radioGroupId, String questionName, Map<Integer, String> idsToStringAnswersMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.warn("No answers for question with varname: " + questionName);
			return false;
		}

		int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
		if (selectedRadioButtonId < 0) {
			LOGGER.warn("No selected radio button for question with varname: " + questionName);
			return false;
		}

		String answerString = idsToStringAnswersMap.get(selectedRadioButtonId);
		if (null == answerString || Strings.isEmpty(answerString)) {
			LOGGER.error("No string answer in idsToStringAnswersMap for RadioButton id = " + selectedRadioButtonId);
			return false;
		}

		for (QuestionAnswer questionAnswer : questionAnswers) {
			if (questionAnswer.getValue().equalsIgnoreCase(answerString)) {
				SubjectAnswer subjectAnswer = new SubjectAnswer(questionAnswer);
				petitionAnswers.overwriteAnswer(subjectAnswer);
				return true;
			}
		}

		LOGGER.error("No question answer found for string answer = " + answerString);
		return false;
	}


	protected boolean addAnswersToRadioGroupFromStandardQuestionUsingTag(int radioGroupId, String questionName) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.removeAllViews();

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.error("No question in petition with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.error("No answers for question with varname: " + questionName);
			return false;
		}

		for (QuestionAnswer answer : questionAnswers) {
			int answerId = answer.getId();
			if (answerId < 0) {
				LOGGER.error("Answer has wrong Id = " + answerId + " for question with varname: " + questionName);
				continue;
			}

			String answerTitle = answer.getName();
			if (null == answerTitle || Strings.isEmpty(answerTitle)) {
				answerTitle = answer.getValue();
			}
			if (null == answerTitle || Strings.isEmpty(answerTitle)) {
				LOGGER.error("Answer with Id = " + answerId + " has no name or value for question with varname: " + questionName);
				continue;
			}

			//RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(radioButtonLayoutId, null);
			RadioButton radioButton = new RadioButton(this);
			radioButton.setText(answerTitle);
			radioButton.setTag(answerId);
			radioGroup.addView(radioButton);
		}

		return true;
	}

	/**
	 * Add items to a RadioGroup with specified id from answers for standard question with specified quesiton name and set each item tag to answer id.
	 * @param radioGroupId - id of RadioGroup where to add items from standard quesiton;
	 * @param questionName - a question varname where from to get answers;
	 * @param radioButtonLayoutId - a layout which will be used for each radio group item (optional);
	 * @return true if successfully get the standard question with the specified name and get answers for it, find the RadioGroup with the specified id and add items which correspond to the question answers, false otherwise.
	 */
	protected boolean addAnswersToRadioGroupFromStandardQuestionUsingTagWithCustomLayout(int radioGroupId, String questionName, int radioButtonLayoutId) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.removeAllViews();

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.error("No question in petition with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.error("No answers for question with varname: " + questionName);
			return false;
		}

		for (QuestionAnswer answer : questionAnswers) {
			int answerId = answer.getId();
			if (answerId < 0) {
				LOGGER.error("Answer has wrong Id = " + answerId + " for question with varname: " + questionName);
				continue;
			}

			String answerTitle = answer.getName();
			if (null == answerTitle || Strings.isEmpty(answerTitle)) {
				answerTitle = answer.getValue();
			}
			if (null == answerTitle || Strings.isEmpty(answerTitle)) {
				LOGGER.error("Answer with Id = " + answerId + " has no name or value for question with varname: " + questionName);
				continue;
			}

			RadioButton radioButton = null;
			if (radioButtonLayoutId > 0) {
				radioButton = (RadioButton) getLayoutInflater().inflate(radioButtonLayoutId, null);
			} else {
				radioButton = new RadioButton(this);
			}
			radioButton.setText(answerTitle);
			radioButton.setTag(answerId);
			radioGroup.addView(radioButton);
		}

		return true;
	}


	/**
	 * Adds/overwrites standard answer, using checked radio button tag as answer id.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @return true if successfully get selected radio button id, tag, and found suitable question answer, false otherwise
	 */
	protected boolean addStandardAnswerFromRadioGroupUsingTag(int radioGroupId, String questionName) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.warn("No answers for question with varname: " + questionName);
			return false;
		}

		int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
		if (selectedRadioButtonId < 0) {
			LOGGER.warn("No selected radio button for question with varname: " + questionName);
			return false;
		}

		RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(selectedRadioButtonId);
		if (null == checkedRadioButton) {
			LOGGER.warn("No selected radio button for question with varname: " + questionName);
			return false;
		}

		Integer chekedRadioButtonTag = (Integer) checkedRadioButton.getTag();
		if (null == chekedRadioButtonTag) {
			LOGGER.warn("Selected radio button has not Tag for question with varname: " + questionName);
			return false;
		}

		for (QuestionAnswer questionAnswer : questionAnswers) {
			if (questionAnswer.getId() == chekedRadioButtonTag) {
				SubjectAnswer subjectAnswer = new SubjectAnswer(questionAnswer);
				petitionAnswers.overwriteAnswer(subjectAnswer);
				return true;
			}
		}

		LOGGER.error("No question answer found for answer with id = " + chekedRadioButtonTag);
		return false;
	}

	/**
	 * Select radio button using standard answer for question with specified name from petitionAnswers, using radio button tag as answer id.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @return true if successfully select radio button with appropriate tag, otherwise false
	 */
	protected boolean setSelectedRadioButtonUsingStandardAnswerByTag(int radioGroupId, String questionName) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.clearCheck(); // clear selection first

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		int answerId = subjectAnswer.getAnswerId();
		if (SubjectAnswer.NO_VALUE == answerId) {
			LOGGER.error("No answer id found for question with varname: " + questionName);
			return false;
		}

		RadioButton radioButton = (RadioButton)radioGroup.findViewWithTag(answerId);
		if (null == radioButton) {
			LOGGER.error("No radio button found for tag = " + answerId);
			return false;
		}

		radioButton.setChecked(true);
		return true;
	}


	/**
	 * Adds/overwrites standard answer, using hash map to convert selected radio button id to string answer and then find suitable question answer.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToAnswersIdsMap - Map<Integer, Integer> to convert selected radio button id to answer id
	 * @return true if successfully get selected radio button id and convert it to answer id, false otherwise
	 */
	protected boolean addStandardAnswerFromRadioGroupByAnswerId(int radioGroupId, String questionName, Map<Integer, Integer> idsToAnswersIdsMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.warn("No answers for question with varname: " + questionName);
			return false;
		}

		int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();
		if (selectedRadioButtonId < 0) {
			LOGGER.warn("No selected radio button for question with varname: " + questionName);
			return false;
		}

		Integer answerId = idsToAnswersIdsMap.get(selectedRadioButtonId);
		if (null == answerId) {
			LOGGER.error("No answer id found in idsToAnswersIdsMap for RadioButton id = " + selectedRadioButtonId);
			return false;
		}

		for (QuestionAnswer questionAnswer : questionAnswers) {
			if (questionAnswer.getId() == answerId) {
				SubjectAnswer subjectAnswer = new SubjectAnswer(questionAnswer);
				petitionAnswers.overwriteAnswer(subjectAnswer);
				return true;
			}
		}

		LOGGER.error("No question answer found for answer with id = " + answerId);
		return false;
	}


	/**
	 * Select radio button by standard answer from petitionAnswers, using hash map to convert string answer (with appropriate answer id) to radio button id.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToStringAnswersMap - Map<Integer, String> to convert selected radio button id to string answer (from question answers), the same map as for addStandardAnswerFromRadioGroupUsingString() method
	 * @return true if successfully get standard answer id for specified question, convert it to answer string, convert it to radio button id and select radio button with appropriate id, otherwise false
	 */
	protected boolean setSelectedRadioButtonUsingStandardAnswerByAnswerString(int radioGroupId, String questionName, Map<Integer, String> idsToStringAnswersMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.clearCheck(); // clear selection first

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		int answerId = subjectAnswer.getAnswerId();
		if (SubjectAnswer.NO_VALUE == answerId) {
			LOGGER.error("No answer id found for question with varname: " + questionName);
			return false;
		}

		List<QuestionAnswer> questionAnswers = question.getAnswers();
		if (null == questionAnswers) {
			LOGGER.error("No answers for question with varname: " + questionName);
			return false;
		}

		String answerString = null;
		for (QuestionAnswer questionAnswer : questionAnswers) {
			if (questionAnswer.getId() == answerId) {
				answerString = questionAnswer.getValue();
				break;
			}
		}

		if (null == answerString) {
			LOGGER.error("No answer string found for question with varname: " + questionName);
			return false;
		}

		int radioButtonId = -1;
		for (Map.Entry entry : idsToStringAnswersMap.entrySet()) {
			String entryAnswerString = (String)entry.getValue();
			if (entryAnswerString.equalsIgnoreCase(answerString)) {
				radioButtonId = (Integer)entry.getKey();
				break;
			}
		}

		if (radioButtonId < 0) {
			LOGGER.error("No radio button id found for answer: " + answerString);
			return false;
		}

		RadioButton radioButton = (RadioButton)radioGroup.findViewById(radioButtonId);
		if (null == radioButton) {
			LOGGER.error("No radio button found for item id = " + radioButtonId);
			return false;
		}

		radioButton.setChecked(true);
		return true;
	}

	/**
	 * Select radio button by standard answer from petitionAnswers, using hash map to convert string answer (with appropriate answer id) to radio button id.
	 * @param radioGroupId - id of radio group
	 * @param questionName - question name
	 * @param idsToAnswersIdsMap - Map<Integer, Integer> to convert selected radio button id to answer id, the same map as for addStandardAnswerFromRadioGroupUsingId() method
	 * @return true if successfully get standard answer id for specified question, convert it to answer string, convert it to radio button id and select radio button with appropriate id, otherwise false
	 */
	protected boolean setSelectedRadioButtonUsingStandardAnswerByAnswerId(int radioGroupId, String questionName, Map<Integer, Integer> idsToAnswersIdsMap) {
		RadioGroup radioGroup = (RadioGroup) findViewById(radioGroupId);
		//        if (null == radioGroup) {
		//            LOGGER.error("No radio group found for id = " + radioGroupId);
		//            return false;
		//        }
		radioGroup.clearCheck(); // clear selection first

		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		int answerId = subjectAnswer.getAnswerId();
		if (SubjectAnswer.NO_VALUE == answerId) {
			LOGGER.error("No answer id found for question with varname: " + questionName);
			return false;
		}

		int radioButtonId = -1;
		for (Map.Entry entry : idsToAnswersIdsMap.entrySet()) {
			Integer entryAnswerId = (Integer)entry.getValue();
			if (entryAnswerId == answerId) {
				radioButtonId = (Integer)entry.getKey();
				break;
			}
		}

		if (radioButtonId < 0) {
			LOGGER.error("No radio button id found for answer with id = " + answerId);
			return false;
		}

		RadioButton radioButton = (RadioButton)radioGroup.findViewById(radioButtonId);
		if (null == radioButton) {
			LOGGER.error("No radio button found for item id = " + radioButtonId);
			return false;
		}

		radioButton.setChecked(true);
		return true;
	}

	/**
	 * Checks if a subject answer for a question with the specified varname exists.
	 * @param questionName - a varname of the question, which should be checked;
	 * @return true if subject answer for the specified question exists, false otherwise.
	 */
	protected boolean isAnswerExist(String questionName) {
		Question question = petition.getQuestionWithVarName(questionName);
		if (null == question) {
			LOGGER.warn("No question in petition with varname: " + questionName);
			return false;
		}

		SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
		if (null == subjectAnswer) {
			LOGGER.warn("No answer found for question with varname: " + questionName);
			return false;
		}

		return true;
	}



	/**
	 * Clear all EditText controls inside specified ViewGroup.
	 * @param vg - ViewGroup where to clear all EditText controls
	 */
	protected void clearEditTextViews(ViewGroup vg) {
		for (int i = 0; i < vg.getChildCount(); i++) {
			View v = (View)vg.getChildAt(i);
			if (v instanceof EditText) {
				((EditText)v).setText("");
			} else if (v instanceof ViewGroup && ((ViewGroup)v).getChildCount() > 0) {
				clearEditTextViews((ViewGroup)v);
			}
		}
	}

	public KnockEvent addBatteryInfoToKnock(KnockEvent knock) {
		EventInfo eventInfo = getGlobalData().getEventInfo(LocationProcessor.getLastKnownGpsLocation(getApplicationContext()), new BatteryPerformance(this));
		knock.addEventInfo(eventInfo);
		return knock;

	}
	
	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(broadcastReceiver);
	}

}
