package com.grassroots.petition.activities;

import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.petition.R;
import com.grassroots.petition.models.NearestNeighborInfo;
import com.grassroots.petition.models.Neighbor;
import com.grassroots.petition.models.ScriptProperty;
import com.grassroots.petition.tasks.FetchNearestNeighborTask;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;

public class NearestNeighborActivity extends BasePetitionActivity {
	ProgressDialog pd = null;
	NearestNeighborInfo nearestNeighborInfo = null;
	LayoutInflater inflater = null;
	TableLayout neighborsListTable = null;
	View nearestNeighborRowView = null;
	public static final String HTML_MIME_TYPE = "text/html";
	public static final String HTML_BEGIN = "<html><body><center><b>";
	public static final String HTML_END = "</b></center></body></html>";
	private Button backButton, nextButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nearestneighbor);
		fetchNearestNeighborDetails();
		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		if (null != headerTitle) {
			headerTitle.setText(getString(R.string.nearest_neighbor_header));
		}
		backButton = (Button) findViewById(R.id.back_button);
		nextButton = (Button) findViewById(R.id.next_button);
		nextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				finish();
			}
		});

		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void fetchNearestNeighborDetails() {
		
		FetchNearestNeighborTask nearestNeighborTask = new FetchNearestNeighborTask(
				this);
		try {
			if(isNetworkAvailable()){
				pd = ProgressDialog.show(this, "Loading nearest neighbor details",
						"Please wait ...");
				nearestNeighborInfo = nearestNeighborTask.execute().get();
				pd.dismiss();
				setUpViews();
			}else{
				
				AlertDialog.Builder builder = ConfirmCancelDialogBuilder.getAlertDialogBuilder(this);
		        builder.setMessage(getString(R.string.nonetworkconnection));
		        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialogInterface, int i) {
		                hideActiveDialog();
		                finish();
		            }
		        });
		        builder.create().show();
			}
		} catch (InterruptedException e) {
			pd.dismiss();
			e.printStackTrace();
		} catch (ExecutionException e) {
			pd.dismiss();
			e.printStackTrace();
		}
	}
	
	
	private boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private void setUpViews() {
		Log.d("nearestneighbor", " nearest neighbor info : no of neighbors : "
				+ nearestNeighborInfo.getNeighbors().size());
		WebView nearestNeighborInfoView = (WebView) findViewById(R.id.nearestNeighborhtmlValueTextView);
		List<ScriptProperty> scriptproperties = nearestNeighborInfo
				.getScriptProperties();
		List<Neighbor> neighborList = nearestNeighborInfo.getNeighbors();
		// Left side View
		for (int i = 0; i < scriptproperties.size(); i++) {
			if (scriptproperties.get(i).getName()
					.equalsIgnoreCase("NearestNeighbor")) {
				nearestNeighborInfoView.loadData(
						wrapQuestionInHtml(scriptproperties.get(i).getValue()),
						HTML_MIME_TYPE, null);
				break;
			}
		}

		// Right Side View
		inflater = LayoutInflater.from(this);
		neighborsListTable = (TableLayout) findViewById(R.id.neighborsListTableView);
		for (int i = 0; i < neighborList.size(); i++) {
			nearestNeighborRowView = inflater.inflate(
					R.layout.nearestneighbortablerow, null);
			TextView neighborNameView = (TextView) nearestNeighborRowView
					.findViewById(R.id.neighborNameTextView);
			TextView neighborAddressView = (TextView) nearestNeighborRowView
					.findViewById(R.id.neighborAddressTextView);
			neighborNameView.setText(neighborList.get(i).getFirstName() + " "
					+ neighborList.get(i).getLastName());
			neighborAddressView.setText(neighborList.get(i).getAddress());
			neighborsListTable.addView(nearestNeighborRowView);
		}
	}

	private String wrapQuestionInHtml(String questionText) {
		return HTML_BEGIN + questionText + HTML_END;
	}

}
