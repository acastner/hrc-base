package com.grassroots.petition.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.grassroots.petition.R;
import com.grassroots.petition.utils.ClassRetriever;
import org.apache.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey Sotnikov
 * Date: 25.07.13
 * Time: 15:50
 * Logout Prompt Activity
 * Starts from MidnightPromptService at 12.00am
 * Prompts user to cancel or proceed with logout
 */
public class BaseLogoutPromptActivity extends BaseActivity {

    private static final String TAG = BaseLogoutPromptActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private static final int TIMEOUT = 30000;

    private Button logoutButton;
    private Button cancelButton;
    private Timer timer;            /* Logout user after TIMEOUT period of inactivity */

    private boolean shouldLogoutOnPause;

    protected int getLayout() {
        return R.layout.logout_promt;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        shouldLogoutOnPause = true;

        setContentView(getLayout());

        logoutButton = (Button) findViewById(R.id.btnLogout);
        cancelButton = (Button) findViewById(R.id.btnCancel);

        if (logoutButton != null) {
            logoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    logoutUser();
                    finish();
                }
            });
        } else {
            LOGGER.error("Had null logoutButton");
        }

        if (cancelButton != null) {
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shouldLogoutOnPause = false;
                    finish();
                }
            });
        } else {
            LOGGER.error("Had null cancelButton");
        }

        startTimer();
    }


    @Override
    public void onPause() {
        stopTimer();
        if (shouldLogoutOnPause) logoutUser();
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        shouldLogoutOnPause = false;
        super.onBackPressed();
    }





    /**
     * Starts timer for TIMEOUT interval
     * Logout user after timer fires (in case of user inactivity)
     */
    private void startTimer() {
        stopTimer();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                timer.cancel();
                logoutUser();
                finish();
            }
        }, TIMEOUT);
    }

    /**
     * Stops timer
     */
    private void stopTimer() {
        if (null != timer) {
            timer.cancel();
            timer = null;
        }
    }




    /**
     * Stops timer
     */
    private void logoutUser() {
        shouldLogoutOnPause = false;
        Intent i = new Intent(getApplicationContext(), ClassRetriever.getLoginActivity(getApplicationContext()));
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

}
