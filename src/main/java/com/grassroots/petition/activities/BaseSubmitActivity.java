package com.grassroots.petition.activities;

import android.graphics.Color;
import android.os.Bundle;

import com.grassroots.modules.GrassrootsApiService;
import com.grassroots.petition.GlobalData;
import com.grassroots.utils.AuthenticationHeaderPreference;
import com.grassroots.utils.ImeiHeaderPreference;
import com.grassroots.utils.OAuthTokenPreference;
import com.grassroots.utils.SaleTokenPreference;
import com.grassroots.utils.StringPreference;

import javax.inject.Inject;

/**
 * Created by jasongallagher on 7/6/17.
 */

public class BaseSubmitActivity extends BasePetitionActivityWithMenu {

    protected @Inject @OAuthTokenPreference
    StringPreference oAuthTokenPreference;
    protected @Inject @ImeiHeaderPreference
    StringPreference imeiHeaderPreference;
    protected @Inject @SaleTokenPreference
    StringPreference saleTokenPreference;

    protected @Inject
    GrassrootsApiService restService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((GlobalData) getApplication()).getApplicationComponent().inject(this);
        setTitle("Review" + "-" + getLocationName());
        if(petition.isTraining())   {
            setTitle("Review" + "-" + getLocationName()+ "-TRAINING PAYMENTS MODE");
            setTitleColor(Color.RED);
        }
    }
}