package com.grassroots.petition.activities;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bugsense.trace.BugSenseHandler;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.exceptions.GoToActivityExceptionHandler;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.BatteryPerformance;
import com.grassroots.petition.utils.ClassRetriever;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.ConfirmCancelDialogBuilder;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

/**
 * Contains shared logic for all Activities, mostly around starting activities and sending/receiving data
 * between activities
 */
public class BaseActivity extends SherlockFragmentActivity {
    public static final boolean isDebug = false;
    public static final boolean isSilent = true;    // Don't sent emails and don't use bugsense

    public static final String LOGOUT_ACTION = "com.grassroots.petition.LOGOUT_ACTION";

    protected static final String ACTIVITY_RESULT_KEY = "ACTIVITY_RESULT_KEY";
    protected static final String CALLING_ACTIVITY_KEY = "CALLING_ACTIVITY_KEY";
    protected static final int NOTIFICATION_ID = 123;

    private static final String TAG = BaseActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private static final int LOGOUT_TIMEOUT = 30000;
    protected Dialog activeDialog = null;

    private BroadcastReceiver crashReceiver = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LOGGER.debug("Creating: " + this.getClass());
        Log.e("Creating: " , "Class : " +this.getClass().getSimpleName());
        String subjectInfoClassName = "FundSubjectInfoActivity";

        GrassrootsRestClient.initialize(getApplicationContext());
        Thread.setDefaultUncaughtExceptionHandler(new GoToActivityExceptionHandler(this));

       
        // Support for small displays (Samsung dart)
        /*
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            // Assume Android 4.0, 7.0" tablet screen size
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            // Assume Android 2.2, Samsung Dart
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        */
        
    }

    @Override
    public void onStart() {
        super.onStart();
        LOGGER.debug(this.getClass() + ":onStart()");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        LOGGER.debug(this.getClass() + ":onRestart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        LOGGER.debug(this.getClass() + ":onResume()");

        if (crashReceiver == null) {
            crashReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    logout();
                }
            };
        }

        if (!BaseLoginActivity.class.isAssignableFrom(this.getClass())) {
            IntentFilter crashIntentFilter = new IntentFilter(LOGOUT_ACTION);
            registerReceiver(crashReceiver, crashIntentFilter);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LOGGER.debug(this.getClass() + ":onPause()");
        if (!BaseLoginActivity.class.isAssignableFrom(this.getClass())) {
            unregisterReceiver(crashReceiver);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LOGGER.debug(this.getClass() + ":onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LOGGER.debug(this.getClass() + ":onDestroy()");
    }

    /**
     * Called after onNewIntent and only if activity should clear its state
     * Should be overridden in a subclass
     */
    public void onReload() {
        LOGGER.debug(this.getClass() + ":onReload()");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOGGER.debug(this.getClass() + ":onActivityResult() requestCode:" + requestCode + " resultCode:" + resultCode);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        int result = getActivityResult(intent);
        String callingActivity = getCallingActivityClassName(intent);
        LOGGER.debug(this.getClass() + ":onNewIntent() Activity:" + callingActivity + " Result:" + result);

        // Need to clear Activity
        if (result == RESULT_OK) onReload();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LOGGER.debug(this.getClass() + ":onBackPressed()");
    }


    /**
     * Activity result, needs to determine calling activity state
     * @return
     */
    protected int getActivityResult() {
        return getIntent().getIntExtra(ACTIVITY_RESULT_KEY, RESULT_OK);
    }

    protected void setActivityResult(int result) {
        getIntent().putExtra(ACTIVITY_RESULT_KEY, result);
        //setResult(result);
    }

    protected static int getActivityResult(Intent intent) {
        return intent.getIntExtra(ACTIVITY_RESULT_KEY, RESULT_OK);
    }

    protected static void setActivityResult(Intent intent, int result) {
        intent.putExtra(ACTIVITY_RESULT_KEY, result);
    }


    /**
     * Calling Activity className, needs to determine calling activity className
     * @return
     */
    protected String getCallingActivityClassName() {
        String className = getIntent().getStringExtra(CALLING_ACTIVITY_KEY);
        return className == null ? this.getClass().getName() : className;
    }

    protected void setCallingActivityClassName() {
        getIntent().putExtra(CALLING_ACTIVITY_KEY, this.getClass().getName());
    }

    protected static String getCallingActivityClassName(Intent intent) {
        return intent.getStringExtra(CALLING_ACTIVITY_KEY);
    }

    protected static void setCallingActivityClassName(Intent intent, String className) {
        intent.putExtra(CALLING_ACTIVITY_KEY, className);
    }


    /**
     * User Notification methods
     */
    public void notifyUser(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    /**
     * Show a ProgressDialog notification to the user
     * @param message
     */
    public void showProgress(String message) {
        hideActiveDialog();
        activeDialog = new ProgressDialog(this);
        ((ProgressDialog)activeDialog).setMessage(message); //"Logging in...");
        activeDialog.setCancelable(false);
        activeDialog.show();
    }

    /**
     * Hides an ActiveDialog from user
     */
    public void hideActiveDialog() {
        if (activeDialog != null) {
            try {
                activeDialog.dismiss();
            } catch (Exception ex) {

            }
            activeDialog = null;
        }
    }

    /**
     * Add notification icon into the status bar with custom message
     * @param message
     */
    public void showNotification(String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.icon) // notification icon
                .setContentTitle(getResources().getString(R.string.app_name)) // title for notification
                .setContentText(message) // message for notification
                .setAutoCancel(true); // clear notification after click

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    /**
     * Hides notification icon from status bar
     */
    public void hideNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }


    /**
     * Logout user from app
     */
    protected void logout() {
        try {
            EventInfo eventInfo = ((GlobalData) getApplicationContext()).getEventInfo(LocationProcessor.getLastKnownGpsLocation(getApplicationContext()), new BatteryPerformance(this));
            GrassrootsRestClient.getClient().logout(eventInfo);
            ((GlobalData) getApplicationContext()).clearPreferences();
            ((GlobalData) getApplicationContext()).clearPetition();
        } catch (Exception e) {
            String errorMessage = getResources().getString(R.string.unable_to_start_rps);
            LOGGER.error(errorMessage);
            notifyUser(errorMessage);
        } finally {
            startActivity(ClassRetriever.getLoginActivity(getApplicationContext()));
        }
    }

    /**
     * Force logout user from app with error message
     */
    public void logoutWithError(String errorMessage, Exception exception) {
        exception.printStackTrace();
        LOGGER.error( errorMessage );
        notifyUser( errorMessage );
        sendBugSenseException( errorMessage, exception );
        logout();
    }

    private void sendBugSenseException (String errorMessage, Exception exception)
    {
        BugSenseHandler.addCrashExtraData( "Error Message", errorMessage );
        BugSenseHandler.sendException( exception );
    }

    /**
     * Show dialog, and logout when user confirms
     */
    public void showLogoutDialog() {
        hideActiveDialog();
        activeDialog = ConfirmCancelDialogBuilder.createLogoutConfirmDialog(this, new ConfirmCancelDialogBuilder.ViewListener() {
            @Override
            public void onConfirm(Object value) {
                logout();
            }

            @Override
            public void onCancel() {
            }
        });
        activeDialog.show();
    }

    /**
     * Show logout dialog, wait LOGOUT_TIMEOUT, logout if user confirms or timeout occured
     */
    public void showLogoutDialogWithTimeout() {
        hideActiveDialog();
        showNotification(getString(R.string.YOU_WILL_BE_LOGGED_OFF_IN_30_SECONDS_MESSAGE)); // TODO move to strings

        // Setup Dissmiss timer
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                hideNotification();
                hideActiveDialog();
                timer.cancel();
                logout();
            }
        }, LOGOUT_TIMEOUT);

        activeDialog = ConfirmCancelDialogBuilder.createLogoutCancelDialog(this, new ConfirmCancelDialogBuilder.ViewListener() {
            @Override
            public void onConfirm(Object value) {
                timer.cancel();
            }

            @Override
            public void onCancel() {
            }
        });

        activeDialog.show();
    }





//	------------------------------------------------------------------------------------------------
//  Starting activities methods
//	------------------------------------------------------------------------------------------------
    /**
     * Start the specified activity with the provided arguments
     * @param activityClass
     * @param extras
     */
    public void startActivity(Class activityClass, Pair<String, Parcelable>... extras) {
        startActivity(activityClass, RESULT_OK, extras);
    }

    /**
     * Start the specified activity with the provided arguments. If ResultCode is RESULT_OK, onReload will be called
     * @param activityClass
     * @param resultCode
     * @param extras
     */
    public void startActivity(Class activityClass, int resultCode, Pair<String, Parcelable>... extras) {
        LOGGER.debug("From: " + this.getClass() + " starting activity: " + activityClass);

        Intent activityIntent = new Intent(this, activityClass);
        setActivityResult(activityIntent, resultCode);
        setCallingActivityClassName(activityIntent, this.getClass().getName());

        if (null != extras) {
            for (Pair<String, Parcelable> parcelable : extras) {
                activityIntent.putExtra(parcelable.first, parcelable.second);
            }
        }

        if (BaseLoginActivity.class.isAssignableFrom(activityClass)) {
            // Clear Activity stack on logout
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else {
            // Don't destroy activities, just bring to front
          //  activityIntent.setFlags(Intent.);
        }
        startActivity(activityIntent);
    }
}
