package com.grassroots.petition.activities;

import java.util.List;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.MarketingSlides;
import com.grassroots.petition.views.MarketingView;

/**
 * Either shows an individual slide show or a selection screen to select which folder to view.
 * The folders live in assets/marketing/
 *
 * You can have one or more folders here and a button will be show to choose from the different options.
 */
public class MarketingLinksActivity extends BasePetitionActivity
{
    private List<MarketingSlides> slidesList;

    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.marketing_links );
        configureHeader();
        configureSlides();
    }

    /**
     * Configures header text.
     */
    private void configureHeader ()
    {
        TextView header = (TextView) findViewById( R.id.header_title );
        header.setText( getString( R.string.marketing_slides ) );
    }

    /**
     * Configures view with buttons for multiple slide shows or a single dialog when only one folder exists.
     */
    private void configureSlides ()
    {
        // If there is only one slide show, show it
        slidesList = GlobalData.getSlides();
        if( slidesList.size() == 1 )
        {
            showSingleSlideShow();
        }
        // Otherwise, show a view to select which slide show to view
        else
        {
            addButtonsForAvailableSlideShows();
        }
    }

    /**
     * Configures a dialog with slides.
     */
    private void showSingleSlideShow ()
    {
        showSlides( slidesList.get( 0 ).getSlides(), true );
    }

    /**
     * Creates and shows a dialog with images to page through.
     * @param slides - list of bitmaps (slides)
     *
     * Selecting one of the buttons opens a dialog for that slide show.
     * @param shouldFinish
     */
    private void showSlides (List<Bitmap> slides, boolean shouldFinish)
    {
        createSlideDialog( slides, shouldFinish ).show();
    }

    /**
     * Creates a dialog with a list of slide images.
     *
     * @param imageBitmaps - list of bitmaps
     * @param shouldFinish
     * @return a dialog with image slides
     */
    public Dialog createSlideDialog (List<Bitmap> imageBitmaps, boolean shouldFinish)
    {
        // Configure view
        MarketingView marketingView = (MarketingView) View.inflate( this, R.layout.marketing, null );
        marketingView.setImageBitmaps( imageBitmaps );

        // Configure dialog
        Dialog marketingDialog = new Dialog( this, android.R.style.Theme_Black_NoTitleBar_Fullscreen );
        marketingDialog.setContentView( marketingView );
        if( shouldFinish )
        {
            marketingDialog.setOnCancelListener( createOnFinishListener() );
        }

        return marketingDialog;
    }
    
    /**
     * Creates listener that finishes the current activity.
     *
     * @return a cancel listener that finishes the current activity
     */
    private DialogInterface.OnCancelListener createOnFinishListener ()
    {
        return new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel (DialogInterface dialogInterface)
            {
                MarketingLinksActivity.this.finish();
            }
        };
    }

    /**
     * Adds a button to the view for each folder in assets/marketing/
     *
     * Selecting one of the buttons opens a dialog for that slide show.
     */
    private void addButtonsForAvailableSlideShows ()
    {
        LinearLayout buttonContainer = (LinearLayout) findViewById( R.id.slide_show_button_container );
        for ( final MarketingSlides slideShow : slidesList )
        {
            Button startSlideShowButton = createSlideShowButton( slideShow, buttonContainer );
            startSlideShowButton.setOnClickListener( createSlideShowListener( slideShow ) );
            buttonContainer.addView( startSlideShowButton );
        }
    }

    /**
     * Inflates the slide show button.
     *
     * @return an inflated button (styled in XML)
     */
    private Button createSlideShowButton( MarketingSlides slideShow, LinearLayout parent )
    {
        Button startSlideShowButton = (Button) getLayoutInflater().inflate( R.layout.slide_show_button, parent, false );
        startSlideShowButton.setText( slideShow.getName() );
        return startSlideShowButton;
    }

    /**
     */
    private View.OnClickListener createSlideShowListener (final MarketingSlides slideShow)
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                showSlides( slideShow.getSlides(), false );
            }
        };
    }
    
    @Override
    public void onBackPressed() {
    	Log.e("OnBack Pressed",  "BackPressed");
    	super.onBackPressed();
    	GlobalData.clearMarketingSlides();
    }
}
