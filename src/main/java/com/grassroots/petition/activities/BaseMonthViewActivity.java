package com.grassroots.petition.activities;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.models.FreeTimeInterval;
import com.grassroots.petition.tasks.UpdateAppointmentCalendarEventsTask;
import com.grassroots.petition.utils.ClassRetriever;
import com.squareup.timessquare.CalendarPickerView;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Date;
import java.util.List;

public class BaseMonthViewActivity extends BasePetitionActivity
{
	private static final String TAG = BaseMonthViewActivity.class.getSimpleName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private CalendarPickerView calendarPickerView;

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.calendar_month_view );
		configureCalendar();
		updateCalendar();
		configureButtons();
	}

	@Override
	public void onReload() {
		super.onReload();
		// remove previous selection if any
		if (null != calendarPickerView)
		{
			initializeCalendar();
		}

		// update appointments calendar data
		updateCalendar();
	}

	private void configureCalendar()
	{
		calendarPickerView = (CalendarPickerView) findViewById( R.id.calendar_view );
		initializeCalendar();
	}

	private void initializeCalendar()
	{
		Date today = new Date();
		java.util.Calendar nextYear = java.util.Calendar.getInstance();
		nextYear.add( java.util.Calendar.YEAR, 1 );
		calendarPickerView.init(today, nextYear.getTime());
		calendarPickerView.selectDate( today );
	}

	private void updateCalendar() {
		new UpdateAppointmentCalendarEventsTask(new UpdateAppointmentCalendarEventsTask.UpdateAppointmentCalendarEventsTaskListener() {
			@Override
			public void onPreExecute() {
				showProgress(getString(R.string.UPDATING_APPOINTMENT_CALENDAR_EVENTS_MESSAGE));
			}

			@Override
			public void onPostExecute(Boolean result, String resultString) {
				hideActiveDialog();

				if (result) {
					LOGGER.info("Appointments successfully updated.");
					// TODO: some specific code can be placed here
					// NOTE: updated appointments is already in appointmentCalendar
				} else {
					LOGGER.warn("Error to update appointments.");
				}
			}
		}).execute(getApplicationContext());
	}

	private void configureButtons()
	{
		Button endInterviewButton = (Button) findViewById(R.id.end_interview_button);
		if (null != endInterviewButton) {
			endInterviewButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					onEndInterview();
				}
			});
		}

		Button nextButton = (Button) findViewById(R.id.next_button);
		if (null != nextButton) {
			nextButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					goToNext();
				}
			});
		}

		Button skipButton = (Button) findViewById(R.id.skip_button);
		if (null != skipButton) {
			skipButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					onSkip();
				}
			});
		}

		Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
		if (null != marketingButton) {
			marketingButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					onMarketing();
				}
			});

			File imgDir = new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
			if(imgDir.list() != null)
			{
				marketingButton.setVisibility(View.VISIBLE);
			}
			else
			{
				marketingButton.setVisibility(View.GONE);
			}
		}
	}

	protected void onEndInterview() {
		startActivity(ClassRetriever.getTerminationActivity(getApplicationContext()));
	}

	protected void onSkip() {
		startActivity(getNextActivity());
	}

	protected void goToNext() {
		Date selectedDate = calendarPickerView.getSelectedDate();
		if (null == selectedDate)
		{
			notifyUser(getString(R.string.SELECT_DATE_MESSAGE));
			return;
		}

		List<FreeTimeInterval> freeIntervals = appointmentCalendar.getFreeIntervalsForDate(selectedDate);

		if (freeIntervals == null || freeIntervals.isEmpty()) {
			notifyUser(getString(R.string.NO_FREE_TIME_INTERVAL_ON_THE_DATE_MESSAGE));
			return;
		}

		startActivity( MultiDayFragmentActivity.createIntent(BaseMonthViewActivity.this,
				MultiDayFragmentActivity.createDayViewBundle(selectedDate), false) );
	}
}