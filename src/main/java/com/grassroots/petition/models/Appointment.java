package com.grassroots.petition.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.activities.BaseSubjectInfoActivity;
import com.grassroots.petition.utils.JsonParser;

import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;

/**
 * Stores data about an appointment, which has start time field (Date), duration (in minutes) and description (String) fields.
 * startTime field is serialized/deserialized to/from JSON have ISO8601 date format.
 * Could be created from an object of type EventTimeInterval .
 * Objects of this type are created on the client and serialized to JSON and then sent to the backend.
 */
public class Appointment implements Json {
	private static final String TAG = Appointment.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * Description of current appointment.
     */
    private String description;
    /**
     * Start date-time of current appointment.
     */
    private Date startTime;
    /**
     * Duration in minutes of current appointment.
     */
    private int duration;
    
    private int calendarId;

    /**
     * An EventTimeInterval object which was used to create current appointment if any, or null.
     */
    @Expose(serialize = false, deserialize = false)
    private EventTimeInterval event; // local object

    /**
     * Constructor with a full list of arguments.
     * @param description - description of current appointment
     * @param startTime - date-time of start time of current appointment
     * @param duration - duration in minutes of current appointment
     */
    public Appointment(String description, Date startTime, int duration, int calendarId)
    {
        this.description = description;
        this.startTime = startTime;
        this.duration = duration;
        this.calendarId = calendarId;
    }

    /**
     * Constructor to create an Appointment object from an existing EventTimeInterval object.
     * @param event - an EventTimeInterval object which is used to create an Appointment object.
     */
    public Appointment(EventTimeInterval event)
    {
        this.event = event;

        if (null == event) return;
        this.description = event.getTitle();
        this.startTime = event.getStartTime();
        this.duration = (int)event.getTimeIntervalInMinutes();
        this.calendarId = 3;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public EventTimeInterval getEvent() {
        return event;
    }
    
    public void setCalendarId(int id)
    {
    	calendarId = id;
    }
    
    public int getCalendarId()
    {
    	return calendarId;
    }

    /**
     * returns DateTimeFormatter to serialize/deserialize startTime and endTime fields from JSON
     * @return  DateTimeFormatter to serialize/deserialize startTime and endTime fields from JSON.
     */
    public DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ"); // ISO8601 date format
        //return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm"); // ISO8601 date format without Time Zone
    }

    @Override
    public String toJson() {
    	
    	LOGGER.debug("IM HERE");
    	
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (duration != that.duration) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + duration;
        return result;
    }

    /**
     * returns end time of current Appointment instead of duration
     * @return  a Date object with end time of current Appointment instead of duration
     */
    public Date getEndTime() {
        if (null == startTime) return null;
        Calendar c = Calendar.getInstance();
        c.setTime(startTime);
        c.add(Calendar.MINUTE, duration);
        return  c.getTime();
    }
}
