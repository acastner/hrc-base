package com.grassroots.petition.models;

import com.grassroots.petition.utils.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * A collection of knocks to send to the server
 * Implements Json interface
 */
public class Knocks implements Json {
    /**
     * List of knock events
     */
    private List<KnockEvent> knocks;

    public Knocks() {
        knocks = new ArrayList<KnockEvent>();
    }

    /**
     * Adds knock event to the list
     * @param event - knock event
     */
    public void addKnockEvent(KnockEvent event) {
        knocks.add(event);
    }

    public boolean hasKnocks() {
        return !knocks.isEmpty();
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }
}
