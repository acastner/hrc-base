package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cart (Container for Products list with cost and quantities) data model.
 */
public class Cart implements Json, Parcelable {
    /**
     * List of ProductOrder objects (Products + Quantity + Cost) which were added to cart.
     */
    private final List<ProductOrder> items = new ArrayList<ProductOrder>();

    /**
     * Payment data type field, used to show which type is used for paymentData field (ACHData or CardData).
     */
    private String paymentDataType;

    /**
     * Payment Processor ID
     */
    @SerializedName("PaymentProcessorTypeId")
    public int paymentProcessorTypeId;

    @SerializedName("MobileId")
    private String mobileId;

    @SerializedName("PaymentMethodType")
    private Integer PaymentMethodType;

    public void setPaymentMethodType(int PaymentMethodType) {
        this.PaymentMethodType = PaymentMethodType;
    }

    public Integer getPaymentMethodType()   { return PaymentMethodType; }

    public String getMobileId() {
        return mobileId;
    }

    public void setMobileId(String mobileId) {
        this.mobileId = mobileId;
    }

    public String getRecurring() {
        return recurring;
    }

    public void setRecurring(String recurring) {
        this.recurring = recurring;
    }

    public String recurring = "";
    private boolean isSpreedly;

    public boolean isSpreedly() {
        return isSpreedly;
    }

    public void setSpreedly(boolean spreedly) {
        isSpreedly = spreedly;
    }
    /**
     * Payment data types hash map, where key is payment data type name and value is payment data type.
     */
    private static final Map<String, Class<? extends ParcelableData>> paymentDataTypes = new HashMap<String, Class<? extends ParcelableData>>();
    {
        paymentDataTypes.put(CardData.class.getName(), CardData.class);
        paymentDataTypes.put(ACHData.class.getName(), ACHData.class);
    }

    /**
     * Payment data object used one of the available types ACHData or CardData
     */
    private ParcelableData paymentData;

    public ParcelableData getPaymentData() {
        return paymentData;
    }
    public void setPaymentData(ParcelableData data) {
        this.paymentDataType = data.getClass().getName();
        this.paymentData = data;
    }

    public void setPaymentDataToCard() {
        this.paymentDataType = "CardData";
    }

    public void clearPaymentData() {
        this.paymentDataType = null;
        this.paymentData = null;
    }
    
    /**
     * Returns the type of payment data, ACHData or CardData 
     */
    public String getPaymentDataType() {
        return paymentDataType;
    }

    /**
     * Boolean that represents a paper check payment
     *
     * Note: Handled separately from other payment types
     */
    private boolean isPaperCheck;

    /**
     * Sets the status of paper check payments
     *
     * @param isPaperCheck - set status to this value
     */
    public void setPaperCheck (boolean isPaperCheck)
    {
        this.isPaperCheck = isPaperCheck;
    }

    /**
     * Gets the status of paper check payments.
     *
     * @return the paper check payment status
     */
    public boolean isPaperCheckPayment ()
    {
        return isPaperCheck;
    }

    /**
     * Boolean that represents a cash payment
     *
     * Note: Handled separately from other payment types
     */
    private boolean isCash;

    /**
     * Sets the status of cash payments.
     *
     * @param isCash - set status to this value
     */
    public void setCash (boolean isCash)
    {
        this.isCash = isCash;
    }

    /**
     * Gets the status of cash payments.
     *
     * @return the cash payment status
     */
    public boolean isCashPayment ()
    {
        return isCash;
    }

    /**
     * Generates instances of Cart class from a Parcel
     */
    public static final Parcelable.Creator<Cart> CREATOR = new Creator<Cart>() {

        @Override
        public Cart createFromParcel(Parcel parcel) {
            return new Cart(parcel);
        }

        @Override
        public Cart[] newArray(int i) {
            return new Cart[i];
        }
    };

    /**
     * Key-name {@value} for storing Cart objects as key-value.
     */
    public static final String DATA_KEY = "Cart";

    /* Methods which implement Parcelable interface */
    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.<String, Parcelable>create(DATA_KEY, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Empty public constructor
     */
    public Cart() {

    }

    /**
     * Constructor which creates a Cart object from a Parcel object.
     */
    private Cart(Parcel parcel) {
        parcel.readTypedList(items, ProductOrder.CREATOR);

        paymentDataType = parcel.readString();
        if (paymentDataType != null) {
            Class<? extends ParcelableData> dataClass = paymentDataTypes.get(paymentDataType);
            if (dataClass != null)
                paymentData = parcel.readParcelable(dataClass.getClassLoader());
        }

        isCash = (0 != parcel.readInt());
        isPaperCheck = (0 != parcel.readInt());
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(getItems());
        parcel.writeString( paymentDataType );
        parcel.writeParcelable( paymentData, 0 );
        parcel.writeInt( isCash ? 1 : 0 );
        parcel.writeInt( isPaperCheck ? 1 : 0 );
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    public List<ProductOrder> getItems() {
        return items;
    }

    /**
     * Adds a donation product to items list (cost should be specified),
     * the product should NOT contain cost, i.e. cost field should be null.
     * This method is used to add a donation product to items list.
     * @param product - a product to add to items list.
     * @param cost - a cost of product (required)
     */
    public void AddProduct(Product product, String cost) {
        if (null == product || null == cost) return;

        AddItem(product, null, cost);
    }

    /**
     * Overwrites an existing donation product in the items list,
     * if items list contains a product with the same SKU number, otherwise adds a new one.
     * the product should NOT contain cost field, i.e. cost field should NOT be null.
     * @param product - a product to overwrite in items list.
     * @param cost - a cost of product (required)
     */
    public void OverwriteProduct(Product product, String cost) {
        if (null == product || null == cost) return;

        OverwriteItem(product, null, cost);
    }

    /**
     * Adds an ordinary product (not donation product) to items list (quantity should be specified)
     * the product should contain cost field, i.e. cost field should NOT be null.
     * @param product - a product to add to items list.
     * @param productQuantity - quantity of products (required)
     */
    public void AddProduct(Product product, int productQuantity) {
        if (null == product || productQuantity < 1) return;
        AddItem(product, productQuantity, null);
    }

    /**
     * Overwrites an existing ordinary product (not donation product) in the items list,
     * if items list contains a product with the same SKU number, otherwise adds a new one.
     * the product should contain cost field, i.e. cost field should NOT be null.
     * @param product - a product to overwrite in items list.
     * @param productQuantity - quantity of products (required)
     */
    public void OverwriteProduct(Product product, int productQuantity) {
        if (null == product || productQuantity < 1) return;
        OverwriteItem(product, productQuantity, null);
    }

    /**
     * Adds a ProductOrder object to items list, using Product object, quantity, and cost.
     * If items list contains product with the same SKU number as the product which is added,
     * and the product which is added is not an donation (productQuantity != null), then
     * the method increase quantity of existing product in items list.
     * productQuantity and productCost can not be simultaneously null or not null.
     * @param product - a Product object which is used to create new ProductOrder object
     * @param productQuantity - quantity of the specified product, which is used to create new ProductOrder object (can be null - means the product is a donation)
     * @param productCost - cost of the specified product, which is used to create new ProductOrder object (can be null - means the product is NOT a donation, but an ordinary product)
     */
    private void AddItem(Product product, Integer productQuantity, String productCost) {
        if (null == product ||
                (null == productQuantity && null == productCost) ||
                (null != productQuantity && null != productCost))
            return;

        for (ProductOrder item : items) {
            if (item.getSKU().equals(product.getSKU()))
            {
                if (null == productCost) { // ordinary product
                    item.setQuantity(item.getQuantity() + productQuantity);
                } else { // donation
                    // already in items
                }
                return;
            }
        }

        if (null == productCost) { // ordinary product
            items.add(new ProductOrder(product, productQuantity));
        } else { // donation
            items.add(new ProductOrder(product, productCost));
        }
    }

    /**
     * Adds new ProductOrder object to the items list
     * Or replace an existing ProductOrder object with the new one (completely remove previous ProductOrder object).
     * productQuantity and productCost can not be simultaneously null or not null.
     * @param product - a Product object which is used to create new ProductOrder object
     * @param productQuantity - quantity of the specified product, which is used to create new ProductOrder object (can be null - means the product is a donation)
     * @param productCost - cost of the specified product, which is used to create new ProductOrder object (can be null - means the product is NOT a donation, but an ordinary product)
     */
    private void OverwriteItem(Product product, Integer productQuantity, String productCost) {
        if (null == product ||
                (null == productQuantity && null == productCost) ||
                (null != productQuantity && null != productCost))
            return;

        for (ProductOrder item : items) {
            if (item.getSKU().equals(product.getSKU())) {
                items.remove(item);
                break;
            }
        }

        if (null == productCost) { // ordinary product
            items.add(new ProductOrder(product, productQuantity));
        } else { // donation
            items.add(new ProductOrder(product, productCost));
        }
    }

    /**
     * Removes specified product from items list using it's SKU.
     * @param product - a product which should be removed from items list.
     */
    public void removeProduct(Product product) {
        removeItem(product.getSKU());
    }

    /**
     * Removes all products from items list.
     */
    public void removeAllProducts() {
        items.clear();
    }

    /**
     * Remove product from items list by specified SKU.
     * @param SKU - string with SKU number which is used to remove a product from items list, if any.
     */
    private void removeItem(String SKU) {
        for (ProductOrder item : items) {
            if (item.getSKU().equals(SKU)) {
                items.remove(item);
                break;
            }
        }
    }

    /**
     * Set Payment Processor ID for this cart
     */

    public void setPaymentProcessorTypeId(int paymentProcessorTypeId)    {
        this.paymentProcessorTypeId = paymentProcessorTypeId;
    }

    /**
     * Get Payment Processor ID for this cart
     */

    public int getPaymentProcessorTypeId() {
        return paymentProcessorTypeId;
    }
}
