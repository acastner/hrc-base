package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

/**
 * A potential answer to a multiple choice question. Not to be confused with a SubjectAnswer
 */
public class QuestionAnswer implements Parcelable {
    public static final Parcelable.Creator<QuestionAnswer> CREATOR = new Creator<QuestionAnswer>() {
        @Override
        public QuestionAnswer createFromParcel(Parcel parcel) {
            return new QuestionAnswer(parcel);
        }

        @Override
        public QuestionAnswer[] newArray(int i) {
            return new QuestionAnswer[i];
        }
    };

    /**
     * Predefined constant representing no branch: {@value #NO_BRANCH}
     */
    private static final int NO_BRANCH = -1;
    /**
     * Question id question answer belongs to
     */
    @SerializedName("Question")
    private int questionId;
    /**
     * answer id
     */
    private int id;
    /**
     * answer titles
     */
    private String name;
    /**
     * answer value (which is showed to the user)
     */
    private String value;
    /**
     * branch flag, indicates next answer id
     */
    private int branch = NO_BRANCH;

    public QuestionAnswer() {

    }

    public QuestionAnswer(int questionId, String name, String value) {
        this.questionId = questionId;
        this.name = name;
        this.value = value;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getBranch() {
        return branch;
    }

    public void setBranch(int branch) {
        this.branch = branch;
    }

    /**
     * Indicates whether question contains a branch
     */
    public boolean hasBranch() {
        return branch != NO_BRANCH;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(questionId);
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(value);
        parcel.writeInt(branch);
    }

    private QuestionAnswer(Parcel input) {
        questionId = input.readInt();
        id = input.readInt();
        name = input.readString();
        value = input.readString();
        branch = input.readInt();
    }
}
