package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;
/**
 * Location model (retrieved if locationlist is enabled for petition)
 * @author sharika
 *
 */
public class Location implements Json{

    @SerializedName("Name")
    private String name;
    
    @SerializedName("Subjects")
    private int subjects;
    
    @SerializedName("Canvassed")
    private int canvassed;
    
    @SerializedName("Success")
    private int success;
    
    @SerializedName("Attempted")
    private int attempted;
    
    @SerializedName("Unattempted")
    private int unattempted;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSubjects() {
        return subjects;
    }

    public void setSubjects(int subjects) {
        this.subjects = subjects;
    }

    public int getCanvassed() {
        return canvassed;
    }

    public void setCanvassed(int canvassed) {
        this.canvassed = canvassed;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getAttempted() {
        return attempted;
    }

    public void setAttempted(int attempted) {
        this.attempted = attempted;
    }

    public int getUnattempted() {
        return unattempted;
    }

    public void setUnattempted(int unattempted) {
        this.unattempted = unattempted;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);    
    }

}
