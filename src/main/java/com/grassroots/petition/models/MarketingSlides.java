package com.grassroots.petition.models;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;

import org.apache.log4j.Logger;

import com.grassroots.petition.GlobalData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the slides to show in a marketing presentation.
 */
public class MarketingSlides {
	private static final String TAG = MarketingSlides.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	/**
	 * Looks in the MARKETING_FOLDER for folders containing images and uses them to generate MarketingSlides,
	 * one collection of bitmaps per folder
	 * @param context calling activity
	 * @return a list of marketing slides
	 */
	public static List<MarketingSlides> getMarketingSlides(Context context) {
		List<MarketingSlides> slides = new ArrayList<MarketingSlides>();
		AssetManager assetManager = context.getAssets();
		try {

			//TODO FIXME This needs to get rolled into a common function with MarketingMaterialsType.java
			String mFolder = MarketingMaterialType.MARKETING_FOLDER;
			if (!((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL)) {
				// Assume Android 2.2, Samsung Dart screen size
				mFolder = MarketingMaterialType.MARKETING_FOLDER_SMALL;
			}

			List<Bitmap> slideImages = new ArrayList<Bitmap>();
			String[] marketingFolders = assetManager.list(mFolder);
			for(String folder : marketingFolders) {
				String folderPath = mFolder + File.separator + folder;
				String[] imageNames = assetManager.list(folderPath);

				Bitmap imageBitmap = null;
				Bitmap imageBitmapMutable = null;
				for(String imageName : imageNames) {
					InputStream imageStream = assetManager.open(folderPath + File.separator + imageName);

					try{

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig = Config.RGB_565;
						Bitmap bitmap = BitmapFactory.decodeStream(imageStream, null, options);
						imageBitmapMutable =  Bitmap.createScaledBitmap(bitmap,bitmap.getWidth(), bitmap.getHeight(), true);
						imageStream.close();
					}catch(OutOfMemoryError e){
						try{
							BitmapFactory.Options options=new BitmapFactory.Options();
							options.inSampleSize = 4;
							options.inScaled = false;
							imageBitmapMutable=BitmapFactory.decodeStream(imageStream,null,options);
						}catch(OutOfMemoryError e2){
							//If it crashes again return null here and show a message to the user that marketing materials cannot be loaded.
							return null;
						}
					}

					slideImages.add(imageBitmapMutable);
				}
						
			}

			//grabbing images from android internal file system
			try {
				File fDir=new File(GlobalData.TMP_DIRECTORY + File.separator + "marketing");
				String[] files = fDir.list();

				for(String file : files)
				{
					File f=new File(fDir, file);
					Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
					slideImages.add(b);
				}
				slides.add(new MarketingSlides(fDir.getName(), slideImages));
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}	
			
		} catch (IOException e) {
			LOGGER.error("Trouble retrieving marketing folders", e);
		}

		return slides;
	}

	/**
	 * Name of folder with slides
	 */
	private final String name;
	/**
	 * List of bitmaps from each folder
	 */
	private final List<Bitmap> slides;
	public MarketingSlides(String name, List<Bitmap> slides) {
		this.name = name;
		this.slides = slides;
	}

	public String getName() {
		return name;
	}

	public List<Bitmap> getSlides() {
		return slides;
	}
}
