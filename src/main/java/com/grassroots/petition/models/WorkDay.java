package com.grassroots.petition.models;

import com.grassroots.petition.utils.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Collections;

/**
 * Stores data about free time intervals divided by days of week.
 * Each day of week is representing by corresponding field.
 */
public class WorkDay implements Json {
    /**
     * Stores a list of free time intervals for Monday.
     */
    private List<FreeTimeInterval> Monday;
    /**
     * Stores a list of free time intervals for Tuesday.
     */
    private List<FreeTimeInterval> Tuesday;
    /**
     * Stores a list of free time intervals for Wednesday.
     */
    private List<FreeTimeInterval> Wednesday;
    /**
     * Stores a list of free time intervals for Thursday.
     */
    private List<FreeTimeInterval> Thursday;
    /**
     * Stores a list of free time intervals for Friday.
     */
    private List<FreeTimeInterval> Friday;
    /**
     * Stores a list of free time intervals for Saturday.
     */
    private List<FreeTimeInterval> Saturday;
    /**
     * Stores a list of free time intervals for Sunday.
     */
    private List<FreeTimeInterval> Sunday;

    public List<FreeTimeInterval> getMonday()
    {
        return this.Monday;
    }

    public List<FreeTimeInterval> getTuesday()
    {
        return this.Tuesday;
    }

    public List<FreeTimeInterval> getWednesday()
    {
        return this.Wednesday;
    }

    public List<FreeTimeInterval> getThursday()
    {
        return this.Thursday;
    }

    public List<FreeTimeInterval> getFriday()
    {
        return this.Friday;
    }

    public List<FreeTimeInterval> getSaturday()
    {
        return this.Saturday;
    }

    public List<FreeTimeInterval> getSunday()
    {
        return this.Sunday;
    }

    /**
     * Returns an list of free time intervals for the day of week from the specified date.
     * @param date - date which is used to get free time intervals.
     * @return  list of free time intervals for the day of week from the specified date.
     */
    public List<FreeTimeInterval> getFreeIntervalsForDate(Date date)
    {
        List<FreeTimeInterval> eventsList = getFreeIntervalsForDay(date.getDay());
        if (null == eventsList) {
            eventsList = new ArrayList<FreeTimeInterval>();
        } else {
            for (FreeTimeInterval timeInterval : eventsList) {
                timeInterval.setDay(date);
            }
            Collections.sort(eventsList, new BaseTimeInterval.TimeIntervalsComparator());
        }

        return eventsList;
    }

    /**
     * Returns an list of free time intervals for the day of week by it's number.
     * @param day - number of day of week to get free time intervals for.
     * @return  list of free time intervals for the specified day of week.
     */
    public List<FreeTimeInterval> getFreeIntervalsForDay(int day)
    {
        switch (day)
        {
            case 1: return this.Monday;
            case 2: return this.Tuesday;
            case 3: return this.Wednesday;
            case 4: return this.Thursday;
            case 5: return this.Friday;
            case 6: return this.Sunday;
            case 0: return this.Sunday;
        }

        return new ArrayList<FreeTimeInterval>();
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkDay that = (WorkDay) o;

        if (Monday != null ? !Monday.equals(that.Monday) : that.Monday != null) return false;
        if (Tuesday != null ? !Tuesday.equals(that.Tuesday) : that.Tuesday != null) return false;
        if (Wednesday != null ? !Wednesday.equals(that.Wednesday) : that.Wednesday != null) return false;
        if (Thursday != null ? !Thursday.equals(that.Thursday) : that.Thursday != null) return false;
        if (Friday != null ? !Friday.equals(that.Friday) : that.Friday != null) return false;
        if (Saturday != null ? !Saturday.equals(that.Saturday) : that.Saturday != null) return false;
        if (Sunday != null ? !Sunday.equals(that.Sunday) : that.Sunday != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = Monday != null ? Monday.hashCode() : 0;
        result = 31 * result + (Tuesday != null ? Tuesday.hashCode() : 0);
        result = 31 * result + (Wednesday != null ? Wednesday.hashCode() : 0);
        result = 31 * result + (Thursday != null ? Thursday.hashCode() : 0);
        result = 31 * result + (Friday != null ? Friday.hashCode() : 0);
        result = 31 * result + (Saturday != null ? Saturday.hashCode() : 0);
        result = 31 * result + (Sunday != null ? Sunday.hashCode() : 0);
        return result;
    }
}
