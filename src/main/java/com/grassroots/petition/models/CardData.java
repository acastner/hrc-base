package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.grassroots.petition.utils.JsonParser;
import org.apache.log4j.Logger;

/**
 * ACH - Automated Clearing House network
 * Data model used for payment by Credit Cards.
 */
public class CardData implements ParcelableData {
    private static final String TAG = CardData.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Key-name {@value} for storing CardData objects as key-value.
     */
    public static final String DATA_KEY = "CardData";
    private final String expiry;

    @Override
    public String getKey(){
        return DATA_KEY;
    }

    /**
     * String for storing a device name.
     */
    private final String device;
    public String getDevice() {
        return device;
    }

    /**
     * String for storing a device serial number.
     */
    private final String serial;


    public String getSerial() {
        return serial;
    }

    /**
     * String for storing data from track #1.
     */
    private final String track1;
    public String getTrack1() {
        return track1;
    }

    /**
     * String for storing data from track #2.
     */
    private final String track2;
	private final String cardholder;
	private final String encryptedTrack;
    public String getTrack2() {
        return track2;
    }


    /* Methods which implement Parcelable interface */

    public String getCardholder() {
		return cardholder;
	}

	public String getEncTrack() {
		return encryptedTrack;
	}

	public String getExpiry() {return expiry;}

    public String getMaskedPan() { return maskedPan;}


	/**
     * Generates instances of CardData class from a Parcel
     */
    public static final Parcelable.Creator<CardData> CREATOR = new Creator<CardData>() {
        @Override
        public CardData createFromParcel(Parcel parcel) {
            return new CardData(parcel);
        }

        @Override
        public CardData[] newArray(int i) {
            return new CardData[i];
        }
    };

    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.<String, Parcelable>create(DATA_KEY, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }



    /**
     * Constructor to create a CardData object from device, serial, track1, track2 strings.
     */
    public CardData(String device, String serial, String track1, String track2,String cardholder,String encTrack) {
        this.device = device;
        this.serial = serial;
        this.track1 = track1;
        this.track2 = track2;
        this.cardholder = cardholder;
        this.encryptedTrack = encTrack;
        this.expiry = "";
        this.maskedPan = "";
    }

    private String maskedPan = "";

    public CardData(String device, String serial, String track1, String track2,String cardholder,String encTrack, String expiry, String maskedPan) {
        this.device = device;
        this.serial = serial;
        this.track1 = track1;
        this.track2 = track2;
        this.cardholder = cardholder;
        this.encryptedTrack = encTrack;
        this.expiry = expiry;
        this.maskedPan = maskedPan;
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(device);
        parcel.writeString(serial);
        parcel.writeString(track1);
        parcel.writeString(track2);
        parcel.writeString(cardholder);
        parcel.writeString(encryptedTrack);
    }

    private CardData(Parcel parcel) {
        device = parcel.readString();
        serial = parcel.readString();
        track1 = parcel.readString();
        track2 = parcel.readString();
        cardholder = parcel.readString();
        encryptedTrack = parcel.readString();
        this.expiry = "";
        this.maskedPan = "";
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }
}
