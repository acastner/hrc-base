package com.grassroots.petition.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.Strings;

/**
 * Canvasser info, petition script, and questions to ask
 * Deserialized from JSON, which is loaded from the backend.
 */
public class Petition implements Json {

    public boolean isTraining() {
        return isTraining;
    }

    public void setTraining(boolean training) {
        isTraining = training;
    }

    @SerializedName("IsTraining")
    private boolean isTraining = false;



    /**
     * Key-name {@value} for storing Petition object as key-value.
     */
    public static final String PETITION_KEY = "Petition";

    /**
     * full json string
     */
    private JSONObject petitionJSON;
    /**
     * email string
     */
    private String email = "Email" ;
    /**
     * Petition name
     */
    private String name;
    /**
     * Petition script name
     */
    private String script;
    /**
     * Is Petition requires subject's signature
     */
    private boolean signature;
    /**
     * Petition description
     */
    private String petition;
    /**
     * Canvasser first name
     */
    @SerializedName("UserFirstName")
    private String canvasserFirstName;
    /**
     * Canvasser last name
     */
    @SerializedName("UserLastName")
    private String canvasserLastName;
    /**
     * List of questions
     */
    private List<Question> questions;
    /**
     * List of answers for provided questions
     */
    private List<QuestionAnswer> answers;
    /**
     * List of products/donations
     */
    private List<Product> products;
    /**
     * Petition Id
     */
    private int id;

    /**
     * Flag to indicate that current Petition supports TransUnion Credit Check
     */
    @SerializedName("CreditCheck")
    private boolean supportCreditCheck;

    /**
     * Flag to indicate that current Petition supports creation of new Appointment for each subject (Reservation)
     */
    @SerializedName("Reservation")
    private boolean supportReservation;
    /**
     * Minimum reservation time in minutes
     */
    @SerializedName("ReservationMinimum")
    private int minimumReservationTime;
    /**
     * Suggested reservation time in minutes
     */
    @SerializedName("ReservationSuggested")
    private int suggestedReservationTime;
    /**
     * Maximum reservation time in minutes
     */
    @SerializedName("ReservationMaximum")
    private int maximumReservationTime;

    /**
     * Maximum reservation time in minutes
     */
    @SerializedName("Bidirectional")
    private boolean bidirectional;
    
    //payment related fields from script properties
 private int paymentProcessorId;
    
    private long paymentProcessorMerchantId;
    
    private String paymentProcessorUser;
    
    private String paymentProcessorPassword;
    
    private String merchantGroupingId;

    private String affiliate;

    private String campaign;

    private String reportGroup;

    
    public void setPaymentProcessorId(int id)
    {
    	paymentProcessorId = id;
    }
    
    public int getPaymentProcessorId()
    {
    	return paymentProcessorId;
    }
    
    public void setPaymentProcessorMerchantId(long id)
    {
    	paymentProcessorMerchantId = id;
    }
    
    public long getPaymentProcessorMerchantId()
    {
    	return paymentProcessorMerchantId;
    }
    
    public void setPaymentProcessorUser(String user)
    {
    	paymentProcessorUser = user;
    }
    
    public String getPaymentProcessorUser()
    {
    	return paymentProcessorUser;
    }
    
    public void setPaymentProcessorPassword(String pw)
    {
    	paymentProcessorPassword = pw;
    }
    
    public String getPaymentProcessorPassword()
    {
    	return paymentProcessorPassword;
    }
    
    
    /**
     * Is Petition has walklist
     */
    @SerializedName("Walklist")
    private boolean walklist;
    
    
    /**
     * Is Petition has nearestneighbor
     */
    private boolean nearestNeighbor;
    
    public boolean isWalklist() {
        return walklist;
    }

    public void setWalklist(boolean walklist) {
        this.walklist = walklist;
    }

    public boolean isLocationList() {
        return locationList;
    }

    public void setLocationList(boolean locationList) {
        this.locationList = locationList;
    }

    /**
     * Is Petition has walklist
     */
    @SerializedName("LocationList")
    private boolean locationList;
    
    public boolean isBidirectional() {
        return bidirectional;
    }

    public void setBidirectional(boolean bidirectional) {
        this.bidirectional = bidirectional;
    }

    public String getCanvasserFirstName() {
        return canvasserFirstName;
    }

    public void setCanvasserFirstName(String canvasserFirstName) {
        this.canvasserFirstName = canvasserFirstName;
    }

    public String getCanvasserLastName() {
        return canvasserLastName;
    }

    public void setCanvasserLastName(String canvasserLastName) {
        this.canvasserLastName = canvasserLastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public boolean isSignature() {
        return signature;
    }

    public void setSignature(boolean signature) {
        this.signature = signature;
    }

    public String getPetition() {
        return petition;
    }

    public void setPetition(String petition) {
        this.petition = petition;
    }

    public void setAnswers(List<QuestionAnswer> answers) {
        this.answers = answers;
    }

    public List<Product> getProducts() {
        if(products == null)
            return Collections.emptyList();
        return products;
    }

    /**
     * Provides a list of terminated questions (magic questions for termination page)
     * @return a list of terminated questions
     */
    public List<Question> getTerminatedQuestions() {
        List<Question> terminatedQuestions = new ArrayList<Question>();
        for (Question question : getQuestions()) {
            if (question.isTerminated())
                terminatedQuestions.add(question);
        }
        return terminatedQuestions;
    }

    /**
     * Provides a list of shipping questions (magic questions for shipping information page)
     * @return a list of shipping questions
     */
    public List<Question> getShippingQuestions() {
        List<Question> shippingQuestions = new ArrayList<Question>();
        for (Question question : getQuestions()) {
            if (question.isShipping())
                shippingQuestions.add(question);
        }
        return shippingQuestions;
    }

    /**
     * Indicates whether petition contains shipping questions
     * @return true if petition has shipping questions and false otherwise
     */
    public boolean hasShippingQuestions() {
        for (Question question : getQuestions()) {
            if (question.isShipping()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Indicates whether petition contains referrer questions
     * @return true if petition has referrer questions and false otherwise
     */
    public boolean hasReferrerQuestions() {
        for (Question question : getQuestions()) {
            if (question.isReferrer()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Indicates whether petition contains at least one product
     * @return true if petition has products and false otherwise
     */
    public boolean hasProducts() {
        return products != null && !products.isEmpty();
    }

    /**
     * Indicates whether petition contains at least one Donation product
     * (Donation product is a product which has cost equals to null)
     * @return true if petition has donation and false otherwise
     */
    public boolean hasDonation() {
        // TODO: need to implement right implementation
        if (null != products && !products.isEmpty()) {
            for (Product product: products) {
                String cost = product.getCost();
                if (Strings.isEmpty(cost)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get a map of the referrer number (IE, for question var name $referrer3, referrer number is 3) to the
     * referrer questions: full name, email, phone
     * @return map of the referrer number to the questions
     */
    public Map<Integer, List<Question>> getReferrerQuestions() {
        Map<Integer, List<Question>> referrerNumberToQuestions = new HashMap<Integer, List<Question>>();
        for (Question question : getQuestions()) {
            if (question.isReferrer()) {
                int referrerNumber = question.getReferrerNumber();

                if (referrerNumber < 0)
                    continue;
                List<Question> questions = referrerNumberToQuestions.get(referrerNumber);
                if (questions == null) {
                    questions = new ArrayList<Question>();
                    referrerNumberToQuestions.put(referrerNumber, questions);
                }
                questions.add(question);
            }
        }
        return referrerNumberToQuestions;
    }

    /**
     * Provides a list of subject information questions (magic questions for subject information page)
     * @return a list of subject information questions
     */
    public List<Question> getSubjectInformationQuestions() {//TODO caching here so we aren't recalculating this constantly
        List<Question> subjectInformationQuestions = new ArrayList<Question>();
        for (Question question : getQuestions()) {
            if (question.isSubjectInfo()) {
                subjectInformationQuestions.add(question);
            }
        }

		Question email = getEmailQuestionIfMagic();
		if(email != null){
			subjectInformationQuestions.add(email);
		}
        return subjectInformationQuestions;
    }

    /**
     * Provides a list of petition questions with varnames specified in questionNames
     * @param questionNames a list of questions varnames
     * @return list of petition questions
     */
    public List<Question> getQuestionWithVarNames(Collection<String> questionNames) {
        List<Question> questions = new ArrayList<Question>();
        for(String questionName : questionNames) { //TODO use a map here so this is faster
            for(Question question : getQuestions()) {
                if(questionName.equals(question.getVarNameNo$())) {
                    questions.add(question);
                    break;
                }
            }
        }
        return questions;
    }

    /**
     * Provide question with specified varnaem
     * @param varName - varname of requred question
     * @return question with the specified varname
     */
    public Question getQuestionWithVarName(String varName) {
        if(Strings.isEmpty(varName))
            return null;
        for(Question question : getQuestions()) {
            if(varName.equals(question.getVarNameNo$())) {
                return question;
            }
        }
        return null;
    }

    /**
     * Indicates whether petition contains non-magic questions
     * @return true if petition contains non magic questions
     */
    public boolean hasNonMagicQuestions() {
        for (Question question : getQuestions()) {
            if (!question.isMagic()) {
                return true;
            }
        }
        return false;
    }
    /**
     * Get the questions that are not considered magic. Only non magic questions are displayed in the Questions view
     * @return a list of Non Magic questions
     */
    public List<Question> getNonMagicQuestions() {
        List<Question> nonSubjectInformationQuestions = new ArrayList<Question>();

        for (Question question : getQuestions()) {
            if (!question.isMagic()) {
                nonSubjectInformationQuestions.add(question);
            }
        }
        return nonSubjectInformationQuestions;
    }

       /**
     * Get Magic questions, except subject information question, termination questions, referrer questions, signature question.
     * @return a list of Magic questions
     */
    public List<Question> getMagicQuestions() {
        List<Question> magicQuestionsList = new ArrayList<Question>();

        for (Question question : getQuestions()) {
            if (question.isMagic() &&
                !question.isSubjectInfo() &&
                !question.isTerminated() &&
                !question.isReferrer() &&
                !question.isSignature())
            {
                magicQuestionsList.add(question);
            }
        }
        return magicQuestionsList;
    }
    /**
     * Get the email question if this is magic
     * @return
     */
    public Question getEmailQuestionIfMagic(){
  	  for (Question question : getQuestions()) {
  		  if(question.getName().equals(email) && question.isMagic()){
  			  return question;
  		  }
  	  }
		return null;
  	
  }
    /**
     * A mapping from question id to the question for non magic questions
     * @return a map from question id to the question for non magic questions
     */
    public LinkedHashMap<Integer, Question> getNonMagicQuestionMapping() {
        LinkedHashMap<Integer, Question> idsToQuestionsMapping = new LinkedHashMap<Integer, Question>();

        List<Question> questions = getQuestions();
        Collections.sort(questions, new Comparator<Question>() {
            @Override
            public int compare(Question q1, Question q2) {
                // sort by order ascending
                return (q1.getOrder() < q2.getOrder()) ? -1 : ((q1.getOrder() == q2.getOrder()) ? 0 : 1);
            }
        });
        for (Question question : questions) {
            if (!question.isMagic()) {
                idsToQuestionsMapping.put(question.getId(), question);
            }
        }
        return idsToQuestionsMapping;
    }

    /**
     * Should be used internally over the questions var as it ensures the question answers are merged into
     * the questions
     * @return a list of questions after mergin with questions answers
     */
    public List<Question> getQuestions() {
        mergeQuestionAnswers(); //TODO this is slow but I don't care at the moment
        return questions == null ?
                Collections.<Question>emptyList() :
                questions;
    }

    /**
     * True if any of the specified var names are not present in the collection of questions
     * @param names - varlist of questions varnames
     * @return True if any of the specified varnames are not present in the collection of questions
     */
    public boolean isMissingAnyQuestionNames(String... names) {
        return isMissingAnyQuestionNames(Arrays.asList(names));
    }

    /**
     * True if any of the specified var names are not present in the collection of questions
     * @param names - a list of questions varnames
     * @return True if any of the specified varnames are not present in the collection of questions
     */
    public boolean isMissingAnyQuestionNames(Collection<String> names) {
        if(questions == null && !names.isEmpty()) {
            return true;
        }
        Set<String> questionNames = new HashSet<String>();
        for (Question question : questions) {//we don't need the answers merged in
            questionNames.add(question.getVarNameNo$());
        }
        return !questionNames.containsAll(names);
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    /**
     * Binds answers from petition answers list with appropriate questions from petition list.
     */
    private void mergeQuestionAnswers() {
        if (answers == null || questions == null)
            return;
        Map<Integer, List<QuestionAnswer>> idsToAnswers = new HashMap<Integer, List<QuestionAnswer>>();
        for (QuestionAnswer answer : answers) {
            int id = answer.getQuestionId();
            List<QuestionAnswer> answerList = idsToAnswers.get(id);
            if (answerList == null) {
                answerList = new ArrayList<QuestionAnswer>();
                idsToAnswers.put(id, answerList);
            }
            answerList.add(answer);
        }

        for (Question question : questions) {
            List<QuestionAnswer> answersList = idsToAnswers.get(question.getId());
            if (answersList == null)
                continue;
            question.setAnswers(answersList);
        }
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }


    /**
     * Indicates whether petition support appointments
     * @return true if petition is support appointments reservation
     */
    public boolean isSupportReservation() {
        return supportReservation;
    }

    public void setSupportReservation(boolean supportReservation) {
        this.supportReservation = supportReservation;
    }

    /**
     * Indicates whether petition support subject credit check (TUNA)
     * @return true if petition is support credits checking
     */
    public boolean isSupportCreditCheck() {
        // NOTE: tested 08/09/2013 - unable to set Enable credit check flag
        // TODO: uncomment
        //return supportCreditCheck;
        // TODO: remove line below
        return true;
    }

    public void setSupportCreditCheck(boolean supportCreditCheck) {
        this.supportCreditCheck = supportCreditCheck;
    }

    public int getMinimumReservationTime() {
        return minimumReservationTime;
    }

    public void setMinimumReservationTime(int minimumReservationTime) {
        this.minimumReservationTime = minimumReservationTime;
    }

    public int getSuggestedReservationTime() {
        return suggestedReservationTime;
    }

    public void setSuggestedReservationTime(int suggestedReservationTime) {
        this.suggestedReservationTime = suggestedReservationTime;
    }

    public int getMaximumReservationTime() {
        return maximumReservationTime;
    }

    public boolean hasNearestNeighborInfo() {
		return nearestNeighbor;
	}

	public void setNearestNeighbor(boolean nearestNeighbor) {
		this.nearestNeighbor = nearestNeighbor;
	}

	public void setMaximumReservationTime(int maximumReservationTime) {
        this.maximumReservationTime = maximumReservationTime;
    }
    
    public void setPetitionJSON(String json)
    {
    	try {
			petitionJSON = new JSONObject(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public JSONObject getPetitionJSON()
    {
    	return petitionJSON;
    }

	public String getMerchantGroupingId() {
		return merchantGroupingId;
	}

	public void setMerchantGroupingId(String merchantGroupingId) {
		this.merchantGroupingId = merchantGroupingId;
	}

	public String getAffiliate() {
		return affiliate;
	}

	public void setAffiliate(String affiliate) {
		this.affiliate = affiliate;
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public String getReportGroup() {
		return reportGroup;
	}

	public void setReportGroup(String reportGroup) {
		this.reportGroup = reportGroup;
	}
    
    
}
