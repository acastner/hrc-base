package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

public class ScriptProperty implements Json{
	@Override
	public String toJson() {
		return JsonParser.getParser().jsonify(this);
	}
	
	@SerializedName("Id")
    private String Id;
    
    @SerializedName("PetitionId")
    private String PetitionId;
    
    @SerializedName("Name")
    private String name;
    
    @SerializedName("Value")
    private String value;
    
    @SerializedName("PropertyType")
    private String propertyType;
    
    @SerializedName("Data")
    private String data;
    
    @SerializedName("ContentType")
    private String contentType;
    
    @SerializedName("Pin")
    private String pin;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getPetitionId() {
		return PetitionId;
	}

	public void setPetitionId(String petitionId) {
		PetitionId = petitionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	
}
