package com.grassroots.petition.models;

import android.text.TextUtils;

/**
 * Server response error
 */
public class ResponseError {
    /**
     * Error type
     * NOTE: may become an enum
     */
    private String type;
    /**
     * Error message (to show to the user)
     */
    private String message;

    public ResponseError() {

    }

    public ResponseError(String type, String msg) {
        this.type = type;
        this.message = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Checks whether error regards to the invalid IMEI
     */
    public boolean isImeiError() {
        if (message == null)
            return false;
        return message.contains("imei");
    }

    /**
     * Checks whether error regards to the authentication
     */
    public boolean isAuthenticationError() {
        if (message == null)
            return false;
        return message.contains("auth");
    }

    public String toString() {
        return "Response Error: " + ((TextUtils.isEmpty(type)) ? "" : "type(" + type + ")") + ((TextUtils.isEmpty(message)) ? "" : " \"" + message + "\"");
    }
}
