package com.grassroots.petition.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

public class NearestNeighborInfo implements Json{
	
	/**
     * List of nearest Neighbors
     */
	@SerializedName("Neighbors")
    private List<Neighbor> neighbors;
	
    /**
     * List of script properties
     */
	@SerializedName("ScriptProperties")
    private List<ScriptProperty> scriptProperties;
	

	public List<Neighbor> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(List<Neighbor> neighbors) {
		this.neighbors = neighbors;
	}

	public List<ScriptProperty> getScriptProperties() {
		return scriptProperties;
	}

	public void setScriptProperties(List<ScriptProperty> scriptProperties) {
		this.scriptProperties = scriptProperties;
	}

	@Override
	public String toJson() {
		return JsonParser.getParser().jsonify(this);
	}

}
