package com.grassroots.petition.models;

import android.content.Context;
import android.content.res.Configuration;

import java.io.IOException;

/**
 * The type of MarketingMaterials in this app - none if there are no materials, single if there is only a
 * single set of slides, and multiple if there are several sets of slides
 */
public enum MarketingMaterialType {

    NONE, SINGLE, MULTIPLE;
    public static final String MARKETING_FOLDER = "marketing";
    public static final String MARKETING_FOLDER_SMALL = "marketing-small";
    public static MarketingMaterialType cachedType;

    /**
     * Provides marketing material type by searching in different marketing material folders: {@value #MARKETING_FOLDER} / {@value #MARKETING_FOLDER_SMALL}
     * @param context - context
     * @return MarketingMaterialType object
     */
    public static MarketingMaterialType getMarketingMaterialType(Context context) {
        if(cachedType != null) {
            return cachedType;
        }
        try {
            //TODO cache this
            String mFolder = MARKETING_FOLDER;
            if (!((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_NORMAL)) {
                // Assume Android 2.2, Samsung Dart screen size
                mFolder = MARKETING_FOLDER_SMALL;
            }

            String[] marketingFolder = context.getAssets().list(mFolder);
            if (marketingFolder == null || marketingFolder.length == 0)
                cachedType = MarketingMaterialType.NONE;
            else if (marketingFolder.length == 1)
                cachedType = MarketingMaterialType.SINGLE;
            else
                cachedType = MarketingMaterialType.MULTIPLE;
        } catch (IOException e) {
            cachedType = MarketingMaterialType.NONE;
        }
        return cachedType;
    }
}
