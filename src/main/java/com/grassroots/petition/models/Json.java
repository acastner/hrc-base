package com.grassroots.petition.models;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 * Interface used for serializing objects to Json.
 */
public interface Json {
    public String toJson();
}
