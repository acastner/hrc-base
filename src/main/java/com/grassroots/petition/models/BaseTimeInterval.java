package com.grassroots.petition.models;


import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

/**
 * Abstract class for storing data about start and end time of an time interval or event.
 * Also contains description of time interval or event in title field.
 * The instances of this class can be created locally or deserialized from JSON, which is loaded from backend.
 *
 * TODO: Consider using Calendar instead of Date. Date is deprecated, Calendar is not.
 */
public abstract class BaseTimeInterval implements Json
{
    /**
     * Description of current time interval or event.
     */
    private String title;
    /**
     * Start date-time of current time interval or event.
     */
    @SerializedName("Start")
    private Date startTime;
    /**
     * End date-time of current time interval or event.
     */
    @SerializedName("End")
    private Date endTime;

    public BaseTimeInterval()
    {
//        Calendar c = Calendar.getInstance();
//        startTime = endTime = c.getTime();
//        title = "";
    }

    public BaseTimeInterval(Date startTime, Date endTime)
    {
        this(null, startTime, endTime);
    }

    /**
     * Constructor with a full list of arguments.
     * @param title - description of time interval or event.
     * @param startTime - date-time of start time of time interval or event.
     * @param endTime - date-time of start time of time interval or event.
     */
    public BaseTimeInterval(String title, Date startTime, Date endTime)
    {
        this.title = title;

        if (null != startTime && null != endTime)
        {
            if (startTime.compareTo(endTime) > 0) // startTime > endTime
            {
                // swap dates
                this.startTime = endTime;
                this.endTime = startTime;
            } else {
                this.startTime = startTime;
                this.endTime = endTime;
            }

            if (this.startTime.compareTo(this.endTime) == 0)
            {
                Calendar c = Calendar.getInstance();
                c.setTime(this.endTime);
                c.add(Calendar.MINUTE, 1);
                this.endTime = c.getTime();
            }
        }
    }

    /**
     * Constructor with a full list of arguments and specified date, which is used to set date in startTime and endTime fields.
     * @param day - a date to set date in startTime and endTime fields.
     * @param title - description of time interval or event.
     * @param startTime - date-time of start time of time interval or event.
     * @param endTime - date-time of start time of time interval or event.
     */
    public BaseTimeInterval(Date day, String title, Date startTime, Date endTime)
    {
        this(title, startTime, endTime);
        setDay(day);
    }

    /**
     * Sets specified date in startTime and endTime fields.
     * @param day - a date to set date in startTime and endTime fields.
     */
    public void setDay(Date day)
    {
        this.startTime = new Date(day.getYear(), day.getMonth(), day.getDate(), startTime.getHours(), startTime.getMinutes());
        this.endTime = new Date(day.getYear(), day.getMonth(), day.getDate(), endTime.getHours(), endTime.getMinutes());
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public void setStartTime( Date startTime )
    {
        this.startTime = startTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime( Date endTime )
    {
        this.endTime = endTime;
    }

    /**
     * Returns length in minutes of time interval which is represented by current object.
     * @return  length in minutes between startTime and endTime.
     */
    public long getTimeIntervalInMinutes() {
        if (this.endTime == null || this.startTime == null) return 0;
        long diff = Math.abs(this.endTime.getTime() - this.startTime.getTime());

        return (diff/ (1000*60));
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseTimeInterval that = (BaseTimeInterval) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        return result;
    }

    /**
     * returns DateTimeFormatter to serialize/deserialize startTime and endTime fields from JSON
     * @return  DateTimeFormatter to serialize/deserialize startTime and endTime fields from JSON.
     */
    public abstract DateTimeFormatter getDateTimeFormatter();

    /**
     * public static class which implements Comparator interface to compare BaseTimeInterval objects (for sorting for example).
     * Compare BaseTimeInterval objects by startTime fields, like ordinary Date objects.
     */
    public static class TimeIntervalsComparator implements Comparator<BaseTimeInterval> {
        public int compare(BaseTimeInterval ti1, BaseTimeInterval ti2) {
            return ti1.getStartTime().compareTo(ti2.getStartTime());
        }
    }
}
