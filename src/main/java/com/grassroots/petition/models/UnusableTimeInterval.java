package com.grassroots.petition.models;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * Stores data about unusable time interval.
 * startTime and endTime fields in JSON have "HH:mm" time format.
 */
public class UnusableTimeInterval extends BaseTimeInterval
{
    public UnusableTimeInterval() {
        super();
    }

    public UnusableTimeInterval(Date startTime, Date endTime) {
        super(startTime, endTime);
    }

    public UnusableTimeInterval(String title, Date startTime, Date endTime) {
        super(title, startTime, endTime);
    }

    public UnusableTimeInterval(Date day, String title, Date startTime, Date endTime) {
        super(day, title, startTime, endTime);
    }

    @Override public DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormat.forPattern("HH:mm");
    }
}
