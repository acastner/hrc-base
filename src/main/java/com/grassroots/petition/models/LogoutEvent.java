package com.grassroots.petition.models;

import com.grassroots.petition.utils.JsonParser;

/**
 * Sent on logout
 */
public class LogoutEvent implements Json {

    private GpsLocation location;

    public LogoutEvent() {
    }

    public LogoutEvent(GpsLocation location) {
        this.location = location;
    }

    public GpsLocation getLocation() {
        return location;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogoutEvent that = (LogoutEvent) o;

        if (location != null ? !location.equals(that.location) : that.location != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = location != null ? location.hashCode() : 0;
        return result;
    }
}
