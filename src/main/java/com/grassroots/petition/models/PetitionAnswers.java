package com.grassroots.petition.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.utils.Dates;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.Tracker;

import org.apache.log4j.Logger;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Canvassed subject answers to a petition
 */
public class PetitionAnswers implements Json{
    public static final String TAG = PetitionAnswers.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Answer id
     */
    private int id;
    /**
     * Subject id
     */
    private int hid;
    /**
     * time string (which has format Dates.TIME_FORMAT)
     */
    public String time;

    /**
     * Selected appointment or null
     */
    private Appointment appointment;
    /**
     * Cart with selected products, quantities and cost
     */
    private Cart cart;
    /**
     * Subject object
     */
    private transient Subject subject;
    /**
     * Signature object
     */
    private transient Signature signature;
    /**
     * A list of subject's answers
     */
    private List<SubjectAnswer> answers = new ArrayList<SubjectAnswer>();

    /**
     * Latitude of location
     */
    private double latitude;
    /**
     * Longitude of location
     */
    private double longitude;
    /**
     * Type of received location
     */
    private String type;
    /**
     * Uncertainty of location
     */
    private int uncertainty;
    
    /**
     * Payment Response to server
     */
    @SerializedName("PaymentResponse")
    private String paymentResponse;
    
    /**
     * Survey elapsed time
     */
    @SerializedName("SurveyElapsedTime")
    private SurveyElapsedTime surveyElapsedTime = new SurveyElapsedTime();

    @SerializedName("MobileSurveyId")
    private String mobileSurveyId;

    public String getMobileSurveyId() {
        return mobileSurveyId;
    }

    public void setMobileSurveyId(String mobileSurveyId) {
        this.mobileSurveyId = mobileSurveyId;
    }

    public String getSurveyData() {
        return surveyData;
    }

    public void setSurveyData(String surveyData) {
        this.surveyData = surveyData;
    }

    @SerializedName("SurveyData")
    private String surveyData;

    public PetitionAnswers() {
        setMobileSurveyId(java.util.UUID.randomUUID().toString());
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//	------------------------------------------------------------------------------------------------
//  Subject handling methods
//	------------------------------------------------------------------------------------------------
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Subject getSubject() {
        return getNeverNullSubject();
    }

    protected Subject getNeverNullSubject() {
        if (subject == null) {
            LOGGER.error("Subject is null, returning new instance. In " + this.getClass());
            subject = new Subject();
        }
        return subject;
    }

//	------------------------------------------------------------------------------------------------
//  Cart handling methods
//	------------------------------------------------------------------------------------------------
    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Cart getCart() {
        return getNeverNullCart();
    }

    protected Cart getNeverNullCart() {
        if (cart == null) {
            LOGGER.error("Cart is null, returning new instance. In " + this.getClass());
            cart = new Cart();
        }
        return cart;
    }
    
//	------------------------------------------------------------------------------------------------
//  Payment Process Response handling methods
//	------------------------------------------------------------------------------------------------
    
    public void setPaymentResponse(String response)
    {
    	paymentResponse = response;
    }

//	------------------------------------------------------------------------------------------------
//  Appointment handling methods
//	------------------------------------------------------------------------------------------------
    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Appointment getAppointment() {
        return appointment;
    }

//	------------------------------------------------------------------------------------------------
//  Answers handling methods
//	------------------------------------------------------------------------------------------------
    public void setAnswers(List<SubjectAnswer> answers) {
        this.answers = answers;
    }

    public List<SubjectAnswer> getAnswers() {
        if (answers == null)
            return new ArrayList<com.grassroots.petition.models.SubjectAnswer>();
        return answers;
    }

    /**
     * Add a list of answers
     */
    public void addAnswers(Collection<SubjectAnswer> answers) {
        if (this.answers == null)
            this.answers = new ArrayList<SubjectAnswer>();
        this.answers.addAll(answers);
    }

    /**
     * Overwrite existing subject answers with a specified list of answers
     */
    public void overwriteAnswers(Collection<SubjectAnswer> answers) {
        if (this.answers == null)
            this.answers = new ArrayList<SubjectAnswer>();
        for (SubjectAnswer answer : answers) {
            overwriteAnswer(answer);
        }
    }

    /**
     * Adds subject answer to a list of answers if provided answer is not null
     */
    public void addAnswer(SubjectAnswer answer) {
        if (answer == null)
            return;
        if (answers == null)
            answers = new ArrayList<SubjectAnswer>();
        answers.add(answer);
    }

    /**
     * Provides answers for specified question
     */
    public SubjectAnswer getAnswerTo(Question question) {
        for (SubjectAnswer answer : getAnswers()) {
            if (answer.getQuestionId() == question.getId()) {
                return answer;
            }
        }
        return null;
    }

    /**
     * Overwrites the existing answer if newAnswer is not null
     * @param newAnswer
     */
    public void overwriteAnswer(SubjectAnswer newAnswer) {
        if (newAnswer == null)
            return;
        SubjectAnswer answerToReplace = null;
        for (SubjectAnswer answer : answers) {
            if (newAnswer.getQuestionId() == answer.getQuestionId()) {
                answerToReplace = answer;
                break;
            }
        }
        if (answerToReplace != null)
            answers.remove(answerToReplace);
        if (newAnswer != null && newAnswer.hasAnswer())
            answers.add(newAnswer);
    }

    /**
     * Remove existing answer for question if any
     * @param question
     */
    public boolean removeAnswer(Question question) {
        if (null == question) return false;
        int questionId = question.getId();
        for (SubjectAnswer answer : answers) {
            if (answer.getQuestionId() == questionId) {
                answers.remove(answer);
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a string presentation of the answer for a particular question.
     * NOTE: Although it is unused in core delmonico, this may be used in client applications (e.g. complete-solar)
     *
     * @param question - question to get the answer to
     *
     * @return a string representation of the answer to the given question.
     */
    public String getStringAnswerTo (Question question)
    {
        if( question == null ) {
            return "";
        }

        SubjectAnswer sa = getAnswerTo( question );

        if( sa == null ) {
            return "";
        }

        String result = sa.getStringAnswer();

        return result == null ? "" : result;
    }

//	------------------------------------------------------------------------------------------------
//  Signature handling methods
//	------------------------------------------------------------------------------------------------
    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    public Signature getSignature() {
        return signature;
    }

    public Bitmap getSignatureBitmap() {
        if (signature != null) {
            byte[] signatureBytes = signature.asCompressedByteArray();
            return BitmapFactory.decodeByteArray(signatureBytes, 0, signatureBytes.length);
        }

        return null;
    }

//	------------------------------------------------------------------------------------------------
//	------------------------------------------------------------------------------------------------
    public void setGpsLocation(GpsLocation location) {
        if (location != null) {
            time = Dates.getTimeStamp();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            type = location.getType();
            uncertainty = location.getUncertainty();
        }
    }


    /**
     * Add the answers to the given questions, populated by the given subject model.
     * Also sets the hid value
     * @param subjectQuestions
     * @param subject
     */
    public void mergeInAnswersAndSetIdFields(List<Question> subjectQuestions, Subject subject) {
        List<SubjectAnswer> subjectAnswers = subject.toSubjectAnswers(subjectQuestions);
        overwriteAnswers(subjectAnswers);
        String hidString = subject.getHid();
        if (Strings.isNotEmpty(hidString)) {
            try {
                int hid = Integer.parseInt(hidString);
                setHid(hid);
            } catch (NumberFormatException e) {
                LOGGER.debug("Trouble parsing hid: " + hidString);
            }
        }
    }

    /**
     * Convert the answers that have photo locations set to bitmaps
     * @param activity
     */
    public void convertPhotoFilesToAnswers(BasePetitionActivity activity) {
        for(SubjectAnswer answer : getAnswers()) {
            if(answer.hasPictureAnswer()) {
                answer.convertLocationToPictureAnswer(activity);
            }
        }
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PetitionAnswers that = (PetitionAnswers) o;

        if (hid != that.hid) return false;
        if (id != that.id) return false;
        if (answers != null ? !answers.equals(that.answers) : that.answers != null) return false;
        if (signature != null ? !signature.equals(that.signature) : that.signature != null) return false;
        if (cart != null ? !cart.equals(that.cart) : that.cart != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + hid;
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        result = 31 * result + (signature != null ? signature.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (cart != null ? cart.hashCode() : 0);
        return result;
    }
    
//	------------------------------------------------------------------------------------------------
//  Survey start and end time handling methods
//	------------------------------------------------------------------------------------------------
    
    public void setSurveyStart(String start)
    {
    	surveyElapsedTime.setSurveyStart(start);   	
    	Tracker.appendLog("Setting survey start time.");
    	LOGGER.debug("Setting survey start time.");
    } 
    
    public String getSurveyStart()
    {
    	return surveyElapsedTime.getSurveyStart();
    } 
    
    public void setSurveyEnd(String end)
    {
    	surveyElapsedTime.setSurveyEnd(end);   	
    	Tracker.appendLog("Setting survey end time.");
    	LOGGER.debug("Setting survey end time.");
    } 
    
    public String getSurveyEnd()
    {
    	return surveyElapsedTime.getSurveyEnd();
    }
    
}
