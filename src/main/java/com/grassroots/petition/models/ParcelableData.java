package com.grassroots.petition.models;

import android.os.Parcelable;

/**
 * Declares interface for Sending Data between Intents and toJSON
 *  Combines Json and Parcelable interfaces.
 */
public interface ParcelableData extends Json, Parcelable {
    /**
     * @return key-name string which will be used for storing object as key-value.
     */
    public String getKey();
}
