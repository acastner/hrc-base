package com.grassroots.petition.models;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Data model to store data required for Instant-Prescreen request.
 */
public class InstantPrescreenRequest implements Json
{
    /**
     * Date format used for DOB JSON field
     */
    public static final String DOB_DATE_FORMAT = "MM/dd/yyyy";

    private String firstName;
    private String lastName;

    private String address;
    private String city;
    private String state;
    private String zipCode;

    private String phone;

    private Date DOB;

    public InstantPrescreenRequest(String firstName, String lastName, String address, String city, String state, String zipCode, String phone, Date DOB)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.phone = phone;
        this.DOB = DOB;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    /**
     * Validates objects: check is all required fields are in place?
     * @return true - if all required fields is in place.
     */
    public boolean isValid()
    {
        if (!TextUtils.isEmpty(firstName) &&
                !TextUtils.isEmpty(lastName) &&
                !TextUtils.isEmpty(address) &&
                !TextUtils.isEmpty(city) &&
                !TextUtils.isEmpty(state) &&
                !TextUtils.isEmpty(zipCode))
        {
            return true;
        }
        return false;
    }

    /**
     * Returns default date format used for DOB JSON field: {@value #DOB_DATE_FORMAT}
     * @return {@value #DOB_DATE_FORMAT}
     */
    protected static DateFormat getDOBDateFormat() {
        return new SimpleDateFormat(DOB_DATE_FORMAT, Locale.getDefault());
    }

    /**
     * @return Formatted DOB Date field using DOB_DATE_FORMAT = {@value #DOB_DATE_FORMAT}
     */
    public String getFormattedDOB() {
        if (null != DOB) {
            return getDOBDateFormat().format(DOB);
        }
        return null;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }
}
