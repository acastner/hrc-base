package com.grassroots.petition.models;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * Stores data about a free time interval.
 * startTime and endTime fields in JSON have "HH:mm" time format.
 *
 * TODO: Consider using Calendar instead of Date. Date is deprecated, Calendar is not.
 */
public class FreeTimeInterval extends BaseTimeInterval
{
    public FreeTimeInterval() {
        super();
    }

    public FreeTimeInterval(Date startTime, Date endTime) {
        super(startTime, endTime);
    }

    public FreeTimeInterval(String title, Date startTime, Date endTime) {
        super(title, startTime, endTime);
    }

    public FreeTimeInterval(Date day, String title, Date startTime, Date endTime) {
        super(day, title, startTime, endTime);
    }

    @Override public DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormat.forPattern("HH:mm");
    }
}
