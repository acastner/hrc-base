package com.grassroots.petition.models;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;

/**
 * The status of a subject
 */
public enum KnockStatus {
    NOT_KNOCKED(0, GlobalData.getContext().getString(R.string.KNOCK_STATUS_NOT_KNOCKED)),
    NOT_HOME(1, GlobalData.getContext().getString(R.string.KNOCK_STATUS_NOT_HOME)),
    REFUSED(2, GlobalData.getContext().getString(R.string.KNOCK_STATUS_REFUSED)),
    INACCESSIBLE(3, GlobalData.getContext().getString(R.string.KNOCK_STATUS_INACCESSIBLE)),
    DECEASED(4, GlobalData.getContext().getString(R.string.KNOCK_STATUS_DECEASED)),
    MOVED(5, GlobalData.getContext().getString(R.string.KNOCK_STATUS_MOVED)),
    LANGUAGE_BARRIER(6, GlobalData.getContext().getString(R.string.KNOCK_STATUS_LANGUAGE_BARRIER)),
    NOT_AN_ADDRESS(8, GlobalData.getContext().getString(R.string.KNOCK_STATUS_NOT_AN_ADDRESS)),
    VACANT_OR_SALE(10, GlobalData.getContext().getString(R.string.KNOCK_STATUS_VACANT_OR_SALE)),
    COME_BACK_LATER(13, GlobalData.getContext().getString(R.string.KNOCK_STATUS_COME_BACK_LATER)),
    SUCCESS(14, GlobalData.getContext().getString(R.string.KNOCK_STATUS_COMPLETED));

    public static KnockStatus fromOrdinal(int ordinal) {
        KnockStatus[] statuses = values();
        if (0 <= ordinal && ordinal < statuses.length) {
            return statuses[ordinal];
        }
        return NOT_KNOCKED;
    }

    /**
     * Converts ordinal id of knock status to string description
     * @param id - of knock status
     * @return string description of knock status
     */
    public static KnockStatus fromId(int id) {
        for (KnockStatus status : values()) {
            if (id == status.getRepresentation()) {
                return status;
            }
        }
        return NOT_KNOCKED;
    }

    private final int representation;
    private final String displayText;

    /**
     * Instantiates KnockStatus object by it's id, resources string id (used for localization), and default string (in English)
     * @param rep - ordinal representation of KnockStatus,
     * @param displayText - localized status description
     */
    private KnockStatus(int rep, String displayText) {
        representation = rep;
        this.displayText = displayText;
    }

    /**
     * Knock status id
     * @return id of knock status
     */
    public int getRepresentation() {
        return representation;
    }

    /**
     * The text to display to the user
     * @return text to display
     */
    public String displayText() {
        return displayText;
    }
}