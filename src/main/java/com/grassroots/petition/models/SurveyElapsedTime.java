package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;

public class SurveyElapsedTime {

	 /**
     * Survey start time
     */
    @SerializedName("SurveyStartTime")
    private String surveyStart;

    
    /**
     * Survey end time
     */
    @SerializedName("SurveyEndTime")
    private String surveyEnd;

    
    
    public void setSurveyStart(String start)
    {
    	surveyStart = start;
    } 
    
    public String getSurveyStart()
    {
    	return surveyStart;
    } 
    
    public void setSurveyEnd(String end)
    {
    	surveyEnd = end;
    } 
    
    public String getSurveyEnd()
    {
    	return surveyEnd;
    }
}
