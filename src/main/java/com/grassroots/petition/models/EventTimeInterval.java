package com.grassroots.petition.models;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;

/**
 * Stores data about an event, which has start time, end time and title (description).
 * startTime and endTime fields are serialized/deserialized to/from JSON have ISO8601 date format.
 * Could be created from an Appointment's type object.
 * Objects of this type predominantly are loaded from backend.
 */
public class EventTimeInterval extends BaseTimeInterval
{
    public EventTimeInterval() {
        super();
    }

    /**
     * Constructor to initialize an EventTimeInterval object from an existing Appointment object.
     * @param appointment - an appointment object which is used to initialize an EventTimeInterval object.
     */
    public EventTimeInterval(Appointment appointment) {
        super();
        Calendar c = Calendar.getInstance();
        c.setTime(appointment.getStartTime());
        c.add(Calendar.MINUTE, appointment.getDuration());
        this.setTitle(appointment.getDescription());
        this.setStartTime(appointment.getStartTime());
        this.setEndTime(c.getTime());
    }

    public EventTimeInterval(Date startTime, Date endTime) {
        super(startTime, endTime);
    }

    public EventTimeInterval(String title, Date startTime, Date endTime) {
        super(title, startTime, endTime);
    }

    public EventTimeInterval(Date day, String title, Date startTime, Date endTime) {
        super(day, title, startTime, endTime);
    }

    @Override public DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ"); // ISO8601 date format
        //return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm"); // ISO8601 date format without Time Zone
    }
}
