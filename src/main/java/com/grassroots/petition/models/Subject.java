package com.grassroots.petition.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.Strings;
import org.apache.log4j.Logger;
import org.osmdroid.util.GeoPoint;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.grassroots.petition.models.Question.*;

/**
 * Information about someone being canvassed.
 * Note that all subjects have a unique id that is used to identify when they have been knocked. Make sure
 * you don't lose this id
 */
public class Subject implements Parcelable {
    private static final String TAG = Subject.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * Pattern used to get house number from addressLine1
     */
    private static final Pattern NUMBER_PATTERN = Pattern.compile("(\\d+)");
    
    private static final Pattern NUMBER_PATTERN_WITH_HYPHEN = Pattern.compile("(\\d+-\\d+)");
    /**
     * Knock status: means not set
     */
    public static final int NOT_KNOCKED = -1;
    /**
     * GeoPoint means no location received
     */
    public static final GeoPoint DEFAULT_MISSING_LAT_LNG = new GeoPoint(0, 0);
    /**
     * Key-name {@value} to identify Subject by it's account number
     */
    public static final String ACCOUNT_NUMBER = "AccountNumber";

    /**
     * Gender enum
     */
    public enum Gender { //binary gender? oh man....
        M, F
    }

    public static final Parcelable.Creator<Subject> CREATOR = new Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel parcel) {
            return new Subject(parcel);
        }

        @Override
        public Subject[] newArray(int i) {
            return new Subject[i];
        }
    };

    /**
     * Sort by street name, then house number, then apartment number
     */
    public static final Comparator<Subject> ADDRESS_COMPARATOR = new Comparator<Subject>() {
        @Override
        public int compare(Subject subject, Subject subject2)
        {
            /** Grab full address and turn it into one complete string **/
            String address1 = subject.getStreetName() + " " + subject.getHouseNumber() + " " + subject.getAddressLine2();
            String address2 = subject2.getStreetName() + " " + subject2.getHouseNumber() + " " + subject2.getAddressLine2();

            /** Run sort **/
            return compareAddresses( address1, address2 );
        }

		/** Determines if a character is numeric or an alphabet character **/
        private final boolean isDigit(char ch)
        {
            return ch >= 48 && ch <= 57;
        }

		/** Length of string is passed in for improved efficiency (only need to calculate it once) **/
        private final String getChunk(String s, int slength, int marker)
        {
            StringBuilder chunk = new StringBuilder();
            char c = s.charAt(marker);
            chunk.append(c);
            marker++;
            if (isDigit(c))
            {
                while (marker < slength)
                {
                    c = s.charAt(marker);
                    if (!isDigit(c))
                        break;
                    chunk.append(c);
                    marker++;
                }
            } else
            {
                while (marker < slength)
                {
                    c = s.charAt(marker);
                    if (isDigit(c))
                        break;
                    chunk.append(c);
                    marker++;
                }
            }
            return chunk.toString();
        }
		
		/**
         *  Compares two address strings against each other to determine their place in the sort order.
         *
         *  @param thisString - string 1 for comparison
         *  @param thatString - string 2 for comparison
         *
         *  @return returns a string representing sort order
         */
        public int compareAddresses (String thisString, String thatString)
        {
            int thisMarker = 0;
            int thatMarker = 0;
            int thisStringLength = thisString.length();
            int thatStringLength = thatString.length();

            while (thisMarker < thisStringLength && thatMarker < thatStringLength)
            {
                String thisChunk = getChunk(thisString, thisStringLength, thisMarker);
                thisMarker += thisChunk.length();

                String thatChunk = getChunk(thatString, thatStringLength, thatMarker);
                thatMarker += thatChunk.length();

                /**If both chunks contain numeric characters, sort them numerically **/
                int result = 0;
                if (isDigit(thisChunk.charAt(0)) && isDigit(thatChunk.charAt(0)))
                {
                    /** Simple chunk comparison by length. **/
                    int thisChunkLength = thisChunk.length();
                    result = thisChunkLength - thatChunk.length();
                    /** If equal, the first different number counts **/
                    if (result == 0)
                    {
                        for (int i = 0; i < thisChunkLength; i++)
                        {
                            result = thisChunk.charAt(i) - thatChunk.charAt(i);
                            if (result != 0)
                            {
                                return result;
                            }
                        }
                    }
                } else
                {
                    result = thisChunk.compareTo(thatChunk);
                }

                if (result != 0)
                    return result;
            }

            return thisStringLength - thatStringLength;
        }
    };

    /**
     * Key-name {@value} for storing Subject object as key-value.
     */
    public static final String SUBJECT_KEY = "subject";
    /**
     * Subjects counter used to set subject's id
     */
    private static int idCounter = 0;
    /**
     * Unique subject id
     */
    private String hid;
    /**
     * Subject Salutation
     */
    private String salutation;
    /**
     * Subject suffix
     */
    private String suffix;
    /**
     * first name
     */
    private String firstName;
    /**
     * last name
     */
    private String lastName;
    /**
     * middle name
     */
    private String middleName;
    /**
     * driver license number
     */
    private String licenseNumber;

    /**
     * age, years
     */
    private int age;
    /**
     * gender
     */
    private Gender gender;

    /**
     * Subject Address Line 1
     */
    @SerializedName("Address")
    private String addressLine1;
    /**
     * Subject Address Line 2
     */
    @SerializedName("Apartment")
    private String addressLine2;
    /**
     * City
     */
    private String city;
    /**
     * State
     */
    private String state;
    /**
     * Zip
     */
    private String zip;
    
    /**
     * Country
     */
    private String country;
    
    

	/**
     * Address location latitude
     */
    private String latitude;
    /**
     * Address location longitude
     */
    private String longitude;

    /**
     * subject's email
     */
    private String email;
    /**
     * subject's cell phone
     */
    private String cellPhone;
    /**
     * subject's home phone
     */
    private String homePhone;
    /**
     * subject's default phone
     */
    private String phone;
    /**
     * subject's address even or odd
     */
    private int isEven;

    /**
     * subject's location
     */
    private String location;
    
    /**
     * subject's occupation
     */
    private String occupation;
    /**
     * subject's employer
     */
    private String employer;
    /**
     * subject's employer city
     */
    private String emploerCity;
    /**
     * subject's employer state
     */
    private String employerState;

    /**
     * Other subject fields hash-map: key - field name, value - field value
     */
    private Map<String, String> other = new LinkedHashMap<String, String>();


	private String otherJSON;

    /**
     * Subject knock status
     */
    @SerializedName("Status")
    private KnockStatus knockStatus = KnockStatus.NOT_KNOCKED;
    private final int id;
    
    /**
     * subject's color
     */
    private String color; 

    public Subject() {
        id = idCounter++;
    }

    public Subject(int id) {
        this.id = id;
    }

    /**
     * For testing only
     * @param addressLine1
     * @param addressLine2
     */
    @Deprecated
    protected Subject(String addressLine1, String addressLine2) {
        this();
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        city = "Philadelphia";
        zip = "19123";
        state = "PA";
    }

    protected Subject(String addressLine1, String addressLine2, String city, String state, String zip, String country, String latitude, String longitude,String color) {
    	this();
    	this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.color=color;
    }

    public Subject(String addressLine1, String addressLine2, String city, String state, String zip, String country,
                   String lastName, String firstName, String middleName, String suffix, String driversLicense) {
        this();
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.suffix = suffix;
        this.licenseNumber = driversLicense;
    }



    public boolean isValid() {
        if (
            Strings.isEmpty(addressLine1) ||
            Strings.isEmpty(city) ||
            Strings.isEmpty(state) ||
            Strings.isEmpty(zip))
            return false;

        return true;
    }

    /**
     * create a new street address representation of this subject
     * @return
     */
    public StreetAddressSubject toStreetAddressSubject() {
        return new StreetAddressSubject(addressLine1, addressLine2, city, state, zip, country, latitude, longitude, id, knockStatus,color);
    }

    /**
     * Convert this subject to answers to the given questions, if this subject has a non null answer to the
     * given question
     * @param subjectInformationQuestions
     * @return
     */
    public List<SubjectAnswer> toSubjectAnswers(List<Question> subjectInformationQuestions) {
        List<SubjectAnswer> answers = new ArrayList<SubjectAnswer>();
        for (Question question : subjectInformationQuestions) {
            String answer = getSubjectValueFor(question.getVarNameNo$());
            if (answer != null) {
                answers.add(new SubjectAnswer(question.getId(), answer));
            }
        }
        return answers;
    }

    /**
     * This is probably better as some sort of enum. But, returns the subject value for the given question var
     * name. Matches the start of the magic name, so STATE_SHIPPING will return the subject state, but
     * SHIPPING_STATE will not.
     * @param magicVarField
     * @return
     */
    public String getSubjectValueFor(String magicVarField) {
        if(magicVarField == null)
            return null;
        //GROSS AHOY
        if (magicVarField.startsWith(SALUTATION)) {
            return getSalutation();
        } else if (magicVarField.startsWith(SUFFIX)) {
            return getSuffix();
        } else if (magicVarField.startsWith(FIRST_NAME)) {
            return getFirstName();
        } else if (magicVarField.startsWith(LAST_NAME)) {
            return getLastName();
        } else if (magicVarField.startsWith(ADDRESS)) {
            return getAddressLine1();
        } else if (magicVarField.startsWith(ADDRESS_2)) {
            return getAddressLine2();
        } else if (magicVarField.startsWith(CITY)) {
            return getCity();
        } else if (magicVarField.startsWith(STATE)) {
            return getState();
        } else if (magicVarField.startsWith(ZIP)) {
            return getZip();
        }else if (magicVarField.startsWith(COUNTRY)) {
            return getCountry();
        }else if (magicVarField.startsWith(HOME)) {
            return getHomePhone();
        } else if (magicVarField.startsWith(CELL)) {
            return getCellPhone();
        } else if (magicVarField.startsWith(EMAIL)) {
            return getEmail();
        } else if (magicVarField.startsWith(PHONE)) {
            return getPhone();
        } else if (magicVarField.startsWith(LOCATION)) {
			return getLocation();
		}

        return null;
    }


    public void merge(Subject subject) {
        if (subject == null) return;

        if (Strings.isNotEmpty(subject.getHid()))           this.hid = subject.getHid();
        if (Strings.isNotEmpty(subject.getSalutation()))    this.salutation = subject.getSalutation();
        if (Strings.isNotEmpty(subject.getSuffix()))        this.suffix = subject.getSuffix();
        if (Strings.isNotEmpty(subject.getFirstName()))     this.firstName = subject.getFirstName();
        if (Strings.isNotEmpty(subject.getLastName()))      this.lastName = subject.getLastName();
        if (Strings.isNotEmpty(subject.getMiddleName()))    this.middleName = subject.getMiddleName();
        if (Strings.isNotEmpty(subject.getLicenseNumber())) this.licenseNumber = subject.getLicenseNumber();

        if (subject.getAge() > 0)                           this.age = subject.getAge();
        if (subject.getGender() != Gender.M)                this.gender = subject.getGender();

        if (Strings.isNotEmpty(subject.getAddressLine1()))  this.addressLine1 = subject.getAddressLine1();
        if (Strings.isNotEmpty(subject.getAddressLine2()))  this.addressLine2 = subject.getAddressLine2();
        if (Strings.isNotEmpty(subject.getCity()))          this.city = subject.getCity();
        if (Strings.isNotEmpty(subject.getState()))         this.state = subject.getState();
        if (Strings.isNotEmpty(subject.getZip()))           this.zip = subject.getZip();
        if (Strings.isNotEmpty(subject.getCountry()))       this.country = subject.getCountry();
        if (Strings.isNotEmpty(subject.getLatitude()))      this.latitude = subject.getLatitude();
        if (Strings.isNotEmpty(subject.getLongitude()))     this.longitude = subject.getLongitude();
        if (Strings.isNotEmpty(subject.getCellPhone()))     this.cellPhone = subject.getCellPhone();
        if (Strings.isNotEmpty(subject.getHomePhone()))     this.homePhone = subject.getHomePhone();
        if (Strings.isNotEmpty(subject.getPhone()))         this.phone = subject.getPhone();
        if (Strings.isNotEmpty(subject.getLocation()))      this.location = subject.getLocation();
        Map<String, String> subjectOtherFieldsMapping= subject.getOtherFieldsMapping();
        if (subjectOtherFieldsMapping != null && !subjectOtherFieldsMapping.isEmpty()) {
            for (Map.Entry<String, String> entry : subjectOtherFieldsMapping.entrySet()) {
                other.put(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * If this subject has an address, apartment, city, state, or zip
     * @return
     */
    public boolean hasAddressData() {
        return anyNotEmpty(getAddressLine1(), getAddressLine2(), getCity(), getZip(), getState());
    }

    private boolean anyNotEmpty(String... fields) {
        for (String field : fields) {
            if (Strings.isNotEmpty(field))
                return true;
        }
        return false;
    }

    private String houseNumber = null;

    /**
     * Gets subject's house number as a string.
     * Gets house number using house number pattern defined in NUMBER_PATTERN {@value #NUMBER_PATTERN}
     * House number is gotten from addressLine1.
     */
    public String getHouseNumber() {
        if (houseNumber != null) {
            return houseNumber;
        }
        String address = getAddressLine1();
        if (Strings.isEmpty(address))
            return "";
        Matcher matches = null;
        if(address.contains("-")){
        	matches = NUMBER_PATTERN_WITH_HYPHEN.matcher(address);
        }else{
        	matches = NUMBER_PATTERN.matcher(address);
        }
        if (matches.find()) {
            return matches.group();
        } else {
            return "";
        }
    }

    /**
     * Gets subject's house number as integer.
     * Look at getHouseNumber() for further info.
     */
    public int getHouseNumberInt() {
        String houseNumber = getHouseNumber();
        try {
            return Integer.parseInt(houseNumber);
        }catch(NumberFormatException nfe) {
            LOGGER.debug("Unable to convert house number to integer: " + houseNumber);
            return 0;
        }
    }

    private Boolean isHouseNumberEven = null;

    /**
     * Whether the house number is even. If unable to parse the house number, defaults to true
     * @return
     */
    public boolean isHouseNumberEven() {
        if (isHouseNumberEven != null) {
            return isHouseNumberEven.booleanValue();
        }
        try {
            String houseNo = getHouseNumber();
            if(houseNo.contains("-")){
                houseNo = houseNo.replaceAll("-", "");
            }
            int houseNumber = Integer.parseInt(houseNo);
            isHouseNumberEven = Boolean.valueOf(houseNumber % 2 == 0);
            return isHouseNumberEven.booleanValue();
        } catch (NumberFormatException e) {
            return true;
        }
    }

    private String streetName = null;

    /**
     * Gets street name of subject address.
     * Street name is gotten from addressLine1 field by removing spaces.
     * @return street name without spaces.
     */
    public String getStreetName() {
        if (streetName != null)
            return streetName;
        String address = getAddressLine1();//TODO this is truly awful code
        if (Strings.isEmpty(address))
            return "";
        String[] split = address.split(" ");
        if (split.length < 2) {
            if (!isHouseNumber(address)) {
                streetName = address;
                return address;
            } else
                return "";
        }
        String streetName = "";
        String separator = "";
        for (int i = 1; i < split.length; i++) {
            String addressPart = split[i];
            addressPart.replaceAll(" ", ""); //strip whitespace
            if (Strings.isEmpty(addressPart))
                continue;
            streetName = streetName + separator + addressPart;
            separator = " ";
        }
        this.streetName = streetName;
        return streetName;
    }

    /**
     * Checks is string a house number (integer).
     */
    private boolean isHouseNumber(String address) {
        boolean isHouseNumber = true;
        try {
            Integer.parseInt(address);
        } catch (NumberFormatException nfe) {
            isHouseNumber = false;
        }
        return isHouseNumber;
    }

    /**
     * Get a displayable version of the address on a single line.
     * Includes: Address line 1, City, State, Zip separated by ',' comma.
     */
    public String getFullAddress() {
        String separator = "";
        StringBuilder addressBuilder = new StringBuilder();
        separator = addField(getAddressLine1(), separator, addressBuilder);
        separator = addField(getCity(), separator, addressBuilder);
        addField(getState(), separator, addressBuilder);
        addField(getZip(), " ", addressBuilder);
        return addressBuilder.toString();
    }

    /**
     * Provides formatted subject's address.
     * Includes: First Name, Last Name, Address Line 1, Address Line 2, City, State, Zip (separated by new lines)
     */
    public String getFormattedAddress() {
        StringBuilder addressBuilder = new StringBuilder();
        String separator = "";
        separator = addIfNotNull(getFirstName(), addressBuilder, separator, " ");
        separator = addIfNotNull(getLastName(), addressBuilder, separator, "\n");
        separator = addIfNotNull(getAddressLine1(), addressBuilder, separator, "\n");
        separator = addIfNotNull(getAddressLine2(), addressBuilder, separator, "\n");
        separator = addIfNotNull(getCity(), addressBuilder, separator, ", ");
        separator = addIfNotNull(getState(), addressBuilder, separator, " ");
        addIfNotNull(getZip(), addressBuilder, separator, " ");
        return addressBuilder.toString();
    }

    /**
     * Concatenates string representing field value if not empty, using separator string in the specified string builder.
     */
    private String addIfNotNull(String field, StringBuilder builder, String separator, String newSeparator) {
        if (Strings.isNotEmpty(field)) {
            builder.append(separator);
            builder.append(field);
            separator = newSeparator;
        }
        return separator;
    }

    /**
     * Concatenates string representing field value, using separator string in the specified string builder.
     */
    private String addField(String field, String separator, StringBuilder addressBuilder) {
        if (Strings.isNotEmpty(field)) {
            addressBuilder.append(separator);
            addressBuilder.append(field);
            return ", ";
        }
        return separator;
    }

    public Map<String, String> getOtherFieldsMapping() {
        return other == null ?
                new LinkedHashMap<String, String>() :
                other;
    }

    public void setOtherFieldsMapping(Map<String, String> other) {
        this.other = new LinkedHashMap<String, String>(other);
    }

	public void setOtherJSON(String json)
	{
		this.otherJSON = json;
	}

	public String getOtherJSON()
	{
		return this.otherJSON;
	}

    /**
     * Get the answer for an autofill question of type TEXT from Other Map, if it exists on this subject. Otherwise returns null.
     * @param question
     * @return SubjectAnswer with string answer or null.
     */
    public SubjectAnswer getOtherAutofillAnswerTo(Question question) {
        if (null == question) {
            LOGGER.error("Question is null.");
            return null;
        }

        String varName = question.getVarNameNo$();
        if(null == varName || Strings.isEmpty(varName)) {
            LOGGER.warn("Question " + varName + " has empty varName.");
            return null;
        }

        Question.Type questionType = question.getType();
        if (Question.Type.TEXT != questionType &&
            Question.Type.MULTILINE_TEXT != questionType) {
            LOGGER.warn("Question " + varName + " is not TEXT type.");
            return null;
        }

        String questionAnswer = getOtherFieldsMapping().get(varName);
        if (null == questionAnswer || Strings.isEmpty(questionAnswer)) {
            return null;
        }

        SubjectAnswer answer = new SubjectAnswer();
        answer.setQuestionId(question.getId());
        answer.setStringAnswer(questionAnswer);

        return answer;
    }

    /**
     * Get the other key value pairs for the specified questions
     * @param questions
     * @return
     */
    public Map<String, String> getOtherVariablesNotMatchingQuestions(List<Question> questions) {
        Map<String, String> other = getOtherFieldsMapping();
        if (null == questions || questions.isEmpty()) {
            return other;
        }

        LinkedHashMap<String, String> otherFieldMappingNotMatching = new LinkedHashMap<String, String>();
        Set<String> questionVarNames = new HashSet<String>();
        for (Question question : questions) {
            questionVarNames.add(question.getVarNameNo$());
        }

        for (Map.Entry<String, String> otherField : other.entrySet()) {
            if (questionVarNames.contains(otherField.getKey())) {
                continue;
            }
            otherFieldMappingNotMatching.put(otherField.getKey(), otherField.getValue());
        }

        return otherFieldMappingNotMatching;
    }

    public void setKnockedStatus(KnockStatus knockStatus) {
        this.knockStatus = knockStatus;
    }

    public KnockStatus getKnockStatus() {
        return knockStatus;
    }

    /**
     * Checks whether subject has been ever knocked
     */
    public boolean isKnocked() {
        return !KnockStatus.NOT_KNOCKED.equals(knockStatus);
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * Subject's address location
     */
    private GeoPoint geoPoint = null;

    public GeoPoint getGeoPoint() {
        if (geoPoint != null)
            return geoPoint;
        String latS = getLatitude();
        String lngS = getLongitude();
        if (latS == null || lngS == null) {
            geoPoint = DEFAULT_MISSING_LAT_LNG;
            return geoPoint;
        }
        try {
            double lat = Double.parseDouble(latS);
            double lng = Double.parseDouble(lngS);
            geoPoint = new GeoPoint(lat, lng);
            return geoPoint;
        } catch (NumberFormatException e) {
            geoPoint = DEFAULT_MISSING_LAT_LNG;
            return geoPoint;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
    public Subject toSubjectAddress() {
        return new Subject(getAddressLine1(), getAddressLine2(), getCity(), getState(), getZip(), getCountry(), getLatitude(), getLongitude(),getColor());
    }

    public int getId() {
        return id;
    }

    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.create(SUBJECT_KEY, (Parcelable) this);
    }

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Map<String, String> otherFieldsMapping = getOtherFieldsMapping();
        if(otherFieldsMapping.isEmpty())
            parcel.writeBundle(Bundle.EMPTY);
        else {
            Bundle mapBundle = new Bundle();
            for(Map.Entry<String, String> entry : otherFieldsMapping.entrySet()) {
                mapBundle.putString(entry.getKey(), entry.getValue());
            }
            parcel.writeBundle(mapBundle);
        }
        parcel.writeInt(id);
        parcel.writeInt(knockStatus.ordinal());
        parcel.writeString(getSalutation());
        parcel.writeString(getSuffix());
        parcel.writeString(getFirstName());
        parcel.writeString(getLastName());
        parcel.writeString(getAddressLine1());
        parcel.writeString(getAddressLine2());
        parcel.writeString(getCity());
        parcel.writeString(getState());
        parcel.writeString(getZip());
        parcel.writeString(getLocation());
        parcel.writeString(getEmail());
        parcel.writeString(getHomePhone());
        parcel.writeString(getCellPhone());
        parcel.writeString(getPhone());
        parcel.writeString(getHid());
        parcel.writeString(getLatitude());
        parcel.writeString(getLongitude());

        Gender gender = getGender();
        if (gender == null) {
            parcel.writeByte((byte)0);
        }else {
            parcel.writeByte((byte)1);
            parcel.writeString(gender.name());
        }
    }

    private Subject(Parcel in) {
        Bundle mapBundle = in.readBundle();
        other = new LinkedHashMap<String, String>();
        for(String key : mapBundle.keySet()) {
            other.put(key, mapBundle.getString(key));
        }
        id = in.readInt();
        int knockOrdinal = in.readInt();
        setKnockedStatus(KnockStatus.fromOrdinal(knockOrdinal));
        setSalutation(in.readString());
        setSuffix(in.readString());
        setFirstName(in.readString());
        setLastName(in.readString());
        setAddressLine1(in.readString());
        setAddressLine2(in.readString());
        setCity(in.readString());
        setState(in.readString());
        setZip(in.readString());
        setLocation(in.readString());
        setEmail(in.readString());
        setHomePhone(in.readString());
        setCellPhone(in.readString());
        setPhone(in.readString());
        setHid(in.readString());
        setLatitude(in.readString());
        setLongitude(in.readString());
        
        if(in.readByte() == 1) {
            String genderName = in.readString();
            if (Strings.isNotEmpty(genderName)) {
                Gender gender = Gender.valueOf(genderName);
                setGender(gender);
            }
        }else {
            setGender(null);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        if (age != subject.age) return false;
        if (addressLine1 != null ? !addressLine1.equals(subject.addressLine1) : subject.addressLine1 != null)
            return false;
        if (addressLine2 != null ? !addressLine2.equals(subject.addressLine2) : subject.addressLine2 != null)
            return false;
        if (cellPhone != null ? !cellPhone.equals(subject.cellPhone) : subject.cellPhone != null) return false;
        if (phone != null ? !phone.equals(subject.phone) : subject.phone != null) return false;
        if (city != null ? !city.equals(subject.city) : subject.city != null) return false;
        if (email != null ? !email.equals(subject.email) : subject.email != null) return false;
        if (firstName != null ? !firstName.equals(subject.firstName) : subject.firstName != null) return false;
        if (gender != subject.gender) return false;
        if (hid != null ? !hid.equals(subject.hid) : subject.hid != null) return false;
        if (homePhone != null ? !homePhone.equals(subject.homePhone) : subject.homePhone != null) return false;
        if (lastName != null ? !lastName.equals(subject.lastName) : subject.lastName != null) return false;
        if (latitude != null ? !latitude.equals(subject.latitude) : subject.latitude != null) return false;
        if (longitude != null ? !longitude.equals(subject.longitude) : subject.longitude != null) return false;
        if (salutation != null ? !salutation.equals(subject.salutation) : subject.salutation != null) return false;
        if (state != null ? !state.equals(subject.state) : subject.state != null) return false;
        if (suffix != null ? !suffix.equals(subject.suffix) : subject.suffix != null) return false;
        if (zip != null ? !zip.equals(subject.zip) : subject.zip != null) return false;
        if (location != null ? !location.equals(subject.location) : subject.location != null) return false;
        if (isEven != subject.isEven) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hid != null ? hid.hashCode() : 0;
        result = 31 * result + (salutation != null ? salutation.hashCode() : 0);
        result = 31 * result + (suffix != null ? suffix.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
        result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (cellPhone != null ? cellPhone.hashCode() : 0);
        result = 31 * result + (homePhone != null ? homePhone.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + isEven;

        return result;
    }

    @Override
    public String toString() {
        try {
            return getFullAddress();
        } catch (Throwable t) {
            return super.toString();
        }
    }
    
    public int isEven() {
        return isEven;
    }

    public void setEven(int isEven) {
        this.isEven = isEven;
    }
    
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getEmploerCity() {
		return emploerCity;
	}

	public void setEmploerCity(String emploerCity) {
		this.emploerCity = emploerCity;
	}

	public String getEmployerState() {
		return employerState;
	}

	public void setEmployerState(String employerState) {
		this.employerState = employerState;
	}
    
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
