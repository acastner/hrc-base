package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.grassroots.petition.utils.JsonParser;

/**
 * A product and cc info for sending to the server
 */
public class ProductOrder implements Json, Parcelable {

    /**
     * product SKU number
     */
    private final String SKU;
    /**
     * product quantity
     */
    private Integer quantity;
    /**
     * product cost represented by string
     */
    private final String cost;


    // base constructor
    private ProductOrder(String SKU, Integer quantity, String cost) {
        this.SKU = SKU;
        this.quantity = quantity;
        this.cost = cost;
    }

    /**
     * Instantiates product order object for ordinary product (NOT donation product)
     * @param product - ordinary product (NOT donation)
     * @param quantity - quantity (should be specified)
     */
    public ProductOrder(Product product, int quantity) {
        this(product.getSKU(), quantity, null);
    }

    /**
     * Instantiates product order object for donation product
     * @param product - donation product (NOT donation)
     * @param cost - ammount of donation (should be specified)
     */
    public ProductOrder(Product product, String cost) {
        this(product.getSKU(), null, cost);
    }

    public String getSKU() {
        return SKU;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCost() {
        return cost;
    }



    public static final Parcelable.Creator<ProductOrder> CREATOR = new Creator<ProductOrder>() {

        @Override
        public ProductOrder createFromParcel(Parcel parcel) {
            return new ProductOrder(parcel);
        }

        @Override
        public ProductOrder[] newArray(int i) {
            return new ProductOrder[i];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(SKU);
        parcel.writeValue(quantity);
        //parcel.writeInt(quantity);
        parcel.writeString(cost);
    }

    private ProductOrder(Parcel parcel) {
        SKU = parcel.readString();
        quantity = (Integer)parcel.readValue(null);
        //quantity = parcel.readInt();
        cost = parcel.readString();
    }

    public static final String PRODUCT_ORDER_KEY = "ProductOrder";
    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.create(PRODUCT_ORDER_KEY, (Parcelable) this);
    }



    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }
}
