package com.grassroots.petition.models;

import com.grassroots.petition.utils.JsonParser;

/**
 * Data Model contains required fields to perform login (send  on login to the server)
 */
public class LoginEvent implements Json {
    /**
     * username
     */
    private String username;
    /**
     * password
     */
    private String password;
    /**
     * location of the event
     */
    private GpsLocation location;

    public LoginEvent() {
    }

    public LoginEvent(String username, String password, GpsLocation location) {
        this.username = username;
        this.password = password;
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public GpsLocation getLocation() {
        return location;
    }

    public void setLocation(GpsLocation location) {
        this.location = location;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginEvent that = (LoginEvent) o;

        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }
}
