package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Container of server error responses. Query was successful if there were no error responses
 */
public class Response {
    public static final Response SUCCESS_RESPONSE = new Response(Collections.<ResponseError>unmodifiableList(Collections.<ResponseError>emptyList())) {
        public void setResponseErrors(List<ResponseError> errors) {
        }
    };
    /**
     * List of errors
     */
    @SerializedName("errors")
    private List<ResponseError> responseErrors;

    public Response() {
    }

    public Response(List<ResponseError> errors) {
        responseErrors = errors;
    }

    public Response(ResponseError... errors) {
        responseErrors = Arrays.asList(errors);
    }

    public boolean wasSuccess() {
        return responseErrors.isEmpty();
    }

    public List<ResponseError> getResponseErrors() {
        return responseErrors;
    }

    public void setResponseErrors(List<ResponseError> errors) {
        responseErrors = errors;
    }
}
