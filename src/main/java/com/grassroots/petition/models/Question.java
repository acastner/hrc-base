package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.Strings;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Question data model object.
 * 
 * Represents a question and it's answers, and other question data (e.g. magic, required, etc.).
 * Contains all required string constants used to determine type of question.
 */
public class Question implements Parcelable { // TODO: implement Parcelable
    public static final Parcelable.Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel parcel) {
            return new Question(parcel);
        }

        @Override
        public Question[] newArray(int i) {
            return new Question[i];
        }
    };

    private static final String TAG = Question.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * A set of subject information magic questions varnames
     */
    public static final Set<String> SUBJECT_INFORMATION_NAMES = new HashSet<String>();
    /**
     * A set of shipping information magic questions varnames
     */
    public static final Set<String> SHIPPING_INFORMATION_NAMES = new HashSet<String>();

    /**
     * A set of magic questions varnames
     */
    public static final Set<String> MAGIC_QUESTION_NAMES = new HashSet<String>();

    /* Magic questions varnames suffixes */
    public static final String SALUTATION = "SALUTATION";
    public static final String SUFFIX = "SUFFIX";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String ADDRESS = "ADDRESS";
    public static final String ADDRESS_2 = "APARTMENT";
    public static final String CITY = "CITY";
    public static final String STATE = "STATE";
    public static final String ZIP = "ZIP";
    public static final String COUNTRY = "COUNTRY";

    public static final String HOME = "HOME";
    public static final String CELL = "CELL";
    public static final String EMAIL = "EMAIL";
    public static final String PHONE = "PHONE";
    public static final String TERMINATED = "TERMINATED";
    public static final String TERMINATED_PREFIX = "$TERM";
    public static final String SIGNATURE = "SIGNATURE";
    public static final String LOCATION = "LOCATION";
   
    

    /**
     * Credit check varname
     */
    public static final String CREDIT_CHECK = "CREDIT_CHECK";

    /**
     * Shipping question varnames suffix
     */
    public static final String SHIPPING_POSTSCRIPT = "_SHIPPING";

    /**
     * Fill subject information questions varnames list
     */
    /**Note: Email field is not explicitly added to the subject information here. If the question is marked as magic, 
    then it will be added on the customer information screen. If the email question is not marked as magic,
     it will show in the questions list (AQOL).
     **/
    static {
        Question.SUBJECT_INFORMATION_NAMES.add(Question.SALUTATION);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.SUFFIX);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.FIRST_NAME);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.LAST_NAME);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.ADDRESS);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.ADDRESS_2);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.CITY);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.STATE);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.ZIP);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.LOCATION);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.HOME);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.CELL);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.PHONE);
        Question.SUBJECT_INFORMATION_NAMES.add(Question.COUNTRY);

        
    }

    /**
     * Fill shipping information questions varnames list
     */
    static {
        Question.SHIPPING_INFORMATION_NAMES.add(Question.FIRST_NAME + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.LAST_NAME + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.ADDRESS + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.ADDRESS_2 + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.CITY + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.STATE + SHIPPING_POSTSCRIPT);
        Question.SHIPPING_INFORMATION_NAMES.add(Question.ZIP + SHIPPING_POSTSCRIPT);
    }

    public static final String REFERRER_NAME = "_name";
    public static final String REFERRER_PHONE = "_phone";
    public static final String REFERRER_EMAIL = "_email";
    public static final String REFERRER_PRE_SCRIPT = "referrer";
    public static final int NUMBER_OF_REFERRERS = 7;

    /**
     * Fill magic questions varnames list
     */
    static {
        try {
            Class magicQuestionClass = Class.forName(".models.MagicQuestions");
            Field magicQuestionField = magicQuestionClass.getDeclaredField("MAGIC_QUESTION_NAMES");
            Collection<String> extraMagicQuestions = (Collection<String>) magicQuestionField.get(null);
            MAGIC_QUESTION_NAMES.addAll(extraMagicQuestions);
        } catch (ClassNotFoundException e) {
            LOGGER.debug("No magic question class defined", e);
        } catch (NoSuchFieldException e) {
            LOGGER.error("Magic questions exist but no MAGIC_QUESTION_NAMES field exists", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("Can't access magic question field MAGIC_QUESTION_NAMES", e);
        }
        MAGIC_QUESTION_NAMES.addAll(SUBJECT_INFORMATION_NAMES);
        MAGIC_QUESTION_NAMES.addAll(SHIPPING_INFORMATION_NAMES);
        Question.MAGIC_QUESTION_NAMES.add(Question.TERMINATED);
        //Question.MAGIC_QUESTION_NAMES.add(Question.SIGNATURE);
        Question.MAGIC_QUESTION_NAMES.add(Question.CREDIT_CHECK);
        for (int i = 0; i < NUMBER_OF_REFERRERS; i++) {
            Question.MAGIC_QUESTION_NAMES.add(REFERRER_PRE_SCRIPT + i + REFERRER_NAME);
            Question.MAGIC_QUESTION_NAMES.add(REFERRER_PRE_SCRIPT + i + REFERRER_PHONE);
            Question.MAGIC_QUESTION_NAMES.add(REFERRER_PRE_SCRIPT + i + REFERRER_EMAIL);
        }
    }

    //For true/false questions
    public static final int FALSE_ANSWER_ID = 0;
    public static final int TRUE_ANSWER_ID = 1;

    /**
     * Questions Type enum
     */
    public static enum Type {
        MULTILINE_TEXT("FreeFormMultiLine"), TEXT("FreeFormSingleLine"), MULTIPLE("Standard"), 
        TRUEFALSE("YesNo"), SIGNATURE("Signature"), PICTURE("Picture"), CALENDAR("Calendar");

        private final String jsonType;

        private Type(String jsonType) {
            this.jsonType = jsonType;
        }

        public String getJsonType() {
            return jsonType;
        }
    }

    /**
     * Unique question id
     */
    private int id;
    /**
     * Question titles
     */
    @SerializedName("Title")
    private String name;
    /**
     * Question type (look at Type)
     */
    private Type type;
    @SerializedName("Variable")
    /**
     * Question varname, begins with '$' sign (question key-name unique in current petition)
     */
    private String varName;
    /**
     * Question text
     */
    private String text;
    /**
     * Question order
     */
    private int order;
    /**
     * Flag indicates whether question is magic
     */
    private boolean magic;
    /**
     * Flag indicates whether question is required
     */
    private boolean required;
    /**
     * Flag indicates whether question has a tally
     */
    @SerializedName("QuestionTally")
    private boolean questionTally;

    /**
     * List of predefined answers for question
     */
    private List<QuestionAnswer> answers = new ArrayList<QuestionAnswer>();

    public Question() {
    }

    public Question(String name) {
        this("whatever", Type.TEXT, name);
    }

    public Question(String name, Type type, String varName) {
        this(0, name, type, varName, Collections.<QuestionAnswer>emptyList());
    }

    public Question(int id, String name, Type type, String varName, List<QuestionAnswer> questionAnswers) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.varName = varName;
        this.answers = questionAnswers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getVarName() {
        return varName;
    }

    /**
     * Question varname without '$' sign (question key-name unique in current petition)
     */
    private transient String varNameNo$;

    /**
     * get question's variable name without the preceding $
     */
    public String getVarNameNo$() {
        if(varNameNo$ != null)
            return varNameNo$;
        varNameNo$ = getVarName();
        if(Strings.isEmpty(varNameNo$))
            return "";
        varNameNo$ = varNameNo$.startsWith("$") && varNameNo$.length() > 1 ?
                varNameNo$.substring(1) :
                varNameNo$;
        return varNameNo$;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


    public List<QuestionAnswer> getAnswers() {
        if (answers == null)
            return new ArrayList<QuestionAnswer>();
        return answers;
    }

    /**
     * Provides predefined answer by it's id
     */
    public QuestionAnswer getAnswerWithId(int answerId) {
        for(QuestionAnswer answer : getAnswers()) {
            if(answer.getId() == answerId)
                return answer;
        }
        return null;
    }

    /**
     * get the answers sorted by answer id
     */
    public List<QuestionAnswer> getSortedAnswers() {
        Collections.sort(getAnswers(), new Comparator<QuestionAnswer>() {
            @Override
            public int compare(QuestionAnswer questionAnswer, QuestionAnswer questionAnswer2) {
                return questionAnswer.getId() - questionAnswer2.getId();
            }
        });
        return getAnswers();
    }

    public void setAnswers(List<QuestionAnswer> questionAnswers) {
        this.answers = questionAnswers;
    }

    /**
     * Checks question type
     */
    public boolean is(Type type) {
        if(this.type == null)
            return type == null;
        return this.type.equals(type);
    }

    /**
     * Indicates whether question has magic type
     */
    public boolean isMagic() {
        return magic || MAGIC_QUESTION_NAMES.contains(getVarNameNo$()) || isTerminated() || isShipping();
    }

    public boolean isRequired() {
        return required;
    }
    
    public boolean hasTally() {
        return questionTally;
    }

    /**
     * Indicates whether question is subject information magic question.
	 *
	 * Since we handle $EMAIL specially now, make sure it's considered manually.
     */
    public boolean isSubjectInfo() {
		if(SUBJECT_INFORMATION_NAMES.contains(getVarNameNo$()))
			return true;
		if(getVarNameNo$().equals("EMAIL") && isMagic())
			return true;

		return false;
    }

    /**
     * Indicates whether question is referrer magic question
     */
    public boolean isReferrer() {
        return varContains(REFERRER_PRE_SCRIPT);
    }

    /**
     * Indicates whether question is shipping magic question
     */
    public boolean isShipping() {
        return SHIPPING_INFORMATION_NAMES.contains(getVarNameNo$());
    }

    /**
     * Indicates whether question is picture
     */
    public boolean isPicture() {
        return Type.PICTURE == type;
    }
    
    /**
     * Indicates whether question is calendar question
     */
    public boolean isCalendar() {
        return Type.CALENDAR == type;
    }

    /**
     * Indicates whether question is signature magic question
     */
    public boolean isSignature() {
        return Type.SIGNATURE == type;
    }

    /**
     * Indicates whether question is terminaiton magic question
     */
    public boolean isTerminated() {
        return varName != null && varName.startsWith(TERMINATED_PREFIX);
    }

    /**
     * Gets question's referrer number or -1
     */
    public int getReferrerNumber() {
        String name = getVarNameNo$();
        int lengthOfPrescript = REFERRER_PRE_SCRIPT.length();
        if (Strings.isEmpty(name) || name.length() <= lengthOfPrescript) {
            return -1;
        }

        String number = name.substring(lengthOfPrescript, lengthOfPrescript + 1);
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException nfe) {
            return -1;
        }
    }

    /**
     * Provides referrer post script or null
     * @return
     */
    public String getReferrerPostScript() {
        int referrerNumber = getReferrerNumber();
        if (referrerNumber == -1) {
            return null;
        }
        String name = getVarNameNo$();
        String[] referrerPieces = name.split(String.valueOf(referrerNumber));
        if (referrerPieces.length < 2) {
            return null;
        }
        return referrerPieces[1];
    }

    /**
     * Checks whether question's varname contains specified substring
     * @param toCheck
     * @return
     */
    private boolean varContains(String toCheck) {
        String name = getVarName();
        if (Strings.isEmpty(name)) {
            return false;
        }
        return name.contains(toCheck);
    }

    /**
     * Provides a string of predefined answer by it's id
     */
    public String getAnswerText(int answerId) {
        if(Type.TRUEFALSE.equals(type)) {
            if(answerId == TRUE_ANSWER_ID)
                return "Yes";
            else
                return "No";
        }
        for (QuestionAnswer answer : getAnswers()) {
            if (answerId == answer.getId()) {
                return answer.getValue();
            }
        }
        return "";
    }

    public String toString() {
        return getName();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(type.jsonType);
        parcel.writeString(varName);
        parcel.writeString(text);
        parcel.writeInt(order);
        parcel.writeInt(magic ? 1 : 0);
        parcel.writeInt( required ? 1 : 0 );
        parcel.writeInt( questionTally ? 1 : 0 );
        parcel.writeList(answers);
    }

    private Question(Parcel input) {
        id = input.readInt();
        name = input.readString();
        type = Type.valueOf(input.readString());
        varName = input.readString();
        text = input.readString();
        order = input.readInt();
        magic = (0 == input.readInt()) ? false : true;
        required = (0 != input.readInt());
        questionTally = (0 != input.readInt());
        input.readTypedList(answers, QuestionAnswer.CREATOR);
    }
}
