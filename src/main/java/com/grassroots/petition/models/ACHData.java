package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.grassroots.petition.utils.JsonParser;
import org.apache.log4j.Logger;

/**
 * ACH - Automated Clearing House network
 * Data model used for payment by check.
 */

public class ACHData implements ParcelableData {
    private static final String TAG = ACHData.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Key-name {@value} for storing ACHData objects as key-value.
     */
    private static final String DATA_KEY = "ACHData";
    @Override
    public String getKey(){
        return DATA_KEY;
    }

    /**
     * String for storing an ABARouting value
     */
    private final String ABARouting;
    public String getABARouting() {
        return ABARouting;
    }

    /**
     * String for storing an ACHAccount value
     */
    private final String ACHAccount;
    public String getACHAccount() {
        return ACHAccount;
    }

    /**
     * String for storing an ACHType value
     */
    private final String ACHType;
    public String getACHType() {
        return ACHType;
    }



    /* Methods which implement Parcelable interface */

    /**
     * Generates instances of ACHData class from a Parcel
     */
    public static final Parcelable.Creator<ACHData> CREATOR = new Creator<ACHData>() {
        @Override
        public ACHData createFromParcel(Parcel parcel) {
            return new ACHData(parcel);
        }

        @Override
        public ACHData[] newArray(int i) {
            return new ACHData[i];
        }
    };

    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.<String, Parcelable>create(DATA_KEY, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }





    /**
     * Constructor to create an ACHData object from ABARouting, ACHAccount, ACHType strings.
     */
    public ACHData(String ABARouting, String ACHAccount, String ACHType) {
        this.ABARouting = ABARouting;
        this.ACHAccount = ACHAccount;
        this.ACHType = ACHType;
    }

    /**
     * Constructor to create ACHData from a Parcel object.
     */
    private ACHData(Parcel parcel) {
        ABARouting = parcel.readString();
        ACHAccount = parcel.readString();
        ACHType = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ABARouting);
        parcel.writeString(ACHAccount);
        parcel.writeString(ACHType);
    }


    /* Method which implements Json interface */

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }
}
