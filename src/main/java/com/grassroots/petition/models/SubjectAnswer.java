package com.grassroots.petition.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.Strings;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

/**
 * A canvassed person's answer to a question
 */
public class SubjectAnswer implements Json, Parcelable {
    public static final Creator<SubjectAnswer> CREATOR = new Creator<SubjectAnswer>() {
        @Override
        public SubjectAnswer createFromParcel(Parcel parcel) {
            return new SubjectAnswer(parcel);
        }

        @Override
        public SubjectAnswer[] newArray(int i) {
            return new SubjectAnswer[i];
        }
    };
    /**
     * Constant indicates subject answer is not assigned to any question or subject answer doesn't contain question answer
     */
    public static final int NO_VALUE = -1;

    public static final String QUESTION_ID = "%QUESTION_ID%";
    public static final String ANSWER = "%ANSWER%";
    private static final String QUESTION_JSON = "{\"question\": " + QUESTION_ID + ", \"answer\": " + ANSWER + " }";

    /**
     * id of question, current answer belongs to
     */
    private int questionId = NO_VALUE;
    /**
     * String with answer
     */
    private String stringAnswer;
    /**
     * Picture with answer
     */
    private String pictureLocation;
    /**
     * id of predefined question answer, current answer belongs to
     */
    private int answerId = NO_VALUE;
    /**
     * An array of bytes which represents current picture answer
     */
    private byte[] pictureAnswer = new byte[0];
    private Signature signature;

    public SubjectAnswer() {
    }

    public SubjectAnswer(int questionId, Bitmap picture) {
        setPictureAnswer(picture);
        this.questionId = questionId;
    }

    public SubjectAnswer(int questionId, String answer) {
        this(questionId, answer, NO_VALUE);
    }

    public SubjectAnswer(int questionId, boolean answer) {
        this(questionId, null, answer ? Question.TRUE_ANSWER_ID : Question.FALSE_ANSWER_ID);
    }

    public SubjectAnswer(int questionId, String answer, int answerId) {
        this.questionId = questionId;
        stringAnswer = answer == null ? answer : answer.trim();
        this.answerId = answerId;
    }

    public SubjectAnswer(QuestionAnswer questionAnswer) {
        this(questionAnswer.getQuestionId(), null, questionAnswer.getId());
    }

    /**
     * Get a displayable string answer to the specified question, if exists
     * If subject answer uses predefined question answer then use predefined question answer text
     * otherwise use stringAnswer text
     */
    public String getAnswerString(Question question) {
        if (hasAnswerId()) {
            String answerText = question.getAnswerText(getAnswerId());
            if (answerText == null &&
                    (answerId == 0 || answerId == 1))
            {
                Context context = GlobalData.getContext();
                answerText = answerId == 0 ?
                        context.getString(R.string.ANSWER_TEXT_YES) :
                        context.getString(R.string.ANSWER_TEXT_NO);
            }
            return answerText;
        } else {
            return getStringAnswer();
        }
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getStringAnswer() {
        return stringAnswer;
    }

    public void setStringAnswer(String stringAnswer) {
        this.stringAnswer = stringAnswer == null ? stringAnswer : stringAnswer.trim();
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    /**
     * Checks whether subject answer uses predefined question answer
     */
    public boolean hasAnswerId() {
        return answerId != NO_VALUE;
    }

    public void setPictureAnswer(byte[] pictureAnswer) {
        this.pictureAnswer = pictureAnswer;
    }

    /**
     * Compress and set the picture answer
     * @param picture
     */
    public void setPictureAnswer(Bitmap picture) {
        if(picture != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            pictureAnswer = stream.toByteArray();
        }
    }

    /**
     * Stores picture file location
     */
    public void setPictureAnswerPlaceholder(String fileLocation) {
        pictureLocation = fileLocation;
    }

    /**
     * Gets picture answer as an array of bytes
     */
    public byte[] getPictureAnswer() {
        return pictureAnswer;
    }

    public String getPictureLocation() {
        return pictureLocation;
    }

    /**
     * Gets picture answer as a base 64 encoded string, which represents array of bytes
     */
    public String getPictureAsString() {
        return Base64.encodeToString(pictureAnswer, Base64.NO_WRAP);
    }

    public Bitmap getPictureBitmap() {
        return BitmapFactory.decodeByteArray(pictureAnswer, 0, pictureAnswer.length);
    }

    /**
     * Checks whether answer represents picture
     */
    public boolean hasPictureAnswer() {
        return pictureAnswer.length > 0 || Strings.isNotEmpty(pictureLocation) ;
    }

    /**
     * Since picture answers tend to be too large to transfer between intents, we transfer only the
     * location. Then before we send the server the petition answers we need to read in the photo bytes
     * @param activity
     */
    public void convertLocationToPictureAnswer(BasePetitionActivity activity) {
        setPictureAnswer(activity.readScaledPhoto(pictureLocation));
    }

    public boolean hasStringAnswer() {
        return Strings.isNotEmpty(stringAnswer);
    }

    public boolean hasAnswer() {
        return hasAnswerId() || hasStringAnswer() || hasPictureAnswer();
    }

    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(getQuestionId());
        parcel.writeString(getStringAnswer());
        parcel.writeInt(getAnswerId());
        parcel.writeString(pictureLocation);
        parcel.writeInt(pictureAnswer.length);
        parcel.writeByteArray(pictureAnswer);
    }

    private SubjectAnswer(Parcel parcel) {
        this(parcel.readInt(), parcel.readString(), parcel.readInt());
        pictureLocation = parcel.readString();
        int pictureSize = parcel.readInt();
        pictureAnswer = new byte[pictureSize];
        parcel.readByteArray(pictureAnswer);
    }

    @Override
    public String toString() {
        return toJson();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectAnswer that = (SubjectAnswer) o;

        if (answerId != that.answerId) return false;
        if (questionId != that.questionId) return false;
        if (!Arrays.equals(pictureAnswer, that.pictureAnswer)) return false;
        if (stringAnswer != null ? !stringAnswer.equals(that.stringAnswer) : that.stringAnswer != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = questionId;
        result = 31 * result + (stringAnswer != null ? stringAnswer.hashCode() : 0);
        result = 31 * result + answerId;
        result = 31 * result + (pictureAnswer != null ? Arrays.hashCode(pictureAnswer) : 0);
        return result;
    }

    public void setSignatureAnswer(Signature signature) {
        this.signature = signature;
        
    }

    public Signature getSignature() {
        return signature;
    }
    
    
}
