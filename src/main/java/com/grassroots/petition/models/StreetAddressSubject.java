package com.grassroots.petition.models;

/**
 * A subject whose equals and hashCode methods depend solely on the first line of their address plus city, state, and zip
 */

public class StreetAddressSubject extends Subject {

    public StreetAddressSubject(String addressLine1, String addressLine2, String city, String state, String zip, String country, String latitude, String longitude, int id, KnockStatus knockStatus,String color) {
        super(addressLine1, addressLine2, city, state, zip, country, latitude, longitude,color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        if (getAddressLine1() != null ? !getAddressLine1().equals(subject.getAddressLine1()) : subject.getAddressLine1() != null)
            return false;

        if (getCity() != null ? !getCity().equals(subject.getCity()) : subject.getCity() != null) return false;
        if (getState() != null ? !getState().equals(subject.getState()) : subject.getState() != null) return false;

        if (getZip() != null ? !getZip().equals(subject.getZip()) : subject.getZip() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (getAddressLine1() != null ? getAddressLine1().hashCode() : 0);
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getZip() != null ? getZip().hashCode() : 0);
        return result;
    }
}
