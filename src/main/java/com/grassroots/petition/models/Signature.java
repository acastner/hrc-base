package com.grassroots.petition.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Pair;
import com.grassroots.petition.utils.Bitmaps;

import java.io.ByteArrayOutputStream;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 * Signature data model
 */
public class Signature {
    /**
     * Key-name {@value} for storing Signature objects as key-value.
     */
    public static final String SIGNATURE_KEY = "signature";
    /**
     * Signature bitmap
     */
    private final Bitmap signatureBitmap;

    public Signature(Bitmap bitmap) {
        signatureBitmap = bitmap;
    }

    /**
     * Represents signature's bitmap as compressed bytes array (bitmap has PNG format with 100% quality)
     * @return a ByteArrayOutputStream
     */
    public byte[] asCompressedByteArray() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Represents signature's bitmap as bitmap scaled to 100 pixels by height
     * @return a ByteArrayOutputStream
     */
    public Bitmap asScaledBitmap(Context context) {
        return Bitmaps.scaleBitmapToSize(signatureBitmap, 100, context);
    }

    /**
     * Represents signature's bitmap as bitmap scaled to 100 pixels by height
     * @return a ByteArrayOutputStream
     */
    public Pair<String, Parcelable> asScaledPair(Context context) {
        return Pair.<String, Parcelable>create(SIGNATURE_KEY, asScaledBitmap(context));
    }

    /**
     * Represents signature's bitmap as base64encoded string (used for emails and post requests)
     * @return a base64encoded string
     */
    public String asBase64EncodedString(){
        return Base64.encodeToString(asCompressedByteArray(), Base64.NO_WRAP);
    }
}
