package com.grassroots.petition.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.Walklist.HouseNumberFilter;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.Performance;
import com.grassroots.petition.utils.Strings;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 * Data model stores a list of subjects, which should be canvassed in current petition
 */
public class Walklist implements Parcelable, Json {
    public static final Parcelable.Creator<Walklist> CREATOR = new Creator<Walklist>() {

        @Override
        public Walklist createFromParcel(Parcel parcel) {
            return new Walklist(parcel);
        }

        @Override
        public Walklist[] newArray(int i) {
            return new Walklist[i];
        }
    };
    /**
     * Key-name {@value} for storing Walklist objects as key-value.
     */
    public static final String WALKLIST_KEY = "Walklist";
    public static final String TAG = Walklist.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * House Number Filter Enum.
     * Provides Even, Odd, All
     */
    public enum HouseNumberFilter {
        ODD(R.string.odd),EVEN(R.string.even), ALL(R.string.all);

        static {
            ODD.setNext(ALL);
            EVEN.setNext(ODD);
            ALL.setNext(EVEN);
        }

        private final int displayTextId;
        /**
         * Returns Next filter element.
         */
        private HouseNumberFilter next;

        private HouseNumberFilter(int displayTextId) {
            this.displayTextId = displayTextId;
        }

        private void setNext(HouseNumberFilter next) {
            this.next = next;
        }

        public int getDisplayTextId() {
            return displayTextId;
        }

        public HouseNumberFilter getNext() {
            return next;
        }
    }

    //There is magic between these two fields. They must be kept in sync; no adding to subjects without updating
    //the address subject map, same with removing. That means if people are calling getSubjects, they should not
    //be mutating the list
    /**
     * Subjects list.
     */
    @SerializedName("Walklist")
    private List<Subject> subjects;
    /**
     * Mapping Subject which represents single address to a list of subjects which belong to this address.
     */
    public transient Map<Subject, List<Subject>> addressSubjectMap = new HashMap<Subject, List<Subject>>();
    /**
     * Mapping Subject street address to a list of Subjects belong to this address.
     */
    private transient Map<StreetAddressSubject, List<Subject>> streetAddressSubjectMap = new HashMap<StreetAddressSubject, List<Subject>>();

    public Walklist() {
        this(new ArrayList<Subject>());
    }

    public Walklist(List<Subject> subjects) {
        this.subjects = subjects;
        System.err.println("In walklist constructor");
    }

    public Walklist(Parcel parcel) {
        this();
        parcel.readTypedList(subjects, Subject.CREATOR);
    }


    public void setSubjects(List<Subject> subjects) {
        this.subjects = new ArrayList<Subject>(subjects);
    }

    /**
     * Do not mutate this object
     */
    public List<Subject> getSubjects() {
        return subjects;
    }

    public Subject get(int i) {
        return subjects.get(i);
    }

    /**
     * Number of subjects in walklist.
     */
    public int size() {
        return subjects.size();
    }

    /**
     * Is walklist empty.
     */
    public boolean isEmpty() {
        return subjects.isEmpty();
    }

    /**
     * Gets a list of subjects which belong to the specified subject address.
     * @param subject - a subject which is used to get address
     * @return a list of subjects
     */
    public List<Subject> getSameAddressSubjects(Subject subject) {
        populateSubjectAddressMap();
        return addressSubjectMap.get(subject.toSubjectAddress());
    }

    /**
     * Gets a list of subjects which belong to the specified street address.
     * @param subject - street address
     * @return alist of subjects
     */
    public List<Subject> getSameStreetAddressSubjects(Subject subject) {
        populateSubjectAddressMap();
        return streetAddressSubjectMap.get(subject.toStreetAddressSubject());
    }

    /**
     * Gets a list of subjects addresses
     */
    public List<Subject> getAddressSubjects() {
        populateSubjectAddressMap();
        return new ArrayList<Subject>(addressSubjectMap.keySet());
    }

    /**
     * Gets a list of subjects with even house number addresses
     */
    public List<Subject> getEvenAddressSubjects() {
        return getFilteredAddressSubjects(true);
    }

    /**
     * Gets a list of subjects with odd house number addresses
     */
    public List<Subject> getOddAddressSubjects() {
        return getFilteredAddressSubjects(false);
    }

    /**
     * Gets a list of subjects using specified house number filter.
     */
    public List<Subject> getAddressSubjects(HouseNumberFilter houseNumberFilter) {
        WalklistDataSource walklistDataSource = new WalklistDataSource(GlobalData.getContext());
        long startTime = System.currentTimeMillis();
        try {
            switch (houseNumberFilter) {
                case ALL:
                    return getAddressSubjects();
                case EVEN:
                    return walklistDataSource.getWalklistForEven().getAddressSubjects();
                case ODD:
                    return walklistDataSource.getWalklistForOdd().getAddressSubjects();
            }
        } finally {
            Performance.end(startTime, "Getting address subjects for: " + houseNumberFilter);
        }
        return new ArrayList<Subject>();
    }

    /**
     * Gets a list of subjects using event house number flag.
     */
    private List<Subject> getFilteredAddressSubjects(boolean returnEvens) {
        populateSubjectAddressMap();
        long startTime = System.currentTimeMillis();
        List<Subject> filteredSubjects = new ArrayList<Subject>();
        for (Subject subject : getAddressSubjects()) {
            if (subject.isHouseNumberEven() == returnEvens) {
                filteredSubjects.add(subject);
            }
        }
        Performance.end(startTime, "getting filtered address subjects, returning evens? " + returnEvens);
        return filteredSubjects;
    }


    /**
     * Gets a list of s
     * @param filter
     * @return
     */
    public List<Subject> filterStreetAddress(String filter) {
        return filterStreetAddress(filter, HouseNumberFilter.ALL);
    }

    /**
     * Gets a list of subjects addresses using specified street filter string and house number filter.
     * @param filter - string with street filter (a part of street name from addressLine1)
     * @param numberFilter - house number filter
     * @return list of subjects
     */
    public List<Subject> filterStreetAddress(String filter, HouseNumberFilter numberFilter) {
        if (Strings.isEmpty(filter)) {
            List<Subject> addressSubjects = getAddressSubjects(numberFilter);
            return addressSubjects;
        }
        List<Subject> filteredList = new ArrayList<Subject>();
        filter = filter.toUpperCase();
        long startTime = System.currentTimeMillis();
        List<Subject> streetAddressSubjects = getStreetAddressSubjects(filter,numberFilter,null);
        filteredList.addAll(streetAddressSubjects);
        Performance.end(startTime, "filtering on street name");
        return filteredList;
    }

    private List<Subject> getStreetAddressSubjects(String filter, HouseNumberFilter numberFilter, KnockStatus statusFilter) {
        WalklistDataSource walklistDataSource = new WalklistDataSource(GlobalData.getContext());
        long startTime = System.currentTimeMillis();
        try {
            if(statusFilter!=null){
                return walklistDataSource.getWalklistForStreetAndHouse(filter,numberFilter.ordinal(),""+statusFilter.getRepresentation());
            }else{
                return walklistDataSource.getWalklistForStreetAndHouse(filter,numberFilter.ordinal());
            }
           
        } finally {
            Performance.end(startTime, "Getting street address subjects for: " + filter);
        }
    }

    /**
     * Gets a list of subjects addresses using specified street filter string, house number filter and knock status.
     * @param filter - string with street filter (a part of street name from addressLine1)
     * @param numberFilter - house number filter
     * @param statusFilter - knock status filter
     * @return list of subjects
     */
    public List<Subject> filterStreetAddress(String filter, HouseNumberFilter numberFilter, KnockStatus statusFilter) {
        List<Subject> filteredList = new ArrayList<Subject>();
        if(filter != null)
        filter = filter.toUpperCase();
        long startTime = System.currentTimeMillis();
        List<Subject> streetAddressSubjects = getStreetAddressSubjects(filter,numberFilter,statusFilter);
        filteredList.addAll(streetAddressSubjects);
        Performance.end(startTime, "filtering on street name");
        return filteredList;
    }

    /**
     * Gets knock status with the highest id value among subjects which belong to the specified address.
     * @param address - subjects address where to look for the highest knock status
     * @return knock status with the highest id value
     */
    public KnockStatus getHighestStatus(Subject address) {
        List<Subject> subjects = getSameAddressSubjects(address); // Gets a list of subjects which belong to the specified subject address.
        return getHighestStatus(subjects);
    }

    /**
     * Gets knock status with the highest id value among specified subjects.
     */
    private KnockStatus getHighestStatus(List<Subject> subjects) {
        KnockStatus highestStatus = KnockStatus.NOT_KNOCKED;
        for (Subject subject : subjects) {
            KnockStatus subjectStatus = subject.getKnockStatus();
            if (highestStatus.getRepresentation() < subjectStatus.getRepresentation()) {
                highestStatus = subjectStatus;
            }
        }
        return highestStatus;
    }

    /**
     * Gets knock status with the highest id value among subjects which belong to the specified address.
     * @param address - subjects address where to look for the highest knock status
     * @return knock status with the highest id value
     */
    public KnockStatus getHighestApartmentStatus(Subject address) {
        return getHighestStatus(getSameStreetAddressSubjects(address));
    }

    /**
     * Gets a list of available street names from walklist
     * @return
     */
    public List<String> getStreetNames() {
        List<Subject> subjectAddressList = getAddressSubjects();
        List<String> addressList = new ArrayList<String>();

        for (Subject address: subjectAddressList) {
            String streetName = address.getStreetName();
            if (Strings.isNotEmpty(streetName)) {
                if (addressList.indexOf(streetName) == -1)
                    addressList.add(address.getStreetName());
            }
        }

        Collections.sort(addressList);

        return addressList;
    }

    /**
     * Gets a list of available lastnames from walklist
     * @return
     */
    public List<Subject> getSubjectsForLastName(Context context,String filter) {
        WalklistDataSource walklistDataSource = new WalklistDataSource(context);
        Walklist walklist = walklistDataSource.getLastNamesFromWalklist(filter);
        return walklist.getSubjects();
    }
    
    /**
     * Fill in mappings for addressSubjectMap and streetAddressSubjectMap.
     */
    private void populateSubjectAddressMap() {
        if (addressSubjectMap.isEmpty() || streetAddressSubjectMap.isEmpty()) {
            long startTime = System.currentTimeMillis();
            for (Subject subject : getSubjects()) {
                Subject subjectAddress = subject.toSubjectAddress();
                List<Subject> subjectList = addressSubjectMap.get(subjectAddress);
                if (subjectList == null) {
                    subjectList = new ArrayList<Subject>();
                    addressSubjectMap.put(subjectAddress, subjectList);
                    
                }
                subjectList.add(subject);

                StreetAddressSubject streetAddressSubject = subject.toStreetAddressSubject();
                List<Subject> streetSubjectList = streetAddressSubjectMap.get(streetAddressSubject);
                if(streetSubjectList == null) {
                    streetSubjectList = new ArrayList<Subject>();
                    streetAddressSubjectMap.put(streetAddressSubject, streetSubjectList);
                }
                streetSubjectList.add(subject);
            }
            Performance.end(startTime, "Populate subject address map");
        }
    }

    /**
     * Gets subject by id.
     */
    public Subject getSubject(int id) {
        for (Subject subject : getSubjects()) {
            if (subject.getId() == id)
                return subject;
        }
        return null;
    }

    /**
     * Gets subject index in subjects list.
     */
    public int getIndex(Subject subject) {
        return subjects.indexOf(subject);
    }

    /**
     * Set SUCCESS knock status by subject id.
     */
    public void setKnocked(int subjectId) {
        setKnocked(subjectId, KnockStatus.SUCCESS);
    }
    
    /**
     * Set SUCCESS knock status by subject.
     */
    public void setKnocked(Subject subject) {
    	 Subject toKnock = subject;
         if (toKnock != null ) {
             Subject knockedSubjectAddress = toKnock.toSubjectAddress();
             StreetAddressSubject knockedSubjectStreetAddress = toKnock.toStreetAddressSubject();
             knockedSubjectAddress.setKnockedStatus(KnockStatus.SUCCESS);
             knockedSubjectStreetAddress.setKnockedStatus(KnockStatus.SUCCESS);

             // Update addressSubjectMap cache
             if (!addressSubjectMap.isEmpty()) {
                 Subject subjectAddress = toKnock.toSubjectAddress();
                 List<Subject> subjectList = addressSubjectMap.get(subjectAddress);
                 if (subjectList != null) {
                     addressSubjectMap.remove(subjectAddress);
                     addressSubjectMap.put(knockedSubjectAddress, subjectList);
                 }
             }

             // Update streetAddressSubjectMap cache
             if (!streetAddressSubjectMap.isEmpty()) {
                 Subject subjectStreetAddress = toKnock.toStreetAddressSubject();
                 List<Subject> subjectList = streetAddressSubjectMap.get(subjectStreetAddress);
                 if (subjectList != null) {
                     streetAddressSubjectMap.remove(subjectStreetAddress);
                     streetAddressSubjectMap.put(knockedSubjectStreetAddress, subjectList);
                 }
             }

             toKnock.setKnockedStatus(KnockStatus.SUCCESS); 
         }    
    }

    /**
     * Set knock status by subject id.
     */
    public void setKnocked(int subjectId, KnockStatus status) {
        Subject toKnock = getSubject(subjectId);
        if (toKnock != null && status != null) {
            Subject knockedSubjectAddress = toKnock.toSubjectAddress();
            StreetAddressSubject knockedSubjectStreetAddress = toKnock.toStreetAddressSubject();
            knockedSubjectAddress.setKnockedStatus(status);
            knockedSubjectStreetAddress.setKnockedStatus(status);

            // Update addressSubjectMap cache
            if (!addressSubjectMap.isEmpty()) {
                Subject subjectAddress = toKnock.toSubjectAddress();
                List<Subject> subjectList = addressSubjectMap.get(subjectAddress);
                if (subjectList != null) {
                    addressSubjectMap.remove(subjectAddress);
                    addressSubjectMap.put(knockedSubjectAddress, subjectList);
                }
            }

            // Update streetAddressSubjectMap cache
            if (!streetAddressSubjectMap.isEmpty()) {
                Subject subjectStreetAddress = toKnock.toStreetAddressSubject();
                List<Subject> subjectList = streetAddressSubjectMap.get(subjectStreetAddress);
                if (subjectList != null) {
                    streetAddressSubjectMap.remove(subjectStreetAddress);
                    streetAddressSubjectMap.put(knockedSubjectStreetAddress, subjectList);
                }
            }

            toKnock.setKnockedStatus(status);

        } else {
            LOGGER.error("Tried to knock with unfound subject or null KnockStatus: " + subjectId + ", status: " + status);
        }
    }

    /**
     * Remove subject from walklist.
     */
    public void removeSubject(Subject subject) {
        if (subject == null)
            return;
        Subject subjectAddress = subject.toSubjectAddress();
        List<Subject> subjectsToRemove = addressSubjectMap.get(subjectAddress);
        if (subjectsToRemove != null)
            subjects.removeAll(subjectsToRemove);
        addressSubjectMap.remove(subjectAddress);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(subjects);
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    public List<String> getAllLastNames(Context context) {
        WalklistDataSource walklistdatasource = new WalklistDataSource(context);
        return walklistdatasource.getDistinctLastNames();
    }
    
    public List<Subject> getLastNamesFromWalklist(Context context,String lastnamefilter) {
        WalklistDataSource walklistdatasource = new WalklistDataSource(context);
        return walklistdatasource.getLastNamesFromWalklist(lastnamefilter).getSubjects();
    }

	public List<Subject> filterStreetAddress(KnockStatus knockStatusFilter,
			String lastNameFilter) {
		
		WalklistDataSource walklistDataSource = new WalklistDataSource(GlobalData.getContext());
        long startTime = System.currentTimeMillis();
        int knockOrdinal = -1;
        if(knockStatusFilter != null ){
            knockOrdinal = knockStatusFilter.getRepresentation();
        }
        if(knockStatusFilter == null){
        	return walklistDataSource.getWalklistForLastName(lastNameFilter);
        }
        
        return walklistDataSource.getWalklistForLastName(""+knockOrdinal,lastNameFilter);           
	}
    
}
