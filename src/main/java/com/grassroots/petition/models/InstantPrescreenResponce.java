package com.grassroots.petition.models;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

/**
 * Data Model to receive data from web-service for Instant Prescreen request.
 */
public class InstantPrescreenResponce implements Json
{
    public static final String INSTANT_PRESCREEN_RESPONSE_IS_APPROVED_STRING_CONST = "Approved";
    /**
     * Reference Number integer value, should contain smth. like 1234567.
     */
    private String referenceNumber; // NOTE: can be 32 or 64 bits length or more ?
    /**
     * Code string, should contain "A" string value.
     */
    private String code;
    /**
     * Text string, indicates whether client was approved: should contain case sensitive "Approved" string.
     */
    @SerializedName("TEXT")
    private String text;
    /**
     * Decision Level integer, should contain 1.
     */
    private Integer decisionLevel;

    public InstantPrescreenResponce(String referenceNumber, String code, String text, Integer decisionLevel) {
        this.referenceNumber = referenceNumber;
        this.code = code;
        this.text = text;
        this.decisionLevel = decisionLevel;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getDecisionLevel() {
        return decisionLevel;
    }

    public void setDecisionLevel(Integer decisionLevel) {
        this.decisionLevel = decisionLevel;
    }

    /**
     * Checks if client is approved: text field should be case sensitive equal to {@value #INSTANT_PRESCREEN_RESPONSE_IS_APPROVED_STRING_CONST} string.
     * @return true if client is approved (text == {@value #INSTANT_PRESCREEN_RESPONSE_IS_APPROVED_STRING_CONST}), otherwise false.
     */
    public boolean isClientApproved() {
        if (!TextUtils.isEmpty(text)) {
            if (INSTANT_PRESCREEN_RESPONSE_IS_APPROVED_STRING_CONST.equalsIgnoreCase(text)) {
                return true;
            }
        }
        return false;
    }
}
