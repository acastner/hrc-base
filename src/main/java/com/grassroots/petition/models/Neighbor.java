package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

public class Neighbor implements Json{

	@Override
	public String toJson() {
		return JsonParser.getParser().jsonify(this);
	}
	
	
	@SerializedName("DistanceInMeters")
    private String DistanceInMeters;
    
    @SerializedName("Latitude")
    private String Latitude;
    
    @SerializedName("Longitude")
    private String Longitude;
    
    @SerializedName("FirstName")
    private String FirstName;
    
    @SerializedName("LastName")
    private String LastName;
    
    @SerializedName("Address")
    private String Address;

	public String getDistanceInMeters() {
		return DistanceInMeters;
	}

	public void setDistanceInMeters(String distanceInMeters) {
		DistanceInMeters = distanceInMeters;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
    
   
    

}
