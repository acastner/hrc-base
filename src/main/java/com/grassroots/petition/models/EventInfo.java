package com.grassroots.petition.models;

public class EventInfo extends GpsLocation {

    private int Batterylevel;
    private int Signalstrength;
    private int Runningappscount;
    private float Brightness;
    private boolean Bluetooth;
    private int Bondeddevices;
    private boolean Wifi;
    private String Imei;
    private String Phonemodel;
    private String Networktype;
    
    public int getBatterylevel() {
        return Batterylevel;
    }
    public void setBatterylevel(int batterylevel) {
        Batterylevel = batterylevel;
    }
    public int getSignalstrength() {
        return Signalstrength;
    }
    public void setSignalstrength(int signalstrength) {
        Signalstrength = signalstrength;
    }
    public int getRunningappscount() {
        return Runningappscount;
    }
    public void setRunningappscount(int runningappscount) {
        Runningappscount = runningappscount;
    }
    public float getBrightness() {
        return Brightness;
    }
    public void setBrightness(float f) {
        Brightness = f;
    }
    public boolean isBluetooth() {
        return Bluetooth;
    }
    public void setBluetooth(boolean bluetooth) {
        Bluetooth = bluetooth;
    }
    public int getBondeddevices() {
        return Bondeddevices;
    }
    public void setBondeddevices(int bondeddevices) {
        Bondeddevices = bondeddevices;
    }
    public boolean isWifi() {
        return Wifi;
    }
    public void setWifi(boolean wifi) {
        Wifi = wifi;
    }
    public String getImei() {
        return Imei;
    }
    public void setImei(String imei) {
        Imei = imei;
    }
    public String getPhonemodel() {
        return Phonemodel;
    }
    public void setPhonemodel(String phonemodel) {
        Phonemodel = phonemodel;
    }
    public String getNetworktype() {
        return Networktype;
    }
    public void setNetworktype(String networktype) {
        Networktype = networktype;
    }
}
