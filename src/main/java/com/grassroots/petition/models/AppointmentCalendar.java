package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.utils.JsonParser;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Stores data about free and busy time intervals.
 * workday field stores data about free time intervals divided by days of week.
 * events list field stores data about events/busy time intervals.
 * Instances of this class are deserialized from JSON, which is loaded from backend.
 *
 * TODO: Consider using Calendar instead of Date. Date is deprecated, Calendar is not.
 */
public class AppointmentCalendar implements Json {
    /**
     * Key-name {@value} for storing AppointmentCalendar object as key-value.
     */
    public static final String CALENDAR_KEY = "CALENDAR_KEY";

    private static final String TAG = BasePetitionActivity.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Contain workdays each of workday is a list of FreeTimeInterval
     */
    @SerializedName("Workday")
    private WorkDay workday;

    /**
     * Contain a list of events EventTimeIntervals
     */
    @SerializedName("Events")
    private List<EventTimeInterval> events;

    /**
     * The date of the last update of appointmentCalendar
     */
    private Date lastUpdateDate;

    /**
     * Constructor to initialize an AppointmentCalendar object with the WorkDay object and a list of EventTimeIntervals (events objects).
     * @param workDay - a WorkDay object to initialize newly creating AppointmentCalendar (optional);
     * @param events - a list of EventTimeIntervals (events objects) which is used to initialize newly creating AppointmentCalendar (optional).
     */
    public AppointmentCalendar( WorkDay workDay, List<EventTimeInterval> events )
    {
        this.lastUpdateDate = new Date();
        this.workday = workDay;
        this.events = events;
    }

    /**
     * Returns an list of free time intervals for the specified date from the WorkDay field.
     * @param date - date which is used to get time intervals with the same day of week from the workday field list.
     * @return list of free time intervals for the day of week from the specified date.
     */
    public List<FreeTimeInterval> getFreeIntervalsForDate(Date date)
    {
        if (null == workday) {
            workday = new WorkDay();
        }

        return workday.getFreeIntervalsForDate(date);
    }

    /**
     * Returns an events list for the specified date from the events field list.
     * @param date - date which is used to get events with the same dates from the events field list.
     * @return list of events for the day of week from the specified date.
     */
    public List<EventTimeInterval> getEventsForDate(Date date)
    {
        if (null == events) {
            events = new ArrayList<EventTimeInterval>();
        }

        if (date == null) return events;

        List<EventTimeInterval> list = new ArrayList<EventTimeInterval>();
        for (EventTimeInterval event : events)
        {
            Date eventDate = event.getStartTime();
            // TODO: check this dates comparison (potential issues)
            if (eventDate.getYear() == date.getYear() &&
                eventDate.getMonth() == date.getMonth() &&
                eventDate.getDate() == date.getDate())
            {
                list.add(event);
            }
        }

        Collections.sort(list, new BaseTimeInterval.TimeIntervalsComparator());

        return list;
    }

    /**
     * Removes an event from the events field list. Checks if the specified event is already in the events field list.
     * @param event - an event to remove from the events field list.
     * @return true - if event was successfully removed, false - otherwise.
     */
    public boolean removeEvent(EventTimeInterval event)
    {
        if (null != event && events.remove(event))
        {
            Collections.sort(events, new BaseTimeInterval.TimeIntervalsComparator());
            return true;
        }

        return false;
    }

    /**
     * Adds an event to the events field list. Checks if the specified event is already in the events field list.
     * @param event - an event to add to the events field list.
     * @return true - if event was successfully added, false - otherwise.
     */
    public boolean addNewEvent(EventTimeInterval event)
    {
        if (null == event) {
            return false;
        }

        // TODO: implement checking if the event which is added is already in the events list
        events.add(event);
        Collections.sort(events, new BaseTimeInterval.TimeIntervalsComparator());

        return true;
    }

    /**
     * Update events field (only adds new events from the specified EventTimeInterval list)
     * @param events - a list of events which are used to update event field.
     */
    public void updateEvents(List<EventTimeInterval> events) {
        if (events == null || events.isEmpty()) return;

        if (this.events == null || this.events.isEmpty()) {
            this.events = events;
            return;
        }

        // Add Only new events
        for (EventTimeInterval i : events) {
            boolean isExists = false;
            for (EventTimeInterval j : this.events) {
                if (i.equals(j)) {
                    isExists = true;
                    break;
                }
            }

            if (!isExists) this.events.add(i);
        }
    }


// ******************************************************************************************
// ********************************* Time Intervals Helpers *********************************
// ******************************************************************************************

    /**
     * Provides an array of free time intervals for the specified time interval (if any).
     * Free time intervals are getting from Worday field by specified day of week, which is taken from specified date.
     * Unavailable time intervals are getting from events field by specified date.
     * @param selectedTimeInterval - time interval which should contain actual date for stat and end time.
     * @return an array of free time intervals, sorted by start time, if for the day of week from the specified date exist at least one free time interval.
     */
    public FreeTimeInterval getAvailableFreeIntervalForTimeInterval(BaseTimeInterval selectedTimeInterval)
    {
        Date selectedStartTime = selectedTimeInterval.getStartTime();
        Date selectedEndTime = selectedTimeInterval.getEndTime();

        List<FreeTimeInterval> freeIntervalsList = getFreeTimeIntervalsForDate(selectedStartTime);

        for (FreeTimeInterval freeTimeInterval : freeIntervalsList)
        {
            Date freeStartTime = freeTimeInterval.getStartTime();
            Date freeEndTime = freeTimeInterval.getEndTime();

            // #1 check if selectedStartTime is inside free time interval
            if ( (freeStartTime.compareTo(selectedStartTime) <= 0) &&
                 (selectedStartTime.compareTo(freeEndTime) < 0) )
            {
                return new FreeTimeInterval(freeStartTime, freeEndTime);
            }

            // #2 check if selectedEndTime is inside free time interval
            if ( (freeStartTime.compareTo(selectedEndTime) < 0) &&
                 (selectedEndTime.compareTo(freeEndTime) <= 0) )
            {
                return new FreeTimeInterval(freeStartTime, freeEndTime);
            }

            // #2 check if free time interval is inside selected time interval
            if ( (selectedStartTime.compareTo(freeStartTime) <= 0) &&
                 (freeEndTime.compareTo(selectedEndTime) <= 0) )
            {
                return new FreeTimeInterval(freeStartTime, freeEndTime);
            }
        }

        return null;
    }

    /**
     * Check if specified time interval is inside free time intervals (one of the time intervals in Events field)
     * @param selectedTimeInterval - time interval to check.
     * @return true - if specified time interval is inside of busy time intervals, false - otherwise.
     */
    public boolean isTimeIntervalInsideFreeIntervals(BaseTimeInterval selectedTimeInterval)
    {
        Date selectedStartTime = selectedTimeInterval.getStartTime();
        Date selectedEndTime = selectedTimeInterval.getEndTime();

        List<FreeTimeInterval> freeIntervalsList = getFreeTimeIntervalsForDate(selectedStartTime);

        for (FreeTimeInterval freeTimeInterval : freeIntervalsList)
        {
            Date freeStartTime = freeTimeInterval.getStartTime();
            Date freeEndTime = freeTimeInterval.getEndTime();

            // check if selectedTimeInterval is inside free time interval
            if ( (freeStartTime.compareTo(selectedStartTime) <= 0) &&
                 (selectedEndTime.compareTo(freeEndTime) <= 0) )
            {
                return true;
            }
        }


        return false;
    }

    /**
     * Check if specified time interval is inside busy time interval (one of the time intervals from Events field)
     * and/or is not inside free time interval (one of the time intervals which is in WorkDay field).
     *
     * @param appointment - time interval to check.
     * @return true - if specified time interval is inside of busy time intervals, false - otherwise.
     */
    public boolean appointmentConflictsWithBusy(BaseTimeInterval appointment)
    {
        // Loop through busy intervals for the day, check proposed appointment for conflicts with busy times.
        for (BaseTimeInterval busyTime : getBusyTimeIntervalsForDate(appointment.getStartTime()))
        {
            // Good: Non-intersecting appointment
            if (appointmentBeforeBusy(appointment, busyTime)) {
                continue;
            }
            // Good: Non-intersecting appointment after the busy time
            else if (appointmentAfterBusy(appointment, busyTime)) {
                continue;
            }
            // Conflicting: Intersects
            else if (startsBeforeEndsDuringBusy(appointment, busyTime))
            {
               return true;
            }
            // Conflicting: Intersects
            else if (appointmentDuringBusy(appointment, busyTime))
            {
                return true;
            }
            // Conflicting: Intersects
            else if (startsDuringEndsAfterBusy(appointment, busyTime))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks for a non-intersecting appointment before the busy time.
     *
     * @param appointment - time interval to check
     * @param busyTime - busy interval to compare against
     *
     * @return true - if specified time interval is inside of busy time interval, false - otherwise.
     */

    private boolean appointmentBeforeBusy(BaseTimeInterval appointment, BaseTimeInterval busyTime) {
        return appointment.getStartTime().compareTo(busyTime.getStartTime()) < 0 &&
               appointment.getEndTime().compareTo(busyTime.getStartTime())   < 0;
    }

    /**
     * Checks for a non-intersecting appointment after the busy time.
     *
     * @param appointment - time interval to check
     * @param busyTime - busy interval to compare against
     *
     * @return true - if specified time interval is inside of busy time interval, false - otherwise.
     */
    private boolean appointmentAfterBusy(BaseTimeInterval appointment, BaseTimeInterval busyTime) {
        return appointment.getStartTime().compareTo(busyTime.getEndTime()) > 0 &&
               appointment.getEndTime().compareTo(busyTime.getEndTime())   > 0;
    }

    /**
     * Checks for a conflicting appointment that is entirely contained within the busy time.
     *
     * @param appointment - time interval to check
     * @param busyTime - busy interval to compare against
     *
     * @return true - if specified time interval is inside of busy time interval, false - otherwise.
     */
    private boolean appointmentDuringBusy(BaseTimeInterval appointment, BaseTimeInterval busyTime) {
        return appointment.getStartTime().compareTo(busyTime.getStartTime()) >= 0 &&
               appointment.getEndTime().compareTo(busyTime.getEndTime())     <= 0;
    }

    /**
     * Checks for a conflicting appointment that starts before the busy time and ends during the busy time.
     *
     * @param appointment - time interval to check
     * @param busyTime - busy interval to compare against
     *
     * @return true - if specified time interval is inside of busy time interval, false - otherwise.
     */
    private boolean startsBeforeEndsDuringBusy(BaseTimeInterval appointment, BaseTimeInterval busyTime) {
        return appointment.getStartTime().compareTo(busyTime.getStartTime()) < 0 &&
               appointment.getEndTime().compareTo(busyTime.getStartTime())   > 0;
    }

    /**
     * Checks for a non-intersecting appointment after the busy time.
     *
     * @param appointment - time interval to check
     * @param busyTime - busy interval to compare against
     *
     * @return true - if specified time interval is inside of busy time interval, false - otherwise.
     */
    private boolean startsDuringEndsAfterBusy(BaseTimeInterval appointment, BaseTimeInterval busyTime) {
        return appointment.getStartTime().compareTo(busyTime.getEndTime()) < 0 &&
               appointment.getEndTime().compareTo(busyTime.getEndTime())  >= 0;
    }

    /**
     * Provides a list of busy (unavailable) time interval for specified date sorted ascending by stat time.
     * @param date - date for which generate time intervals.
     * @return an array of FreeTimeInterval, sorted by start time.
     */
    public List<FreeTimeInterval> getFreeTimeIntervalsForDate(Date date)
    {
        List<FreeTimeInterval> tmpFreeIntervalsList = new ArrayList<FreeTimeInterval>();

        List<BaseTimeInterval> timeIntervalsList = getAllTimeIntervalsForDate(date);

        for (BaseTimeInterval timeInterval : timeIntervalsList)
        {
            if (timeInterval instanceof FreeTimeInterval)
            {
                tmpFreeIntervalsList.add((FreeTimeInterval)timeInterval);
            }
        }

        return tmpFreeIntervalsList;
    }

    /**
     * Provides a list of busy (unavailable) time interval for specified date sorted ascending by stat time.
     * @param date - date for which generate time intervals.
     * @return an array of FreeTimeInterval, sorted by start time.
     */
    public List<BaseTimeInterval> getBusyTimeIntervalsForDate(Date date)
    {
        List<BaseTimeInterval> tmpBusyIntervalsList = new ArrayList<BaseTimeInterval>();

        List<BaseTimeInterval> timeIntervalsList = getAllTimeIntervalsForDate(date);

        for (BaseTimeInterval timeInterval : timeIntervalsList)
        {
            if ( (timeInterval instanceof UnusableTimeInterval) ||
                 (timeInterval instanceof EventTimeInterval) )
            {
                tmpBusyIntervalsList.add(timeInterval);
            }
        }

        return tmpBusyIntervalsList;
    }

    /**
     * Generate an array of time intervals of different types, if there is free time intervals available, sorted by start time, for the specified date.
     * Available time intervals are:
     * FreeTimeInterval - for free time intervals;
     * EventTimeInterval - for unavailable time intervals;
     * Available time intervals are getting from worday field by specified day of week, which is taken from specified date.
     * Unavailable time intervals are getting from events field by specified date.
     * @param date - date for which generate time intervals.
     * @return an array of time intervals of different types, sorted by start time, for the specified date.
     */
    public List<BaseTimeInterval> getAllTimeIntervalsForDate(Date date)
    {
        List<BaseTimeInterval> tmpTimeIntervalsList = new ArrayList<BaseTimeInterval>();

        // get Appointments intervals (NOTE: assume sorted by start time)
        List<EventTimeInterval> eventsTimeIntervalsList = getEventsForDate(date);

        // get available time intervals from availableIntervalsList and divide them by existing events (NOTE: assume sorted by start time)
        List<FreeTimeInterval> freeTimeIntervalsList = getFreeIntervalsForDate(date);
        if (null == freeTimeIntervalsList) {
            LOGGER.warn("Appointments Calendar contains empty free time intervals list for date = " + date);
            return tmpTimeIntervalsList;
        }

        Date prevFreeIntervalEndTime = null;
        for (FreeTimeInterval freeTimeInterval : freeTimeIntervalsList)
        {
            // get current free interval start and end time
            Date freeIntervalStartTime = freeTimeInterval.getStartTime();
            Date freeIntervalEndTime = freeTimeInterval.getEndTime();

            // validate start & end times of current free time interval
            if (freeIntervalStartTime.compareTo(freeIntervalEndTime) >= 0) // freeIntervalStartTime >= freeIntervalEndTime
            {
                LOGGER.error("Free interval start time >= end time."); // incorrect time interval
                continue; // ignore free interval
            }


            if (null != prevFreeIntervalEndTime)
            {
                // check previous free interval doesn't overlap with current free interval
                if (prevFreeIntervalEndTime.compareTo(freeIntervalStartTime) > 0) // prevFreeIntervalEndTime > freeIntervalStartTime
                {
                    LOGGER.error("Current free interval start time > previous free interval end time."); // intervals are overlapping
                    continue; // ignore free interval
                }

                // if previous free interval end time < current free interval start time, then add unusable time interval (block)
                if (prevFreeIntervalEndTime.compareTo(freeIntervalStartTime) < 0)
                {
                    addTimeIntervalToList(new EventTimeInterval(prevFreeIntervalEndTime, freeIntervalStartTime), tmpTimeIntervalsList);
                }
            }

            for (EventTimeInterval eventTimeInterval : eventsTimeIntervalsList)
            {
                Date eventStartTime = eventTimeInterval.getStartTime();
                Date eventEndTime = eventTimeInterval.getEndTime();

                // validate time intervals

                // validate event begin & end time
                if (eventStartTime.compareTo(eventEndTime) >= 0) // eventStartTime >= eventEndTime
                {
                    // swap start & end times
                    eventTimeInterval.setStartTime(eventEndTime);
                    eventTimeInterval.setEndTime(eventStartTime);
                    //continue; // ignore event
                }
                if (eventEndTime.compareTo(freeIntervalStartTime) <= 0) // eventEndTime <= freeIntervalStartTime
                {
                    continue; // ignore event
                }
                if (eventStartTime.compareTo(freeIntervalEndTime) >= 0) // eventStartTime >= freeIntervalEndTime
                {
                    continue; // ignore event
                }

                // special checks & corrections
                if (eventStartTime.compareTo(freeIntervalStartTime) < 0) // eventStartTime < freeIntervalStartTime
                {
                    eventStartTime = freeIntervalStartTime;
                }
                if (eventEndTime.compareTo(freeIntervalEndTime) > 0) // eventEndTime > freeIntervalEndTime
                {
                    eventEndTime = freeIntervalEndTime;
                }


                // divide free time by events
                if (freeIntervalStartTime.compareTo(eventStartTime) < 0) // freeIntervalStartTime < eventStartTime
                {
                    // create new free time interval [free interval start time; event start time]
                    addTimeIntervalToList(new FreeTimeInterval(null, freeIntervalStartTime, eventStartTime), tmpTimeIntervalsList);
                }

                // add current event
                addTimeIntervalToList(eventTimeInterval, tmpTimeIntervalsList);

                // cut off event's time length from head of remain free time interval
                freeIntervalStartTime = eventEndTime;
            }


            // add tail of remain free time interval, if any
            if (freeIntervalStartTime.compareTo(freeIntervalEndTime) < 0) {
                addTimeIntervalToList(new FreeTimeInterval(null, freeIntervalStartTime, freeIntervalEndTime), tmpTimeIntervalsList);
            }

            // save end time of previous free interval
            prevFreeIntervalEndTime = freeIntervalEndTime;
        }

        return tmpTimeIntervalsList;
    }

    /**
     * A helper method to add a specified BaseTimeInterval to a specified list.
     * If a previous time interval in the list is the same type as the time interval which is added and
     * if a previous time interval overlaps with the time interval which is added,
     * then merge both intervals in single interval and replace previous time interval in the list with the merged one.
     */
    protected void addTimeIntervalToList(BaseTimeInterval newTimeInterval, List<BaseTimeInterval> timeIntervalsList)
    {
        if (timeIntervalsList.size() > 0)
        {
            // get previous time interval if any
            int index = timeIntervalsList.size() - 1;
            BaseTimeInterval prevTimeInterval = timeIntervalsList.get(index);
            if (newTimeInterval instanceof EventTimeInterval &&
                prevTimeInterval instanceof EventTimeInterval)
            {
                if (prevTimeInterval.getEndTime().compareTo(newTimeInterval.getStartTime()) >= 0)
                {
                    // merge previous & current time interval and add to the list
                    timeIntervalsList.set(index, new EventTimeInterval(prevTimeInterval.getStartTime(), newTimeInterval.getEndTime()));
                    return;
                }
            } else
            if (newTimeInterval instanceof FreeTimeInterval &&
                prevTimeInterval instanceof FreeTimeInterval)
            {
                if (prevTimeInterval.getEndTime().compareTo(newTimeInterval.getStartTime()) >= 0)
                {
                    // merge previous & current time interval and add to the list
                    timeIntervalsList.set(index, new FreeTimeInterval(prevTimeInterval.getStartTime(), newTimeInterval.getEndTime()));
                    return;
                }
            }
        }

        timeIntervalsList.add(newTimeInterval);
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }


    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppointmentCalendar that = (AppointmentCalendar) o;

        if (workday != null ? !workday.equals(that.workday) : that.workday != null) return false;
        if (events != null ? !events.equals(that.events) : that.events != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = workday != null ? workday.hashCode() : 0;
        result = 31 * result + (events != null ? events.hashCode() : 0);
        return result;
    }
}
