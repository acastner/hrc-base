package com.grassroots.petition.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;
import com.grassroots.petition.utils.JsonParser;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 * Product data model.
 * Implements Json and Parcelable interface
 */
public class Product implements Json, Parcelable {
    public static final Parcelable.Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel parcel) {
            return new Product(parcel);
        }

        @Override
        public Product[] newArray(int i) {
            return new Product[i];
        }
    };

    /**
     * Key-name {@value} for storing Product objects as key-value.
     */
    public static final String PRODUCT_KEY = "product";

    /**
     * SKU number to unique identify product
     */
    private String SKU;
    /**
     * Name of product
     */
    private String name;
    /**
     * Description of product
     */
    private String description;
    /**
     * String with cost of product (can be used any currency format)
     */
    private String cost;
    /**
     * Recurring value
     */
    private String recurring;

    /**
     * Constructor to build a product
     * @param SKU - product unique identifier
     * @param name - Name of product
     * @param description - Description of product
     * @param cost - String with cost of product (can be used any currency format)
     * @param recurring - Recurring value
     */
    public Product(String SKU, String name, String description, String cost, String recurring) {
        this.SKU = SKU;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.recurring = recurring;
    }

    public Product() {

    }

    public String getSKU() {
        return SKU;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCost() {
        return cost;
    }

    public String getRecurring() {
        return recurring;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    public Pair<String, Parcelable> asParcelablePair() {
        return Pair.<String, Parcelable>create(PRODUCT_KEY, this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (SKU != null ? !SKU.equals(product.SKU) : product.SKU != null) return false;
        if (cost != null ? !cost.equals(product.cost) : product.cost != null) return false;
        if (description != null ? !description.equals(product.description) : product.description != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (recurring != null ? !recurring.equals(product.recurring) : product.recurring != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = SKU != null ? SKU.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (recurring != null ? recurring.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(cost);
        parcel.writeString(description);
        parcel.writeString(name);
        parcel.writeString(recurring);
        parcel.writeString(SKU);
    }

    private Product(Parcel input) {
        cost = input.readString();
        description = input.readString();
        name = input.readString();
        recurring = input.readString();
        SKU = input.readString();
    }
}
