package com.grassroots.petition.models;

import android.location.Location;
import android.location.LocationManager;
import com.grassroots.petition.utils.Dates;
import com.grassroots.petition.utils.JsonParser;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 * GPSLocation data model
 * Stores location coordinates, type of method to get location, time
 */
public class GpsLocation implements Json {
    /**
     * Key-name {@value} which is used to define type of method: GPS.
     */
    public static final String GPS = "GPS";
    /**
     * Key-name {@value} which is used to define type of method: Network.
     */
    public static final String NETWORK = "Cell";
    /**
     * Key-name {@value} which is used to define type of method: Fake.
     */
    public static final String FAKE = "FAKE";

    /**
     * Fake GPS location, use this if you cannot get new cords from GPS or Network providers
     */
    public static final GpsLocation FAKE_GPS_LOCATION = new GpsLocation(0, 0, -1, GpsLocation.FAKE);

    /**
     * Latitude of location's coordinates
     */
    private double latitude;
    /**
     * Longitude of location's coordinates
     */
    private double longitude;
    /**
     * Type of method current location was received
     */
    private String type;
    /**
     * Precision of location's coordinates
     */
    private int uncertainty;
    /**
     * Time when location was received (has "yyyy-MM-dd'T'HH:mm:ss.SZ" format) (optional)
     */
    private String time;

    public GpsLocation() {
    }

    /**
     * Instantiates GpsLocation by location coordinates and uncertainty
     */
    public GpsLocation(double latitude, double longitude, int uncertainty) {
        this(latitude, longitude, uncertainty, "GPS");
    }

    /**
     * Instantiates GpsLocation by location coordinates, uncertainty and method type to get location
     */
    public GpsLocation(double latitude, double longitude, int uncertainty, String type) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.uncertainty = uncertainty;
        time = Dates.getTimeStamp();
    }


    /**
     * Instantiates GpsLocation from Location
     */
    public GpsLocation(Location location) {
        this(FAKE_GPS_LOCATION);
        if (location != null) {
            this.setLatitude(location.getLatitude());
            this.setLongitude(location.getLongitude());
            this.setUncertainty((int) location.getAccuracy());
            String provider = location.getProvider();
            if (provider != null && provider.length() > 0) {
                if (LocationManager.NETWORK_PROVIDER.equals(provider)) {
                    this.setType(GpsLocation.NETWORK);
                } else if (LocationManager.GPS_PROVIDER.equals(provider)) {
                    this.setType(GpsLocation.GPS);
                }
            }
        }
    }


    /**
     * Instantiates GpsLocation from Location, sets custom Provider string
     */
    public GpsLocation(Location location, String provider) {
        this(location);
        if (provider != null && provider.length() > 0) {
            this.setType(provider);
        }
    }

    protected GpsLocation(GpsLocation location) {
        this(location.getLatitude(), location.getLongitude(), location.getUncertainty(), location.getType());
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUncertainty() {
        return uncertainty;
    }

    public void setUncertainty(int uncertainty) {
        this.uncertainty = uncertainty;
    }

    public String getTime() {
        return time;
    }
    
    public void setTime(String time){
        this.time = time;
    }

    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GpsLocation that = (GpsLocation) o;

        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (uncertainty != that.uncertainty) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = latitude != +0.0d ? Double.doubleToLongBits(latitude) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        temp = longitude != +0.0d ? Double.doubleToLongBits(longitude) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + uncertainty;
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }
}
