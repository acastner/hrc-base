package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

/**
 * An individual knock
 */
public class KnockEvent extends GpsLocation implements Json {
    /**
     * unique identifier for person (HID)
     */
    private int hid;
    /**
     * Type of knock status
     */
    private KnockStatus knockStatus;
    
    /**
     * Subject info will be sent as knock event if petition bidirectional is true.
     */
    
    /**
     * first name
     */
    @SerializedName("FirstName")
    private String firstName;
    /**
     * last name
     */
    @SerializedName("LastName")
    private String lastName;
    /**
     * Subject Address Line 1
     */
    @SerializedName("Address")
    private String addressLine1;
    /**
     * Subject Address Line 2
     */
    @SerializedName("Apartment")
    private String addressLine2;
    /**
     * City
     */
    @SerializedName("City")
    private String city;
    /**
     * State
     */
    @SerializedName("State")
    private String state;
    /**
     * Zip
     */
    @SerializedName("ZIP")
    private String zip;
    /**
     * subject's default phone
     */
    @SerializedName("Phone")
    private String phone;
    /**
     * subject's email
     */
    @SerializedName("Email")
    private String email;
    
    @SerializedName("Location")
    private String subjectLocation;
    private int Batterylevel;
    private int Signalstrength;
    private int Runningappscount;
    private float Brightness;
    private boolean Bluetooth;
    private int Bondeddevices;
    private boolean Wifi;
    private String Imei;
    private String Phonemodel;
    private String Networktype;
    

    
    public KnockEvent() {
    }

    /**
     * Instantiates Knock event object
     * @param hid - unique identifier for person (HID)
     * @param knockStatus - knock status
     * @param location - location of received knock status
     */
    public KnockEvent(int hid, KnockStatus knockStatus, GpsLocation location) {
        super(location);
        this.hid = hid;
        this.knockStatus = knockStatus;
    }
    
    public KnockEvent(KnockStatus knockStatus, GpsLocation location, String firstName, String lastName, String addressLine1,
            String addressLine2, String city, String state, String zip, String phone, String email,String subjectLocation) {
        super(location);
        this.knockStatus = knockStatus;
        this.firstName = firstName;
        this.lastName = lastName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phone = phone;
        this.email = email;
        this.subjectLocation = subjectLocation;
    }


    public int getHid() {
        return hid;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public KnockStatus getKnockStatus() {
        return knockStatus;
    }

    public void setKnockStatus(KnockStatus knockStatus) {
        this.knockStatus = knockStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        KnockEvent that = (KnockEvent) o;

        if (hid != that.hid) return false;
        if (knockStatus != that.knockStatus) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + hid;
        result = 31 * result + (knockStatus != null ? knockStatus.hashCode() : 0);
        return result;
    }
    

    public String getSubjectLocation() {
        return subjectLocation;
    }

    public void setSubjectLocation(String subjectLocation) {
        this.subjectLocation = subjectLocation;
    }

    public void addEventInfo(EventInfo eventInfo) {
        setBatterylevel(eventInfo.getBatterylevel());
        setSignalstrength(eventInfo.getSignalstrength());
        setRunningappscount(eventInfo.getRunningappscount());
        setBluetooth(eventInfo.isBluetooth());
        setBondeddevices(eventInfo.getBondeddevices());
        setBrightness(eventInfo.getBrightness());
        setImei(eventInfo.getImei());
        setPhonemodel(eventInfo.getPhonemodel());
        setWifi(eventInfo.isWifi());
        setNetworktype(eventInfo.getNetworktype());
    }

    public int getBatterylevel() {
        return Batterylevel;
    }

    public void setBatterylevel(int batterylevel) {
        Batterylevel = batterylevel;
    }

    public int getSignalstrength() {
        return Signalstrength;
    }

    public void setSignalstrength(int signalstrength) {
        Signalstrength = signalstrength;
    }

    public int getRunningappscount() {
        return Runningappscount;
    }

    public void setRunningappscount(int runningappscount) {
        Runningappscount = runningappscount;
    }

    public float getBrightness() {
        return Brightness;
    }

    public void setBrightness(float brightness) {
        Brightness = brightness;
    }

    public boolean isBluetooth() {
        return Bluetooth;
    }

    public void setBluetooth(boolean bluetooth) {
        Bluetooth = bluetooth;
    }

    public int getBondeddevices() {
        return Bondeddevices;
    }

    public void setBondeddevices(int bondeddevices) {
        Bondeddevices = bondeddevices;
    }

    public boolean isWifi() {
        return Wifi;
    }

    public void setWifi(boolean wifi) {
        Wifi = wifi;
    }

    public String getImei() {
        return Imei;
    }

    public void setImei(String imei) {
        Imei = imei;
    }

    public String getPhonemodel() {
        return Phonemodel;
    }

    public void setPhonemodel(String phonemodel) {
        Phonemodel = phonemodel;
    }

    public String getNetworktype() {
        return Networktype;
    }

    public void setNetworktype(String networktype) {
        Networktype = networktype;
    }
    
    
}
