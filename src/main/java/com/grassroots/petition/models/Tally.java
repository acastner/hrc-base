package com.grassroots.petition.models;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

public class Tally implements Json{
    /**
     * Key-name {@value} for storing LocationList object as key-value.
     */
    public static final String TALLY_KEY = "tally";
   
    @SerializedName("PetitionID")
    private String PetitionID;
    
    @SerializedName("UserID")
    private String UserID;
    
    @SerializedName("ListName")
    private String ListName;
    
    @SerializedName("Knocks")
    private int Knocks;
    
    @SerializedName("Attempts")
    private int Attempts;
    
    @SerializedName("NotHome")
    private int NotHome;
    
    @SerializedName("ComeBack")
    private int ComeBack;
    
    @SerializedName("Refused")
    private int Refused;
    
    @SerializedName("Contacts")
    private int Contacts;
    
    @SerializedName("DonationsCount")
    private int DonationsCount;
    
    @SerializedName("Donations")
    private float Donations;

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

	public String getPetitionID() {
		return PetitionID;
	}

	public void setPetitionID(String petitionID) {
		PetitionID = petitionID;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getListName() {
		return ListName;
	}

	public void setListName(String listName) {
		ListName = listName;
	}

	public int getKnocks() {
		return Knocks;
	}

	public void setKnocks(int knocks) {
		Knocks = knocks;
	}

	public int getAttempts() {
		return Attempts;
	}

	public void setAttempts(int attempts) {
		Attempts = attempts;
	}

	public int getNotHome() {
		return NotHome;
	}

	public void setNotHome(int notHome) {
		NotHome = notHome;
	}

	public int getComeBack() {
		return ComeBack;
	}

	public void setComeBack(int comeBack) {
		ComeBack = comeBack;
	}

	public int getRefused() {
		return Refused;
	}

	public void setRefused(int refused) {
		Refused = refused;
	}

	public int getContacts() {
		return Contacts;
	}

	public void setContacts(int contacts) {
		Contacts = contacts;
	}

	public int getDonationsCount() {
		return DonationsCount;
	}

	public void setDonationsCount(int donationsCount) {
		DonationsCount = donationsCount;
	}

	public float getDonations() {
		return Donations;
	}

	public void setDonations(float donations) {
		Donations = donations;
	}
    
    
    

}
