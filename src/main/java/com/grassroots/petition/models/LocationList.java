package com.grassroots.petition.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.grassroots.petition.utils.JsonParser;

public class LocationList implements Json {

    /**
     * Key-name {@value} for storing LocationList object as key-value.
     */
    public static final String LOCATIONS_KEY = "locations";
    
    @SerializedName("locations")
    private List<Location> locations;

    @Override
    public String toJson() {
        return JsonParser.getParser().jsonify(this);
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
