package com.grassroots.petition.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.BaseTimeInterval;
import com.grassroots.petition.models.EventTimeInterval;
import com.grassroots.petition.models.FreeTimeInterval;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 02.07.13
 * Time: 21:02
 * To change this template use File | Settings | File Templates.
 */
public class TimeIntervalsAdapter extends ArrayAdapter<BaseTimeInterval>
{
    public static final int HEIGHT_RATION_PIXELS_PER_MINUTE = 2; // TODO: replace with suitable/required

    private int heightRatio = HEIGHT_RATION_PIXELS_PER_MINUTE;

    public void setHeightRatio(int heightRatio)
    {
        this.heightRatio = heightRatio;
        notifyDataSetChanged();
    }

    public TimeIntervalsAdapter(Context context, int textViewResourceId)
    {
        super(context, textViewResourceId);
    }

    private List<BaseTimeInterval> items;
    SimpleDateFormat timeFormatter;

    public TimeIntervalsAdapter(Context context, int resource, List<BaseTimeInterval> items)
    {
        super(context, resource, items);

        this.items = items;
        timeFormatter = new SimpleDateFormat("h:mm a");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final BaseTimeInterval timeInterval = items.get( position );
        final ViewHolder holder;

        if ( null == convertView ) // If starting from scratch
        {
            convertView = View.inflate( getContext(), R.layout.calendar_appointment_view, null );
            holder = ViewHolder.createViewHolder( convertView );
            convertView.setTag( holder );
        }
        else // Recycling
        {
            holder = (ViewHolder) convertView.getTag();
        }

        int height = (int)(timeInterval.getTimeIntervalInMinutes() * heightRatio);

        ViewGroup.LayoutParams params = convertView.getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, height);
        } else {
            params.height = height;
        }

        convertView.setMinimumHeight(height);

        // Set bg color for different time intervals
        Resources resources = getContext().getResources();
        if (timeInterval instanceof EventTimeInterval)
        {
            convertView.setBackgroundColor(resources.getColor(R.color.event_time_interval));
        }
        else if (timeInterval instanceof FreeTimeInterval)
        {
            convertView.setBackgroundColor(resources.getColor(R.color.free_time_interval));
        }
        else // UnusableTimeInterval
        {
            convertView.setBackgroundColor(resources.getColor(R.color.unusable_time_interval));
        }

        if (convertView.isSelected())
        {
            convertView.setBackgroundColor(resources.getColor(R.color.selected_time_interval));
        }

        // Set text for all fields
        setTitle( timeInterval.getTitle(), holder );
        setStart( timeInterval.getStartTime(), holder );
        setEnd( timeInterval.getEndTime(), holder );

        return convertView;
    }

    private void setTitle( String title, ViewHolder holder )
    {
        holder.title.setText( title );
    }

    private void setStart( Date startTime, ViewHolder holder )
    {
        holder.startTime.setText( "" + timeFormatter.format(startTime) );
    }

    private void setEnd( Date endTime, ViewHolder holder )
    {
        holder.endTime.setText( "" + timeFormatter.format(endTime) );
    }

    public static class ViewHolder
    {
        TextView title;
        TextView startTime;
        TextView endTime;

        public static ViewHolder createViewHolder( View view )
        {
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById( R.id.appointment_title );
            viewHolder.startTime = (TextView) view.findViewById( R.id.appointment_start_time );
            viewHolder.endTime = (TextView) view.findViewById( R.id.appointment_end_time );
            return viewHolder;
        }
    }
}
