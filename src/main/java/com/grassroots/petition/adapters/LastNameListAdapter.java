package com.grassroots.petition.adapters;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.utils.Performance;
import com.grassroots.petition.views.lists.SubjectAddressRowView;
/**
 * Adapter to show lastnames on left panel : For locaiton based list (Del version : 1.10.0)
 * @author sharika
 *
 */
public class LastNameListAdapter extends BaseAdapter {

	
	private Walklist walklist;
	private LayoutInflater inflater;
	private BaseWalklistActivity context;
	private ArrayList<String> lastNames;
	private List<Subject> lastNameAddresses;
	private KnockStatus knockStatusFilter;
	private String lastNameFilter;
	List<Subject> list = null;
	private String selectedLastName;
	private ArrayList<String> lastNamesAll;
	

	public LastNameListAdapter(Walklist walklist,LayoutInflater inflater,BaseWalklistActivity context,ArrayList<String> lastNames,
			String selectedLastName){
		this.walklist = walklist;
		this.inflater = inflater;
		this.context = context;
		this.lastNamesAll = lastNames;
		this.lastNames = lastNames;
		knockStatusFilter = null;
		list = walklist.getLastNamesFromWalklist(context, "");
		this.selectedLastName = selectedLastName;
		
		setAddresses(walklist.getLastNamesFromWalklist(context, lastNames.get(0)));		
	}
	@Override
	public int getCount() {
		if(lastNames == null || lastNames.size()==0 || lastNameAddresses==null || lastNameAddresses.size()==0){
		    return 0;
		}
		return lastNames.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lastNames.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		SubjectAddressRowView rowView;
		if (view instanceof SubjectAddressRowView) {
			rowView = (SubjectAddressRowView) view;
		} else {
			rowView = (SubjectAddressRowView) inflater.inflate(
					R.layout.subject_address_row, null);
		}
//		Subject address = lastNameAddresses.get(0);
//		rowView.setSubject(address);
		TextView addressText = (TextView) rowView.findViewById(R.id.address_line);
		addressText.setText(lastNames.get(arg0));
		return rowView;
	}
	
	private void setAddresses(List<Subject> addresses) {
		this.lastNameAddresses = new ArrayList<Subject>(addresses);
	}
	
	public List<Subject> setKnockStatusFilter(KnockStatus filter) {
		long startTime = System.currentTimeMillis();
		knockStatusFilter = filter;
		if (context.isLocationList) {
			if ((knockStatusFilter == null)
					&& (lastNameFilter == null || lastNameFilter.equals(""))) {
				if(selectedLastName == null){
					if(lastNames.size()>0){
						setAddresses(walklist.getLastNamesFromWalklist(context, lastNames.get(0)));
					}
				}
				else{
					setAddresses(walklist.getLastNamesFromWalklist(context, selectedLastName));
				}
				lastNames.clear();
				lastNames.addAll(walklist.getAllLastNames(context));
			} else {
				setAddresses(walklist.filterStreetAddress(
						knockStatusFilter, lastNameFilter));
				lastNames.clear();
				for(Subject subject : lastNameAddresses){
					if(subject.getLastName() != null && subject.getLastName().length()>0){
						if(!lastNames.contains(subject.getLastName())){
							lastNames.add(subject.getLastName());
						}
					}
				}

			}
			
		} 
		notifyDataSetChanged();
		Performance.end(startTime, "Setting knock status filter");
		return lastNameAddresses;
	}

	public List<Subject> setLastnameFilter(String filter) {
		long startTime = System.currentTimeMillis();
		lastNameFilter = filter;
		if (knockStatusFilter == null
				&& (lastNameFilter == null || lastNameFilter.equals(""))) {
			setAddresses(walklist.getLastNamesFromWalklist(context, lastNames.get(0)));
			lastNames.clear();
			lastNames.addAll(lastNamesAll);
		} else {
			setAddresses(walklist.filterStreetAddress(knockStatusFilter, lastNameFilter));
			lastNames.clear();
			lastNames.add(filter);
		}
		notifyDataSetChanged();

		Performance.end(startTime, "Setting last name filter");
		return lastNameAddresses;

	}
	
	public int getPositionOf(String lastname) {
		return lastNames.indexOf(lastname);
	}
	
	public void setSelectedLastName(String lastname) {
          this.selectedLastName = lastname;		
	}


}
