package com.grassroots.petition.adapters;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.ProgressDialog;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.LocationListActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.Location;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.ClassRetriever;

public class LocationListAdapter extends BaseAdapter{

    public LocationListActivity context;
    public List<Location> locationList;
    Pair<Walklist, Response> walklistResponse = null;
    private boolean walklistEnabled = false;
    private Location location = null;
    private int viewPosition = 0;

    public LocationListAdapter(LocationListActivity locationListActivity,
            List<Location> list,boolean walklistEnabled) {
        this.locationList = list;
        this.context = locationListActivity;
        this.walklistEnabled = walklistEnabled;
    }

    @Override
    public int getCount() {
        if (locationList != null && locationList.size() > 0) {
            if(walklistEnabled){
                return (locationList.size()+1);
            }else{
                return locationList.size();
            }
        } else {
        	if(walklistEnabled){
        		return 1;
        	}
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return locationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        viewPosition = position;
        if(walklistEnabled && position == 0){
            convertView = inflater
                    .inflate(R.layout.default_walklist_option_view, null);
        }else{
            if(walklistEnabled){
                viewPosition = position-1;
            }
            convertView = inflater
                    .inflate(R.layout.locationlist_row_view, null);
            location = locationList.get(viewPosition);
            TextView locationName = (TextView) convertView
                    .findViewById(R.id.location_name);
            TextView subjectsCount = (TextView) convertView
                    .findViewById(R.id.subject_count);
            TextView canvassedCount = (TextView) convertView
                    .findViewById(R.id.canvassed_count);
            TextView successCount = (TextView) convertView
                    .findViewById(R.id.success_count);
            TextView attemptedCount = (TextView) convertView
                    .findViewById(R.id.attempted_count);
            TextView unAtemptedCount = (TextView) convertView
                    .findViewById(R.id.unattempted_count);

            locationName.setText(location.getName());
            subjectsCount.setText("" + location.getSubjects());
            canvassedCount.setText("" + location.getCanvassed());
            successCount.setText("" + location.getSuccess());
            attemptedCount.setText("" + location.getAttempted());
            unAtemptedCount.setText("" + location.getUnattempted());
            
            convertView.setTag(viewPosition);
        }
       
       

        convertView.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(final View v) {

                final ProgressDialog pd = ProgressDialog.show(context, context.getString(R.string.pleasewaitmsg),context.getString(R.string.loadingmsg));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                    	
                    	if(walklistEnabled && position == 0){
                    		walklistResponse = GrassrootsRestClient.getClient().syncLoadWalklist();
                    		context.getGlobalData().setLocation(null);
                    	}else{
                    	    location = locationList.get((Integer) v.getTag());
                    		walklistResponse = GrassrootsRestClient.getClient().syncLoadWalklistForLocation(location.getName());
                    	}
                    	   if (walklistResponse.second != null) {
                               if (walklistResponse.second.getResponseErrors() != null)
                                  // errorMessage = parseErrorMessage(walklistResponse.second);
                               return; // this is gross... goes to finally
                           } else {
                               Walklist walklist = walklistResponse.first;
                               WalklistDataSource walklistDataSource = new WalklistDataSource(context);
                               walklistDataSource.clearWalklist();
                               if(walklist != null){
                               walklistDataSource.insertWalklist(walklist.getSubjects(),context.getGlobalData().getLocation());
                               context.setWalklist(walklist);
                               Date lastsyncedTime = new Date(Calendar.getInstance()
                                       .getTimeInMillis());
                               context.getGlobalData().setLastWalklistSyncTime(lastsyncedTime);
                               }
                              
                           }
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pd.dismiss();
                                context.startActivity(ClassRetriever.getWalklistActivity(context));
                             
                            }
                        });
                       
                    }
                }).start();

            }
        });
        return convertView;
    }

   
}
