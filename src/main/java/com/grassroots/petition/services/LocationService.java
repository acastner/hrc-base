package com.grassroots.petition.services;

import org.apache.log4j.Logger;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.grassroots.petition.R;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.LocationProcessor.LocationStatus;

public class LocationService extends Service {
	private static final String TAG = LocationService.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

	@Override
	public void onCreate() {
		super.onCreate();
		LOGGER.debug("LocationService, onCreate");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		registerReceiver(locationReceiver, new IntentFilter(LocationProcessor.LOCATION_INTENT));
        LocationProcessor.getInstance(getApplicationContext()).findAccurateLocation(
                LocationProcessor.LOCATION_INTENT);
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private BroadcastReceiver locationReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			LOGGER.debug("LocationService.locationReceiver, onReceive");

			if (LocationProcessor.LOCATION_INTENT.equals(intent.getAction())) {
				Location location = intent.getParcelableExtra(LocationProcessor.LOCATION_BODY);
				LocationStatus status = (LocationStatus) intent.getSerializableExtra(LocationProcessor.LOCATION_STATUS);

                if (LocationStatus.FOUND.equals(status) && location != null) {
                    GpsLocation gpsLocation = new GpsLocation(location);

                    try {
                        GrassrootsRestClient.getClient(context).saveGpsLocation(gpsLocation);
                    } catch (Exception e) {
                        String errorMessage = getResources().getString(R.string.unable_to_start_rps);
                        LOGGER.error(errorMessage);
                    }
                }
			}
			try {
				unregisterReceiver(this);
			} catch (IllegalArgumentException e) {
				Log.e(TAG, e.toString());
			}
		}
	};

}
