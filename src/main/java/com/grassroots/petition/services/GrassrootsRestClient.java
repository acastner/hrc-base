package com.grassroots.petition.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import com.grassroots.models.Request;
import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.exceptions.JsonParseException;
import com.grassroots.petition.models.AppointmentCalendar;
import com.grassroots.petition.models.EventInfo;
import com.grassroots.petition.models.GpsLocation;
import com.grassroots.petition.models.InstantPrescreenRequest;
import com.grassroots.petition.models.InstantPrescreenResponce;
import com.grassroots.petition.models.Json;
import com.grassroots.petition.models.Knocks;
import com.grassroots.petition.models.LocationList;
import com.grassroots.petition.models.NearestNeighborInfo;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Recipient;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.ResponseError;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Tally;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.utils.JsonParser;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.Tracker;
import com.grassroots.services.RequestPerformerService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * Handles all the communication with the Grassroots server.
 * Some calls route through here but end up being
 * passed off to the RequestPerformerService for execution to ensure they get sent to the server
 */
public class GrassrootsRestClient {

	public static final String TAG = GrassrootsRestClient.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	public static final String PETITION_ACTION = "petition";
	public static final String APPOINTMENT_WINDOWS_ENDPOINT = "calendar/freebusy";
	public static final String APPOINTMENT_WINDOWS_UPDATE_ENDPOINT = "calendar/freebusy";
	public String APPOINTMENT_WINDOWS_ENDPOINT_ID;
	public static final String APPOINTMENT_SINCE_DATE_KEY = "since";
	public static final String APPOINTMENT_UPDATE_SINCE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"; // ISO8601 date format

	public static final String INSTANT_PRESCREEN_REQUEST_URL = "transunion/prescreen";

	public static final String ZIP_KEY = "ZIP";
	private static final String GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json";
	public static final String WALKLIST_ENDPOINT = "walklist";
	public static final String WALKLIST_ENDPOINT_SINCE = "WALKLIST/GET";
	public static final String FETCH_LOCATION_LIST = "locations";
	public static final String TALLY_ENDPOINT = "petition/get-user-tally";
	public static final String NEARESTNEIGHBOR_ENDPOINT = "petition/get-nearest-neighbor";
	public static final String CURRENT_LATITUDE = "CURRENTLATITUDE";
	public static final String CURRENT_LONGITUDE = "CURRENTLONGITUDE";
	public static final String VALIDATE_EMAIL_ENDPOINT = "utils/verify-email";




	/**
	 * Single instance
	 */
	private static GrassrootsRestClient instance;
	private String baseURL = null;
	private String buildSetting = null;


	/**
	 * Create Authentication header, will be added to every request to the server to identify user
	 * @param username
	 * @param password
	 * @return
	 */
	public static Header createAuthenticationHeader(String username, String password) {
		String encoded = Base64.encodeToString((username + ":" + password).getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
		return new BasicHeader("Authorization", "Basic " + encoded);
	}

	/**
	 * Create Header with imei number, will be added to every request to the server to identify current device
	 * @param context - application context
	 * @return
	 */
	public static Header createImeiHeader(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		Log.e("IMEI ","IMEI HEADER:"+imei);
		if (BasePetitionActivity.isDebug)
			imei = "000000000000000";
		return new BasicHeader("imei", imei);
	}

	public interface Callback<RESPONSE_TYPE> {
		public void onCompletion(RESPONSE_TYPE response);

		public void onError(Response errors);
	}

	/**
	 * Convenience pass through implementation of AsyncHttpResponseHandler
	 */
	public static class CallOnFailureHandler extends AsyncHttpResponseHandler {
		private Callback callback;

		public CallOnFailureHandler(Callback<?> callback) {
			this.callback = callback;
		}

		public void onFailure(Throwable t, String msg) {
			LOGGER.error("Failed doing call: " + msg, t);
			callback.onError(new Response(new ResponseError("ResponseFailed", msg)));
		}
	}

	private static final Callback DO_NOTHING = new Callback() {
		@Override
		public void onCompletion(Object response) {

		}

		@Override
		public void onError(Response errors) {

		}
	};

	private static final Header JSON_HEADER = new BasicHeader("Content-Type", "application/json");
	private final AsyncHttpClient client = new AsyncHttpClient();

	public static final int PING_SYNC_CONNECTION_TIMEOUT_SECONDS = 10;
	public static final int PING_SYNC_SO_TIMEOUT = 5;

	public static final int STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS = 30;
	public static final int STANDARD_SYNC_SO_TIMEOUT = 15;

	private final JsonParser parser = new JsonParser();


	private String username;
	private String password;
	private Context applicationContext;
	private Header imeiHeader;

	/**
	 * Private Constructor
	 * @param applicationContext
	 */
	private GrassrootsRestClient(Context applicationContext) {
		this.applicationContext = applicationContext;
		this.buildSetting = applicationContext.getString(R.string.buildsetting);
		this.baseURL = ((GlobalData) GlobalData.getContext()).getBaseUrl();
		LOGGER.debug("Base URL: " + baseURL);
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public static final String REPORTLIST_ENDPOINT = "report/list";

	public ArrayList<String> syncLoadReportList() {
		HttpUriRequest getReportsRequest = new HttpGet(createUrl(REPORTLIST_ENDPOINT));
		addHeadersToRequest(getReportsRequest);
		ArrayList<String> reportlist = new ArrayList<String>();
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting report list GET");
			HttpResponse serverResponse = syncClient.execute(getReportsRequest);
			String responseBody = getResponseBody(serverResponse);

			Log.e("sk","sk report : "+responseBody);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("Report list json: " + responseBody);
				JSONObject obj = null;
				try {
					obj = new JSONObject(responseBody);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (obj != null) {
					Iterator<String> keys = obj.keys();
					while(keys.hasNext()){
						reportlist.add(keys.next());
					}
				}
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Locations response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}finally {
			syncClient.close();
		}
		return reportlist;
	}

	/**
	 * Public Constructor, returns singleinstance
	 * @param applicationContext
	 * @return
	 */
	public static synchronized GrassrootsRestClient initialize(Context applicationContext) {
		if (instance == null) {
			instance = new GrassrootsRestClient(applicationContext);
		}
		return instance;
	}

	public static synchronized GrassrootsRestClient getClient(Context context) {
		if (instance == null) {
			instance = new GrassrootsRestClient(context);
		}
		return instance;
	}

	/**
	 * This should be a safe way to get the client from any activity that extends BasePetitionActivity as
	 * there is a call to #initialize in it's onCreate method
	 * @return
	 */
	public static GrassrootsRestClient getClient() {
		return instance;
	}

	private AndroidHttpClient getSyncClient() {
		return AndroidHttpClient.newInstance("GrassrootsUnwired");
	}

	/**
	 * Retrieve IMEI header from current app context and add it to the headers
	 * @param applicationContext
	 */
	public void setImeiHeader(Context applicationContext) {
		imeiHeader = createImeiHeader(applicationContext);
		client.addHeader(imeiHeader.getName(), imeiHeader.getValue());
	}




	/**
	 * sets calendar id for freebusy call
	 */
	public void setAppointmentWindowsEndpointId(String id)
	{
		APPOINTMENT_WINDOWS_ENDPOINT_ID = "?CalendarID=" + id;
	}



	/**
	 * Async download walklist from server
	 * @param walklistCallback
	 */
	public void loadWalklist(final Callback<Walklist> walklistCallback) {
		client.get(createUrl(WALKLIST_ENDPOINT), new CallOnFailureHandler(walklistCallback) {
			public void onSuccess(String message) {
				if (Strings.isEmpty(message)) {
					walklistCallback.onCompletion(null);
				} else {
					Walklist walklist = parser.toWalklist(message);
					walklistCallback.onCompletion(walklist);
				}

			}
		});
	}

	/**
	 * Sync loads walklist from server (Blocks current thread)
	 * @return - Pair of walklist and server response
	 */
	public Pair<Walklist, Response> syncLoadWalklist() {
		if(BasePetitionActivity.isDebug) {
			String walklistJson = "{\"Walklist\":[{\"Hid\":242424,\"FirstName\":\"SANDRA\",\"LastName\":\"MAHAMMITTE\",\"Address\":\"3355 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0779685974121\",\"Longitude\":\"-74.9896011352539\"},{\"Hid\":242427,\"FirstName\":\"MICHAEL\",\"LastName\":\"GLOWACKI\",\"Address\":\"3335 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157394062\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0785675048828\",\"Longitude\":\"-74.9903030395508\"},{\"Hid\":242449,\"FirstName\":\"CONSTANCE\",\"LastName\":\"NATHANIEL\",\"Address\":\"3347 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156121247\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0783195495605\",\"Longitude\":\"-74.9900131225586\"},{\"Hid\":242455,\"FirstName\":\"ALEXANDER\",\"LastName\":\"TORRES\",\"Address\":\"3367 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157556299\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0777168273926\",\"Longitude\":\"-74.9893035888672\"},{\"Hid\":242457,\"FirstName\":\"LANCE\",\"LastName\":\"WALTER\",\"Address\":\"3375 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.077522277832\",\"Longitude\":\"-74.9890670776367\"},{\"Hid\":242466,\"FirstName\":\"EILEEN\",\"LastName\":\"STRICKER\",\"Address\":\"3373 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156323428\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0775604248047\",\"Longitude\":\"-74.9891204833984\"},{\"Hid\":242482,\"FirstName\":\"EDWIN\",\"LastName\":\"TORRES\",\"Address\":\"3367 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157556299\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0777168273926\",\"Longitude\":\"-74.9893035888672\"},{\"Hid\":242484,\"FirstName\":\"MARCIA\",\"LastName\":\"PATTERSON\",\"Address\":\"3351 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2159346304\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0782241821289\",\"Longitude\":\"-74.9898986816406\"},{\"Hid\":242491,\"FirstName\":\"THOMAS\",\"LastName\":\"EDINGER\",\"Suffix\":\"SR\",\"Address\":\"3345 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156320738\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0783576965332\",\"Longitude\":\"-74.9900665283203\"},{\"Hid\":242511,\"FirstName\":\"JOAN\",\"LastName\":\"RUBIN\",\"Address\":\"3357 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2152810465\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0779304504395\",\"Longitude\":\"-74.9895553588867\"},{\"Hid\":242549,\"FirstName\":\"JOSEPHINE\",\"LastName\":\"FALCONIO\",\"Address\":\"3333 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157258545\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0786247253418\",\"Longitude\":\"-74.9903717041016\"},{\"Hid\":242552,\"FirstName\":\"YOLANDA\",\"LastName\":\"TORRES\",\"Address\":\"3367 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157556299\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0777168273926\",\"Longitude\":\"-74.9893035888672\"},{\"Hid\":242571,\"FirstName\":\"ALLAN\",\"LastName\":\"KNIFFIN\",\"Suffix\":\"SR\",\"Address\":\"3363 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156321159\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0778121948242\",\"Longitude\":\"-74.9894180297852\"},{\"Hid\":242572,\"FirstName\":\"JANET\",\"LastName\":\"MALMROSE\",\"Address\":\"3365 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156328803\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0777740478516\",\"Longitude\":\"-74.9893646240234\"},{\"Hid\":242578,\"FirstName\":\"HELEN\",\"LastName\":\"KNIFFIN\",\"Address\":\"3363 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156321159\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0778121948242\",\"Longitude\":\"-74.9894180297852\"},{\"Hid\":242592,\"FirstName\":\"GAYE\",\"LastName\":\"BAILEY\",\"Address\":\"3365 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0777740478516\",\"Longitude\":\"-74.9893646240234\"},{\"Hid\":242613,\"FirstName\":\"JAMIE\",\"LastName\":\"CONROY\",\"Address\":\"3377 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156323421\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0774841308594\",\"Longitude\":\"-74.9890213012695\"},{\"Hid\":242621,\"FirstName\":\"CATHERINE\",\"LastName\":\"EDINGER\",\"Address\":\"3345 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156320738\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0783576965332\",\"Longitude\":\"-74.9900665283203\"},{\"Hid\":242675,\"FirstName\":\"CHARLES\",\"LastName\":\"MAHAMMITTE\",\"Address\":\"3355 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0779685974121\",\"Longitude\":\"-74.9896011352539\"},{\"Hid\":242687,\"FirstName\":\"KENISHA\",\"LastName\":\"PERRY\",\"Address\":\"3337 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2159218350\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0785255432129\",\"Longitude\":\"-74.9902572631836\"},{\"Hid\":242697,\"FirstName\":\"EILEEN\",\"LastName\":\"WEINGARD\",\"Address\":\"3317 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.078801587224\",\"Longitude\":\"-74.9909075349569\"},{\"Hid\":246015,\"FirstName\":\"MARYANNE\",\"LastName\":\"BURGMANN\",\"Address\":\"3829 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157690306\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0736351013184\",\"Longitude\":\"-74.98291015625\"},{\"Hid\":246027,\"FirstName\":\"MARISA\",\"LastName\":\"FERSICK\",\"Address\":\"3813 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156379643\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0741195678711\",\"Longitude\":\"-74.9836959838867\"},{\"Hid\":246033,\"FirstName\":\"EDWARD\",\"LastName\":\"CAMACHO\",\"Address\":\"3825 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2152891727\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0737609863281\",\"Longitude\":\"-74.9830932617188\"},{\"Hid\":246042,\"FirstName\":\"MICHAEL\",\"LastName\":\"FERSICK\",\"Address\":\"3813 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156379643\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0741195678711\",\"Longitude\":\"-74.9836959838867\"},{\"Hid\":246051,\"FirstName\":\"NOEL\",\"LastName\":\"ABEJO\",\"Address\":\"3827 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2152819843\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0736999511719\",\"Longitude\":\"-74.9830017089844\"},{\"Hid\":246063,\"FirstName\":\"MUHAMMED\",\"LastName\":\"ISLAM\",\"Address\":\"3833 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156378174\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0734939575195\",\"Longitude\":\"-74.9827194213867\"},{\"Hid\":246067,\"FirstName\":\"SUSAN\",\"LastName\":\"FRITSCH\",\"Address\":\"3839 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2153332934\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0732955932617\",\"Longitude\":\"-74.9824447631836\"},{\"Hid\":246074,\"FirstName\":\"MARIANNE\",\"LastName\":\"KILRAIN\",\"Address\":\"3809 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0742340087891\",\"Longitude\":\"-74.9839019775391\"},{\"Hid\":246091,\"FirstName\":\"JAMES\",\"LastName\":\"THIERGARTNER\",\"Address\":\"3815 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2153340565\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0740699768066\",\"Longitude\":\"-74.9835968017578\"},{\"Hid\":246100,\"FirstName\":\"ROBERT\",\"LastName\":\"LYSZKOWSKI\",\"Suffix\":\"JR\",\"Address\":\"3853 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2158240268\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0728416442871\",\"Longitude\":\"-74.981819152832\"},{\"Hid\":246109,\"FirstName\":\"WILLIAM\",\"LastName\":\"KILRAIN\",\"Address\":\"3809 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0742340087891\",\"Longitude\":\"-74.9839019775391\"},{\"Hid\":246118,\"FirstName\":\"LORRAINE\",\"LastName\":\"DAVEY\",\"Address\":\"3825 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2152891727\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0737609863281\",\"Longitude\":\"-74.9830932617188\"},{\"Hid\":246133,\"FirstName\":\"DONNA MARIE\",\"LastName\":\"THIERGARTNER\",\"Address\":\"3815 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2153340565\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0740699768066\",\"Longitude\":\"-74.9835968017578\"},{\"Hid\":246137,\"FirstName\":\"LINDA\",\"LastName\":\"FERSICK\",\"Address\":\"3813 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156379643\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0741195678711\",\"Longitude\":\"-74.9836959838867\"},{\"Hid\":246153,\"FirstName\":\"DEBORAH\",\"LastName\":\"MILLER\",\"Address\":\"3805 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2158242845\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.074348449707\",\"Longitude\":\"-74.9841156005859\"},{\"Hid\":246163,\"FirstName\":\"CAROL\",\"LastName\":\"COMMODARO\",\"Address\":\"3841 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156370956\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0732345581055\",\"Longitude\":\"-74.9823532104492\"},{\"Hid\":246170,\"FirstName\":\"MARK\",\"LastName\":\"BURGMANN\",\"Address\":\"3829 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2157690306\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0736351013184\",\"Longitude\":\"-74.98291015625\"},{\"Hid\":246173,\"FirstName\":\"GULA\",\"LastName\":\"RINA\",\"Address\":\"3833 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2156378170\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0734939575195\",\"Longitude\":\"-74.9827194213867\"},{\"Hid\":246175,\"FirstName\":\"FERNANDO\",\"LastName\":\"GISPERT\",\"Address\":\"3819 RED LION RD\",\"City\":\"PHILADELPHIA\",\"State\":\"PA\",\"Zip\":\"19114\",\"Apartment\":\"\",\"Phone\":\"2158241849\",\"Status\":\"\",\"Other\":{\"Other\":\"\"},\"Latitude\":\"40.0739555358887\",\"Longitude\":\"-74.9833908081055\"}]}";
			Walklist walklist = parser.toWalklist(walklistJson);
			return Pair.create(walklist, null);
		}
		HttpUriRequest getWalklist = new HttpGet(createUrl(WALKLIST_ENDPOINT));
		addHeadersToRequest(getWalklist);
		Walklist walklist = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting walklist GET");
			HttpResponse serverResponse = syncClient.execute(getWalklist);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());
			if (isSuccess(serverResponse)) {
				if(serverResponse.getStatusLine().getStatusCode() == 204){
					// 204 No Content, do nothing
					LOGGER.debug("No content, not parsing.");
				}else{
					LOGGER.debug("Walklist json: " + responseBody);
					walklist = parser.toWalklist(responseBody);
					getGlobalData().setLocation(null);
				}
			} else {
				LOGGER.error("Petition response error (" + serverResponse.getStatusLine() + "): " + response);
				response = parseErrors(responseBody);
			}
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_WALKLIST_MESSAGE)));
		} finally {
			syncClient.close();
		}
		return Pair.create(walklist, response);
	}

	/**
	 * Sync loads walklist from server for a particular location
	 * @return - Pair of walklist and server response
	 */
	public Pair<Walklist, Response> syncLoadWalklistForLocation(String location) {
		String fetchWalklistSinceUrl = createUrl(WALKLIST_ENDPOINT_SINCE);
		StringBuilder urlBuilder = new StringBuilder(fetchWalklistSinceUrl);
		String paramSeparator = "?";
		Pair<String, String> pair = Pair.create("location", location);
		urlBuilder.append(paramSeparator)
		.append(pair.first).append("=")
		.append(URLEncoder.encode((pair.second)));
		String url = urlBuilder.toString();
		HttpUriRequest getWalklist = new HttpGet(url);
		addHeadersToRequest(getWalklist);

		Walklist walklist = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS,
				STANDARD_SYNC_SO_TIMEOUT);
		LOGGER.debug("Walklist url: " + url);
		try {
			HttpResponse serverResponse = syncClient.execute(getWalklist);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());
			if (isSuccess(serverResponse)) {
				if (serverResponse.getStatusLine().getStatusCode() == 204) {
					// 204 No Content, do nothing
					LOGGER.debug("No content, not parsing.");
				} else {
					LOGGER.debug("Walklist json: " + responseBody);
					walklist = parser.toWalklist(responseBody);
					getGlobalData().setLocation(location);
				}
			} else {
				LOGGER.error("Petition response error ("
						+ serverResponse.getStatusLine() + "): " + response);
				response = parseErrors(responseBody);
			}
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(
					new ResponseError(
							"ServerError",
							applicationContext
							.getString(R.string.IOEXCEPTION_WHILE_LOADING_WALKLIST_MESSAGE)));
		} finally {
			syncClient.close();
		}       
		return Pair.create(walklist, response);
	}

	/**
	 * Loads walklist from server since a given time
	 * @param location passing locaiton for location based lists ,if location is null do normal walklist fetch,else pass optional parameter location
	 * 
	 * @return - Pair of walklist and server response
	 */
	public Pair<Walklist, Response> syncLoadWalklistSince(Date sinceTime, String location) {

		String fetchWalklistSinceUrl = createUrl(WALKLIST_ENDPOINT_SINCE);
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ssZ");
		String dateInISO = sdf.format(sinceTime);
		StringBuilder urlBuilder = new StringBuilder(fetchWalklistSinceUrl);
		String paramSeparator = "?";
		Pair<String, String> pair = Pair.create("since", dateInISO);
		urlBuilder.append(paramSeparator)
		.append(pair.first).append("=")
		.append(pair.second);
		if(location != null){
			Pair<String, String> locationParam = Pair.create("location",location);
			urlBuilder.append("&")
			.append(locationParam.first).append("=")
			.append(URLEncoder.encode(locationParam.second));
		}
		String url = urlBuilder.toString();
		HttpUriRequest getWalklist = new HttpGet(url);
		addHeadersToRequest(getWalklist);

		Walklist walklist = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS,
				STANDARD_SYNC_SO_TIMEOUT);
		LOGGER.debug("Walklist url: " + url);
		try {
			HttpResponse serverResponse = syncClient.execute(getWalklist);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());
			if (isSuccess(serverResponse)) {
				if (serverResponse.getStatusLine().getStatusCode() == 204) {
					// 204 No Content, do nothing
					LOGGER.debug("No content, not parsing.");
				} else {
					LOGGER.debug("Walklist json: " + responseBody);
					walklist = parser.toWalklist(responseBody);
				}
			} else {
				LOGGER.error("Petition response error ("
						+ serverResponse.getStatusLine() + "): " + response);
				response = parseErrors(responseBody);
			}
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(
					new ResponseError(
							"ServerError",
							applicationContext
							.getString(R.string.IOEXCEPTION_WHILE_LOADING_WALKLIST_MESSAGE)));
		} finally {
			syncClient.close();
		}       
		return Pair.create(walklist, response);
	}

	/**
	 * Async load petiton
	 * @param petitionCallback
	 */
	public void loadPetition(final Callback<Petition> petitionCallback) {
		client.get(createUrl(PETITION_ACTION), new CallOnFailureHandler(petitionCallback) {
			public void onSuccess(String message) {
				try {
					Petition petition = parser.toPetition(message);
					petitionCallback.onCompletion(petition);
				} catch (JsonParseException jpe) {
					petitionCallback.onError(parseErrors(message));
				}
			}
		});
	}

	/**
	 * Sync loads petition from server (Blocks current thread)
	 * @return - Pair of petition and server response
	 */
	public Pair<Petition, Response> syncLoadPetition() {
		if(BasePetitionActivity.isDebug) {
			String petitionJson = "{\"Id\":502,\"Reservation\":\"true\",\"ReservationMinimum\":15,\"ReservationMaximum\":90,\"ReservationSuggested\":30,\"Name\":\"Demo Sales Campaign\",\"Script\":\"Greeting:  \\tHi, I’m [NAME] with Ethical Electric (or affinity group).  How are you? \\r\\n\\r\\nPurpose:   \\tThe reason I’m here is I’m working to get the neighborhood off of dirty electricity from coal and over to 100% clean local wind and solar power. \\r\\n\\r\\nClipboard: \\tHere, take a look.  (Hand over clipboard)\\r\\n\\r\\nProblem: \\tUnfortunately, today we still get over 90% of our power in Pennsylvania from dirty sources like coal.  (Point to picture)  This is terrible for our health and the environment.\\r\\n\\r\\nSolution: \\tThe good news is that Pennsylvania is an open electricity state, so we all get to choose where our power comes from.\\r\\n\\r\\nAction: \\tSo today I’m switching people’s power over to Clean Energy Choice which is 100% local Pennsylvania wind sourced from places like the Cassellman Wind Farm in Somerset County (Point to picture).\\r\\n\\r\\n\\t\\tLet’s sign you up.  (Flip to enrollment form)\\r\\n\\r\\n\",\"Signature\":true,\"Walklist\":true,\"Petition\":\"<p><span style=\\\"font-size: 18pt;\\\"><strong>New Customer!</strong></span></p><p>$FIRST_NAME $LAST_NAME</p><p>$ADDRESS</p><p>$APARTMENT</p><p>$CITY, $STATE, $ZIP</p><p>Home: $HOME</p><p>Cell: $CELL&nbsp;</p><p>PECO: $PECO</p><p>Email: $EMAIL</p><p>Plan: <strong>$PLAN</strong></p><p>$SIGNATURE&nbsp;</p>\",\"Questions\":[{\"Id\":1901,\"Title\":\"First Name\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$FIRST_NAME\"},{\"Id\":1902,\"Title\":\"Last Name\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$LAST_NAME\"},{\"Id\":1903,\"Title\":\"Address\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$ADDRESS\"},{\"Id\":1904,\"Title\":\"Apartment\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$APARTMENT\"},{\"Id\":1905,\"Title\":\"City\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$CITY\"},{\"Id\":1906,\"Title\":\"State\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$STATE\"},{\"Id\":1907,\"Title\":\"Zip\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$ZIP\"},{\"Id\":1908,\"Title\":\"Signature\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$SIGNATURE\"},{\"Id\":901,\"Title\":\"First Name\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$FIRST_NAME_SHIPPING\"},{\"Id\":902,\"Title\":\"Last Name\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$LAST_NAME_SHIPPING\"},{\"Id\":903,\"Title\":\"Address\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$ADDRESS_SHIPPING\"},{\"Id\":904,\"Title\":\"Apartment\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$APARTMENT_SHIPPING\"},{\"Id\":905,\"Title\":\"City\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$CITY_SHIPPING\"},{\"Id\":906,\"Title\":\"State\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$STATE_SHIPPING\"},{\"Id\":907,\"Title\":\"Zip\",\"Type\":\"FreeFormSingleLine\",\"Variable\":\"$ZIP_SHIPPING\"},{\"Id\":1909,\"Title\":\"PECO #\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"PECO Account Number\",\"Variable\":\"$PECO\"},{\"Id\":1910,\"Title\":\"Cell\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"Cell Phone\",\"Variable\":\"$CELL\"},{\"Id\":1911,\"Title\":\"Plan\",\"Type\":\"Standard\",\"Text\":\"Pick a plan\",\"Variable\":\"$PLAN\"},{\"Id\":1912,\"Title\":\"referrer1_name\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer1_name\",\"Variable\":\"$referrer1_name\"},{\"Id\":1913,\"Title\":\"Terminated\",\"Type\":\"Standard\",\"Text\":\"Why was this survey terminated\",\"Variable\":\"$TERM\"},{\"Id\":1914,\"Title\":\"referrer1_phone\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer1_phone\",\"Variable\":\"$referrer1_phone\"},{\"Id\":1915,\"Title\":\"referrer2_name\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer2_name\",\"Variable\":\"$referrer2_name\"},{\"Id\":1916,\"Title\":\"referrer1_email\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer1_email\",\"Variable\":\"$referrer1_email\"},{\"Id\":1917,\"Title\":\"referrer2_phone\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer2_phone\",\"Variable\":\"$referrer2_phone\"},{\"Id\":1918,\"Title\":\"referrer2_email\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer2_email\",\"Variable\":\"$referrer2_email\"},{\"Id\":1919,\"Title\":\"referrer3_name\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer3_name\",\"Variable\":\"$referrer3_name\"},{\"Id\":1920,\"Title\":\"referrer3_phone\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer3_phone\",\"Variable\":\"$referrer3_phone\"},{\"Id\":1921,\"Title\":\"referrer3_email\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"referrer3_email\",\"Variable\":\"$referrer3_email\"},{\"Id\":1922,\"Title\":\"Home\",\"Type\":\"Standard\",\"Text\":\"Phone\",\"Variable\":\"$HOME\"},{\"Id\":1923,\"Title\":\"Email\",\"Type\":\"FreeFormSingleLine\",\"Text\":\"Email\",\"Variable\":\"$EMAIL\"}],\"Answers\":[{\"Id\":4561,\"Question\":1911,\"Value\":\"Clean Energy Choice Gold Fixed\"},{\"Id\":4562,\"Question\":1911,\"Value\":\"Clean Energy Choice Gold Variable\"},{\"Id\":4563,\"Question\":1911,\"Value\":\"Clean Energy Choice Fixed\"},{\"Id\":4564,\"Question\":1911,\"Value\":\"Clean Energy Choice Variable\"},{\"Id\":4565,\"Question\":1911,\"Value\":\"Simple Clean Energy Choice Variable\"},{\"Id\":4566,\"Question\":1912,\"Value\":\"One\"},{\"Id\":4567,\"Question\":1912,\"Value\":\"Two\"},{\"Id\":4568,\"Question\":1912,\"Value\":\"Three\"},{\"Id\":4569,\"Question\":1913,\"Value\":\"Door slam\"},{\"Id\":4570,\"Question\":1913,\"Value\":\"Took too much time to complete\"},{\"Id\":4571,\"Question\":1913,\"Value\":\"Don't want to look for PECO bill\"},{\"Id\":4572,\"Question\":1913,\"Value\":\"Need more information/Send me more information\"},{\"Id\":4573,\"Question\":1913,\"Value\":\"Happy with current electric provider\"},{\"Id\":4574,\"Question\":1922,\"Value\":\"100\"}],\"UserFirstName\":\"John\",\"UserLastName\":\"McCarthy (TESTER)\",\"POS\":true,\"Products\":[{\"SKU\":\"MCCART-1\",\"Name\":\"John McCarthy(tm)\",\"Description\":\"One Java engineer\",\"Cost\":99.95,\"Recurring\":\"NO\"},{\"SKU\":\"OMGWEEK\",\"Name\":\"Weekly new client requirements\",\"Description\":\"Why settle for developing the same thing each day?\",\"Cost\":225.00,\"Recurring\":\"WEEKLY\"},{\"SKU\":\"POLISH\",\"Name\":\"Jeff Labonski(tm)\",\"Description\":\"Bendable action figure, can express exasperation and disappointment\",\"Cost\":35.00,\"Recurring\":\"NO\"}]}";
			Petition petition = parser.toPetition(petitionJson);
			return Pair.create(petition, null);
		}
		HttpUriRequest getPetition = new HttpGet(createUrl(PETITION_ACTION));
		addHeadersToRequest(getPetition);
		Petition petition = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting petition GET");
			HttpResponse serverResponse = syncClient.execute(getPetition);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("Petition json: " + responseBody);

				petition = parser.toPetition(responseBody);
				petition.setPetitionJSON(responseBody);
				Tracker.appendLog("\n" + Tracker.getTime() + " Petition JSON: " + responseBody);

			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Petition response error (" + serverResponse.getStatusLine() + "): " + response);
				Tracker.appendLog("/n" + Tracker.getTime() + " " + "Petition response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}finally {
			syncClient.close();
		}
		return Pair.create(petition, response);
	}

	/**
	 * Sync loads location list from server 
	 * @return - Pair of location and server response
	 */
	public Pair<LocationList, Response> syncLoadLocationList() {
		HttpUriRequest getLocationRequest = new HttpGet(createUrl(FETCH_LOCATION_LIST));
		addHeadersToRequest(getLocationRequest);
		LocationList locationList = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting locationlist GET");
			HttpResponse serverResponse = syncClient.execute(getLocationRequest);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("Locations json: " + responseBody);
				locationList = parser.toLocationList(responseBody);
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Locations response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}finally {
			syncClient.close();
		}
		return Pair.create(locationList, response);
	}

	/**
	 * Sync loads tally
	 * @return - Pair of tally and server response
	 */
	public Pair<Tally, Response> syncLoadTally() {
		HttpUriRequest getTallyRequest = new HttpGet(createUrl(TALLY_ENDPOINT));
		addHeadersToRequest(getTallyRequest);
		Tally tally = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting tally GET");
			HttpResponse serverResponse = syncClient.execute(getTallyRequest);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("Tally json: " + responseBody);
				tally = parser.toTally(responseBody);
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Locations response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}finally {
			syncClient.close();
		}
		return Pair.create(tally, response);
	}

	/**
	 * Async load Appointment Windows
	 */
	public void loadAppointmentWindows(final Callback<String> petitionCallback) {
		client.get(createUrl(APPOINTMENT_WINDOWS_ENDPOINT), new CallOnFailureHandler(petitionCallback) {
			public void onSuccess(String message) {
				try {
					//Petition petition = parser.toPetition(message);
					petitionCallback.onCompletion(message);
				} catch (JsonParseException jpe) {
					petitionCallback.onError(parseErrors(message));
				}
			}
		});
	}

	/**
	 * Sync load AppointmentCalendar (Blocks current thread)
	 * @param apiUrl
	 * @return - Pair of AppointmentCalendar and serverResponse
	 */
	private Pair<AppointmentCalendar, Response> syncLoadAppointmentWindows(String apiUrl) {
		if(BasePetitionActivity.isDebug) {
			//AssetManager.open("");
			//String appointmentWindowsJson = "{\"Workday\":{\"Sunday\":[{\"Start\":\"09:00\",\"End\":\"11:59\"},{\"Start\":\"13:00\",\"End\":\"18:59\"}],\"Monday\":[{\"Start\":\"09:00\",\"End\":\"11:59\"},{\"Start\":\"13:00\",\"End\":\"18:59\"}]},\"Events\":[{\"Start\":\"2013-07-12T09:00:00-04:00\",\"End\":\"2013-07-12T10:00:00-04:00\"},{\"Start\":\"2013-08-12T09:00:00-04:00\",\"End\":\"2013-08-12T10:00:00-04:00\"},{\"Start\":\"2013-09-12T09:00:00-04:00\",\"End\":\"2013-09-12T10:00:00-04:00\"}]}";
			String appointmentWindowsJson = "{\n" +
					"    \"Workday\": {\n" +
					"        \"Sunday\": [\n" +
					"            {\n" +
					"                \"Start\": \"09:00\",\n" +
					"                \"End\": \"11:59\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"Start\": \"13:00\",\n" +
					"                \"End\": \"18:59\"\n" +
					"            }\n" +
					"        ],\n" +
					"        \"Monday\": [\n" +
					"            {\n" +
					"                \"Start\": \"09:00\",\n" +
					"                \"End\": \"11:59\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"Start\": \"13:00\",\n" +
					"                \"End\": \"18:59\"\n" +
					"            }\n" +
					"        ],\n" +
					"        \"Friday\": [\n" +
					"            {\n" +
					"                \"Start\": \"09:00\",\n" +
					"                \"End\": \"12:00\"\n" +
					"            },\n" +
					"            {\n" +
					"                \"Start\": \"13:00\",\n" +
					"                \"End\": \"20:00\"\n" +
					"            }\n" +
					"        ]\n" +
					"    },\n" +
					"    \"Events\": [\n" +
					"        {\n" +
					"            \"Start\": \"2013-07-15T09:00:00+03:00\",\n" +
					"            \"End\": \"2013-07-15T10:00:00+03:00\"\n" +
					"        },\n" +
					"        {\n" +
					"            \"Start\": \"2013-07-15T09:00:00+03:00\",\n" +
					"            \"End\": \"2013-07-15T10:30:00+03:00\"\n" +
					"        },\n" +
					"        {\n" +
					"            \"Start\": \"2013-07-15T10:30:00+03:00\",\n" +
					"            \"End\": \"2013-07-15T11:00:00+03:00\"\n" +
					"        },\n" +
					"        {\n" +
					"            \"Start\": \"2013-07-15T11:30:00+03:00\",\n" +
					"            \"End\": \"2013-07-15T13:30:00+03:00\"\n" +
					"        }\n" +
					"    ]\n" +
					"}";
			AppointmentCalendar calendar = parser.toAppointmentCalendar(appointmentWindowsJson);
			return Pair.create(calendar, null);
		}
		HttpUriRequest getAppointmentWindows = new HttpGet(createUrl(apiUrl));
		addHeadersToRequest(getAppointmentWindows);
		AppointmentCalendar calendar = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting appointmentWindows GET");
			HttpResponse serverResponse = syncClient.execute(getAppointmentWindows);
			String responseBody = getResponseBody(serverResponse);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("AppointmentWindows json: " + responseBody);
				calendar = parser.toAppointmentCalendar(responseBody);
				if (null != calendar) {
					getGlobalData().setAppointmentCalendar(calendar);
					calendar.setLastUpdateDate(new Date());
				}
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("AppointmentWindows response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		} finally {
			syncClient.close();
		}
		return Pair.create(calendar, response);
	}

	public Pair<AppointmentCalendar, Response> syncLoadAppointmentWindows() {
		return syncLoadAppointmentWindows(APPOINTMENT_WINDOWS_ENDPOINT + APPOINTMENT_WINDOWS_ENDPOINT_ID);
	}

	/**
	 * Updates Appointment calendar
	 * @return
	 */
	public Pair<AppointmentCalendar, Response> syncUpdateAppointmentWindows() {
		AppointmentCalendar calendar = getAppointmentCalendar();
		Date lastUpdateDate = calendar.getLastUpdateDate();
		DateTimeFormatter dtf = DateTimeFormat.forPattern(APPOINTMENT_UPDATE_SINCE_DATE_FORMAT);
		String lastUpdateDateString = dtf.print(new DateTime(lastUpdateDate));

		String url = addQueryParams(APPOINTMENT_WINDOWS_UPDATE_ENDPOINT + APPOINTMENT_WINDOWS_ENDPOINT_ID, 
				Pair.create(APPOINTMENT_SINCE_DATE_KEY, lastUpdateDateString));
		return syncLoadAppointmentWindows(url);
	}

	/**
	 * Sends Instant Prescreen Request object (InstantPrescreenRequest) synchronously using INSTANT_PRESCREEN_REQUEST_URL URI.
	 * Process and parse response to get Instant Prescreen response object (InstantPrescreenResponce) and return it.
	 * @param prescreenRequestObject - Instant Prescreen Request (InstantPrescreenRequest) object which will be send
	 * @return - Pair of InstantPrescreenResponce object and Response object (with error description).
	 */
	public Pair<InstantPrescreenResponce, Response> syncSendInstantRequest(InstantPrescreenRequest prescreenRequestObject)
	{
		if (BasePetitionActivity.isDebug)
		{
			String fakeJSON =
					"{\n" +
							"\"ReferenceNumber\":1234567,\n" +
							"\"Code\":\"A\",\n" +
							"\"Text\":\"Approved\",\n" +
							"\"DecisionLevel\":1\n" +
							"}";
			InstantPrescreenResponce prescreenResponceObject = parser.toInstantPrescreenResponce(fakeJSON);
			return Pair.create(prescreenResponceObject, null);
		}

		InstantPrescreenResponce prescreenResponceObject = null;
		Response response = null;

		if (prescreenRequestObject == null) Pair.create(prescreenResponceObject, null);

		// Convert prescreenRequestObject to get request params
		// It's used here because of server implementation (it doesn't support POST request)
		List<Pair<String, String>> params = new ArrayList<Pair<String, String>>();
		if (Strings.isNotEmpty(prescreenRequestObject.getFirstName()))
			params.add(Pair.create("firstName", prescreenRequestObject.getFirstName()));

		if (Strings.isNotEmpty(prescreenRequestObject.getLastName()))
			params.add(Pair.create("lastName", prescreenRequestObject.getLastName()));

		if (Strings.isNotEmpty(prescreenRequestObject.getAddress()))
			params.add(Pair.create("address", prescreenRequestObject.getAddress()));

		if (Strings.isNotEmpty(prescreenRequestObject.getCity()))
			params.add(Pair.create("city", prescreenRequestObject.getCity()));

		if (Strings.isNotEmpty(prescreenRequestObject.getState()))
			params.add(Pair.create("state", prescreenRequestObject.getState()));

		if (Strings.isNotEmpty(prescreenRequestObject.getZipCode()))
			params.add(Pair.create("zipCode", prescreenRequestObject.getZipCode()));

		if (Strings.isNotEmpty(prescreenRequestObject.getPhone()))
			params.add(Pair.create("phone", prescreenRequestObject.getPhone()));

		Pair<String, String>[] paramsArray = new Pair[params.size()];
		paramsArray = params.toArray(paramsArray);

		String url = addQueryParams(createUrl(INSTANT_PRESCREEN_REQUEST_URL), paramsArray);

		HttpGet httpRequest = new HttpGet(url);
		addHeadersToRequest(httpRequest);

		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);

		try
		{
			LOGGER.debug("Attempt to send instant prescreen request.");
			HttpResponse httpResponse = syncClient.execute(httpRequest);
			String httpResponseBody = getResponseBody(httpResponse);
			LOGGER.debug(httpResponse.getStatusLine());

			if (isSuccess(httpResponse) && Strings.isNotEmpty(httpResponseBody))
			{
				LOGGER.debug("Instant prescreen request's response body: " + httpResponseBody);
				try {
					prescreenResponceObject = parser.toInstantPrescreenResponce(httpResponseBody);
				} catch (JsonParseException jpe) {
					// parse error
					response = new Response(new ResponseError("NoResponse", applicationContext.getString(R.string.INSTANT_PRESCREEN_CLIENT_ERROR_MESSAGE)));
				}
			} else {
				// empty petition body is a failure
				response = parseErrors(httpResponseBody);
				LOGGER.error("Instant prescreen request's response error (" + httpResponse.getStatusLine() + "): " + response);
			}
		}
		catch (ClientProtocolException cpe)
		{
			// request error
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		}
		catch (IOException e)
		{
			// request IO error
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}
		finally
		{
			syncClient.close();
		}

		return Pair.create(prescreenResponceObject, response);
	}


	/**
	 * Add particular header to the request (imei, auth headers...)
	 * @param request
	 */
	private void addHeadersToRequest(HttpUriRequest request) {
		for(Header header : getHeaders()) {
			request.addHeader(header);
		}
	}

	/**
	 * Compose list of request headers
	 * @return - headers list
	 */
	private List<Header> getHeaders() {
		List<Header> headers = new ArrayList<Header>(3);
		headers.add(imeiHeader);
		headers.add(JSON_HEADER);
		if(hasAuthenticationInformation()) {
			headers.add(createAuthenticationHeader(username, password));
		}
		return headers;
	}

	/**
	 * Returns true, if userName and Password have been entered
	 * @return
	 */
	private boolean hasAuthenticationInformation() {
		return (Strings.isNotEmpty(username) && Strings.isNotEmpty(password));
	}

	/**
	 * Returns only Response Body from HttpResponse
	 */
	private String getResponseBody(HttpResponse serverResponse) throws IOException {
		String responseBody = null;
		HttpEntity entity = null;
		HttpEntity temp = serverResponse.getEntity();
		if (temp != null) {
			entity = new BufferedHttpEntity(temp);
			responseBody = EntityUtils.toString(entity, "UTF-8");
		}
		return responseBody;
	}

	/**
	 * Set Login Information to generate request headers
	 * @param username
	 * @param password
	 */
	public void setLoginInformation(String username, String password) {
		this.username = username;
		this.password = password;
		addAuthorizationInfoToClients(username, password);
	}

	/**
	 * Inserts login request into the queue and send it to the server
	 * @param username
	 * @param password
	 * @param location  - current GpsLocation
	 * @param batteryPerformance 
	 * @throws Exception
	 */
	public void login(final String username, final String password, EventInfo eventinfo) throws Exception {
		setLoginInformation(username, password);
		Request request = createRequest(eventinfo, createUrl("events", "login"));
		insert(request);
	}

	/**
	 * Async ping server (use it to check connection to the server)
	 * @param pingCallback
	 */
	public void ping(final Callback<Boolean> pingCallback) {
		client.get(createUrl("ping"), new CallOnFailureHandler(pingCallback) {
			public void onSuccess(String message) {
				pingCallback.onCompletion(Boolean.TRUE);
			}

			public void onFailure(Throwable t, String msg) {
				pingCallback.onError(parseErrors(msg));
			}
		});
	}

	/**
	 * Sync ping server (use it to check connection to the server) (Blocks current thread)
	 * @return
	 */
	public boolean syncPing() {
		HttpUriRequest getPing = new HttpGet(createUrl("ping"));
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, PING_SYNC_CONNECTION_TIMEOUT_SECONDS, PING_SYNC_SO_TIMEOUT);
		try {
			HttpResponse response = syncClient.execute(getPing);
			boolean isSuccess = isSuccess(response);
			if (isSuccess) {
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			syncClient.close();
		}
		return false;
	}


	/**
	 * Sends request to the geolocation server and gets address, street name, etc.
	 * @param location
	 * @param subjectCallback
	 */
	public void reverseGeocodeLocation(Location location, final Callback<Set<Subject>> subjectCallback) {
		if (null == location) {
			LOGGER.error("Location is null!");
			return;
		}
		String latLng = location.getLatitude() + "," + location.getLongitude();
		String url = GEOCODE_URL + "?latlng=" + latLng + "&sensor=true";
		client.get(url, new CallOnFailureHandler(subjectCallback) {
			public void onSuccess(String message) {
				subjectCallback.onCompletion(parseGeocodeResponse(message));
			}
		});
	}

	/**
	 * Sends request to the geolocation server and get location coords
	 * @param address
	 * @param locationCallback
	 */
	public void forwardGeocodeAddress(final String address, final Callback<GeoPoint> locationCallback) {
		String encodedAddress = address;
		try {
			encodedAddress = URLEncoder.encode(address, "utf-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Trouble encoding address, cancelling geocode", e);
			return;
		}
		String url = GEOCODE_URL + "?address=" + encodedAddress + "&sensor=false";
		client.get(url, new CallOnFailureHandler(locationCallback) {
			public void onSuccess(String message) {
				GeoPoint location = parseForwardGeocodeResponse(message);
				if (location == null) {
					locationCallback.onError(new Response(new ResponseError("NoResponse", applicationContext.getString(R.string.NO_LOCATION_FOUND_FOR_ADDRESS_MESSAGE) + address)));
				} else {
					locationCallback.onCompletion(location);
				}
			}
		});
	}

	/**
	 * Parse geolocation server response (Json) and get GeoPoint object
	 * @param message
	 * @return
	 */
	private GeoPoint parseForwardGeocodeResponse(String message) {
		try {
			JSONObject responseObject = new JSONObject(message);
			JSONArray resultsArray = responseObject.optJSONArray("results");
			if (resultsArray != null) {
				if (resultsArray.length() > 0) {
					JSONObject resultObject = resultsArray.getJSONObject(0);
					JSONObject geometryObject = resultObject.optJSONObject("geometry");
					if (geometryObject != null) {
						JSONObject latLng = geometryObject.optJSONObject("location");
						if (latLng != null) {
							Double lat = latLng.optDouble("lat");
							Double lng = latLng.optDouble("lng");
							if (lat != null && lng != null) {
								return new GeoPoint(lat.doubleValue(), lng.doubleValue());
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			LOGGER.error("Difficulty parsing forward geocode", e);
		}
		return null;
	}

	/**
	 * Parse geolocation server response (Json) and get set of Subject objects
	 * @param message
	 * @return  - set of Subject objects
	 */
	private Set<Subject> parseGeocodeResponse(String message) {
		Set<Subject> subjectList = new HashSet<Subject>();
		try {
			JSONObject resultObject = new JSONObject(message);
			JSONArray addressArray = resultObject.optJSONArray("results");

			if (addressArray == null || addressArray.length() == 0) {
				return subjectList;
			}
			for (int i = 0; i < addressArray.length(); i++) {
				JSONObject result = addressArray.getJSONObject(i);
				//                if(!isType(result, "street_address")) //Uncomment this to only return address results
				//                    continue;
				Subject subject = new Subject();
				String streetName = null;
				String houseNumber = null;
				if (result != null) {
					JSONArray addressComponents = result.optJSONArray("address_components");
					for (int j = 0; j < addressComponents.length(); j++) {
						JSONObject addressComponent = addressComponents.getJSONObject(j);
						if (addressComponent != null) {
							if (isType(addressComponent, "street_number")) {
								houseNumber = addressComponent.optString("long_name", "");
								if(houseNumber.contains("-")){
									//it contains a range of house numbers(eg.407-417),if so avoid it.
									houseNumber = null;
								}
							} else if (isType(addressComponent, "route")) {
								streetName = addressComponent.optString("long_name", "");
								if (houseNumber != null && streetName != null) {
									subject.setAddressLine1(houseNumber + " " + streetName);
								}
							} else if (isType(addressComponent, "locality")) {
								subject.setCity(addressComponent.optString("long_name"));
							} else if (isType(addressComponent, "postal_code")) {
								subject.setZip(addressComponent.optString("long_name"));
							} else if (isType(addressComponent, "administrative_area_level_1")) {
								subject.setState(addressComponent.optString("short_name"));
							}
						}
					}
					if (subject.hasAddressData()){
						if(subject.getCity() != null && subject.getState()!=null & subject.getZip() != null){
							subjectList.add(subject);
							break;
						}

					}
				}
			}
		}catch(Throwable t) {
			LOGGER.error("Error parsing geocode response: " + message, t);
		}
		return subjectList;
	}

	private void setSyncTimeout(AndroidHttpClient syncClient, int connectionTimeoutSeconds, int soTimeoutSeconds) {
		HttpParams params = syncClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, connectionTimeoutSeconds * 1000);
		HttpConnectionParams.setSoTimeout(params, soTimeoutSeconds * 1000); //how long before timing out after a connecting is established
	}

	private boolean isType(JSONObject object, String fieldType) throws JSONException {
		JSONArray array = object.optJSONArray("types");
		if (array == null)
			return false;
		for (int i = 0; i < array.length(); i++) {
			String fieldValue = array.getString(i);
			if (fieldType.equals(fieldValue))
				return true;
		}
		return false;
	}

	/**
	 * Returns true if server returned positive response
	 * @param response
	 * @return
	 */
	private boolean isSuccess(HttpResponse response) {
		int statusCode = response.getStatusLine().getStatusCode();
		return 200 <= statusCode && statusCode <= 299;
	}

	/**
	 * Add AuthorizationInfoToClients
	 * @param username
	 * @param password
	 */
	private void addAuthorizationInfoToClients(String username, String password) {
		client.setBasicAuth(username, password);
		client.addHeader(username, password);
	}

	/**
	 * Adds logout request into the queue and sends it to the server
	 * @param location
	 * @throws Exception
	 */
	public void logout(EventInfo event) throws Exception {
		Request request = createRequest(event, createUrl("events", "logout"));
		insert(request);

		username = null;
		password = null;
	}

	/**
	 * Inserts request to the queue and tries to send it to the server
	 * @param request
	 * @throws Exception
	 */
	public void insert(Request request) throws Exception {
		ContentValues[] values = request.toContentValues();
		LOGGER.debug("Inserting: " + values.length + " values");
		for(ContentValues value : values) {
			LOGGER.debug("Value: " + value);
		}
		applicationContext.getContentResolver().bulkInsert(Request.REQUEST_URI, values);

		Intent startRequestService = new Intent();
		startRequestService.setAction(RequestPerformerService.ACTION);
		startRequestService.putExtra(RequestPerformerService.FORCE_RUN_KEY, true);
		applicationContext.sendBroadcast(startRequestService);
	}

	/**
	 * Save knock statuses
	 * Inserts request to the queue and tries to send it to the server
	 * @param knocks
	 * @throws Exception
	 */
	public void saveKnocks(Knocks knocks){
		try {
			Request request = createRequest(knocks, createUrl("events", "knock"));		
			insert(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.debug("SAVE KNOCKS EXCEPTION: " + e.toString());
		}
	}

	public void getRecipient(String zip, final Callback<Recipient> recipientCallback) {
		String url = createUrl(PETITION_ACTION, "recipient-lookup-by-zip");
		url = addQueryParams(url, Pair.create(ZIP_KEY, zip));
		client.get(url, new CallOnFailureHandler(recipientCallback) {
			public void onSuccess(String message) {
				try {
					recipientCallback.onCompletion(parser.toRecipient(message));
				} catch (JsonParseException jpe) {
					recipientCallback.onError(new Response(new ResponseError("NoResponse", applicationContext.getString(R.string.NO_VALID_RECIPIENT_FOR_QUERY_ZIP_MESSAGE))));
				}
			}
		});
	}

	/**
	 * Returns Subjects list (Blocks current thread)
	 * @param lastName
	 * @param zip
	 * @return
	 */
	public  Pair<List<Subject>, Response> getSubjects(String lastName, String zip) {
		String url = createUrl(PETITION_ACTION, "subject-lookup");
		url = addQueryParams(url, Pair.create(ZIP_KEY, zip), Pair.create("lastname", lastName)); //TODO empty strings skipped??

		HttpUriRequest getSubjects = new HttpGet(url);
		addHeadersToRequest(getSubjects);
		List<Subject> subjects = null;

		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			HttpResponse serverResponse = syncClient.execute(getSubjects);
			String responseBody = getResponseBody(serverResponse);

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) {
				LOGGER.debug("Subjects Lookup json: " + responseBody);
				try {
					subjects = parser.toSubjectList(responseBody);
				} catch (JsonParseException jpe) {
					response = new Response(new ResponseError("NoResponse", applicationContext.getString(R.string.NO_SUBJECTS_FOUND_MESSAGE)));
				}
			} else {
				response = parseErrors(responseBody);
			}
		} catch (IOException e) {
			LOGGER.error("Error loading walklist", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_WALKLIST_MESSAGE)));
		} finally {
			syncClient.close();
		}
		return Pair.create(subjects, response);
	}

	/**
	 * Save current GpsLocation
	 * We want to know where canvasser is
	 * Inserts request to the queue and tries to send it to the server
	 * @param location
	 * @throws Exception
	 */
	public void saveGpsLocation(final GpsLocation location) throws Exception {
		if(Strings.isEmpty(username) || Strings.isEmpty(password)) {
			LOGGER.debug("Not saving GPS information, not logged in");
			return;
		}
		Request request = createRequest(location, createUrl("events", "gps"));
		insert(request);
	}

	/**
	 * Save Petition Answers
	 * Inserts request to the queue and tries to send it to the server
	 * @param answers
	 * @throws Exception
	 */
	public void savePetitionAnswers(PetitionAnswers answers) throws Exception {
		Tracker.appendLog(Tracker.getTime() + " " + answers.toJson());
		Request request = createRequest(answers, createUrl("petition", "answers"));
		insert(request);
	}

	private Response parseErrors(String message) {
		Response response;
		try {
			response = parser.toResponse(message);
		} catch (JsonParseException e) {
			List<ResponseError> responseErrors = new ArrayList<ResponseError>();
			ResponseError error = new ResponseError("JsonParse", applicationContext.getString(R.string.FAILED_PARSE_MESSAGE) + message);
			responseErrors.add(error);
			response = new Response(responseErrors);
		}
		return response;
	}

	/**
	 * Construct Request
	 * @param requestBody
	 * @param url
	 * @return
	 */
	private Request createRequest(Json requestBody, String url) {
		return new Request(url, Request.Verb.PUT, requestBody.toJson(), getHeaders());
	}

	/**
	 * Construct full server Url
	 * @param actions
	 * @return
	 */
	private String createUrl(String... actions) {
		baseURL = ((GlobalData) GlobalData.getContext()).getBaseUrl();
		StringBuilder urlBuilder = new StringBuilder(baseURL);
		for (String action : actions) {
			urlBuilder.append("/" + action);
		}
		return urlBuilder.toString();
	}

	/**
	 * Add params to the full server Url
	 * @param baseUrl
	 * @param queryParams
	 * @return
	 */
	// NOTE: use Uri.Builder instead
	private String addQueryParams(String baseUrl, Pair<String, String>... queryParams) {
		StringBuilder urlBuilder = new StringBuilder(baseUrl);
		String paramSeparator = "?";
		for (Pair<String, String> queryParam : queryParams) {
			String paramValueEncodedString = null;
			try {
				paramValueEncodedString = URLEncoder.encode(queryParam.second, "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			}
			if (null != paramValueEncodedString) {
				urlBuilder.append(paramSeparator).append(queryParam.first).append("=").append(paramValueEncodedString);
			}
			paramSeparator = "&";
		}
		return urlBuilder.toString();
	}

	/**
	 * Provides GlobalData singleton object.
	 */
	private GlobalData getGlobalData() {
		return (GlobalData) GlobalData.getContext();
	}

	/**
	 * Provides AppointmentCalendar object loaded from the backend from GlobalData singleton object.
	 */
	protected AppointmentCalendar getAppointmentCalendar() {
		return getGlobalData().getAppointmentCalendar();
	}

	/**
	 * Sync loads nearest neighbor
	 * @return - Pair of nearest neighbor and server response
	 */
	public Pair<NearestNeighborInfo, Response> syncLoadNearestNeighbor() {
		String url = createUrl(NEARESTNEIGHBOR_ENDPOINT);
		Location currentLocation = LocationProcessor.getLastKnownLocation(getGlobalData().getApplicationContext());
		url = addQueryParams(url, Pair.create(CURRENT_LATITUDE, Double.toString(currentLocation.getLatitude())), Pair.create(CURRENT_LONGITUDE, Double.toString(currentLocation.getLongitude())), Pair.create("toprows", Integer.toString(5)));

		HttpUriRequest getNearestNeighborRequest = new HttpGet(url);
		addHeadersToRequest(getNearestNeighborRequest);
		NearestNeighborInfo nearestneighborinfo = null;
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		setSyncTimeout(syncClient, STANDARD_SYNC_CONNECTION_TIMEOUT_SECONDS, STANDARD_SYNC_SO_TIMEOUT);
		try {
			LOGGER.debug("Attempting nearest neighbor GET");
			HttpResponse serverResponse = syncClient.execute(getNearestNeighborRequest);
			String responseBody = getResponseBody(serverResponse);
			String neighborInfoResponseBody = responseBody;

			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) { //empty petition body is a failure
				LOGGER.debug("Nearest Neighbor json: " + responseBody);
				try {
					JSONObject obj = new JSONObject(responseBody);
					neighborInfoResponseBody = (obj.get("NearestNeighbors")).toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				nearestneighborinfo = parser.toNearestNeighbor(neighborInfoResponseBody);
			} else {
				response = parseErrors(neighborInfoResponseBody);
				LOGGER.error("Locations response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}finally {
			syncClient.close();
		}
		return Pair.create(nearestneighborinfo, response);
	}


	/**
	 * Sync loads question tally
	 * @param questionId
	 * @return response
	 */
	public String syncLoadQuestionTally(String questionId)
	{
		String url = createUrl("petition/get-question-tally?QuestionId=" + questionId);
		//String url = createUrl("petition/get-question-tally?QuestionId=10");
		HttpUriRequest questionTallyRequest = new HttpGet(url);
		addHeadersToRequest(questionTallyRequest);

		AndroidHttpClient syncClient = getSyncClient();
		String responseBody = "";

		Response response = null;

		try 
		{
			HttpResponse serverResponse = syncClient.execute(questionTallyRequest);
			responseBody = getResponseBody(serverResponse);

			//LOGGER.debug("RESPONSE " + serverResponse.getStatusLine() + " " + responseBody);

			if (!(isSuccess(serverResponse) 
					&& Strings.isNotEmpty(responseBody) && !responseBody.equals("null"))) //empty petition body is a failure
			{ 
				response = parseErrors(responseBody);
				LOGGER.error("Response error (" + serverResponse.getStatusLine() + "): " + responseBody);

				responseBody = "Unsuccessful";
			}
		} 
		catch (ClientProtocolException cpe) 
		{
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
			}
			LOGGER.error("Protocol exception", cpe);
		}
		catch (IOException e) 
		{
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", applicationContext.getString(R.string.IOEXCEPTION_WHILE_LOADING_PETITION_MESSAGE)));
		}
		finally 
		{
			syncClient.close();
		}

		return responseBody;
	}

	
	
	public String validateEmail(String email){
		email = email.replaceAll(" ", "");
		Log.e("about to verify email", "sk about to verify email "+email);
		HttpUriRequest verifyEmailRequest = new HttpGet(createUrl(VALIDATE_EMAIL_ENDPOINT)+"?email="+email);

//		new HttpGet(createUrl(VALIDATE_EMAIL_ENDPOINT));
//		HttpUriRequest verifyEmailRequest = new HttpGet("http://testing.grassrootsunwired.com/api/utils/verify-email" + "?email="+email);
		Log.e("verify email requ","sk url "+verifyEmailRequest.getURI());
		addHeadersToRequest(verifyEmailRequest);
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		String responseBody = null;
		setSyncTimeout(syncClient, 5, 5);
		try {
			LOGGER.debug("Attempting verify email GET");
			HttpResponse serverResponse = syncClient.execute(verifyEmailRequest);
			responseBody = getResponseBody(serverResponse);

			Log.e("sk","sk verify email : "+responseBody);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) {

				LOGGER.debug("Verify Email json: " + responseBody);
				JSONObject obj = null;
				try {
					obj = new JSONObject(responseBody);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (obj != null) {
					Iterator<String> keys = obj.keys();

				}
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Validation email response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", "IO Exception while validating email"));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", "IO Exception while validating email"));
		}finally {
			syncClient.close();
		}

		 return responseBody;
	}
	
	
	
	
	public String validatePhoneNumber(String phoneNumber){

		Log.e("about to verify phone number", "sk about to verify phone "+phoneNumber);
		HttpUriRequest verifyPhoneRequest = new HttpGet(createUrl("utils/verify-phone")+"?phone="+phoneNumber);

		addHeadersToRequest(verifyPhoneRequest);
		Response response = null;
		AndroidHttpClient syncClient = getSyncClient();
		String responseBody = null;
		setSyncTimeout(syncClient, 5, 5);


		try {
			LOGGER.debug("Attempting verify email GET");
			HttpResponse serverResponse = syncClient.execute(verifyPhoneRequest);
			responseBody = getResponseBody(serverResponse);

			Log.e("sk","sk verify phone : "+responseBody);
			LOGGER.debug(serverResponse.getStatusLine());

			if (isSuccess(serverResponse) && Strings.isNotEmpty(responseBody)) {

				LOGGER.debug("Verify Phone json: " + responseBody);
				JSONObject obj = null;
				try {
					obj = new JSONObject(responseBody);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (obj != null) {
					Iterator<String> keys = obj.keys();

				}
			} else {
				response = parseErrors(responseBody);
				LOGGER.error("Validation phone response error (" + serverResponse.getStatusLine() + "): " + response);
			}
		} catch (ClientProtocolException cpe) {
			boolean isInvalidCredentials = cpe.getCause() instanceof MalformedChallengeException;
			if (isInvalidCredentials) {
				response = new Response(new ResponseError("AuthorizationError", "{\"errors\": [{ \"type\": \"auth\", \"message\": \"Invalid username or password.\" }]}"));
			} else {
				response = new Response(new ResponseError("ServerError", "IO Exception while validating phone"));
			}
			LOGGER.error("Protocol exception", cpe);
		} catch (IOException e) {
			LOGGER.error("IOException", e);
			response = new Response(new ResponseError("ServerError", "IO Exception while validating phone"));
		}finally {
			syncClient.close();
		}

		return responseBody;

	}

}

