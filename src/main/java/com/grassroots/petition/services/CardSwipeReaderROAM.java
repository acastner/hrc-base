package com.grassroots.petition.services;

import java.util.HashMap;

import android.content.Context;
import android.util.Log;

import com.bbpos.swiper.SwiperController;
import com.bbpos.swiper.SwiperController.DecodeResult;
import com.bbpos.swiper.SwiperController.SwiperStateChangedListener;
import com.grassroots.petition.activities.BaseCreditCardSwipeActivity;
import com.grassroots.petition.models.CardData;
import com.grassroots.petition.services.CardSwipeReaderMagTek.CardSwipeListener;
import com.grassroots.petition.views.SwipeView;
import com.grassroots.petition.views.SwipeView.ViewListener;
/**
 * Lsitener for card swipe events -ROAM card reader
 * @author sharika
 *
 */
public class CardSwipeReaderROAM implements SwiperStateChangedListener {

    private SwipeView view;
    private CardSwipeListener cardListener;
    private String ksn, encTrack;
    
    public CardSwipeReaderROAM(SwipeView view, CardSwipeListener cardListener) {
        this.view = view;
        this.cardListener = cardListener;
    }

    public CardSwipeReaderROAM() {
		// TODO Auto-generated constructor stub
	}
    
	@Override
    public void onCardSwipeDetected() {
        Log.e("onCardSwipeDetected","onCardSwipeDetected");
        view.setStatus("Processing...");

    }

    @Override
    public void onDecodeCompleted(HashMap<String, String> decodeData) {

        Log.e("decoded data : ","decoded data : "+decodeData.keySet());
        String cardholdername = decodeData.get("cardholderName");
        
        String track1status = decodeData.get("track1Status");
        String track2Status = decodeData.get("track2Status");
        String ksn = decodeData.get("ksn");
        this.ksn = ksn;


        String formatID = decodeData.get("formatID");
        String encTrack = decodeData.get("encTrack");
        this.encTrack = encTrack;
        String partialTrack = decodeData.get("partialTrack");
        String maskedpan = decodeData.get("maskedPAN");
        String packed = SwiperController.packEncTrackData(formatID,encTrack, partialTrack);
        String expiry = decodeData.get("expiryDate");
        Log.e("onDecodeCompleted packed","packed data : "+packed);
        view.setStatus("Done decoding");
        Log.e("cardholder : ","cardholdername : "+cardholdername);
        Log.e("track1status : ","track1status : "+track1status);
        Log.e("track2Status : ","track2Status : "+track2Status);
        Log.e("maskedpan : ","maskedpan : "+maskedpan);


        Log.e("ksn : ","ksn : "+ksn);
        Log.e("format id :","format id : "+formatID);
        Log.e("enc track","enc track : "+encTrack);
        Log.e("partial track :","partial track : "+partialTrack);


        cardListener.onReceiveCardData(new CardData("roam", ksn, null, null,cardholdername,encTrack, expiry, maskedpan));
        

    }

    @Override
    public void onDecodeError(DecodeResult decodeResult) {
        if (decodeResult == DecodeResult.DECODE_SWIPE_FAIL){
            Log.e("decodeResult","decodeResult:DECODE_SWIPE_FAIL");
        }else if (decodeResult == DecodeResult.DECODE_CRC_ERROR){
            Log.e("decodeResult","decodeResult:DECODE_CRC_ERROR");
        }else if (decodeResult == DecodeResult.DECODE_COMM_ERROR){
            Log.e("decodeResult","decodeResult:DECODE_COMM_ERROR");
        }else{
            Log.e("decodeResult","decodeResult:unknown");
        }
        view.setStatus("Decode error");
    }

    @Override
    public void onDevicePlugged() {
        Log.e("onDevicePlugged","onDevicePlugged");
        view.setStatus("Device plugged");

    }

    @Override
    public void onDeviceUnplugged() {
        Log.e("onDeviceUnplugged","onDeviceUnplugged");
        view.setStatus("Device unplugged");

    }

    @Override
    public void onError(String arg0) {
        Log.e("onError","onError"+arg0);

    }

    @Override
    public void onGetKsnCompleted(String arg0) {
        Log.e("onGetKsnCompleted","onGetKsnCompleted" + arg0);

    }

    @Override
    public void onInterrupted() {
        Log.e("onInterrupted","onInterrupted");

    }

    @Override
    public void onNoDeviceDetected() {
        Log.e("onNoDeviceDetected","onNoDeviceDetected");

    }

    @Override
    public void onSwiperHere(boolean arg0) {
        Log.e("onSwiperHere","onSwiperHere : "+arg0);
    }

    @Override
    public void onTimeout() {
        Log.e("onTimeout","onTimeout");
        view.setStatus("Timeout");
    }

    @Override
    public void onWaitingForCardSwipe() {
        Log.e("onWaitingForCardSwipe","onWaitingForCardSwipe");
        view.setStatus("Waiting For CardSwipe");
    }

    @Override
    public void onWaitingForDevice() {
        Log.e("onWaitingForDevice","onWaitingForDevice");
        view.setStatus("Waiting For Device");
    }

}
