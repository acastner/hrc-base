package com.grassroots.petition.services;


import android.util.Base64;
import com.grassroots.petition.models.CardData;
import com.magtek.mobile.android.scra.MTSCRAException;
import com.magtek.mobile.android.scra.MagTekSCRA;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import com.magtek.mobile.android.scra.SCRAConfiguration;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;

/**
 *	Reader for MagTek 
 *	Based on CardSwipeReader for IDTech/Unimag and 
 *	<br>	almost all code come from	MagTekDemo.java( MagTek DEMO)
 *
 */
public class CardSwipeReaderMagTek {

	//	Based on MagTek audio reader prog. ref. manual for android of September 2012 
	public static interface CardSwipeListener {
		public void onStartSwipe();
		public void onReceiveCardData(CardData cardData);
		public void onFailure(int index, String msg);
		public void onConnecting();
		public void onConnected();
		public void onDisconnected();
		public void onTimeout(String msg);			// no such equivalence function found in MagTek
	}

	private AudioManager audioManager;

	private MagTekSCRA mMTSCRA;
	private Handler magTekDataHandler = new Handler(new SCRAHandlerCallback());

    private boolean headsetPluggedIn;
    private boolean swipeSuccess;

	private int mIntCurrentVolume;

    private static final String TAG = CardSwipeReaderMagTek.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

	private final Context context;
	private final CardSwipeListener listener;

	public CardSwipeReaderMagTek (Context context, CardSwipeListener listener) {
		this.context = context;
		this.listener = listener;
        InitializeData();
	}

	private void InitializeData() 
	{
        mMTSCRA = new MagTekSCRA(magTekDataHandler);
        mMTSCRA.setDeviceType(MagTekSCRA.DEVICE_TYPE_AUDIO);

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mMTSCRA.clearBuffers();
        headsetPluggedIn = false;
        swipeSuccess = false;
        minVolume();
	}


	/**
	 * set manually configuration parameters based on Model of Android device.
	 * <br>For HUAWEI MEDIAPAD device it does not have any so it does nothing
	 * 
	 * @throws MTSCRAException
	 */
	void setAudioConfigManual()throws MTSCRAException
	{
		String model = android.os.Build.MODEL.toUpperCase();
        LOGGER.debug("setAudioConfigManual Model=" + model );

		try
		{
			if(model.contains("DROID RAZR") || model.toUpperCase().contains("XT910"))
			{
                LOGGER.debug("Found Setting for :"  + model);
				mMTSCRA.setConfigurationParams("INPUT_SAMPLE_RATE_IN_HZ=48000,");
			}
			else if ((model.equals("DROID PRO"))||
					(model.equals("MB508"))||
					(model.equals("DROIDX"))||
					(model.equals("DROID2"))||
					(model.equals("MB525")))
			{
                LOGGER.debug("Found Setting for :"  + model);
				mMTSCRA.setConfigurationParams("INPUT_SAMPLE_RATE_IN_HZ=32000,");
			}    	
			else if ((model.equals("GT-I9300"))||//S3 GSM Unlocked
					(model.equals("SPH-L710"))||//S3 Sprint
					(model.equals("SGH-T999"))||//S3 T-Mobile
					(model.equals("SCH-I535"))||//S3 Verizon
					(model.equals("SCH-R530"))||//S3 US Cellular
					(model.equals("SAMSUNG-SGH-I747"))||// S3 AT&T
					(model.equals("M532"))||//Fujitsu
					(model.equals("GT-P3113")))//Galaxy Tab 2, 7.0

			{
                LOGGER.debug("Found Setting for :"  + model);
				mMTSCRA.setConfigurationParams("INPUT_AUDIO_SOURCE=VRECOG,");
			}else if(model.equals("HUAWEI MEDIAPAD")){
                LOGGER.debug("Found known HUAWEI MEDIAPAD");
                String config = "";
                //config += "INPUT_WAVE_FORM=0,";
                //config += "INPUT_SAMPLE_RATE_IN_HZ=32000,";
                //config += "INPUT_AUDIO_SOURCE=VRECOG,";
                //config += "INPUT_AUDIO_SOURCE=VCALL,";
                //config += "INPUT_AUDIO_SOURCE=MIC,";
                //mMTSCRA.setConfigurationParams(config);
            }else{
                LOGGER.debug("No previously known device found, setting default audio parameters");
                //mMTSCRA.setConfigurationParams("INPUT_AUDIO_SOURCE=VRECOG,");
            }

            LOGGER.debug("end of setting");
		}
		catch(MTSCRAException ex)
		{
            LOGGER.error("Audio configuration exception:", ex);
			throw new MTSCRAException(ex.getMessage());
		}

	}


    public boolean isHeadsetPluggedIn() {
        return headsetPluggedIn;
    }

    public void setHeadsetPluggedIn(boolean headsetPluggedIn) {
        this.headsetPluggedIn = headsetPluggedIn;
    }

	/**
	 * Enable/Disable MagTek reader
	 */
	public void openReader() {
	    if(headsetPluggedIn && !mMTSCRA.isDeviceConnected())
				openDevice();
	}

    public void closeReader(){
        if (mMTSCRA != null)
            closeDevice();
    }

    public void reset()
    {
        magTekDataHandler = new Handler(new SCRAHandlerCallback());
        mMTSCRA = new MagTekSCRA(magTekDataHandler);
        mMTSCRA.setDeviceType(MagTekSCRA.DEVICE_TYPE_AUDIO);
        mMTSCRA.clearBuffers();


    }

	private void openDevice()
	{
        LOGGER.debug("openDevice()");

        if(!headsetPluggedIn){
            LOGGER.debug("Asked to open but no device plugged in");
            return;
        }

		try
		{
			setAudioConfigManual();
            maxVolume();
			mMTSCRA.openDevice();
		}
		catch(MTSCRAException ex)
		{
            LOGGER.error("openDevice() Exception", ex);
		}

        if(!mMTSCRA.isDeviceConnected()){
            LOGGER.error("Device opened but not connected");
            listener.onDisconnected();
        }
	}

	private void closeDevice()
	{
        LOGGER.debug("Closing device");
        mMTSCRA.clearBuffers();
		mMTSCRA.closeDevice();
        minVolume();
	}


    public boolean isReady()
    {
        if(isHeadsetPluggedIn() &&
                mMTSCRA != null &&
                mMTSCRA.isDeviceConnected())
            return true;
        else
            return false;

    }
	/**
	 * 		show Log of data read 
	 */
	private void logResponseData()
	{
		String strVersion = "";
		try
		{
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			strVersion =  pInfo.versionName;

		}
		catch(Exception ex)
		{

		}
		String strLog = "App.Version=" +strVersion + ",SDK.Version=" + mMTSCRA.getSDKVersion();
        LOGGER.debug(strLog);



		String strResponse =  mMTSCRA.getResponseData();
		if(strResponse ==null) {
            LOGGER.warn("response null");
			return;
		}


		LOGGER.debug("Response.Length=" +strResponse.length() );

		LOGGER.debug( "EncryptionStatus=" + mMTSCRA.getEncryptionStatus() );
		LOGGER.debug("SDK.Version=" + mMTSCRA.getSDKVersion() );
		LOGGER.debug("Reader.Type=" + mMTSCRA.getDeviceType() );
		LOGGER.debug("Track.Status=" + mMTSCRA.getTrackDecodeStatus() );
		LOGGER.debug("KSN=" + mMTSCRA.getKSN());
		LOGGER.debug("Track1.Masked=" + mMTSCRA.getTrack1Masked() );
		LOGGER.debug("Track2.Masked=" + mMTSCRA.getTrack2Masked() );
		LOGGER.debug("Track3.Masked=" + mMTSCRA.getTrack3Masked() );
		LOGGER.debug("Track1.Encrypted=" + mMTSCRA.getTrack1() );
		LOGGER.debug("Track2.Encrypted=" + mMTSCRA.getTrack2() );
		LOGGER.debug("Track3.Encrypted=" + mMTSCRA.getTrack3() );
		LOGGER.debug("MagnePrint.Encrypted=" + mMTSCRA.getMagnePrint() );
		LOGGER.debug("MagnePrint.Status=" + mMTSCRA.getMagnePrintStatus() );
		LOGGER.debug("Card.IIN=" + mMTSCRA.getCardIIN() );
		LOGGER.debug("Card.Name=" + mMTSCRA.getCardName() );
		LOGGER.debug("Card.Last4=" + mMTSCRA.getCardLast4() );
		LOGGER.debug("Card.ExpDate=" + mMTSCRA.getCardExpDate() );
		LOGGER.debug("Card.SvcCode=" + mMTSCRA.getCardServiceCode() );
		LOGGER.debug("Card.PANLength=" + mMTSCRA.getCardPANLength() );
		LOGGER.debug("Device.Serial=" + mMTSCRA.getDeviceSerial());
        LOGGER.debug("Device MagTek serial=" + mMTSCRA.getMagTekDeviceSerial());
        LOGGER.debug("Battery Level=" + mMTSCRA.getBatteryLevel());

		LOGGER.debug("SessionID=" + mMTSCRA.getSessionID() );

		LOGGER.debug("Card.Status=" + mMTSCRA.getCardStatus() );
		LOGGER.debug("Firmware.Partnumber=" + mMTSCRA.getFirmware());
		LOGGER.debug("MagTek.SN=" + mMTSCRA.getMagTekDeviceSerial());
		LOGGER.debug("TLV.Version=" + mMTSCRA.getTLVVersion());
		LOGGER.debug("HashCode=" + mMTSCRA.getHashCode());
        LOGGER.debug("");
		String tstrTkStatus = mMTSCRA.getTrackDecodeStatus();
		String tstrTk1Status="01";
		String tstrTk2Status="01";
		String tstrTk3Status="01";

		//  JJJJJJJ
		if(tstrTkStatus.length() ==6)
		{
			tstrTk1Status=tstrTkStatus.substring(0,2);
			tstrTk2Status=tstrTkStatus.substring(2,4);
			tstrTk3Status=tstrTkStatus.substring(4,6);
			LOGGER.debug("Track1.Status=" + tstrTk1Status );
			LOGGER.debug("Track2.Status=" + tstrTk2Status );
			LOGGER.debug("Track3.Status=" + tstrTk3Status );
		} else {
			LOGGER.debug("Track status length=" +  tstrTkStatus.length() );
			LOGGER.debug("Track status=" +  tstrTkStatus );
		}
			
		
		LOGGER.debug("Response.Raw=" + strResponse );


	}    

	/**
	 * check for Decode status of Track 1 and 2
	 * 
	 * @return	true 	= ok
	 * <br> 	false	= error or blank
	 */
	private boolean isTrackOneTwoDecodeStatusOk() {
		String tstrTkStatus = mMTSCRA.getTrackDecodeStatus();
		String tstrTk1Status;
		String tstrTk2Status;

        LOGGER.debug("Track status length=" +  tstrTkStatus.length() );
		LOGGER.debug( "Track status=" +  tstrTkStatus );
		
		if(tstrTkStatus.length() != 6)
		{
			return false;
		}
		
		//  should be 00 to be track 1 and 2 to be OK
		tstrTk1Status=tstrTkStatus.substring(0,2);
		tstrTk2Status=tstrTkStatus.substring(2,4);
		LOGGER.debug("Track1.Status=" + tstrTk1Status );
		LOGGER.debug("Track2.Status=" + tstrTk2Status );
		return tstrTk1Status.equalsIgnoreCase("00") && 
				tstrTk2Status.equalsIgnoreCase("00")				;
	}
	

	public void maxVolume()
	{
        LOGGER.debug("Max volume");
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),AudioManager.FLAG_SHOW_UI);


	}
	public void minVolume()
	{
        LOGGER.debug("Min volume");
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_SHOW_UI);
	}


    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private byte[] hexStringToByteArray2(String data)
    {
        byte[] results = new byte[data.length() / 2];

        for (int i = 0, k = 0; i + 1 < data.length(); i += 2, k++)
        {
            results[k] = (byte) (Character.digit(data.charAt(i), 16) << 4);
            results[k] += (byte) (Character.digit(data.charAt(i + 1), 16));
        }

        return results;
    }

	/**
	 * 
	 * Handler to intercept MagTek status
	 *
	 */
	private class SCRAHandlerCallback implements Callback {
		public boolean handleMessage(Message msg) 
		{
			try
			{
				switch (msg.what) 
				{
				case MagTekSCRA.DEVICE_MESSAGE_STATE_CHANGE:
					switch (msg.arg1) {
					case MagTekSCRA.DEVICE_STATE_CONNECTED:
                        LOGGER.debug("DEVICE HANDLER: Connected");
						listener.onConnected();
						break;
					case MagTekSCRA.DEVICE_STATE_CONNECTING:
                        LOGGER.debug("DEVICE HANDLER: Connecting");
						listener.onConnecting();
						break;
					case MagTekSCRA.DEVICE_STATE_DISCONNECTED:
                        LOGGER.debug("DEVICE HANDLER: Disconnected");
                        // TODO: this should be more complex and handle a swipe success properly.
                        if(swipeSuccess){
                            LOGGER.debug("I am ignoring a disconnect event as I was just swiped!");
                            swipeSuccess = false;
                            break;
                        }
						listener.onDisconnected();
						break;
					}
					break;
				case MagTekSCRA.DEVICE_MESSAGE_DATA_START:
                    LOGGER.debug("DEVICE HANDLER: Data Start");
					if (msg.obj != null)
					{
						listener.onStartSwipe();
						return true;
					}
					break;
				case MagTekSCRA.DEVICE_MESSAGE_DATA_CHANGE:
                    LOGGER.debug("DEVICE HANDLER: Data Change");
					if (msg.obj != null) 
					{
                        LOGGER.debug("Transfer ended");
						logResponseData();
						
//						check for track 1 and 2 status 						
						if (!isTrackOneTwoDecodeStatusOk()) {
                            LOGGER.error("track Error or blank" );
							listener.onFailure(0, "Track decode error... Please Swipe Again." );
							return true;
						}

						String device = "MagTekADynamo"; // Override for Propay
						//String serial = mMTSCRA.getDeviceSerial();
                        String serial = mMTSCRA.getKSN();
						String track1 = mMTSCRA.getTrack1();
						String track2 = mMTSCRA.getTrack2();

                        LOGGER.debug("track1=" + track1);
                        LOGGER.debug("track2=" + track2);

                        // Base64 it up
                        //serial = Base64.encodeToString(serial.getBytes(Charset.forName("US-ASCII")), Base64.NO_WRAP);

                        //track1 = Base64.encodeToString(track1.getBytes(Charset.forName("US-ASCII")), Base64.NO_WRAP);
                        //track2 = Base64.encodeToString(track2.getBytes(Charset.forName("US-ASCII")), Base64.NO_WRAP);

                        //track1 = Base64.encodeToString(hexStringToByteArray2(track1), Base64.NO_WRAP);
                        //track2 = Base64.encodeToString(hexStringToByteArray2(track2), Base64.NO_WRAP);

                        // 	will be closed normally so do not handle disconnected
						//mbDeviceClosedNormallyAfterSwipe = true;
                        swipeSuccess = true;
						listener.onReceiveCardData(new CardData(device, serial, track1, track2 ,null,null));

						msg.obj=null;
						return true;
					}
					break;  
				case MagTekSCRA.DEVICE_MESSAGE_DATA_ERROR:
                    LOGGER.error("DEVICE HANDLER: DATA ERROR");
					listener.onFailure(0, "Card Swipe Error... Please Swipe Again." );
                    String status = mMTSCRA.getTrackDecodeStatus();
                    String device = mMTSCRA.getCardStatus();
                    String foo = (String)msg.obj;

					return true;

                case MagTekSCRA.DEVICE_INACTIVITY_TIMEOUT:
                        return true;
				default:
					if (msg.obj != null) 
					{
						return true;
					}
					break;
				};

			}
			catch(Exception ex)
			{
                LOGGER.error("Unknown error", ex);
			}

			return false;
		}
	}
}
