package com.grassroots.petition.services;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.grassroots.petition.utils.ClassRetriever;
import org.apache.log4j.Logger;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Andrey Sotnikov
 * Date: 25.07.13
 * Time: 15:50
 * Midnight Promt Service
 * Receives actions from alarm manager
 * Manages 12.00am alarm
 */
public class MidnightPromptService extends BroadcastReceiver {

    public static final String LOGOUT_PROMT_ACTION = "MidnightPromptServiceAction";

    private static final String TAG = MidnightPromptService.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    @Override
    public void onReceive(final Context context, Intent intent) {
        LOGGER.debug("Received Midnight alarm action");
        final String action = intent.getAction();

        if (LOGOUT_PROMT_ACTION.equals(action)) {
            Intent i = new Intent();
            i.setClassName(context, ClassRetriever.getLogoutPromtActivity(context).getName());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    /**
     * Sets alarm at 12.00am
     * @param context
     */
    public static void startManager(Context context) {
        LOGGER.debug("Setting midnight alarm");
        Calendar cal =  Calendar.getInstance();
        //set timer you want alarm to work (here I have set it to 12.00am)
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);

        Intent startMidnightServiceIntent = new Intent(MidnightPromptService.LOGOUT_PROMT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, startMidnightServiceIntent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
    }

    /**
     * Cancels previously set alarm
     * @param context
     */
    public static void stopManager(Context context) {
        LOGGER.debug("Stopping midnight alarm");
        Intent stopMidnightServiceIntent = new Intent(context, MidnightPromptService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, stopMidnightServiceIntent, 0);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Activity.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}
