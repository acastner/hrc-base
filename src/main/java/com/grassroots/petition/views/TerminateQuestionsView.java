package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.grassroots.petition.R;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.TypeFaceSetter;

import java.util.List;

/**
 * TerminateQuestionsView
 * Show a scrollable list of termination questions (or really any questions on a submit page)
 * Appears on terminate button tap
 */
public class TerminateQuestionsView extends RelativeLayout {
    /**
     * TerminateQuestionsView listener interface
     */
    public interface ViewListener {
        public void onReview(List<SubjectAnswer> answers);
    }

    /**
     * questionListView listener
     */
    private QuestionListView questionListView;
    /**
     * TerminateQuestionsView listener
     */
    private ViewListener listener;

    /**
     * Default constructor
     * @param context
     */
    public TerminateQuestionsView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public TerminateQuestionsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public TerminateQuestionsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * TerminateQuestionsView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Set list of terminate questions
     * @param questions - list of terminate questions
     * @param petitionAnswers - petitionAnswers
     */
    public void setQuestions(List<Question> questions, PetitionAnswers petitionAnswers) {
        questionListView.setQuestions(questions, petitionAnswers);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        questionListView = (QuestionListView) findViewById(R.id.question_list);

        findViewById(R.id.submit_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onReview(questionListView.getAnswers());


            }
        });
    }
}
