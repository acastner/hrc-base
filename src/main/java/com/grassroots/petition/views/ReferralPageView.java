package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.utils.TypeFaceSetter;

import java.util.HashMap;
import java.util.Map;

/**
 * Allow for entering referral information
 */
public class ReferralPageView extends LinearLayout {
    /**
     * Referral full name
     */
    private EditText fullNameText;
    /**
     * Referral phone
     */
    private EditText phoneText;
    /**
     * Referral email
     */
    private EditText emailText;

    /**
     * Default constructor
     * @param context
     */
    public ReferralPageView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public ReferralPageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Return information about Referrer (name, phone, email)
     * @return map of question name
     */
    public Map<String, String> getReferrerInformation() {
        Map<String, String> referrerInfo = new HashMap<String, String>();
        referrerInfo.put(Question.REFERRER_NAME, fullNameText.getText().toString());
        referrerInfo.put(Question.REFERRER_PHONE, phoneText.getText().toString());
        referrerInfo.put(Question.REFERRER_EMAIL, emailText.getText().toString());
        return referrerInfo;
    }

    /**
     * Clear information about referrer (name, phone, email)
     */
    public void clearReferralData() {
        fullNameText.setText("");
        phoneText.setText("");
        emailText.setText("");
    }

    /**
     * Set custom font onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        fullNameText = (EditText) findViewById(R.id.full_name);
        phoneText = (EditText) findViewById(R.id.phone);
        emailText = (EditText) findViewById(R.id.email);
    }
}
