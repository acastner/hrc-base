package com.grassroots.petition.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * View to display a question and it's answers
 * 
 * TODO: Convert to a fragment. This custom view stuff makes no sense.
 */
public class QuestionView extends LinearLayout {
    public static final String HTML_MIME_TYPE = "text/html";
    public static final String HTML_BEGIN = "<html><body><center><b>";
    public static final String HTML_END = "</b></center></body></html>";

    /**
     * Petition Answers
     */
    PetitionAnswers petitionAnswers;

    /**
     * WebView for displaying HTML encoded questions.
     */
    private WebView formattedQuestionWebview;
    /**
     * Shows question with answers
     */
    private QuestionAnswersView answersView;
    /**
     * Header / title text
     */
    private TextView title;
    private BoundedScrollView questionScrollView;

    /**
     * Default constructor
     * @param context
     */
    public QuestionView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public QuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    @SuppressLint("NewApi")
	public QuestionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Set question on View
     * @param question - question to display
     */
    public void setQuestion (Question question)
    {
        // Set header
        title.setText( question.getName() );

        // If the question text is empty, hide the question view
        String questionText = question.getText( );
        if( TextUtils.isEmpty( questionText ) ) {
            hideQuestionView();
        } else {
            showQuestionView();
        }

        loadHtmlFormattedQuestion( questionText );
        answersView.setQuestion( question );
    }

    /**
     * Set question view max height
     * @param maxHeight - height in pixels
     */
    public void setMaxQuestionHeight (int maxHeight)
    {
        questionScrollView.setMaxHeight( maxHeight );
    }

    private void showQuestionView ()
    {
        formattedQuestionWebview.setVisibility( View.VISIBLE );
    }

    private void hideQuestionView ()
    {
        formattedQuestionWebview.setVisibility( View.GONE );
    }

    private void loadHtmlFormattedQuestion (String questionText)
    {
        hideQuestionView();
        formattedQuestionWebview.loadUrl("about:blank");
        formattedQuestionWebview.loadData( wrapQuestionInHtml( questionText ), HTML_MIME_TYPE, null );
        setWebViewBackgroundColor();
        showQuestionView();
    }

    private String wrapQuestionInHtml (String questionText)
    {
        return HTML_BEGIN + questionText + HTML_END;
    }

    private void setWebViewBackgroundColor ()
    {
        formattedQuestionWebview.setBackgroundColor( Color.TRANSPARENT );
    }

    /**
     * Set question on View, show question number
     * @param question - question to display
     * @param questionNumber - current question number
     */
    public void setQuestion (Question question, int questionNumber)
    {
        String text = questionNumber + ". " + question.getText( );
        loadHtmlFormattedQuestion( text );
        answersView.setQuestion( question );
    }

    /**
     * @return answer for current question
     */
    public SubjectAnswer getAnswer() {
        return answersView.getAnswer();
    }

    /**
     * Set picture on view
     * @param photo - picture bitmap
     * @param fileLocation - picture file location
     */
    public void setPhoto(Bitmap photo, String fileLocation) {
        answersView.setPicture(photo, fileLocation);
    }

    /**
     * PetitionAnswers setter
     * @param petitionAnswers
     */
    public void setPetitionAnswers(PetitionAnswers petitionAnswers) {
        this.petitionAnswers = petitionAnswers;
        answersView.setPetitionAnswers(petitionAnswers);
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
        title = (TextView) findViewById( R.id.header_title );

        // Configure question view
        formattedQuestionWebview = (WebView) findViewById(R.id.question_formatted_webview );
        formattedQuestionWebview.setWebViewClient(new WebViewClient() {  
            @Override  
            public void onPageFinished(WebView view, String url)  
            {  
            	//loading an empty javascript function that will resize the webview before rendering"
            	view.loadUrl("javascript:(function() { " +   "})()");             	
            	view.setVisibility(View.VISIBLE);
            }
        });
        questionScrollView = (BoundedScrollView) findViewById( R.id.question_scrollview );
        questionScrollView.setMaxHeight( getResources().getInteger( R.integer.max_question_view_height_in_pixels ) );
        showQuestionView();

        answersView = (QuestionAnswersView) findViewById(R.id.question_answers);
    }

    public void setPhotoViewListener(PictureQuestionView.ViewListener viewListener) {
        ((PictureQuestionView)findViewById(R.id.picture)).setViewListener(viewListener);
    }

    public void setSignatureQuestion(Question question) {
        // Set header
        title.setText( question.getName() );

        // If the question text is empty, hide the question view
        String questionText = question.getText( );
        if( TextUtils.isEmpty( questionText ) ) {
            hideQuestionView();
        } else {
            showQuestionView();
        }

        loadHtmlFormattedQuestion( questionText );
        answersView.setSignatureQuestion( question );
        
    }

    public Signature getSignature() {
        // TODO Auto-generated method stub
        return answersView.getSignature();
    }
}
