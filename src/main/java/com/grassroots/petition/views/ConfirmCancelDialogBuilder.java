package com.grassroots.petition.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;

import java.util.List;
import java.util.Map;

/**
 * ConfirmCancelDialogBuilder
 * Contains several convenience methods for generating various kinds of dialogs.
 */
public class ConfirmCancelDialogBuilder {

    /**
     * Return themed AlertDialogBuilder (if Android version supports it)
     *
     * @param context - application context
     *
     * @return an AlertDialog.Builder with configured context
     */
    @SuppressWarnings("NewApi")
    public static AlertDialog.Builder getAlertDialogBuilder(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
        } else {
            return new AlertDialog.Builder(context);
        }
    }

    /**
     * Custom listener interface
     * Implement listener to get callback values from dialog
     * @param <T>
     */
    public static interface ViewListener<T> {
        public void onConfirm(T value);
        public void onCancel();
    }

    /**
     * Creates logout dialog, which shows the user that they have 30 seconds to press cancel button
     * or will be logged out automatically.
     *
     * @param context - application context
     * @param listener - ConfirmCancelDialogBuilder.ViewListener
     *
     * @return a dialog with a given listener and context
     */
    public static Dialog createLogoutCancelDialog(Context context, final ViewListener<Void> listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onConfirm(null);
            }
        });
        builder.setTitle(R.string.cancel_logout_question);
        return builder.create();
    }

    /**
     * Creates logout dialog, which asks user to confirm logout
     *
     * @param context - application context
     * @param listener - ConfirmCancelDialogBuilder.ViewListener
     *
     * @return a logout dialog with a given listener and context
     */
    public static Dialog createLogoutConfirmDialog(Context context, final ViewListener<Void> listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onConfirm(null);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onCancel();
            }
        });
        builder.setTitle(R.string.confirm_logout);
        return builder.create();
    }

    /**
     * Creates a multiple choice dialog.
     * Note: Although not used in delmonico core, it may be used by client applications (e.g. complete-solar).
     *
     * @param context - application context
     * @param title - dialog title
     * @param viewListener - view listener
     * @param options - options to show
     *
     * @return a dialog with multiple options.
     */
    public static Dialog createMultipleChoiceDialog (Context context,
                                                     String title,
                                                     final ViewListener<Integer> viewListener,
                                                     String... options)
    {
        AlertDialog.Builder builder = getAlertDialogBuilder( context );
        builder.setTitle( title );

        builder.setPositiveButton( R.string.ok, new
                DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick (DialogInterface dialog, int i)
                    {
                        int selectedPosition =
                                ( (AlertDialog) dialog ).getListView().getCheckedItemPosition();
                        viewListener.onConfirm( selectedPosition );

                    }
                } );
        builder.setSingleChoiceItems( options, -1, new
                DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick (DialogInterface dialogInterface,
                                         int i)
                    {
                    }
                } );
        builder.setNegativeButton( R.string.cancel, new
                DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick (DialogInterface dialogInterface,
                                         int i)
                    {
                        viewListener.onCancel();
                    }
                } );
        return builder.create();
    }

    /**
     * Create an input dialog for missing subject information
     *
     * @param context - application context
     * @param subject - subject object
     * @param subjectInformationQuestions - list of subject information questions
     * @param listener - ConfirmCancelDialogBuilder.ViewListener
     *
     * @return a dialog with missing subject information view configured
     */
    public static Dialog createMissingSubjectInfoDialog(Context context, Subject subject, List<Question> subjectInformationQuestions, final ViewListener<Subject> listener) {
        return createMissingSubjectInfoDialog(context, subject, subjectInformationQuestions, null, listener);
    }

    /**
     * Create an input dialog for missing subject information
     *
     * @param context - application context
     * @param subject - subject object
     * @param subjectInformationQuestions - questions to display
     * @param translateMap translate from the given question var names to the expected subject magic questions
     * @param listener - ConfirmCancelDialogBuilder.ViewListener
     *
     * @return a dialog with missing subject information view configured
     */
    public static Dialog createMissingSubjectInfoDialog(Context context, Subject subject, List<Question> subjectInformationQuestions, Map<String, String> translateMap, final ViewListener<Subject> listener) {
        AlertDialog.Builder infoDialogBuilder = getAlertDialogBuilder(context);

        final SubjectInfoView subjectInfoView = (SubjectInfoView) View.inflate(context, R.layout.subject_info, null);
        infoDialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Subject newSubject = subjectInfoView.getSubject();
                listener.onConfirm(newSubject);
            }
        });
        infoDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onCancel();
            }
        });

        subjectInfoView.setSubject(subject);
        subjectInfoView.setQuestionNameTranslationMap(translateMap);
        subjectInfoView.setSubjectInformationQuestions(subjectInformationQuestions);
        infoDialogBuilder.setView(subjectInfoView);
        infoDialogBuilder.setTitle(R.string.subject_info_header);

        return infoDialogBuilder.create();
    }

    /**
     * Create dialog with single option to choose
     *
     * @param context - application context
     * @param title - title of dialog
     * @param viewListener - ConfirmCancelDialogBuilder.ViewListener
     * @param options
     *
     * @return a dialog with one choice
     */
    public static Dialog createSingleChoiceDialog(Context context, String title, final ViewListener<Integer> viewListener, String... options) {
        AlertDialog.Builder builder = getAlertDialogBuilder(context);
        builder.setTitle(title);

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                viewListener.onConfirm(i);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                viewListener.onCancel();
            }
        });

        return builder.create();
    }

    /**
     * Create dialog with information about credit card safety
     * TODO: Check for usages, this might be dead code
     *
     * @param context - application context
     * @param title - title of dialog
     * @param options
     *
     * @return a dialog that contains information about card safety
     */
    public static Dialog createAboutCardSafetyDialog(Context context, String title, String... options) {
        AlertDialog.Builder builder = getAlertDialogBuilder(context);
        View view = View.inflate(context, R.layout.about_card_safety, null);
        if (title != null) {
            builder.setTitle(title);
        } else {
            builder.setTitle(R.string.about_credit_card_safety_title);
        }

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        builder.setView(view);
        return builder.create();
    }
}
