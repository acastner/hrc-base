package com.grassroots.petition.views;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 09.07.13
 * Time: 13:26
 */

import android.content.Context;
import android.util.AttributeSet;

import android.widget.TextView;
import com.grassroots.petition.R;

public class BusyBoxView extends BaseLinearLayout
{
    public BusyBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setDescription(String title)
    {
        TextView tv = (TextView)findViewById(R.id.description_textview);
        tv.setText(title);
    }
}
