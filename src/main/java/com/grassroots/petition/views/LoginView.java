package com.grassroots.petition.views;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseLoginActivity;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * Login View
 * Shows custom login view layout (username, userpassword, login button,
 * canvass server url and app version)
 */
public class LoginView extends RelativeLayout {
    /**
     * Login View listener interface
     */
    public static interface ViewListener {
        public void onLogin(String username, String password);
    }

    /**
     * User name text field
     */
    private EditText userText;
    /**
     * User password text field
     */
    private EditText passwordText;
    /**
     * Login button
     */
    private Button loginButton;
    /**
     * Login View listener
     */
    private ViewListener viewListener;
    /**
     * ProgressDialog
     */
    private ProgressDialog loadingDialog;
    
    /**
     * alert dialog to select server
     */
    private AlertDialog serverSelectDialog = null;
    
    /**
     * choice of servers
     */
    private CharSequence[] servers = {" Testing "," Staging "," Demo "," Canvass ", " Boulder "
    		, " GBI " , " Fund " , " FW "};
    
    /**
     * selected item in alert dialog to select servers
     */
    private int selectedServer = -1;

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Login View listener setter
     * @param viewListener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /**
     * Set login button enabled if canvass server is available
     * @param isAvailable
     */
    public void setIsServerAvailable(boolean isAvailable) {
        loginButton.setEnabled(isAvailable);
        userText.setEnabled(isAvailable);
        passwordText.setEnabled(isAvailable);
        if (isAvailable) {
            loginButton.setText(R.string.login_button);
        } else {
            loginButton.setText(R.string.ping_check);
        }
    }

    /**
     * Set custom font, button listeners, server url, app version
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        userText = (EditText) findViewById(R.id.txtUser);
        passwordText = (EditText) findViewById(R.id.txtPass);

        userText.setImeOptions(EditorInfo.IME_ACTION_NEXT | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        passwordText.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							InputMethodManager mgr = (InputMethodManager) LoginView.this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
							mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
							LoginView.this.performLogin();
							return true;
						}
						return false;
			}
        });


        if (BaseLoginActivity.autofillCredentials) {
            userText.setText("jm");
            passwordText.setText("jm");
        }

        loginButton = (Button) findViewById(R.id.btnLogin);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                performLogin();
            }
        });

        final TextView view = (TextView) findViewById(R.id.target_server_text);
        
        if( getContext().getString(R.string.buildsetting).equalsIgnoreCase("production")){
        	view.setEnabled(false);
        	
        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_canvass));
        	
        }else if( getContext().getString(R.string.buildsetting).equalsIgnoreCase("internal")){
        	view.setEnabled(true);
        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_staging));
        }
        view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());

        view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Creating and Building the Dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Select server");

                String baseurl = ((GlobalData) GlobalData.getContext()).getBaseUrl();
                selectedServer = -1;
                if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_testing))){
                	selectedServer = 0;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_staging))){
                	selectedServer = 1;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_demo))){
                	selectedServer = 2;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_canvass))){
                	selectedServer = 3;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_boulder))){
                	selectedServer = 4;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_gbi))){
                	selectedServer = 5;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_fund))){
                	selectedServer = 6;
                }else if(baseurl.equalsIgnoreCase(getContext().getString(R.string.base_url_fw))){
                	selectedServer = 7;
                }
                builder.setSingleChoiceItems(servers, selectedServer, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {                  
                    switch(item)
                    {
                        case 0:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_testing));
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());
                            break;
                        case 1:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_staging));
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 2:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_demo));
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 3:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_canvass));           
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 4:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_boulder));           
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 5:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_gbi));           
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 6:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_fund));           
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                        case 7:
                        	((GlobalData) GlobalData.getContext()).setBaseUrl(getContext().getString(R.string.base_url_fw));           
                        	view.setText(((GlobalData) GlobalData.getContext()).getBaseUrl());    
                        	break;
                       
                    }
                    setIsServerAvailable(false);
                	((BaseLoginActivity)(getContext())).startPingServer();
                    serverSelectDialog.dismiss();   
               	    

                    }
                });
                serverSelectDialog = builder.create();
                serverSelectDialog.show();

                
				
			}
		});
       
        this.getContext().getPackageManager();

        try {
            PackageInfo info = this.getContext().getPackageManager().getPackageInfo(this.getContext().getPackageName(), 0);
            TextView version = (TextView) findViewById(R.id.txtVersion);
            version.setText(getContext().getString(R.string.VERSION_TITLE) + " " + info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this.getContext(), getContext().getString(R.string.UNABLE_DETERMINE_APP_VERSION_MESSAGE), Toast.LENGTH_LONG).show();
        }

    }

    protected void performLogin() {
    	String password = getPassword();
        String userName = getUserName();
        if (Strings.isEmpty(password) || Strings.isEmpty(userName)) {
            Toast.makeText(getContext(), getContext().getString(R.string.ENTER_USER_NAME_AND_PASSWORD_MESSAGE), Toast.LENGTH_LONG).show();
        } else {
            viewListener.onLogin(userName, password);
        }
	}

	/**
     * @return username string
     */
    private String getUserName() {
        return userText.getText().toString();
    }

    /**
     * @return user password string
     */
    private String getPassword() {
        return passwordText.getText().toString();
    }

    /**
     * Clear all text fields
     */
    public void clearView(){
        userText.setText("");
        passwordText.setText("");
    }
}
