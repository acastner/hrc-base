package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * BaseLinearLayout
 * Inherit this in your custom view to set custom Font
 */
public class BaseLinearLayout extends LinearLayout {
    /**
     * Default constructor
     * @param context
     */
    public BaseLinearLayout(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public BaseLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Set custom Font onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
    }

    /**
     * Shows Toast message to the user
     * @param message - message to show
     */
    public void notifyUser(String message) {
        Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
