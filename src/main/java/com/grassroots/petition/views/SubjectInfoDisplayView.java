package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.R;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.grassroots.petition.models.Question.*;

/**
 * SubjectInfoDisplayView
 * Display information about a subject
 */
public class SubjectInfoDisplayView extends LinearLayout {
    /**
     * SubjectInfoDisplayView listener interface
     */
    public static interface ViewListener {
        public void onMissingSubjectInformation(Subject subject);
    }
    private static final String TAG = SubjectInfoDisplayView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Subject first name
     */
    private TextView firstName;
    /**
     * Subject last name
     */
    private TextView lastName;
    /**
     * Subject address1
     */
    private TextView address1;
    /**
     * Subject address2
     */
    private TextView address2;
    /**
     * Subject city address
     */
    private TextView city;
    /**
     * Subject state information
     */
    private TextView state;
    /**
     * Subject address zip code
     */
    private TextView zip;
    /**
     * Subject home phone
     */
    private TextView homePhone;
    /**
     * Subject cell phone
     */
    private TextView cellPhone;
    /**
     * Subject email
     */
    private TextView email;

    /**
     * SubjectInfoDisplayView listener
     */
    private ViewListener listener;
    /**
     * Map questions to corresponding view
     */
    private Map<String, TextView> questionsToViews = new HashMap<String, TextView>();
    /**
     * Map that defines how to translate between the questions set in setSubject and the normal subject information
     * questions
     */
    private Map<String, String> questionNameTranslationMap = null;

    /**
     * Default constructor
     * @param context
     */
    public SubjectInfoDisplayView(Context context) {
        super(context);
    }

    /**
     * Default constructo
     * @param context
     * @param attrs
     */
    public SubjectInfoDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * SubjectInfoDisplayView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Fill view with subject
     * @param subject - subject
     * @param subjectInformationQuestions - subject related questions
     */
    public void setSubject(final Subject subject, List<Question> subjectInformationQuestions) {
        for (TextView view : questionsToViews.values()) {
        	if(view != null)
            view.setVisibility(View.INVISIBLE);
        }
        for (Question question : subjectInformationQuestions) {
            String questionVarName = question.getVarNameNo$();
            String translatedName = translate(questionVarName);
            TextView view = questionsToViews.get(translatedName);

            if (view != null) {
                view.setVisibility(View.VISIBLE);
                String viewValue = subject.getSubjectValueFor(translatedName);
                if (Strings.isEmpty(viewValue)) {
                    view.setText(questionVarName);
                    view.setTextColor(getResources().getColor(R.color.missing_data));
                    view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onMissingSubjectInformation(subject);
                        }
                    });
                } else {
                    view.setText(viewValue);
                    view.setTextColor(getResources().getColor(R.color.black));
                    view.setOnClickListener(null);
                }
            } else {
                LOGGER.debug(questionVarName + " translated to " + translatedName + " and no view found");
            }
        }
    }

    /**
     * Translates questionName between the questions set in setSubject and the normal subject information
     * questions
     * @param questionName
     * @return
     */
    private String translate(String questionName) {
        if(questionNameTranslationMap == null)
            return questionName;
        String translatedName = questionNameTranslationMap.get(questionName);
        return Strings.isEmpty(translatedName) ?
                questionName :
                translatedName;
    }

    /**
     * Define how to translate between the questions set in setSubject and the normal subject information
     * questions
     * @param translateMap
     */
    public void setTranslateMap(Map<String, String> translateMap) {
        questionNameTranslationMap = translateMap;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        link(FIRST_NAME, R.id.first_name);
        link(LAST_NAME, R.id.last_name);
        link(ADDRESS, R.id.address_1);
        link(ADDRESS_2, R.id.address_2);
        link(CITY, R.id.city);
        link(STATE, R.id.state);
        link(ZIP, R.id.zip);
        link(HOME, R.id.home_phone);
        link(CELL, R.id.cell_phone);
        link(PHONE, R.id.phone);
        link(EMAIL, R.id.email);
        link(SUFFIX, R.id.suffix); 
        link(COUNTRY, R.id.country); 

    }

    /**
     * Link question to the corresponding view
     * @param question - questinon
     * @param id - view resource id
     */
    private void link(String question, int id) {
        TextView view = (TextView) findViewById(id);
        questionsToViews.put(question, view);
    }
}
