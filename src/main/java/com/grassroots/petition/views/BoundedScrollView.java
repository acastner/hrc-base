package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Custom ScrollView that allows you to set a maximum height.
 */
public class BoundedScrollView extends ScrollView
{
    private int maxHeightPixels = -1;

    public BoundedScrollView (Context context, AttributeSet attrs, int defStyle)
    {
        super( context, attrs, defStyle );
    }

    public BoundedScrollView (Context context, AttributeSet attrs)
    {
        super( context, attrs );
    }

    public BoundedScrollView (Context context)
    {
        super( context );
    }

    public void setMaxHeight (int pixels)
    {
        maxHeightPixels = pixels;
    }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure( widthMeasureSpec, heightMeasureSpec );
        if( maxHeightPixels > -1 && getMeasuredHeight() > maxHeightPixels )
        {
            setMeasuredDimension( getMeasuredWidth(), maxHeightPixels );
        }
    }
}
