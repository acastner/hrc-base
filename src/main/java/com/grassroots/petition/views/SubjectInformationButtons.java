package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.R;

/**
 * Subject information buttons view
 * includes endInterview, assist, next, marketing buttons
 */
public class SubjectInformationButtons extends RelativeLayout {
    /**
     * SubjectInformationButtons interface
     */
    public static interface ViewListener {
        public void onEndInterview(Subject subject);

        public void onAssist();

        public void onNext(Subject subject);

        public void onMarketing();
        
        public void onMoreOptionsSelected();

		public void onTally();
    }

    //TODO subject information buttons interface

    /**
     * SubjectInformationButtons listener
     */
    private ViewListener listener;
    /**
     * View (enclosing) that contains this view
     */
    protected SubjectInfoView enclosingView;

    /**
     * Default constructor
     * @param context
     */
    public SubjectInformationButtons(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SubjectInformationButtons(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public SubjectInformationButtons(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * SubjectInformationButtons listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Enclosing view setter
     * @param enclosingView
     */
    public void setEnclosingView(SubjectInfoView enclosingView) {
        this.enclosingView = enclosingView;
    }
    
    public SubjectInfoView getEnclosingView(){
    	return enclosingView;
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        Button endInterviewButton = (Button) findViewById(R.id.end_interview_button);
        if (null != endInterviewButton) {
            endInterviewButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onEndInterview(enclosingView.getSubject());
                }
            });
        }

        Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
        if (null != marketingButton) {
            marketingButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onMarketing();
                }
            });
        }

        Button autofillFieldsButton = (Button) findViewById(R.id.autofill_fields_button);
        if (null != autofillFieldsButton) {
            autofillFieldsButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onAssist();
                }
            });
        }
        
        Button moreOptionsButton = (Button) findViewById(R.id.more_options_button);
        if (null != moreOptionsButton) {
            moreOptionsButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onMoreOptionsSelected();
                }
            });
        }

        Button nextButton = (Button) findViewById(R.id.next_button);
        if (null != nextButton) {
            nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNext(enclosingView.getSubject());
            }
        });
        }
        
        Button tallyButton = (Button) findViewById(R.id.tally_button);
        if (null != tallyButton) {
        	tallyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTally();
            }
        });
        }
    }
}
