package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * BaseRelativeLayout
 * Inherit this in your custom view to set custom Font
 */
public class BaseRelativeLayout extends RelativeLayout {
    /**
     * Default constructor
     * @param context
     */
    public BaseRelativeLayout(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public BaseRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public BaseRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Set custom Font onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
    }

    /**
     * Shows Toast message to the user
     * @param message - message to show
     */
    public void notifyUser(String message) {
        Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
