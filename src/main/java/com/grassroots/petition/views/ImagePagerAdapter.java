package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * Image Pager Adapter
 * Pages through the provided image resources
 */
public class ImagePagerAdapter extends PagerAdapter {
    public static final String TAG = ImagePagerAdapter.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * List of resource image Ids
     */
    private final List<Integer> imageResourceIds;
    private final Context context;

    /**
     * Default constructor
     * @param context
     * @param imageResourceIds
     */
    public ImagePagerAdapter(Context context, List<Integer> imageResourceIds) {
        this.imageResourceIds = imageResourceIds;
        this.context = context;
    }
    

    /**
     * Instantiate item for the position
     * @param collection
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ImageView imageView = new ImageView(context);
        if (0 <= position && position < imageResourceIds.size()) {
            Integer imageId = imageResourceIds.get(position);
            imageView.setImageResource(imageId);
        } else {
            LOGGER.error("No images left for position, returning empty view");
        }
        collection.addView(imageView);
        return imageView;
    }

    /**
     * Destroy item for the position
     * @param collection
     * @param position
     * @param view
     */
    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
    	ImageView imageview = (ImageView)(view);
    	if(imageview.getDrawable() != null){
    		((BitmapDrawable)(imageview.getDrawable())).getBitmap().recycle();
    	}
        collection.removeView((View) view);
    }

    /**
     * Retirn count of all resource image IDs
     * @return
     */
    @Override
    public int getCount() {
        return imageResourceIds.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }
    
    @Override
    public int getItemPosition(Object object) {
    	// TODO Auto-generated method stub
    	return super.getItemPosition(null);
    }
}
