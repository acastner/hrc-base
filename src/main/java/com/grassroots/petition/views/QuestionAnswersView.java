package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.grassroots.petition.activities.BaseMonthViewActivity;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.models.NearestNeighborInfo;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.QuestionAnswer;
import com.grassroots.petition.models.Response;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.tasks.FetchQuestionTallyTask;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.R;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import org.apache.log4j.Logger;

/**
 *
 * Configures view for the different answer types for a particular question and handles answer capture.
 *
 * Lets a user input an answer to various kinds of questions
 * Supports multiple variants of question types (input: singleLine, multiLine...)
 */
public class QuestionAnswersView extends FrameLayout {
	public static final String TAG = QuestionAnswersView.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	/**
	 * MultiLine answer text view
	 */
	private TextView multilineAnswerText;
	/**
	 * SingleLine answer text view
	 */
	private TextView answerText;
	/**
	 * True-False answer radio group
	 */
	private RadioGroup trueFalseGroup;
	/**
	 * Multiple radio group (can choose just single answer)
	 */
	private RadioGroup multiChoiceGroup;
	/**
<<<<<<< HEAD
=======
	 * Layout for question tally answers
	 */
	private TextView tallyDisplay;
	/**
	 * Multiple checkbox group (can choose multiple answers)
	 */
	private LinearLayout tallyChkboxGroup;
	/**
>>>>>>> 1f509a87e31372f60c22e902e465eac6920369e2
	 * Map to organize question type with subview
	 */
	private final Map<Question.Type, View> questionViewsByType = new EnumMap<Question.Type, View>(Question.Type.class);
	/**
	 * Map to organize button id with QuestionAnswer
	 */
	protected Map<Integer, QuestionAnswer> multiChoiceButtonIdsToAnswers = new HashMap<Integer, QuestionAnswer>();
	/**
	 * Picture answer view
	 */
	private PictureQuestionView pictureQuestionView;
	/**
<<<<<<< HEAD
	 * Calendar answer view
	 */
	private CalendarQuestionView calendarQuestionView;
	/**
=======
>>>>>>> 1f509a87e31372f60c22e902e465eac6920369e2
	 * Current question
	 */
	protected Question currentQuestion;
	/**
	 * Location of file with photo to display in PictureQuestionView
	 */
	private String photoFileLocation;
	/**
	 * PetitionAnswers object, contains all answers for the view
	 */
	protected PetitionAnswers petitionAnswers;

	private SignatureCanvas signatureview;

	/**
	 * Default constructor
	 * @param context
	 */
	public QuestionAnswersView(Context context) {
		super(context);
	}

	/**
	 * Default constructor
	 * @param context
	 * @param attrs
	 */
	public QuestionAnswersView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Default constructor
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public QuestionAnswersView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * Set the current question - resets any existing answers, and the question id will be set on
	 * the subject answer
	 * @param question
	 */
	public void setQuestion(Question question) {
		currentQuestion = question;

		SubjectAnswer subjectAnswer = null;
		if (null != question) {
			subjectAnswer = petitionAnswers.getAnswerTo(question);
		}

		Question.Type type = currentQuestion.getType();

		//setCorrectViewVisible(type);
		switch (type) {
		case MULTILINE_TEXT: {
			multilineAnswerText.setText("");
			if (null != subjectAnswer) {
				String stringAnswer = subjectAnswer.getStringAnswer();
				if (null != stringAnswer && Strings.isNotEmpty(stringAnswer)) {
					multilineAnswerText.setText(stringAnswer);
				}
			}
			break;
		}

		case TEXT: {
			answerText.setText("");
			if (null != subjectAnswer) {
				String stringAnswer = subjectAnswer.getStringAnswer();
				if (null != stringAnswer && Strings.isNotEmpty(stringAnswer)) {
					answerText.setText(stringAnswer);
				}
			}
			break;
		}

		case MULTIPLE: {
			LOGGER.debug("SUBJECTANSWER Multiple " + String.valueOf(subjectAnswer));
			int checkedAnswerId = SubjectAnswer.NO_VALUE;
			if (null != subjectAnswer) {
				checkedAnswerId = subjectAnswer.getAnswerId();
			}

			multiChoiceGroup.removeAllViewsInLayout();
			multiChoiceButtonIdsToAnswers.clear();
			for (QuestionAnswer answerChoice : question.getAnswers()) {
				//RadioButton button = new RadioButton(getContext());
				RadioButton button = (RadioButton) inflate(getContext(), R.layout.question_radio_button, null);
				button.setText(answerChoice.getValue());
				multiChoiceGroup.addView(button);
				multiChoiceButtonIdsToAnswers.put(button.getId(), answerChoice);

				if (answerChoice.getId() == checkedAnswerId) {
					button.setChecked(true);
				}
			}
			break;
		}

		case TRUEFALSE: {
			trueFalseGroup.clearCheck();

			if (null != subjectAnswer) {
				int checkedAnswerId = subjectAnswer.getAnswerId();
				if (Question.TRUE_ANSWER_ID == checkedAnswerId) {
					RadioButton trueRadioButton = (RadioButton)trueFalseGroup.findViewById(R.id.true_radio);
					trueRadioButton.setChecked(true);
				} else if (Question.FALSE_ANSWER_ID == checkedAnswerId) {
					RadioButton falseRadioButton = (RadioButton)trueFalseGroup.findViewById(R.id.false_radio);
					falseRadioButton.setChecked(true);
				}
			}
			break;
		}

		case PICTURE: {
			pictureQuestionView.reset();
			photoFileLocation = null;

			LOGGER.debug("SUBJECTANSWER subjectAnswer picture " + String.valueOf(subjectAnswer));
	
			if (null != subjectAnswer) {
				photoFileLocation = subjectAnswer.getPictureLocation();
				if (Strings.isNotEmpty(photoFileLocation)) {
					BasePetitionActivity activity = (BasePetitionActivity)getContext();
					Bitmap pictureBitmap = activity.readScaledPhoto(photoFileLocation);
					if (null != pictureBitmap) {
						pictureQuestionView.setPicture(pictureBitmap);
					}
				}
			}
			break;
		}

		case SIGNATURE: {
			petitionAnswers.setSignature(signatureview.getSignature());
			break;
		}

		case CALENDAR: {

			LOGGER.debug("SUBJECTANSWER calendar");
			LOGGER.debug("SUBJECTANSWER calendar getAnswers " + String.valueOf(question.getAnswers().get(0).getValue()));
			
//			String calendarName = subjectAnswer.getStringAnswer().split(",")[1].split(": ")[1];
			//String calendarId = subjectAnswer.getStringAnswer().split(",")[0].split(": ")[1];
			
			String calendarId = question.getAnswers().get(0).getValue().split(",")[0].split(": ")[1];
			
			LOGGER.debug("SUBJECTANSWER calendar ID:" + calendarId);
			
			GrassrootsRestClient.getClient().setAppointmentWindowsEndpointId(calendarId);
			
			CalendarQuestionView calView = new CalendarQuestionView(getContext()); //(CalendarQuestionView) this.findViewById(R.id.calendar_question_view);
			calView.updateCalendar();
			
			//GrassrootsRestClient.getClient().syncLoadAppointmentWindows();
			
			String calendarName = question.getAnswers().get(0).getValue().split(",")[1].split(": ")[1];

			TextView calendarText = (TextView) this.findViewById(R.id.calendar_text);
			calendarText.setText(calendarName);

			break;
		}
		}
		LOGGER.debug("SUBJECTANSWER subjectAnswer " + String.valueOf(subjectAnswer));
		
		setCorrectViewVisible(type);
		/*String answerText = "CalendarId: 1, Name: test, " +
        		"ReservationMaximum: 30, ReservationMinimum: 15, ReservationSuggested : 30";
        String calendarName = answerText.split(",")[1].split(": ")[1];

        TextView calendarText = (TextView) this.findViewById(R.id.calendar_text);
        calendarText.setText(calendarName);*/
	

		/*
		 * Question Add-ons
		 */
		if (question.hasTally())
		{
			tallyDisplay.setVisibility(View.VISIBLE);
			displayTally();
		}
	}

	/**
	 * PictureQuestionView ViewListener setter
	 * @param viewListener - PictureQuestionView.ViewListener
	 */
	public void setViewListener(PictureQuestionView.ViewListener viewListener){
		pictureQuestionView.setViewListener(viewListener);
	}

	/**
	 * Picture setter
	 * @param bitmap - picture bitmap
	 * @param fileLocation - picture file lo cation
	 */
	public void setPicture(Bitmap bitmap, String fileLocation) {
		pictureQuestionView.setPicture(bitmap);
		photoFileLocation = fileLocation;
	}

	/**
	 * Set corresponding view visible
	 * @param type - question type
	 */
	private void setCorrectViewVisible(Question.Type type) {
		for (View view : questionViewsByType.values()) {
            view.setVisibility(GONE);
        }
        if(signatureview != null){
        	 signatureview.setVisibility(View.GONE);
        }
        if(questionViewsByType != null)
        questionViewsByType.get(type).setVisibility(VISIBLE);
		
		/*
		 * Question Add-ons
		 */
		tallyDisplay.setVisibility(View.GONE);

	}

	/**
	 * PetitionAnswers setter
	 * @param petitionAnswers
	 */
	public void setPetitionAnswers(PetitionAnswers petitionAnswers) {
		this.petitionAnswers = petitionAnswers;
	}

	/**
	 * Get entered/chosen Answer
	 * @return - subject answer
	 */
	public SubjectAnswer getAnswer() {
		SubjectAnswer answer = new SubjectAnswer();
		answer.setQuestionId(currentQuestion.getId());
		switch (currentQuestion.getType()) {
		case MULTILINE_TEXT:
			answer.setStringAnswer(multilineAnswerText.getText().toString());
			break;
		case TEXT:
			answer.setStringAnswer(answerText.getText().toString());
			break;
		case MULTIPLE:
			QuestionAnswer questionAnswer = multiChoiceButtonIdsToAnswers.get(multiChoiceGroup.getCheckedRadioButtonId());
			if (questionAnswer != null) {
				answer.setAnswerId(questionAnswer.getId());
			}
			break;
		case TRUEFALSE:
			int checkedId = trueFalseGroup.getCheckedRadioButtonId();
			int answerId;
			if (checkedId == R.id.true_radio)
				answerId = Question.TRUE_ANSWER_ID;
			else if (checkedId == R.id.false_radio)
				answerId = Question.FALSE_ANSWER_ID;
			else
				break;
			answer.setAnswerId(answerId);
			break;
		case PICTURE:
			if(Strings.isNotEmpty(photoFileLocation)) {
				answer.setPictureAnswerPlaceholder(photoFileLocation);
			}
			break;
		case SIGNATURE:
			answer.setSignatureAnswer(signatureview.getSignature());
			break;

		case CALENDAR:

			SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(currentQuestion);
			//String calendarIdText = subjectAnswer.getStringAnswer();
			String calendarIdText = currentQuestion.getAnswers().get(0).getValue();
			calendarIdText = calendarIdText.split(",")[0].split(": ")[1];

			answer.setStringAnswer(calendarIdText);

			break;
		}

//		        SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(currentQuestion);
//		        String calendarIdText = subjectAnswer.getStringAnswer();
		/*String answerText = "CalendarId: 1, Name: test, " +
				"ReservationMaximum: 30, ReservationMinimum: 15, ReservationSuggested : 30";
		String calendarIdText = answerText;
		calendarIdText = calendarIdText.split(",")[0].split(": ")[1];

		answer.setStringAnswer(calendarIdText);*/

		return answer;
	}

	/**
	 * Set custom font, subview listeners, init maps
	 */
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		TypeFaceSetter.setRobotoFont(getContext(), this);
		signatureview = (SignatureCanvas) findViewById(R.id.signature_canvas);
		signatureview.setVisibility(View.GONE);
		answerText = (TextView) findViewById(R.id.answer_single_line);
		multilineAnswerText = (TextView) findViewById(R.id.answer_multi_line);
		pictureQuestionView = (PictureQuestionView) findViewById(R.id.picture);
		trueFalseGroup = (RadioGroup) findViewById(R.id.true_false);
		multiChoiceGroup = (RadioGroup) findViewById(R.id.multiple_choice);
		tallyDisplay = (TextView) findViewById(R.id.tally_display);
		questionViewsByType.put(Question.Type.TEXT, findViewById(R.id.answer_single_line));
		questionViewsByType.put(Question.Type.MULTILINE_TEXT, findViewById(R.id.answer_multi_line));
		questionViewsByType.put(Question.Type.MULTIPLE, findViewById(R.id.multiple_choice));
		questionViewsByType.put(Question.Type.TRUEFALSE, findViewById(R.id.true_false));
		questionViewsByType.put(Question.Type.PICTURE, pictureQuestionView);
		questionViewsByType.put(Question.Type.CALENDAR, findViewById(R.id.calendar));
	}

	public void setSignatureQuestion(Question question) {
		for (View view : questionViewsByType.values()) {
			view.setVisibility(GONE);
		}
		signatureview.setVisibility(View.VISIBLE);
	}

	public Signature getSignature() {
		// TODO Auto-generated method stub
		return signatureview.getSignature();
	}


	
	
	
	
	private void displayTally()
	{
		String tallyResults = "", tally = "";
		try {
			tallyResults = new FetchQuestionTallyTask().execute(String.valueOf(currentQuestion.getId())).get();

			//tallyResults = new FetchQuestionTallyTask().execute("15").get(); //for debugging

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Error
		if (tallyResults.equals("Unsuccessful"))
		{      
			tally = "Error: Could Not Get Tally";
		}
		//Success
		else
		{
			try {
				JSONArray jsonArr = new JSONArray(tallyResults);

				for (int i = 0; i < jsonArr.length() ; i++)
				{
					String count = jsonArr.getJSONObject(i).getString("Questiontallycount");
					String response = jsonArr.getJSONObject(i).getString("Questiontallyresponse");
					String template = jsonArr.getJSONObject(i).getString("Tallytemplate");
					template = template.replace("<QUESTIONTALLYCOUNT>", count);
					template = template.replace("<QUESTIONTALLYRESPONSE>", response);

					//create new line if multiple tallies
					if (!tally.isEmpty())
					{
						tally += "<br></br>" + template;
					}
					else
					{
						tally = template;
					}

					//LOGGER.debug("Question Tally: " + jsonArr.getJSONObject(i).getString("Questiontallyresponse"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		tallyDisplay.setText(Html.fromHtml("<html><body>"
				+ tally
				+ "</body></html>"));
	}
}
