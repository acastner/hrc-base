package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Appointment;
import com.grassroots.petition.utils.TypeFaceSetter;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 10.07.13
 * Time: 21:21
 * To change this template use File | Settings | File Templates.
 */
public class AppointmentSummaryView extends LinearLayout {
    private static final String TAG = AppointmentSummaryView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private TextView descriptionTextView;
    private TextView startTimeTextView;
    private TextView durationTextView;

    public AppointmentSummaryView(Context context) {
        super(context);
    }

    public AppointmentSummaryView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private SimpleDateFormat getDateFormat()
    {
        return new SimpleDateFormat( "EEEE, MMMM d, yyyy h:mm a" );
    }

    public void setAppointment(Appointment appointment) {
        if (null == appointment) {
            LOGGER.error("Got null appointment in setAppointment");
            return;
        }

        SimpleDateFormat dateFormat = getDateFormat();

        descriptionTextView.setText(appointment.getDescription());
        startTimeTextView.setText(dateFormat.format(appointment.getStartTime()));
        durationTextView.setText("" + appointment.getDuration() + " " + getContext().getString(R.string.MINUTES_STRING));
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        descriptionTextView = (TextView) findViewById(R.id.description);
        startTimeTextView = (TextView) findViewById(R.id.start_time);
        durationTextView = (TextView) findViewById(R.id.duration);
    }
}
