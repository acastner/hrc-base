package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.QuestionsListAdapter;

import java.util.List;

/**
 * QuestionListView
 * Show a scrolling list of questions
 */
public class QuestionListView extends ListView {
    /**
     * Questions list adapter
     */
    private QuestionsListAdapter questionAdapter;

    /**
     * Default constructor
     * @param context
     */
    public QuestionListView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public QuestionListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public QuestionListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Set list of questions on view
     * @param questions - list of questions
     * @param petitionAnswers - petitionAnswers
     */
    public void setQuestions(List<Question> questions, PetitionAnswers petitionAnswers) {
        questionAdapter = new QuestionsListAdapter(questions, petitionAnswers, getContext());
        setAdapter(questionAdapter);
    }

    /**
     * @return list of answers
     */
    public List<SubjectAnswer> getAnswers() {
        return questionAdapter.getAnswers();
    }

    /**
     * Set custom font onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
    }
}
