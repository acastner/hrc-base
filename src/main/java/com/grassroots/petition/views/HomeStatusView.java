package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import com.grassroots.petition.R;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.utils.TypeFaceSetter;

import java.util.HashMap;
import java.util.Map;

/**
 * HomeStatus View
 * Displays information about a household of subjects
 */
public class HomeStatusView extends RelativeLayout {
    /**
     * Home status view listener interface
     */
    public static interface ViewListener {
        public void onStatusSelected(KnockStatus knockStatus);
        public void onCancel();
    }

    /**
     * Group with status radios
     */
    private RadioGroup statusGroup;

    /**
     * Home status view listener interface
     */
    private ViewListener listener;

    /**
     * Maps radio ID to particular knockStatus object
     */
    private Map<Integer, KnockStatus> buttonIdToStatus = new HashMap<Integer, KnockStatus>();

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public HomeStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public HomeStatusView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Home status view listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Return selected knockStatus
     * @return
     */
    private KnockStatus getStatus() {
        return buttonIdToStatus.get(statusGroup.getCheckedRadioButtonId());
    }

    /**
     * Set custom font, button listeners and fill radioGroup with butotns
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        findViewById(R.id.cancel_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCancel();
            }
        });
        findViewById(R.id.ok_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onStatusSelected(getStatus());
            }
        });
        statusGroup = (RadioGroup) findViewById(R.id.status_choices);
        int buttonToSelect = 0;
        KnockStatus[] knockStatuses = getAvailableKnockStatuses();
        for (KnockStatus knockStatus : knockStatuses) {
            if (KnockStatus.SUCCESS.equals(knockStatus) ||
                    KnockStatus.NOT_KNOCKED.equals(knockStatus)) {
                continue; //don't display success or not knocked options
            }
            RadioButton button = new RadioButton(getContext());
            button.setText(knockStatus.displayText());
            button.setTextColor(R.color.unpressed_button);
            statusGroup.addView(button);
            buttonIdToStatus.put(button.getId(), knockStatus);
            if (KnockStatus.NOT_HOME.equals(knockStatus))
                buttonToSelect = button.getId();
        }
        statusGroup.check(buttonToSelect);
    }

    /**
     * Return all available knock statuses
     * @return
     */
    protected KnockStatus[] getAvailableKnockStatuses() {
        return KnockStatus.values();
    }
}
