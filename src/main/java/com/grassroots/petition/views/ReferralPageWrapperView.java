package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.utils.TypeFaceSetter;

import java.util.Map;

/**
 * ReferralPageWrapperView
 * Displays a referral input and controls
 */
public class ReferralPageWrapperView extends RelativeLayout {
    /**
     * ReferralPageWrapperView listener interface
     */
    public static interface ViewListener {
        public void onNext(Map<String, String> referralMapping);
        public void onEndInterview();
		public void onBack();
    }

    /**
     * ReferralPageWrapperView listener
     */
    private ViewListener listener;
    /**
     * ReferralPageView
     */
    private ReferralPageView referralPageView;
    /**
     * Referral page title view
     * Displays current referral number
     */
    private TextView title;

    /**
     * Default constructor
     * @param context
     */
    public ReferralPageWrapperView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public ReferralPageWrapperView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public ReferralPageWrapperView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * ViewListener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Clear referral information and set page title with particular referral number
     * @param currentReferralNumber - current referral number
     */
    public void clearReferralData(int currentReferralNumber) {
        referralPageView.clearReferralData();
        title.setText(getContext().getString(R.string.REFERRAL_NUMBER_TITLE) + currentReferralNumber);
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);


        referralPageView = (ReferralPageView) findViewById(R.id.referral_page);
        title = (TextView) findViewById(R.id.title);
        findViewById(R.id.next_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNext(referralPageView.getReferrerInformation());
            }
        });
        findViewById(R.id.back_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBack();
            }
        });
        findViewById(R.id.end_interview_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onEndInterview();
            }
        });
    }
}
