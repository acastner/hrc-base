package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.R;

/**
 * SignatureView
 * Signature input and buttons
 */
public class SignatureView extends BaseRelativeLayout {
    /**
     * SignatureView listener interface
     */
    public interface ViewListener {
        public void onEndInterview();
        public void onNext(Signature signature);
        public void onMarketing();
    }

    /**
     * Signature canvas
     */
    private SignatureCanvas signatureCanvas;
    /**
     * SignatureView listener
     */
    private ViewListener listener;

    /**
     * Default constructor
     * @param context
     */
    public SignatureView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SignatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public SignatureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * SignatureView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Clear signature
     */
    public void clearSignature() {
        signatureCanvas.clearSignature();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        signatureCanvas = (SignatureCanvas) findViewById(R.id.signature_canvas);

        Button clearSignatureButton = (Button) findViewById(R.id.clear_signature_button);
        if (null != clearSignatureButton) {
            clearSignatureButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    signatureCanvas.clearSignature();
                }
            });
        }

        Button endInterviewButton = (Button) findViewById(R.id.end_interview_button);
        if (null != endInterviewButton) {
            endInterviewButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onEndInterview();
                }
            });
        }

        Button nextButton = (Button) findViewById(R.id.next_button);
        if (null != nextButton) {
            nextButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    	listener.onNext(signatureCanvas.getSignature());
                }
            });
        }

        Button marketingButton = (Button) findViewById(R.id.marketing_material_button);
        if (null != marketingButton) {
            marketingButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onMarketing();
                }
            });
        }
    }
}
