package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Signature;
import org.apache.log4j.Logger;

/**
 * SignatureCanvas
 * Makes use of code from: http://www.mysamplecode.com/2011/11/android-capture-signature-using-canvas.html
 * A drawable view for gathering signature data
 */
public class SignatureCanvas extends View {

    private static final float STROKE_WIDTH = 5f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
    public static final String TAG = SignatureCanvas.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private Paint paint;
    private Path path = new Path();

    private float lastTouchX;
    private float lastTouchY;
    private final RectF dirtyRect = new RectF();

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SignatureCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = createPaint();
    }
    
    public SignatureCanvas(Context context) {
        super(context,null);
    }

    /**
     * Create Paint that holds style and color information
     * @return
     */
    private Paint createPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(STROKE_WIDTH);
        return paint;
    }

    /**
     * @return bitmap with signature
     */
    public Bitmap getSignatureBitmap() {
        Drawable background = getBackground();
        setBackgroundResource(R.color.white);
        int width = getWidth();
        int height = getHeight();
        if (width <= 0 || height <= 0) {
            LOGGER.error("Zero width or height while trying to get signature bitmap");
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        draw(canvas);
        
        float scaler = (float)400/(float)bitmap.getWidth();
        
        bitmap = Bitmap.createScaledBitmap(
        		bitmap, (int)((float)bitmap.getWidth()*scaler), (int)((float)bitmap.getHeight()*scaler), false);
        
        setBackgroundDrawable(background);
        return bitmap;
    }

    /**
     * @return signature
     */
    public Signature getSignature() {
        Bitmap signatureBitmap = getSignatureBitmap();
        if(signatureBitmap == null)
            return null;
        return new Signature(signatureBitmap);
    }

    /**
     * Checks if signature data was entered
     * @return true if signature data was entered, false - otherwise
     */
    public boolean hasSignatureData() {
        return path.isEmpty() == false;
    }

    /**
     * Clear signature and invalidate canvas
     */
    public void clearSignature() {
        path.reset();
        invalidate();
    }

    /**
     * Draw signature onDraw
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(path, paint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:

                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;

            default:
                return false;
        }

        invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;

        return true;
    }

    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX;
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX;
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY;
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY;
        }
    }

    private void resetDirtyRect(float eventX, float eventY) {
        dirtyRect.left = Math.min(lastTouchX, eventX);
        dirtyRect.right = Math.max(lastTouchX, eventX);
        dirtyRect.top = Math.min(lastTouchY, eventY);
        dirtyRect.bottom = Math.max(lastTouchY, eventY);
    }
}