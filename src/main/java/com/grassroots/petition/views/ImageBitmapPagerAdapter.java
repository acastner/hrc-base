package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * Image Bitmap Pager Adapter
 * Pages through the provided bitmaps
 */
public class ImageBitmapPagerAdapter  extends PagerAdapter {
    public static final String TAG = ImageBitmapPagerAdapter.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * list of image bitmaps
     */
    private final List<Bitmap> imageBitmaps;
    private final Context context;

    /**
     * Default constructor
     * @param context
     * @param imageBitmaps
     */
    public ImageBitmapPagerAdapter(Context context, List<Bitmap> imageBitmaps) {
        this.imageBitmaps = imageBitmaps;
        this.context = context;
    }

    /**
     * Instantiate item for the position
     * @param collection
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ImageView imageView = new ImageView(context);
        if (0 <= position && position < imageBitmaps.size()) {
            Bitmap bitmap = imageBitmaps.get(position);
            imageView.setImageBitmap(bitmap);
        } else {
            LOGGER.error("No images left for position, returning empty view");
        }
        collection.addView(imageView);
        return imageView;
    }

    /**
     * Destroy item for the position
     * @param collection
     * @param position
     * @param view
     */
    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
    	ImageView imageview = ((ImageView)view);
    	//((BitmapDrawable)(imageview.getDrawable())).getBitmap().recycle();
        collection.removeView((View) view);
    }

    /**
     * Retirn count of all image bitmaps
     * @return
     */
    @Override
    public int getCount() {
        return imageBitmaps.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }
}
