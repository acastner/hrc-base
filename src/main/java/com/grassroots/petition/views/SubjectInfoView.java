package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SubjectInfoView
 * For inputting information about a subject
 */
public class SubjectInfoView extends RelativeLayout {
    public static final String TAG = SubjectInfoView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    
    public static String subjectEmail;
    public static String subjectCellPhone;
    public static String subjectHomePhone;
    
    /**
     * Views order
     */
    private static final int[] viewIdsInInputOrder = {R.id.salutation_edit, R.id.suffix_edit,
            R.id.first_name_edit, R.id.last_name_edit, R.id.street_address_edit, R.id.address_2_edit,
            R.id.city_edit, R.id.state_edit, R.id.zip_edit, R.id.home_edit, R.id.cell_edit, R.id.email_edit, R.id.location_edit           
    };

    /**
     * @return views order
     */
    protected int[] getViewIdsInputOrderArray() {
        return viewIdsInInputOrder;
    }


    private final Map<TextView, View> entryFieldsToTitleViews = new HashMap<TextView, View>();
    private final Map<String, TextView> questionNamesToTextViews = new HashMap<String, TextView>();
    private final Map<Integer, TextView> questionIdsToTextViews = new HashMap<Integer, TextView>();
    private Map<String, String> questionNameTranslationMap = null;
    private Subject subject;
    
    private String email = "";
    private String homePhone = "";
    private String cellPhone = "";

    /**
     * Default constructor
     * @param context
     */
    public SubjectInfoView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SubjectInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public SubjectInfoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Translates between the var names in setSubjectInformationQuestions and the default subject question values
     * @param translationMap
     */
    public void setQuestionNameTranslationMap(Map<String, String> translationMap) {
        questionNameTranslationMap = translationMap;
    }

    /**
     * Fill view with subject information
     * @param subject
     */
    public void setSubject(Subject subject) {
        this.subject = subject;
        if (subject == null)
            return;
        
        LOGGER.debug("CHECK EMAIL: " + subject.getEmail());
        LOGGER.debug("CHECK HOME: " + subject.getHomePhone());
        LOGGER.debug("CHECK PHONE: " + subject.getPhone());
        LOGGER.debug("CHECK CELL: " + subject.getCellPhone());
        
        subjectEmail = subject.getEmail();
        subjectHomePhone = subject.getPhone();
        subjectCellPhone = subject.getCellPhone();
        
        setIfNotEmpty(subject.getSalutation(), Question.SALUTATION);
        setIfNotEmpty(subject.getSuffix(), Question.SUFFIX);
        setIfNotEmpty(subject.getFirstName(), Question.FIRST_NAME);
        setIfNotEmpty(subject.getLastName(), Question.LAST_NAME);
        setIfNotEmpty(subject.getAddressLine1(), Question.ADDRESS);
        setIfNotEmpty(subject.getAddressLine2(), Question.ADDRESS_2);
        setIfNotEmpty(subject.getCity(), Question.CITY);
        setIfNotEmpty(subject.getState(), Question.STATE);
        setIfNotEmpty(subject.getZip(), Question.ZIP);     
        setIfNotEmpty(subject.getEmail(), Question.EMAIL);
        setIfNotEmpty(subject.getHomePhone(), Question.HOME);
        //LOGGER.debug("CHECK HOME: " + subject.getHomePhone());
        setIfNotEmpty(subject.getCellPhone(), Question.CELL);
       // LOGGER.debug("CHECK CELL: " + subject.getCellPhone());
        setIfNotEmpty(subject.getCellPhone(), Question.PHONE);
        //LOGGER.debug("CHECK PHONE: " + subject.getPhone());
        setIfNotEmpty(subject.getLocation(), Question.LOCATION);
        

    }

    /**
     * Set the fields te display
     * @param subjectInformationQuestions
     */
    public void setSubjectInformationQuestions(List<Question> subjectInformationQuestions) {
        if (subjectInformationQuestions == null) {
            LOGGER.error("Null subjectinformation questions given");
            return;
        }
        for (View view : questionNamesToTextViews.values()) {
            if (null == view) continue;
            view.setVisibility(View.GONE);
        }
        for (View view : entryFieldsToTitleViews.values()) {
            if (null == view) continue;
            view.setVisibility(View.GONE);
        }
        for (Question question : subjectInformationQuestions) {
            TextView view = questionNamesToTextViews.get(translate(question.getVarNameNo$()));
            if (null == view) continue;
            view.setVisibility(View.VISIBLE);

            View titleView =  entryFieldsToTitleViews.get(view);
            if (null == titleView) continue;
            titleView.setVisibility(View.VISIBLE);

            questionIdsToTextViews.put(question.getId(), view);
        }
        setUpKeyboardNextOrder();
    }

    /**
     * Translates between the var names in setSubjectInformationQuestions and the default subject question values
     * @param questionVarName
     * @return
     */
    protected String translate(String questionVarName) {
        if(questionNameTranslationMap == null) {
            return questionVarName;
        }
        return questionNameTranslationMap.get(questionVarName);
    }

    /**
     * Moves input control to the next view
     */
    protected void setUpKeyboardNextOrder() {
        int[] viewIdsArray = getViewIdsInputOrderArray();
        View lastVisibleView = null;
        for (int id : viewIdsArray) {
            View currentView = findViewById(id);
            if (currentView == null)
                continue;
            if (currentView.getVisibility() == View.VISIBLE) {
                if (lastVisibleView != null) {
                    lastVisibleView.setNextFocusDownId(id);
                }
                lastVisibleView = currentView;
            }
        }
    }

    /**
     * Set text field with value from questionNamesToTextViews
     * 
     * @param fieldValue
     * @param field
     */
    protected void setIfNotEmpty(String fieldValue, String field) {
        TextView fieldView = questionNamesToTextViews.get(field);
        if (null == fieldView) return;
        if (!Strings.isEmpty(fieldValue)) {
            fieldView.setText(fieldValue);
        } else {
            fieldView.setText("");
        }
    }

    /**
     * @return Subject updated with entered data
     */
    public Subject getSubject() {
    	String subjLat = subject.getLatitude();
    	String subjLong = subject.getLongitude();
    	
        Subject subject =  this.subject == null ?
                new Subject() :
                new Subject(this.subject.getId());
        subject.setSalutation(getText(Question.SALUTATION));
        subject.setSuffix(getText(Question.SUFFIX));
        subject.setFirstName(getText(Question.FIRST_NAME));
        subject.setLastName(getText(Question.LAST_NAME));

        subject.setAddressLine1(getText(Question.ADDRESS));
        subject.setAddressLine2(getText(Question.ADDRESS_2));
        subject.setCity(getText(Question.CITY));
        subject.setState(getText(Question.STATE));
        subject.setZip(getText(Question.ZIP));

        subject.setEmail(getText(Question.EMAIL));
        subject.setHomePhone(getText(Question.HOME));
        subject.setCellPhone(getText(Question.CELL));
        subject.setPhone(getText(Question.PHONE));
        subject.setLocation(getText(Question.LOCATION));
        subject.setLatitude(subjLat);
        subject.setLongitude(subjLong);
        


        if (this.subject != null) {
            subject.setHid(this.subject.getHid());
            subject.setOtherFieldsMapping(this.subject.getOtherFieldsMapping());
        }

        return subject;
    }

    /**
     * Return entered text for question name
     * @param questionName - question name
     * @return
     */
    protected String getText(String questionName) {
        TextView textView = questionNamesToTextViews.get(questionName);
        if (null == textView || textView.getText().length() == 0) return null;
        return textView.getText().toString();
    }

    /**
     * Return list of filled answers
     * @return
     */
    protected List<SubjectAnswer> getAnswers() {
        List<SubjectAnswer> answers = new ArrayList<SubjectAnswer>();

        for (Map.Entry<Integer, TextView> idsAndFields : questionIdsToTextViews.entrySet()) {
            TextView textView = idsAndFields.getValue();
            if (null == textView) continue;
            String answer = textView.getText().toString();
            answers.add(new SubjectAnswer(idsAndFields.getKey(), answer));
        }
        return answers;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        //Name
        link(Question.SALUTATION, R.id.salutation_edit);
        link(Question.SUFFIX, R.id.suffix_edit);
        link(Question.FIRST_NAME, R.id.first_name_edit);
        link(Question.LAST_NAME, R.id.last_name_edit);
        linkViews(R.id.name, R.id.salutation_edit, R.id.suffix_edit, R.id.first_name_edit, R.id.last_name_edit);
        //Address
        link(Question.ADDRESS, R.id.street_address_edit);
        link(Question.ADDRESS_2, R.id.address_2_edit);
        link(Question.CITY, R.id.city_edit);
        link(Question.STATE, R.id.state_edit);
        link(Question.ZIP, R.id.zip_edit);
        link(Question.LOCATION, R.id.location_edit);
        linkViews(R.id.address, R.id.street_address_edit, R.id.address_2_edit, R.id.city_edit, R.id.state_edit, R.id.zip_edit);
        //Contact
        link(Question.HOME, R.id.home_edit);
        link(Question.CELL, R.id.cell_edit);
        link(Question.EMAIL, R.id.email_edit);
        linkViews(R.id.other, R.id.home_edit, R.id.cell_edit, R.id.email_edit);
        


    }

    /**
     * Link question names with corresponding views
     * @param questionName - question name
     * @param id - view resource id
     */
    protected void link(String questionName, Integer id) {
        View view = findViewById(id);
        if (null == view) return;
        questionNamesToTextViews.put(questionName, (TextView) view);
    }

    /**
     * Link header with input field views
     * @param headerViewId - header view resource id
     * @param entryFieldIds - array of input field resource ids
     */
    protected void linkViews(int headerViewId, int... entryFieldIds) {
        View headerView = findViewById(headerViewId);
        if (null == headerView) return;
        for (int entryField : entryFieldIds) {
            TextView entryView = (TextView) findViewById(entryField);
            entryFieldsToTitleViews.put(entryView, headerView);
        }
    }
}