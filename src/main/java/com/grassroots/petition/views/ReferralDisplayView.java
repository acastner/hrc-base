package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * ReferralDisplayView
 * Show a referral
 */
public class ReferralDisplayView extends LinearLayout {
    /**
     * Referral full name
     */
    private TextView fullNameText;
    /**
     * Referral email
     */
    private TextView emailText;
    /**
     * Referral phone
     */
    private TextView phoneText;

    /**
     * Default constructor
     * @param context
     */
    public ReferralDisplayView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public ReferralDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Referral setter
     * @param fullName - referral full name
     * @param email - Referral email
     * @param phone - Referral phone
     */
    public void setReferral(String fullName, String email, String phone) {
        fullNameText.setText(fullName);
        emailText.setText(email);
        phoneText.setText(phone);
    }

    /**
     * Set custom font type onFinishInflate
     */
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        fullNameText = (TextView) findViewById(R.id.full_name);
        emailText = (TextView) findViewById(R.id.email);
        phoneText = (TextView) findViewById(R.id.phone);
    }
}
