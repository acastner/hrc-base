package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Signature;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.R;

import java.util.EnumMap;
import java.util.Map;

/**
 * QuestionsView (Container)
 * Displays a view with question and forward and back buttons
 */
public class QuestionsView extends RelativeLayout {
    /**
     * QuestionsView listener interface
     */
    public interface ViewListener {
        public void onBack();
        public void onMarketing();
        public void onNext(SubjectAnswer answer);
		public void onTally();
		public void onClear();
    }

    /**
     * Map to organize question type and corresponding view
     */
    private final Map<Question.Type, View> questionViewsByType = new EnumMap<Question.Type, View>(Question.Type.class);

    /**
     * QuestionsView listener
     */
    private ViewListener listener;
    /**
     * Question view
     */
    private QuestionView questionView;
    

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public QuestionsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public QuestionsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * QuestionsView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Set listener for photo type view
     * @param viewListener
     */
    public void setPhotoViewListener(PictureQuestionView.ViewListener viewListener) {
        ((PictureQuestionView)findViewById(R.id.picture)).setViewListener(viewListener);
    }

    /**
     * Set question on view
     * @param question
     */
    public void setQuestion(Question question) {
        questionView.setQuestion(question);
    }

    /**
     * Set picture on View
     * @param photo - picture bitmap
     * @param fileLocation - picture file location
     */
    public void setPhoto(Bitmap photo, String fileLocation) {
        questionView.setPhoto(photo, fileLocation);
    }

    /**
     * PetitionAnswers setter
     * @param petitionAnswers
     */
    public void setPetitionAnswers(PetitionAnswers petitionAnswers) {
        questionView.setPetitionAnswers(petitionAnswers);
    }

    /**
     * Set custom fon and subView listeners onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        questionView = (QuestionView) findViewById(R.id.question);

        Button marketingMaterialButton = (Button) findViewById(R.id.marketing_material_button);
        if (null != marketingMaterialButton) {
            marketingMaterialButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMarketing();
            }
        });
        }

        Button backButton = (Button) findViewById(R.id.back_button);
        if (null != backButton) {
            backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBack();
            }
        });
        }

        Button nextButton = (Button) findViewById(R.id.next_button);
        if (null != nextButton) {
            nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNext(questionView.getAnswer());
            }
        });
        }
        
        Button tallyButton = (Button) findViewById(R.id.tally_button);
        if (null != tallyButton) {
        	tallyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTally();;
            }
        });
        }
        
        Button clearButton = (Button) findViewById(R.id.clear_button);
        if (null != clearButton) {
        	clearButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClear();;
            }
        });
        }
    }

    public void setSignatureQuestion(Question question) {
        questionView.setSignatureQuestion(question);
    }

    public Signature getSignature() {
        // TODO Auto-generated method stub
        return questionView.getSignature();
    }
}