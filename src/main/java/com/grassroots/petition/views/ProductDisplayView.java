package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.*;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * ProductDisplayView
 * Display an individual product
 *
 * TODO: Make this a fragment so that we have proper access to stuff.
 */
public class ProductDisplayView extends LinearLayout {
    private static final String TAG = ProductDisplayView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    /**
     * Product name
     */
    private TextView nameText;
    /**
     * Product description
     */
    private TextView descriptionText;
    /**
     * Product price
     */
    private TextView costText;
    /**
     * Payment type
     */
    private TextView paymentType;

    /**
     * Default constructor
     * @param context
     */
    public ProductDisplayView(Context context) {
        super(context);
    }

    /**
     * Default Constructor
     * @param context
     * @param attrs
     */
    public ProductDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Fill the view with product
     * @param product - product object
     */
    public void setProduct(Product product) {
        if (null == product) {
            LOGGER.error("Got null product in setProduct");
            return;
        }

        configureName( product.getName() );
        configureDescription( product.getDescription() );
        configureCost( product.getCost() );
    }

    /**
     * Fill the view with product
     * Search product by SKU in petition
     * @param productOrder - product order
     * @param petition - petition that contain products
     * @param cart - cart and payment data
     */
    public void setProduct (ProductOrder productOrder, Petition petition, Cart cart) {
        if (null == productOrder) {
            LOGGER.error("Got null product order in setProduct");
            return;
        }

        if (null == petition) {
            LOGGER.error("Got null petition in setProduct");
            return;
        }

        List<Product> productsList = petition.getProducts();
        for (Product product : productsList) {
            if (productOrder.getSKU().equals(product.getSKU()))
            {
                // Product details
                configureName( product.getName() );
                configureDescription( product.getDescription() );

                // Cost
                String productCost = product.getCost();
                if (null == productCost || Strings.isEmpty(productCost)) {
                    productCost = productOrder.getCost();
                }
                configureCost( productCost );

                // Payment
                configurePaymentType( cart );

                return;
            }
        }

        LOGGER.error("Unable to found product with SKU = " + productOrder.getSKU());
    }

    /**
     * Configure product name in the view.
     *
     * @param name - name of product
     */
    private void configureName (String name)
    {
        nameText.setText(name);
    }

    /**
     * Configure product description in the view.
     *
     * @param description - description of product
     */
    private void configureDescription (String description)
    {
        descriptionText.setText(description);
    }

    /**
     * Configure product cost in the view.
     *
     * @param cost - cost of product
     *
     * TODO: Get dollar string ("$") from strings.xml instead
     */
    private void configureCost (String cost)
    {
        if (null != cost && Strings.isNotEmpty( cost )) {
        	float donAmt = Float.parseFloat(cost);
        	cost = String.format("%.2f", donAmt) ;
            costText.setText("$" + cost);
        }
    }

    /**
     * Fill the view with payment information.
     *
     * @param cart - contains payment data
     *
     * TODO: Move these strings to the cart
     */
    private void configurePaymentType (Cart cart)
    {
        if( cart.isCashPayment() )
        {
            paymentType.setText( "Cash" );
        }
        else if( cart.isPaperCheckPayment() )
        {
            paymentType.setText( "Paper Check" );
        }
        else if( cart.getPaymentData() instanceof CardData )
        {
            paymentType.setText( "Credit" );
        }
        else if( cart.getPaymentData() instanceof ACHData )
        {
            paymentType.setText( "Electronic Check" );
        }
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     *
     * TODO: Is the custom font thing really necessary?
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        nameText = (TextView) findViewById(R.id.name);
        descriptionText = (TextView) findViewById(R.id.description);
        costText = (TextView) findViewById(R.id.cost);
        paymentType = (TextView) findViewById( R.id.product_display_type );
    }
}
