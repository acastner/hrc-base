package com.grassroots.petition.views;

import java.util.Date;

import org.apache.log4j.Logger;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.tasks.LoadAppointmentCalendarEventsTask;
import com.grassroots.petition.tasks.UpdateAppointmentCalendarEventsTask;
import com.squareup.timessquare.CalendarPickerView;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class CalendarQuestionView extends LinearLayout{
	private static final String TAG = CalendarQuestionView.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

	private CalendarPickerView calendarPickerView;
	protected Dialog activeDialog = null;
	
	/**
     * Default constructor
     * @param context
     */
    public CalendarQuestionView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public CalendarQuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public CalendarQuestionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        
        configureCalendar();
		//updateCalendar();
    }  

    private void configureCalendar()
	{
		calendarPickerView = (CalendarPickerView) findViewById( R.id.calendar_view );
		initializeCalendar();
	}

	private void initializeCalendar()
	{
		Date today = new Date();
		java.util.Calendar nextYear = java.util.Calendar.getInstance();
		nextYear.add( java.util.Calendar.YEAR, 1 );
		calendarPickerView.init(today, nextYear.getTime());
		calendarPickerView.selectDate( today );
	}

	public void updateCalendar() {
		new LoadAppointmentCalendarEventsTask(new LoadAppointmentCalendarEventsTask.LoadAppointmentCalendarEventsTaskListener() {
			@Override
			public void onPreExecute() {
				showProgress(getContext().getString(R.string.UPDATING_APPOINTMENT_CALENDAR_EVENTS_MESSAGE));
			}

			@Override
			public void onPostExecute(Boolean result, String resultString) {
				hideActiveDialog();

				if (result) {
					LOGGER.info("Appointments successfully updated.");
					// TODO: some specific code can be placed here
					// NOTE: updated appointments is already in appointmentCalendar
				} else {
					LOGGER.warn("Error to update appointments.");
				}
			}
		}).execute(getContext());
	}
	
	/**
     * Show a ProgressDialog notification to the user
     * @param message
     */
    public void showProgress(String message) {
        hideActiveDialog();
        activeDialog = new ProgressDialog(getContext());
        ((ProgressDialog)activeDialog).setMessage(message); //"Logging in...");
        activeDialog.setCancelable(false);
        activeDialog.show();
    }

    /**
     * Hides an ActiveDialog from user
     */
    public void hideActiveDialog() {
        if (activeDialog != null) {
            activeDialog.dismiss();
            activeDialog = null;
        }
    }
    
}
