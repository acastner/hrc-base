package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.grassroots.petition.R;
import com.grassroots.petition.utils.TypeFaceSetter;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Saraseko Oleg
 * Date: 16.07.13
 * Time: 19:45
 */
public class TimeKeyboardView extends BaseLinearLayout {
    public static String TAG = TimeKeyboardView.class.getSimpleName();

    // Data
    Calendar enteredTime;

    // UI
    private View zeroButton;
    private View fifteenMinuteButton;
    private View thirtyMinuteButton;
    private View fortyFiveMinuteButton;
    private View amButton;
    private View pmButton;

    // Time changed listener
    ViewListener listener;
    public static interface ViewListener {
        public void onTimeChanged(Calendar time);
    }

    public void setTimeChangedListener(ViewListener listener)
    {
        this.listener = listener;
    }

    private void notifyTimeChanged() {
        if (null != listener && null != enteredTime) {
            listener.onTimeChanged(enteredTime);
        }
    }

    public void setInitialTime(Calendar initialTime) {
        enteredTime.clear();
        enteredTime.setTime(initialTime.getTime());
        setHourViewSelection(enteredTime.get(Calendar.HOUR), true);
        setMinute(zeroButton, 0);
        configureAMPM();
    }

    private void setMinute(View view, int minute) {
        enteredTime.set(Calendar.MINUTE, minute);
        view.setSelected(true);
    }

    private void configureAMPM() {
        if (enteredTime.get(Calendar.AM_PM) == Calendar.AM) {
            setAm();
        } else if (enteredTime.get(Calendar.AM_PM) == Calendar.PM) {
            setPm();
        } else {
            Log.d(TAG, "Unclear AM/PM state");
        }
    }

    private void setHourViewSelection(int hour, boolean isSelected) {
        getViewWithTag(hour).setSelected(isSelected);
    }

    private View getViewWithTag(int tag) {
        return findViewWithTag("" + tag);
    }

    public TimeKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        enteredTime = Calendar.getInstance();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        configureHourButtons();
        configureMinuteButtons();
        configureAMPMButtons();
    }

    private void configureHourButtons() {
        zeroButton = findViewById(R.id.buttonMinute00);
        fifteenMinuteButton = findViewById(R.id.buttonMinute15);
        thirtyMinuteButton = findViewById(R.id.buttonMinute30);
        fortyFiveMinuteButton = findViewById(R.id.buttonMinute45);

        for (int tag = 1; tag <= 12; tag++) {
            getViewWithTag(tag).setOnClickListener(createHourClickListener());
        }
    }

    private OnClickListener createHourClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view) {
                clearHours();
                setHour(getViewTagAsInt(view));
                notifyTimeChanged();
            }
        };
    }

    private void clearHours() {
        for (int hour = 1; hour <= 12; hour++) {
            setHourViewSelection(hour, false);
        }
    }

    private void setHour(int hour) {
        if (hour == 12) {
            updateNoonOrMidnight();
        } else {
            enteredTime.set(Calendar.HOUR, hour);
        }

        setHourViewSelection(hour, true);
    }

    private void updateNoonOrMidnight() {
        if (amButton.isSelected()) {
            enteredTime.set(Calendar.HOUR_OF_DAY, 0);
        } else if (pmButton.isSelected()) {
            enteredTime.set(Calendar.HOUR_OF_DAY, 12);
        } else {
            Log.d(TAG, "Unclear AM/PM state");
        }
    }

    private int getViewTagAsInt(View view) {
        return Integer.parseInt((String) view.getTag());
    }

    private void configureMinuteButtons() {
        zeroButton.setOnClickListener(setMinuteClickListener());
        fifteenMinuteButton.setOnClickListener(setMinuteClickListener());
        thirtyMinuteButton.setOnClickListener(setMinuteClickListener());
        fortyFiveMinuteButton.setOnClickListener(setMinuteClickListener());
    }

    private OnClickListener setMinuteClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view) {
                clearMinutes();
                setMinute(view, getViewTagAsInt(view));
                notifyTimeChanged();
            }
        };
    }

    private void clearMinutes() {
        zeroButton.setSelected(false);
        fifteenMinuteButton.setSelected(false);
        thirtyMinuteButton.setSelected(false);
        fortyFiveMinuteButton.setSelected(false);
    }

    private void configureAMPMButtons() {
        amButton = findViewById(R.id.buttonAM);
        amButton.setOnClickListener(createAMClickListener());

        pmButton = findViewById(R.id.buttonPM);
        pmButton.setOnClickListener(createPMClickListener());
    }

    private OnClickListener createAMClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                setAm();
                notifyTimeChanged();
            }
        };
    }

    private OnClickListener createPMClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View view) {
                setPm();
                notifyTimeChanged();
            }
        };
    }

    private void setPm() {
        enteredTime.set(Calendar.AM_PM, Calendar.PM);
        amButton.setSelected(false);
        pmButton.setSelected(true);
    }

    private void setAm() {
        enteredTime.set(Calendar.AM_PM, Calendar.AM);
        amButton.setSelected(true);
        pmButton.setSelected(false);
    }
}
