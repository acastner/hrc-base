package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Product;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * Product row View
 * Display one row with an individual product
 * TODO: Is this used?
 */
public class ProductRowView extends LinearLayout {
    /**
     * ProductRowView listener interface
     */
    public static interface ViewListener {
        public void onChecked();
    }

    /**
     * Product Display custom view
     */
    private ProductDisplayView productDisplay;
    /**
     * Row checkbox
     */
    private RadioButton checkbox;
    /**
     * ProductRowView listener
     */
    private ViewListener listener;
    /**
     * Product
     */
    private Product product;

    /**
     * Default constructor
     * @param context
     */
    public ProductRowView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public ProductRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * ProductRowView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * @return view product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Fill view with product
     * @param product
     */
    public void setProduct(Product product) {
        this.product = product;
        productDisplay.setProduct(product);
    }

    /**
     * @return row checkbox
     */
    public RadioButton getCheckbox() {
        return checkbox;
    }

    /**
     * @return true if product is selected, false - otherwise
     */
    public boolean isProductSelected() {
        return checkbox.isChecked();
    }

    /**
     * Clear check state for row checkbox
     */
    public void uncheck() {
        checkbox.setChecked(false);
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        productDisplay = (ProductDisplayView) findViewById(R.id.product_display);
        checkbox = (RadioButton) findViewById(R.id.checkbox);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(checked) {
                    listener.onChecked();
                }
            }
        });

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkbox.setChecked(true);
            }
        });
    }

    /**
     * Object comparator
     * @param other
     * @return true if object is equal to this instance, false - otherwise
     */
    public boolean equals(Object other) {
        if(other instanceof ProductRowView) {
            Product otherProduct = ((ProductRowView) other).getProduct();
            if(product == null) {
                return otherProduct == null;
            } else {
                return product.equals(otherProduct);
            }
        }
        return false;
    }

    /**
     * @return object hash code
     */
    public int hashCode() {
        if(product == null)
            return 13;
        return product.hashCode();
    }
}
