package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.KnownSubjectListAdapter;

import java.util.List;

/**
 * SubjectSearchView
 * This is unused but may be in the future so leaving it in
 */
public class SubjectSearchView extends RelativeLayout {
    /**
     * SubjectSearchView listener interface
     */
    public static interface ViewListener {
        public void onBack();

        public void onSearch(String firstName, String lastName, String zip);

        public void onNewSubject(Subject subject);
    }

    /**
     * SubjectSearchView listener
     */
    private ViewListener listener;
    /**
     * Subject first name
     */
    private EditText firstName;
    /**
     * Subject last name
     */
    private EditText lastName;
    /**
     * Subject address zip code
     */
    private EditText zip;
    /**
     * Results list view
     */
    private ListView listView;
    private LayoutInflater inflator;

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SubjectSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflator = LayoutInflater.from(context);
    }

    /**
     * SubjectSearchView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Init adapter with known subjects
     * @param knownSubjects
     */
    public void setKnownSubjects(List<Subject> knownSubjects) {
        listView.setAdapter(new KnownSubjectListAdapter(knownSubjects, inflator));
    }

    private String getFirstName() {
        return firstName.getText().toString();
    }

    private String getLastName() {
        return lastName.getText().toString();
    }

    private String getZip() {
        return zip.getText().toString();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
        firstName = (EditText) findViewById(R.id.search_first_name);
        lastName = (EditText) findViewById(R.id.search_last_name);
        zip = (EditText) findViewById(R.id.search_zip);

        listView = (ListView) findViewById(R.id.subject_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Subject subject = (Subject) adapterView.getItemAtPosition(i);
                listener.onNewSubject(subject);
            }
        });


        Button backButton = (Button) findViewById(R.id.search_back_button);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBack();
            }
        });

        Button searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSearch(getFirstName(), getLastName(), getZip());
            }
        });

        Button newSubjectButton = (Button) findViewById(R.id.new_subject_button);
        newSubjectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Subject subject = new Subject();
                subject.setFirstName(getFirstName());
                subject.setLastName(getLastName());
                subject.setZip(getZip());
                listener.onNewSubject(subject);
            }
        });
    }
}
