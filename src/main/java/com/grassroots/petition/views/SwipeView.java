package com.grassroots.petition.views;

import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bbpos.swiper.SwiperController;
import com.bbpos.swiper.SwiperController.SwiperControllerState;
import com.grassroots.petition.R;
import com.grassroots.petition.models.CardData;
import com.grassroots.petition.services.CardSwipeReaderMagTek;
import com.grassroots.petition.services.CardSwipeReaderROAM;
import com.grassroots.petition.services.SwiperCallStateService;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * View for reading card data(TekMag
 */
public class SwipeView extends LinearLayout {
	private static final String TAG = SwipeView.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	private IncomingCallServiceReceiver incomingCallServiceReceiver;
	private final static String INTENT_ACTION_CALL_STATE = "com.grassroots.petition.CALL_STATE";

	public static interface ViewListener {
		public void onReceiveCardData(CardData cardData);

		public void onEndInterview();

		public void onMarketing();

	}

	public CardSwipeReaderMagTek cardSwipeReaderMagtek;
	public CardSwipeReaderROAM cardSwipeReaderROAM;

	private TextView statusText;
	private ViewListener listener;

	public boolean isMagtek;
	private boolean isROAM;

	private SwiperController roamSwiperController;

	private CardSwipeReaderMagTek.CardSwipeListener cardListener = new CardSwipeReaderMagTek.CardSwipeListener() {
		@Override
		public void onStartSwipe() {
			setStatus(getContext().getString(
					R.string.CARD_READER_CONNECTED_MESSAGE));
		}

		@Override
		public void onReceiveCardData(CardData cardData) {
			LOGGER.debug("onReceiveCardData()");

			LOGGER.debug("device=" + cardData.getDevice());
			LOGGER.debug("serial=" + cardData.getSerial());
			LOGGER.debug("track1=" + cardData.getTrack1());
			LOGGER.debug("track2=" + cardData.getTrack2());

			// cardSwipeReader.cleanup();
			setStatus(getContext().getString(R.string.SWIPE_RECEIVED_MESSAGE));
			listener.onReceiveCardData(cardData);
		}

		@Override
		public void onFailure(int index, String msg) {
			setStatus(getContext().getString(R.string.FAILED_READ_CARD_MESSAGE)
					+ msg);

		}

		@Override
		public void onConnecting() {
			setStatus(getContext().getString(
					R.string.PREPARING_CARD_READER_MESSAGE));
		}

		@Override
		public void onConnected() {
			setStatus(getContext().getString(
					R.string.CARD_READER_CONNECTED_MESSAGE));
		}

		@Override
		public void onDisconnected() {
			setStatus(getContext().getString(
					R.string.CARD_READER_DISCONNECTED_MESSAGE));
		}

		@Override
		public void onTimeout(String msg) {
			setStatus(getContext().getString(
					R.string.CARD_READ_TIME_OUT_MESSAGE)
					+ msg);
			// no such function found in MagTek driver reference manual found
		}
	};

	public SwipeView(Context context) {
		super(context);
	}

	public SwipeView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setViewListener(ViewListener listener) {
		this.listener = listener;
	}

	/**
	 * used by BaseViewActivity to enable/disable MagTek reader
	 */
	public void openReader() {
		if (isMagtek) {
			if (!cardSwipeReaderMagtek.isHeadsetPluggedIn()) {
				setStatus(getContext().getString(
						R.string.PLUG_IN_READER_MESSAGE));
				return;
			}

			cardSwipeReaderMagtek.openReader();
			if (cardSwipeReaderMagtek.isReady())
				setStatus(getContext().getString(
						R.string.SWIPEVIEW_READY_MESSAGE));
			else
				setStatus(getContext().getString(
						R.string.SWIPEVIEW_NOT_READY_QUESTION_MESSAGE));
		} else if (isROAM) {
			
				startCallStateService();
				//setSwiperControllerValues();
				if(roamSwiperController != null) {
					roamSwiperController.deleteSwiper();
				}

				try {
					if(SwiperController.getInstance() != null) {
						SwiperController.getInstance().deleteSwiper();
					}
				} catch(Exception ex) {
					ex.printStackTrace();
				}

				try {
					SwiperController.createInstance(getContext(),
							cardSwipeReaderROAM);
				} catch(Exception ex) {
					ex.printStackTrace();
				}

				roamSwiperController = SwiperController.getInstance();
				setSwiperControllerValues();
				try {
					
				if (!roamSwiperController.isDevicePresent()) {
					setStatus(getContext().getString(
							R.string.PLUG_IN_READER_MESSAGE));
					return;
				} 
				if (roamSwiperController.getSwiperState() == SwiperControllerState.STATE_IDLE) {
					setStatus(getContext().getString(
							R.string.SWIPEVIEW_NOT_READY_QUESTION_MESSAGE));
					GetFirmwareTask getfirmwaretask = new GetFirmwareTask();
					getfirmwaretask.execute();
					return;

				} else {
					setStatus(getContext().getString(
							R.string.SWIPEVIEW_READY_MESSAGE));

					return;
				}
			} catch (IllegalStateException ex) {
				ex.printStackTrace();
			}
		}

	}

	public void closeReader() {
		if (isMagtek) {
			cardSwipeReaderMagtek.closeReader();
		} else if (isROAM) {
			if (roamSwiperController != null) {
				try {
					if (roamSwiperController.getSwiperState() != SwiperControllerState.STATE_IDLE) {
						roamSwiperController.stopSwiper();
					}
					roamSwiperController.deleteSwiper();
					roamSwiperController = null;
				} catch (Exception ex) {

				}
			}
		}
		try{
		if (incomingCallServiceReceiver != null) {
			getContext().unregisterReceiver(incomingCallServiceReceiver);
		}
		}catch(Exception e){
			e.printStackTrace();
		}

		setStatus(getContext().getString(
				R.string.SWIPEVIEW_READER_CLOSED_MESSAGE));
	}

	public void resetReader() {
		closeReader();
		if (isMagtek) {
			cardSwipeReaderMagtek.reset();
		}
		openReader();
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		TypeFaceSetter.setRobotoFont(getContext(), this);

		/*
		 * findViewById(R.id.marketing_material_button).setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View view) { listener.onMarketing(); }
		 * });
		 * 
		 * findViewById(R.id.end_interview_button).setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View view) { listener.onEndInterview();
		 * } });
		 */
		if (getResources().getString(R.string.cardreader).equalsIgnoreCase(
				"magtek")) {
			isMagtek = true;
			isROAM = false;
		} else {
			isROAM = true;
			isMagtek = false;
		}
		if (isMagtek) {
			cardSwipeReaderMagtek = new CardSwipeReaderMagTek(getContext(),
					cardListener);
		} else if (isROAM) {
			cardSwipeReaderROAM = new CardSwipeReaderROAM(SwipeView.this,
					cardListener);

		}

		statusText = (TextView) findViewById(R.id.status_text);
		setStatus(getContext()
				.getString(R.string.PREPARING_CARD_READER_MESSAGE));
		// TODO have an actual card reader here

		// change to true line below to enable fake data for testing without
		// reader
		if (false) {
			postDelayed(new Runnable() {
				@Override
				public void run() {
					CardData fakeData = new CardData(
							"MagTekM20",
							"1234567890AA",
							"JUI1MjgyMTA1MDA4OTg1MTI0XlBBWU1FTlRTICAgICAgICAgICAgL0dCIFFBXjE0MDExMDExMDAwMDA3NTIwPw==",
							"nMEDHTwy/GvNmy3wsDjHTFz5kAMkhD59fc/nxIVYdOsEzBrf/NwvlsWzRlbNZ+o8CZVb9ukq0NR1jXthI9eLqg==",
							null,null);

					cardListener.onReceiveCardData(fakeData);
				}
			}, 1000 * 2);
		}
	}

	public void setStatus(final String status) {
		LOGGER.debug(status);
		post(new Runnable() {
			@Override
			public void run() {
				statusText.setText(status);
			}
		});
	}

	// -----------------------------------------------------------------------
	// Inner classes
	// -----------------------------------------------------------------------

	private class IncomingCallServiceReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent
					.getAction()
					.equals(com.grassroots.petition.services.SwiperCallStateService.INTENT_ACTION_INCOMING_CALL)) {
				setStatus("Incoming call detected!");
				try {
					if (roamSwiperController.getSwiperState() != SwiperControllerState.STATE_IDLE) {
						roamSwiperController.stopSwiper();
					}
				} catch (IllegalStateException ex) {
					setStatus("Invalid state");
					ex.printStackTrace();
				}
			}
		} // end-of onReceive
	}

	private void startCallStateService() {
		getContext().startService(new Intent(INTENT_ACTION_CALL_STATE));
		if (incomingCallServiceReceiver == null) {
			incomingCallServiceReceiver = new IncomingCallServiceReceiver();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(SwiperCallStateService.INTENT_ACTION_INCOMING_CALL);
			getContext().registerReceiver(incomingCallServiceReceiver, intentFilter);
		}
	}

	private void setSwiperControllerValues() {
		if (roamSwiperController == null) return;
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
		boolean setDetectDeviceChangePref = settings.getBoolean("setDetectDeviceChangePref", true);
		boolean setFskRequiredPref = settings.getBoolean("setFskRequiredPref", false);
		String setTimeoutlistPref = settings.getString("setTimeoutlistPref", "-1");
		String setKsnChargeUplistPref = settings.getString("setKsnChargeUplistPref", "0.6");
		String setSwipeChargeUplistPref = settings.getString("setSwipeChargeUplistPref", "0.6");
		roamSwiperController.setDetectDeviceChange(setDetectDeviceChangePref);
		roamSwiperController.setFskRequired(setFskRequiredPref);
		roamSwiperController.setSwipeTimeout(Double.parseDouble(setTimeoutlistPref));
		roamSwiperController.setChargeUpTime(Double.parseDouble(setSwipeChargeUplistPref));
		roamSwiperController.setKsnChargeUpTime(Double.parseDouble(setKsnChargeUplistPref));
	}

	public SwiperController getRoamController() {
		return roamSwiperController;
	}

	private class GetFirmwareTask extends AsyncTask<Void, Void, String> {

		protected void onPreExecute() {
		}

		protected String doInBackground(Void... params) {
			try {
				if (roamSwiperController.getSwiperState() == SwiperControllerState.STATE_IDLE) {
					return roamSwiperController.getSwiperFirmwareVersion();
				}
				return null;
			} catch(Exception ex) {
				LOGGER.debug(ex.getMessage());
				return null;
			}
				
		}

		protected void onPostExecute(String firmware) {
			Log.e("firm","task firware :"+firmware);
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("apiVersionPref", SwiperController.getSwiperAPIVersion());
			editor.putString("firmwareVersionPref", firmware);
			editor.commit();
			if(firmware == null || firmware.equalsIgnoreCase("")){
				//Toast.makeText(getContext(), "Cannot detect ROAM reader.Is the correct device plugged in?", Toast.LENGTH_LONG).show();
				setStatus("Sorry,the device plugged in couldn't be detected");
			}else{
				try{
					roamSwiperController.startSwiper();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
		}
	}

}
