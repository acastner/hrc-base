package com.grassroots.petition.views.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;
import org.apache.log4j.Logger;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class AddressRowView extends RelativeLayout {
    private static final String TAG = AddressRowView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    private TextView addressText;

    public AddressRowView(Context context) {
        super(context);
    }

    public AddressRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AddressRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setSubject(Subject address) {
        if (address == null) {
            LOGGER.error("Null subject set on address row");
            return;
        }
        addressText.setText(address.getFullAddress());
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        addressText = (TextView) findViewById(R.id.address_line);
    }

}
