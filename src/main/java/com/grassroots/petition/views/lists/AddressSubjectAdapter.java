package com.grassroots.petition.views.lists;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseActivity;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BaseSubjectInfoActivity;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.fragments.WalklistDetailsFragment;
import com.grassroots.petition.models.*;
import com.grassroots.petition.services.GrassrootsRestClient;
import com.grassroots.petition.utils.LocationProcessor;
import com.grassroots.petition.views.HomeStatusView;

import java.util.ArrayList;
import java.util.List;

// TODO: Apply holder pattern
public class AddressSubjectAdapter extends BaseAdapter
{
    private final WalklistDetailsFragment detailsFragment;
    private final List<Subject> subjectList;
    private final List<Question> questionList;

    // TODO: Do I need this passed in?
    private final LayoutInflater inflater;

    public AddressSubjectAdapter (WalklistDetailsFragment detailsFragment,
                                  List<Subject> subjects,
                                  List<Question> questions,
                                  LayoutInflater inflater)
    {
        this.detailsFragment = detailsFragment;
        this.subjectList = subjects;
        this.inflater = inflater;
        this.questionList = questions;
    }

    @Override
    public int getCount ()
    {
    	if(subjectList == null){
    		return 0;
    	}
        return subjectList.size();
    }

    @Override
    public Object getItem (int i)
    {
        return subjectList.get( i );
    }

    @Override
    public long getItemId (int i)
    {
        return i;
    }

    @Override
    public View getView (int i, View view, ViewGroup viewGroup)
    {
        // Inflate row
        SubjectRowView rowView;
        if( view instanceof SubjectRowView ) {
            rowView = (SubjectRowView) view;
        }
        else {
            rowView = (SubjectRowView) inflater.inflate( R.layout.subject_row, null );
        }

        // Data
        final Subject subject = subjectList.get( i );
        rowView.setSubject( subject, questionList );

        // Buttons
        View individualStatusButton = rowView.findViewById( R.id.set_individual_status_button );
        View surveyButton = rowView.findViewById( R.id.start_survey_button );

        // State: Survey not complete
        // Clicking the cell/button starts the survey and the status can be set for this individual.
        if (subject.getKnockStatus() != KnockStatus.SUCCESS )
        {
            // Enable survey
            surveyButton.setEnabled( true );
            surveyButton.setOnClickListener( createRowClickListener( subject ) );

            // Enable status button
            individualStatusButton.setEnabled( true );
            individualStatusButton.setOnClickListener( createStatusDialogListener( subject ) );
        }
        // State: Survey complete
        // Disable buttons, don't add click listeners.
        else {
            surveyButton.setEnabled( false );
            individualStatusButton.setEnabled( false );
        }
        surveyButton.setVisibility(View.INVISIBLE);
        individualStatusButton.setVisibility(View.INVISIBLE);
        return rowView;
    }

    private View.OnClickListener createRowClickListener (final Subject subject)
    {
        return v -> detailsFragment.startSurvey( subject );
    }

    private View.OnClickListener createStatusDialogListener (final Subject subject)
    {
        return v -> showStatusDialog( subject );
    }

    private void showStatusDialog (final Subject subject)
    {
        final Dialog statusDialog = new Dialog( detailsFragment.getActivity() );
        statusDialog.setTitle( R.string.home_status );
        HomeStatusView view = (HomeStatusView) View.inflate( detailsFragment.getActivity(), R.layout.home_status, null );

        view.setViewListener( new HomeStatusView.ViewListener()
        {
            @Override
            public void onStatusSelected (KnockStatus knockStatus)
            {
                Knocks knocks = new Knocks();
                if( subject != null ) {
                    GpsLocation location = LocationProcessor.getLastKnownGpsLocation( detailsFragment.getActivity() );
                    WalklistDataSource walklistDataSource = new WalklistDataSource(detailsFragment.getActivity());
                    List<Subject> subjects = new ArrayList<Subject>();
                    subject.setKnockedStatus(knockStatus);
                    subjects.add(subject);
                    walklistDataSource.insertWalklist(subjects,((BasePetitionActivity)(detailsFragment.getActivity())).getGlobalData().getLocation());
                    detailsFragment.getWalkList().setKnocked(subject.getId(), knockStatus);
                    detailsFragment.refreshWalklist();
                    detailsFragment.updateSubject(subject);
                    String hid = subject.getHid();
                    if(hid  == null){
                      //call a knock event with subject information
                        KnockEvent knockEvent = new KnockEvent(knockStatus, location, subject.getFirstName(), subject.getLastName(),
                                subject.getAddressLine1(), subject.getAddressLine2(), subject.getCity(), subject.getState(), 
                                subject.getZip(), subject.getPhone(), subject.getEmail(),((BasePetitionActivity)(detailsFragment.getActivity())).getGlobalData().getLocation());
                        knocks.addKnockEvent( knockEvent );
                    }else{
                        knocks.addKnockEvent( ((BasePetitionActivity)(detailsFragment.getActivity())).addBatteryInfoToKnock(new KnockEvent( Integer.parseInt( hid ), knockStatus, location )) );
                    }
                }

                try {
                    GrassrootsRestClient.getClient().saveKnocks( knocks );
                } catch( Exception exception ) {
                    BaseActivity activity = (BaseActivity) detailsFragment.getActivity();
                    activity.logoutWithError( detailsFragment.getString( R.string.error_unable_to_send_knock_status ),
                                              exception );
                }
                statusDialog.cancel();
            }

            @Override
            public void onCancel ()
            {
                statusDialog.cancel();
            }
        } );

        statusDialog.setContentView( view );
        statusDialog.show();
    }
}
