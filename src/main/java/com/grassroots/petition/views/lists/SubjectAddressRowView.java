package com.grassroots.petition.views.lists;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.Strings;
import org.apache.log4j.Logger;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class SubjectAddressRowView extends LinearLayout implements Checkable {
	private static final String TAG = SubjectAddressRowView.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);
	private TextView addressText;
	private TextView statusText;
	private boolean isChecked = false;
	private ImageView colorView = null;
	private Map<Subject, List<Subject>> addSubjectMap;

	public SubjectAddressRowView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setSubject(Subject address) {
		if (address == null) {
			LOGGER.error("Null subject set on address row");
			return;
		}
		Log.e("address","address "+address.getAddressLine1());
		int winningcolor = 0;

		if(addSubjectMap.containsKey(address)){
			List<Subject> subjects = addSubjectMap.get(address);
			for(Subject subject : subjects){
				if(subject.getColor() != null){
					int color = Color.parseColor("#"+subject.getColor());
					LOGGER.debug("address: " + address + " subject color: " + color + " winning color: " + winningcolor);

					/*
					 * lighter colors always win.
					 * color ints are < 0 (lower = darker) unless it is black (zero).
					 * ex: -1 is lighter than -2 , but 0 is black.
					 */
					if((color > winningcolor && winningcolor < 0) || winningcolor == 0){
						winningcolor = color;
					}

				}
			}
		}

		StringBuilder addressBuilder = new StringBuilder();
		String separator = "";
		if (Strings.isNotEmpty(address.getAddressLine1())) {
			addressBuilder.append(address.getAddressLine1());
			separator = ", ";
		}
		if (Strings.isNotEmpty(address.getAddressLine2())) {
			addressBuilder.append(separator);
			addressBuilder.append(address.getAddressLine2());
		}



		if(winningcolor != 0){
			colorView.setBackgroundColor(winningcolor);
			addressText.setTextColor(winningcolor);
		}else{
			colorView.setBackgroundColor(Color.TRANSPARENT);
			addressText.setTextColor(Color.BLACK);
		}

		addressText.setText(addressBuilder.toString());
	}

	public void setStatus(KnockStatus status) {
		if (status == null) {
			return;
		}
		statusText.setText(status.displayText());
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		addressText = (TextView) findViewById(R.id.address_line);
		statusText = (TextView) findViewById(R.id.household_status);
		colorView = (ImageView) findViewById(R.id.colorBar);
	}

	@Override
	public void setChecked(boolean b) {
		isChecked = b;
		if (isChecked) {
			setBackgroundResource(R.color.selected_list_item);
		} else {
			setBackgroundResource(R.color.white);
		}
	}

	@Override
	public boolean isChecked() {
		return isChecked;
	}

	@Override
	public void toggle() {
		setChecked(!isChecked);
	}

	public void setSubjectAddressMap(
			Map<Subject, List<Subject>> addressSubjectMap) {
		this.addSubjectMap = addressSubjectMap;

	}
}
