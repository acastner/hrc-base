package com.grassroots.petition.views.lists;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class AddressListAdapter extends BaseAdapter {

    private final List<Subject> subjectAddresses;
    private final Context context;

    public AddressListAdapter(Set<Subject> subjectAddresses, Context context) {
        this.subjectAddresses = new ArrayList<Subject>(subjectAddresses);
        this.context = context;
    }

    @Override
    public int getCount() {
        return subjectAddresses.size();
    }

    @Override
    public Object getItem(int i) {
        return subjectAddresses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AddressRowView addressRow;
        if (view instanceof AddressRowView) {
            addressRow = (AddressRowView) view;
        } else {
            addressRow = (AddressRowView) View.inflate(context, R.layout.address_row, null);
        }
        addressRow.setSubject(subjectAddresses.get(i));
        return addressRow;
    }
}
