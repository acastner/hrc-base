package com.grassroots.petition.views.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class SubjectSearchRowView extends LinearLayout {
    public SubjectSearchRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSubject(Subject subject) {
        TextView firstName = (TextView) findViewById(R.id.first_name);
        firstName.setText(subject.getFirstName());

        TextView lastName = (TextView) findViewById(R.id.last_name);
        lastName.setText(subject.getLastName());

        TextView address = (TextView) findViewById(R.id.address);
        address.setText(subject.getAddressLine1());
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
    }
}
