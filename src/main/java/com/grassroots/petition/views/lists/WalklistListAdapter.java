package com.grassroots.petition.views.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.grassroots.petition.R;
import com.grassroots.petition.activities.BaseWalklistActivity;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.Walklist;
import com.grassroots.petition.models.Walklist.HouseNumberFilter;
import com.grassroots.petition.utils.Performance;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class WalklistListAdapter extends BaseAdapter implements Filterable {

	private final Walklist walklist;
	private List<Subject> addresses;
	private final LayoutInflater inflater;
	private Walklist.HouseNumberFilter houseNumberFilter;
	private KnockStatus knockStatusFilter;
	private String streetFilter;
	private BaseWalklistActivity context;
	private String lastNameFilter;

	public WalklistListAdapter(Walklist walklist, LayoutInflater inflater,
			BaseWalklistActivity context) {
		long startTime = System.currentTimeMillis();
		this.walklist = walklist;
		this.inflater = inflater;
		houseNumberFilter = Walklist.HouseNumberFilter.ALL;
		knockStatusFilter = null;
		List<Subject> list = walklist.getAddressSubjects(houseNumberFilter);
		setAddresses(list);
		Performance.end(startTime, "Creating new walklist adapter");
		this.context = context;
	}

	@Override
	public int getCount() {
		return addresses.size();
	}

	@Override
	public Object getItem(int i) {
		return addresses.get(i);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	public int getPositionOf(Subject address) {
		if(address.getLatitude()==null){
			address.setLatitude("0.0");
		}
		if(address.getLongitude()==null){
			address.setLongitude("0.0");
		}
		if(addresses.contains(address)){
			Log.e("address : ","address is in list");
		}
		
		return addresses.indexOf(address);
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		SubjectAddressRowView rowView;
		if (view instanceof SubjectAddressRowView) {
			rowView = (SubjectAddressRowView) view;
		} else {
			rowView = (SubjectAddressRowView) inflater.inflate(
					R.layout.subject_address_row, null);
			rowView.setSubjectAddressMap(walklist.addressSubjectMap);

		}
		Subject address = addresses.get(i);
		rowView.setSubject(address);
        
		KnockStatus highestStatus = walklist.getHighestStatus(address);
		rowView.setStatus(highestStatus);
		return rowView;
	}

	// TODO figure out why this method gets ignored
	private void setBackgroundColorBasedOnKnocks(SubjectAddressRowView rowView,
			Subject address) {
		boolean anyKnocked = false;
		List<Subject> sameAddressSubjects = walklist
				.getSameAddressSubjects(address);
		boolean allKnocked = !sameAddressSubjects.isEmpty();
		for (Subject subject : sameAddressSubjects) {
			if (subject.isKnocked()) {
				anyKnocked = true;
			} else {
				allKnocked = false;
			}
		}
		if (allKnocked) {
			rowView.setBackgroundResource(R.color.addresses_all_knocked);
		} else if (anyKnocked) {
			rowView.setBackgroundResource(R.color.addresses_any_knocked);
		}
	}

	public List<Subject> setHouseNumberFilter(Walklist.HouseNumberFilter filter) {
		long startTime = System.currentTimeMillis();
		houseNumberFilter = filter;
		if (context.isLocationList) {
			if (knockStatusFilter == null
					&& (houseNumberFilter == null || houseNumberFilter
							.equals(HouseNumberFilter.ALL))
					&& (lastNameFilter == null || lastNameFilter.equals(""))) {
				setAddresses(walklist.getAddressSubjects());
			} else {
				setAddresses(new Walklist(walklist.filterStreetAddress(
						knockStatusFilter, lastNameFilter)).getAddressSubjects());
			}
		} else {
			if ((streetFilter == null || streetFilter.equalsIgnoreCase("")) && ( houseNumberFilter == null
					|| houseNumberFilter.equals(HouseNumberFilter.ALL))
					&& knockStatusFilter == null) {
				setAddresses(walklist.getAddressSubjects());
			} else {
				setAddresses(new Walklist(walklist.filterStreetAddress(streetFilter,
						houseNumberFilter, knockStatusFilter)).getAddressSubjects());
			}
		}
		notifyDataSetChanged();
		Performance.end(startTime, "Setting house number filter");
		return addresses;
	}

	public List<Subject> setKnockStatusFilter(KnockStatus filter) {
		long startTime = System.currentTimeMillis();
		knockStatusFilter = filter;
		if (context.isLocationList) {
			if (knockStatusFilter == null
					&& (houseNumberFilter == null || houseNumberFilter
							.equals(HouseNumberFilter.ALL))
					&& (lastNameFilter == null || lastNameFilter.equals(""))) {
				setAddresses(walklist.getAddressSubjects());
			} else {
				setAddresses(new Walklist(walklist.filterStreetAddress(
						knockStatusFilter, lastNameFilter)).getAddressSubjects());
			}

		} else {
			if (streetFilter.equalsIgnoreCase("") && ( houseNumberFilter == null
					|| houseNumberFilter.equals(HouseNumberFilter.ALL))
					&& knockStatusFilter == null) {
				setAddresses(walklist.getAddressSubjects());
			} else {
				setAddresses(new Walklist(walklist.filterStreetAddress(streetFilter,
						houseNumberFilter, knockStatusFilter)).getAddressSubjects());
			}
		}
		notifyDataSetChanged();
		Performance.end(startTime, "Setting knock status filter");
		return addresses;
	}

	public List<Subject> setStreetNameFilter(String filter) {
		long startTime = System.currentTimeMillis();
		streetFilter = filter;
		if (knockStatusFilter == null
				&& (houseNumberFilter == null || houseNumberFilter
						.equals(HouseNumberFilter.ALL))
				&& (filter == null || filter.equals(""))) {
			setAddresses(walklist.getAddressSubjects());
		} else {
			setAddresses(new Walklist(walklist.filterStreetAddress(filter, houseNumberFilter, knockStatusFilter)).getAddressSubjects());
		}
		notifyDataSetChanged();

		Performance.end(startTime, "Setting last name filter");
		return addresses;

	}
	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				streetFilter = charSequence.toString();

				FilterResults results = new FilterResults();
				if (streetFilter.equalsIgnoreCase("")
						&& (houseNumberFilter == null || houseNumberFilter.equals(HouseNumberFilter.ALL))
						&& knockStatusFilter == null) {
					results.values = walklist.getAddressSubjects(houseNumberFilter);
				} else {
					results.values = new Walklist(walklist.filterStreetAddress(streetFilter,
							houseNumberFilter, knockStatusFilter)).getAddressSubjects();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
			
				setAddresses((List<Subject>) filterResults.values);
				WalklistListAdapter.this.notifyDataSetChanged();
			}
		};
	}

	private void setAddresses(List<Subject> addresses) {
		this.addresses = new ArrayList<Subject>(addresses);
		long startTime = System.currentTimeMillis();
		Collections.sort(this.addresses, Subject.ADDRESS_COMPARATOR);
		Performance.end(startTime, "sorting addresses");
	}

	public List<Subject> getAddresses() {
		return addresses;
	}
}
