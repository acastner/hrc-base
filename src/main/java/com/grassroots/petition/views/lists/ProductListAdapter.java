package com.grassroots.petition.views.lists;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Product;
import com.grassroots.petition.views.ProductRowView;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Is this used?
 */
public class ProductListAdapter extends BaseAdapter {

    private List<ProductRowView> rows;
    private final List<Product> products;
    private final Context context;

    public ProductListAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
        rows = new ArrayList<ProductRowView>(products.size());
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public Product getSelectedProduct() {
        for(ProductRowView productRowView : rows) {
            if(productRowView.isProductSelected()) {
                return productRowView.getProduct();
            }
        }
        return null;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ProductRowView productRow;
        if(view instanceof ProductRowView) {
            productRow = (ProductRowView) view;
        } else {
            productRow = (ProductRowView) View.inflate(context, R.layout.product_row, null);
            rows.add(productRow);
        }
        productRow.setProduct(products.get(i));
        productRow.setViewListener(new ProductRowView.ViewListener() {
            @Override
            public void onChecked() {
                for(ProductRowView otherRowView : rows){
                    if(productRow.equals(otherRowView))
                        continue;
                    otherRowView.uncheck();
                }
            }
        });
        return productRow;
    }
}
