package com.grassroots.petition.views.lists;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.grassroots.petition.R;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.views.QuestionView;

import java.util.ArrayList;
import java.util.List;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class QuestionsListAdapter extends BaseAdapter {
    private final List<QuestionView> views = new ArrayList<QuestionView>();
    private final List<Question> questions;
    private final PetitionAnswers petitionAnswers;
    private final Context context;

    public QuestionsListAdapter(List<Question> questions, PetitionAnswers petitionAnswers, Context context) {
        this.petitionAnswers = petitionAnswers;
        this.questions = questions;
        this.context = context;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Object getItem(int i) {
        return questions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public List<SubjectAnswer> getAnswers() {
        List<SubjectAnswer> answers = new ArrayList<SubjectAnswer>();
        for (QuestionView questionView : views) {
            SubjectAnswer answer = questionView.getAnswer();
            if (answer.hasAnswer())
                answers.add(answer);
        }
        return answers;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        QuestionView questionView;
        if (view instanceof QuestionView) {
            questionView = (QuestionView) view;
        } else {
            questionView = (QuestionView) View.inflate(context, R.layout.question, null);
            views.add(questionView);
        }
        questionView.setPetitionAnswers(petitionAnswers);
        questionView.setQuestion(questions.get(i), (i + 1));
        return questionView;
    }
}
