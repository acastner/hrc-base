package com.grassroots.petition.views.lists;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class KnownSubjectListAdapter extends BaseAdapter {

    private final List<Subject> knownSubjects;
    private final LayoutInflater inflator;

    public KnownSubjectListAdapter(List<Subject> data, LayoutInflater inflator) {
        if (data == null)
            data = new ArrayList<Subject>();
        this.knownSubjects = data;
        this.inflator = inflator;
    }

    @Override
    public int getCount() {
        return knownSubjects.size();
    }

    @Override
    public Object getItem(int i) {
        return knownSubjects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SubjectSearchRowView rowView;
        if (view instanceof SubjectSearchRowView) {
            rowView = (SubjectSearchRowView) view;
        } else {
            rowView = (SubjectSearchRowView) inflator.inflate(R.layout.subject_list_row, null);
        }
        rowView.setSubject(knownSubjects.get(i));

        return rowView;
    }
}
