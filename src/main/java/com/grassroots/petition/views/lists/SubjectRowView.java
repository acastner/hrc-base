package com.grassroots.petition.views.lists;

import java.util.List;

import org.apache.log4j.Logger;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugsense.trace.BugSenseHandler;
import com.grassroots.petition.R;
import com.grassroots.petition.db.helper.WalklistDataSource;
import com.grassroots.petition.models.KnockStatus;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * <br>
 * Copyright CloudMine LLC. All rights reserved<br>
 * See LICENSE file included with SDK for details.
 */
public class SubjectRowView extends RelativeLayout {
    private TextView subjectText;
    private TextView statusText;
    private TextView otherText;
    private ImageView colorView;

	private static final String TAG = SubjectRowView.class.getName();
	private static final Logger LOGGER = Logger.getLogger(TAG);

    public SubjectRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SubjectRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setSubject(Subject subject, List<Question> questions) {
        setSubjectText(subject);
        setSubjectStatus(subject);
        setOtherSubjectText(subject, questions);
        setSubjectColor(subject.getColor());
    }

    private void setOtherSubjectText(Subject subject, List<Question> questions) {
		String json = subject.getOtherJSON();
        if (json == null || json.length() == 0) {
            return;
        }

		// Here we write our own parser. Nothing fancy. GSON and org.json
		// both fail to maintain field order, and this is seemingly critical for
		// us to maintain.
		//
		// We can do this here, despite JSON not being a regular language, by assuming that
		// it's just k->v pairs in Other.

		// Valid JSON object?
		if(json.startsWith("{") && json.endsWith("}")){
			json = json.substring(1, json.length() - 1);
		}else{
			return;
		}

		StringBuilder builder = new StringBuilder();

		try{

			String key = "";
			String val = "";


			String[] lines = json.split("\\s*,\\s*");
			for(String line : lines){
				String[] pairs = line.split("\":\"");

				key = "";
				val = "";

				for(int i = 0; i < pairs.length; i++){
					String s = pairs[i];

					s.replace("\\\"", "\"");

					if(i == 0){
						if(s.startsWith("\"")){
							s = s.substring(1, s.length());
						}
						key = s;
					}

					if(i == 1){
						if(s.endsWith("\"")){
							s = s.substring(0, s.length() - 1);
						}
						val = s;
					}

				}

				if(key != null && val != null && key.length() != 0 && val.length() != 0){
				    builder.append("\n").append(key).append(": ").append(val);
				}
			}
		}catch(Exception e){
			LOGGER.error("Jeff's homemade json parser has crashed and nobody is surprised.");
			BugSenseHandler.sendExceptionMessage("Jeff's homemade json parser has crashed and nobody is surprised.", null, e);
			return;
		}
		//to prevent setting empty other field with a ":"
        if(builder.toString().length() > 0){
        	 if(subject.getColor() != null){
        		 otherText.setTextColor(Color.parseColor("#"+subject.getColor()));
             }else{
            	 otherText.setTextColor(Color.BLACK);
             }
            otherText.setText(builder.toString());
            otherText.setVisibility( VISIBLE );
        }
        
    }

    private void setSubjectStatus(Subject subject) {
        KnockStatus status = subject.getKnockStatus();
        status = status == null ?
                KnockStatus.NOT_KNOCKED :
                status;
        if(subject.getColor() != null){
        	statusText.setTextColor(Color.parseColor("#"+subject.getColor()));
        }else{
        	statusText.setTextColor(Color.BLACK);
        }
        statusText.setText(status.displayText());
    }

    private void setSubjectText(Subject subject) {
        StringBuilder subjectTextBuilder = new StringBuilder();
        String separator = "";
        separator = appendValue(subject.getLastName(), separator, subjectTextBuilder);
        separator = appendValue(subject.getFirstName(), separator, subjectTextBuilder);
        if (subject.getGender() != null) {
            subjectTextBuilder.append("     ");
            subjectTextBuilder.append(subject.getGender());
        }
        if (subject.getAge() > 0) {
            subjectTextBuilder.append("     ");
            subjectTextBuilder.append(subject.getAge());
        }
        if (subject.isKnocked())
            setBackgroundResource(R.color.knocked_subject);
        else
            setBackgroundResource(R.color.white);
        
        if(subject.getColor() != null){
        	subjectText.setTextColor(Color.parseColor("#"+subject.getColor()));
        }else{
        	subjectText.setTextColor(Color.BLACK);
        }
        subjectText.setText(subjectTextBuilder.toString());
    }
    
    /**
     * Set Subject color
     * @param color in hexcode
     */
    private void setSubjectColor(String color){
        if(color != null){
            boolean isHex = color.matches("^[A-Fa-f0-9]{6}$");
            Log.e("color--","color--"+color);
            if(isHex){
                colorView.setBackgroundColor(Color.parseColor("#"+color.toUpperCase()));
            }
        }else{
        	colorView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private String appendValue(String value, String separator, StringBuilder subjectTextBuilder) {
        if (Strings.isNotEmpty(value)) {
            subjectTextBuilder.append(separator);
            subjectTextBuilder.append(value);
            return ", ";
        }
        return separator;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        subjectText = (TextView) findViewById(R.id.subject_text);
        statusText = (TextView) findViewById(R.id.subject_status);
        otherText = (TextView)findViewById(R.id.other_subject_text);
        colorView = (ImageView) findViewById(R.id.colorBarDetailView);
        
    }
}
