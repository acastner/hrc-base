package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import com.grassroots.petition.R;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * Default Subject Info View
 * Custom view for getting information about subject
 */
public class DefaultSubjectInfoView extends SubjectInfoView implements SubjectInfoViewWithButtonsInterface {

    /**
     * ButtonsView
     */
    private SubjectInformationButtons buttonsView;

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public DefaultSubjectInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public DefaultSubjectInfoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * SubjectInformationButtons.ViewListener setter
     * @param viewListener
     */
    public void setViewListener(SubjectInformationButtons.ViewListener viewListener) {
        buttonsView.setViewListener(viewListener);
    }

    /**
     * Set custom Font onFinishInflate
     */
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        buttonsView = (SubjectInformationButtons) findViewById(R.id.button_panel);
        buttonsView.setEnclosingView(this);
    }
}