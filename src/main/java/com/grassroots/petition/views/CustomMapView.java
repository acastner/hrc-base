package com.grassroots.petition.views;

import java.util.List;

import org.apache.log4j.Logger;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import com.grassroots.petition.GlobalData;
import com.grassroots.petition.fragments.WalklistMapFragment;
import com.grassroots.petition.utils.Tracker;

import android.content.Context;
import android.util.AttributeSet;
/**
 * CustomMapview created to handle map-not-displaying-onfreshinstall-error
 * @author sharika
 *
 */
public class CustomMapView extends MapView {
	
	private static final String TAG = CustomMapView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);

    private List<GeoPoint> points;
    private WalklistMapFragment mapfragment;

    public CustomMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub
        super.onLayout(arg0, arg1, arg2, arg3, arg4);
        mapfragment.zoomToEncompass(points);
        
        LOGGER.debug("onLayout");
        Tracker.appendLog(Tracker.getTime() + " Map manipulated. Zoom Level: " + getZoomLevel());
        Tracker.logMemoryStatus();
    }

    public void setMapFragment(WalklistMapFragment walklistMapFragment) {
        
        this.mapfragment = walklistMapFragment;
    }

    public void setPoints(List<GeoPoint> points) {
        this.points = points;
    }

}
