package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.grassroots.petition.R;

import java.util.List;

/**
 * MarketingView
 * A slide show of images
 *
 * TODO: Have this extend view pager instead of relative layout, remove the layout file if possible.
 */
public class MarketingView extends RelativeLayout {
    /**
     * Default constructor
     * @param context
     */
    public MarketingView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public MarketingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public MarketingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Set list with image resource IDs
     * @param resourceIds - list with image resource IDs
     */
    public void setImages(List<Integer> resourceIds) {

        ViewPager pager = (ViewPager) findViewById(R.id.marketing_pager);

        ImagePagerAdapter adapter = new ImagePagerAdapter(getContext(), resourceIds);
        pager.setOffscreenPageLimit(2);
        pager.setAdapter(adapter);
    }

    /**
     * Set list with image bitmaps
     * @param bitmaps - list with image bitmaps
     */
    public void setImageBitmaps(List<Bitmap> bitmaps) {
        ViewPager pager = (ViewPager)findViewById(R.id.marketing_pager);

        ImageBitmapPagerAdapter adapter = new ImageBitmapPagerAdapter(getContext(), bitmaps);
        pager.setAdapter(adapter);
    }

}
