package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.AddressListAdapter;

import java.util.Set;

/**
 * Nearby Addresses View
 * Displays subject address information. For use with reverse geocoding
 */
public class NearbyAddressesView extends RelativeLayout {
    /**
     * Nearby Address View listener interface
     */
    public static interface ViewListener {
        public void onAddressSelected(Subject address);
        public void onCancel();
    }

    /**
     * Nearby Address View listener
     */
    private ViewListener listener;
    /**
     * List of subject addresses
     */
    private ListView addressesList;
    private Context context;

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public NearbyAddressesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    /**
     * Nearby Address View listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Instantiate and set subject Addresses adapter with addresses
     * @param addresses - set of subject addresses
     */
    public void setAddresses(Set<Subject> addresses) {
        addressesList.setAdapter(new AddressListAdapter(addresses, context));
    }

    /**
     * Set custom font and view listeners onFinishInflate
     */
    public void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
        addressesList = (ListView) findViewById(R.id.nearby_addresses_list);
        addressesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Subject address = (Subject) adapterView.getItemAtPosition(i);
                listener.onAddressSelected(address);
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCancel();
            }
        });
    }
}
