package com.grassroots.petition.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.grassroots.petition.R;
import com.grassroots.petition.models.Product;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.views.lists.ProductListAdapter;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * ProductsView
 * Custom view that shows list with products
 */
public class ProductsView extends RelativeLayout {
    /**
     * ProductsView listener interface
     */
    public static interface ViewListener {
        public void onEndInterview();
        public void onMarketing();
        public void onNext(Product selectedProduct);
    }
    private static final String TAG = ProductsView.class.getName();
    private static final Logger LOGGER = Logger.getLogger(TAG);
    /**
     * ProductsView listener
     */
    private ViewListener listener;
    /**
     * Product listView
     */
    private ListView productList;
    /**
     * ProductList adapter
     */
    private ProductListAdapter productListAdapter;

    /**
     * Default constructor
     * @param context
     */
    public ProductsView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public ProductsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public ProductsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * ProductsView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Set list of products to display
     * @param products list of products
     */
    public void setProducts(List<Product> products) {
        if(products == null) {
            LOGGER.error("Got a null product list");
            return;
        }
        productListAdapter = new ProductListAdapter(products, getContext());
        productList.setAdapter(productListAdapter);
    }

    /**
     * @return selected product
     */
    public Product getSelectedProduct() {
        return productListAdapter.getSelectedProduct();
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        findViewById(R.id.next_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNext(getSelectedProduct());
            }
        });

        findViewById(R.id.marketing_material_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMarketing();
            }
        });

        findViewById(R.id.end_interview_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onEndInterview();
            }
        });

        productList = (ListView) findViewById(R.id.plan_options);
    }
}
