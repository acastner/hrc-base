package com.grassroots.petition.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.grassroots.petition.utils.TypeFaceSetter;
import com.grassroots.petition.R;

/**
 * Picture Question custom View. Shows picture (Photo or chose) to the user.
 *
 * TODO: Convert this to a fragment so it can be dynamically inserted
 *       into the missing question dialog fragment and the AQOL.
 */
public class PictureQuestionView extends RelativeLayout {
    /**
     * PictureQuestionView listener
     */
    private ViewListener listener;

    /**
     * PictureQuestionView listener interface
     */
    public interface ViewListener {
        public void onTakePicture();

    }
    /**
     * Picture imageView
     */
    private ImageView pictureView;

    /**
     * Default constructor
     * @param context
     */
    public PictureQuestionView(Context context) {
        super(context);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public PictureQuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public PictureQuestionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * PictureQuestionView listener setter
     * @param viewListener
     */
    public void setViewListener(ViewListener viewListener) {
        listener = viewListener;
    }

    /**
     * Set bitmap on imageView
     * @param picture - picture bitmap
     */
    public void setPicture(Bitmap picture) {
        Log.e("Picture", "H: "+picture.getHeight()+" W: " +picture.getWidth());
        pictureView.setImageBitmap(picture);
    }

    /**
     * Show the no answer image
     */
    public void reset() {
        pictureView.setImageResource(R.drawable.camera);
    }

    /**
     * Set custom font and subview listeners onFinishInflate
     */
    public void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);

        pictureView = (ImageView) findViewById(R.id.image);
        pictureView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTakePicture();
            }
        });
    }
}
