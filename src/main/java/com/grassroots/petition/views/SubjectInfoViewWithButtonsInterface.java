package com.grassroots.petition.views;

import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Subject;

import java.util.List;
import java.util.Map;

/**
 * Subject Info View with buttons Interface
 * Interface for subject info views
 */
public interface SubjectInfoViewWithButtonsInterface {
    public void setSubject(Subject subject);
    public void setQuestionNameTranslationMap(Map<String, String> translationMap);
    public void setSubjectInformationQuestions(List<Question> questions);
    public void setViewListener(SubjectInformationButtons.ViewListener viewListener);
    public Subject getSubject();
}
