package com.grassroots.petition.views;

import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grassroots.petition.R;
import com.grassroots.petition.models.Cart;
import com.grassroots.petition.models.Petition;
import com.grassroots.petition.models.PetitionAnswers;
import com.grassroots.petition.models.ProductOrder;
import com.grassroots.petition.models.Question;
import com.grassroots.petition.models.Question.Type;
import com.grassroots.petition.models.Subject;
import com.grassroots.petition.models.SubjectAnswer;
import com.grassroots.petition.utils.Strings;
import com.grassroots.petition.utils.TypeFaceSetter;

/**
 * View of all questions, answers, appointments, and selected products. 
 * Unanswered questions are editable.
 */
public class SubmitView extends RelativeLayout {

    public static final String TAG = SubmitView.class.getSimpleName();

    /**
     * SubmitView listener interface
     */
    public static interface ViewListener {
        public void onNext();

        public void onMissingSubjectInfo(Subject subject);

        public void onMissingQuestionAnswer(Question question);
    }

    /**
     * SubmitView listener
     */
    private ViewListener listener;
    /**
     * Subject Information display view
     */
    private SubjectInfoDisplayView subjectInfoDisplayView;
    /**
     * Container for extra questions
     */
    private LinearLayout extraQuestionsContainer;
    /**
     * Container for magic questions
     */
    private LinearLayout magicQuestionsContainer;
    /**
     * Name of canvasser
     */
    private TextView canvasserNameText;
    /**
     * Signature title
     */
    private TextView signatureTitle;
    /**
     * Signature imageView
     */
    private ImageView signatureImage;
    /**
     * Referrer layout
     */
    private View referrerLayout;
    /**
     * Referrer container
     */
    private ViewGroup referrerContainer;
    /**
     * Product display view
     */
    private ProductDisplayView productView;
    /**
     * Container for products
     */
    private View productContainer;
    /**
     * AppointmentSummaryView
     */
    private AppointmentSummaryView appointmentView;
    /**
     * Container for appointment
     */
    private View appointmentContainer;

    /**
     * Default constructor
     * @param context
     * @param attrs
     */
    public SubmitView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Default constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public SubmitView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * SubmitView listener setter
     * @param listener
     */
    public void setViewListener(ViewListener listener) {
        this.listener = listener;
    }

    /**
     * Fill subview with subject
     * @param subject
     * @param petition
     */
    public void setSubject(Subject subject, Petition petition) {
        subjectInfoDisplayView.setSubject(subject, petition.getSubjectInformationQuestions());
    }

    /**
     * Set name of canvasser
     * @param petition - petition that contains canvasser name
     */
    private void setCanvasserName(Petition petition) {
        String first = petition.getCanvasserFirstName();
        String last = petition.getCanvasserLastName();
        canvasserNameText.setText(first + " " + last);
    }

    /**
     * Set PetitionAnswers
     * @param petitionAnswers - petition answers
     * @param petition - petition that contains questions
     */
    public void setPetitionAnswers(PetitionAnswers petitionAnswers, Petition petition) {
        setCanvasserName(petition);

        setSignature(petitionAnswers, petition);

        addExtraAnswersToView(petitionAnswers, petition);

        addMagicQuestionsAnswersToView(petitionAnswers, petition);

        addReferrerAnswersToView(petitionAnswers, petition);

        setCart(petitionAnswers, petition);

        setAppointment(petitionAnswers, petition);
    }

    // TODO: need to implement correct code to support multiple products (or donations) from cart
    private void setCart(PetitionAnswers petitionAnswers, Petition petition) {
        if (petition.hasProducts()) {
            Cart cart = petitionAnswers.getCart();
            if (null != cart) {
                List<ProductOrder> productOrders = cart.getItems();
                if (productOrders.size() > 0) {
                    ProductOrder productOrder = productOrders.get(0);
                    productView.setProduct(productOrder, petition, petitionAnswers.getCart());
                    productContainer.setVisibility(VISIBLE);
                    return;
                }
            }
        }

        productContainer.setVisibility(GONE);
    }

    /**
     * Set signature
     * @param petitionAnswers
     * @param petition - signature enabled petition
     */
    private void setSignature(PetitionAnswers petitionAnswers, Petition petition) {
        if(!petition.isSignature()) {
            signatureTitle.setVisibility(GONE);
            signatureImage.setVisibility(GONE);
            return;
        }
        signatureImage.setVisibility(VISIBLE);
        signatureImage.setImageBitmap(petitionAnswers.getSignatureBitmap());
    }

    /**
     * Set appointment
     * @param petitionAnswers
     * @param petition
     */
    private void setAppointment(PetitionAnswers petitionAnswers, Petition petition) {
        if( !petition.isSupportReservation() ||
            null == petitionAnswers.getAppointment())
        {
            appointmentContainer.setVisibility(GONE);
            return;
        }
        appointmentContainer.setVisibility(VISIBLE);
        appointmentView.setAppointment(petitionAnswers.getAppointment());
    }

    /**
     * Fill view with referrer answers
     * @param petitionAnswers
     * @param petition
     */
    private void addReferrerAnswersToView(PetitionAnswers petitionAnswers, Petition petition) {
        if(!petition.hasReferrerQuestions()) {
            referrerLayout.setVisibility(GONE);
            return;
        }
        referrerLayout.setVisibility(VISIBLE);
        referrerContainer.removeAllViewsInLayout();
        Map<Integer, List<Question>> referrerQuestions = petition.getReferrerQuestions();
        for (List<Question> referrerGroup : referrerQuestions.values()) {
            String name = "";
            String email = "";
            String phone = "";
            for (Question question : referrerGroup) {
                String referrerPostScript = question.getReferrerPostScript();
                SubjectAnswer subjectAnswer = petitionAnswers.getAnswerTo(question);
                if (subjectAnswer == null)
                    continue;
                String questionAnswer = subjectAnswer.getStringAnswer();
                if (Question.REFERRER_EMAIL.equals(referrerPostScript)) {
                    email = questionAnswer;
                } else if (Question.REFERRER_NAME.equals(referrerPostScript)) {
                    name = questionAnswer;
                } else if (Question.REFERRER_PHONE.equals(referrerPostScript)) {
                    phone = questionAnswer;
                }
            }
            ReferralDisplayView referrerView = (ReferralDisplayView) View.inflate(getContext(), R.layout.referral_display, null);
            referrerView.setReferral(name, email, phone);
            referrerContainer.addView(referrerView);
        }
    }

    /**
     * Fill view with extra answers
     * @param petitionAnswers
     * @param petition
     */
    private void addExtraAnswersToView(PetitionAnswers petitionAnswers, Petition petition) {
        extraQuestionsContainer.removeAllViewsInLayout();
        //TODO this has been broken into its own view, changes should not be made here and ethical should be transitioned to using that view
        for (final Question question : petition.getNonMagicQuestions()) {
        	if(question.getType() != Type.SIGNATURE){
                addAnyQuestionToContainer(extraQuestionsContainer, question, petitionAnswers);
        	}
        }
    }

    /**
     * Fill view with answers for magic questions
     * @param petitionAnswers
     * @param petition
     */
    private void addMagicQuestionsAnswersToView(PetitionAnswers petitionAnswers, Petition petition) {
        magicQuestionsContainer.removeAllViewsInLayout();
        List<Question> magicQuestions = petition.getMagicQuestions();
        for (final Question question : magicQuestions) {
            addAnyQuestionToContainer(magicQuestionsContainer, question, petitionAnswers);
        }
    }

    private void addAnyQuestionToContainer(ViewGroup container, final Question question, PetitionAnswers petitionAnswers) {
        View view;
        String questionText = question.getText();
        SubjectAnswer answer = petitionAnswers.getAnswerTo( question );

        // Photo answer
        if( question.isPicture() )
        {
            view = createPictureView( question, answer );
        }
        // Regular question
        else
        {
            view = createQuestionAnswerView( question, answer, questionText );
        }

        if( view == null ) {
            return;
        }

        container.addView( view );
    }

    private TextView createQuestionAnswerView (final Question question, SubjectAnswer answer, String questionText)
    {
        String answerText = answer == null ? null : answer.getAnswerString(question);

        Log.d( TAG, "Question name = " + question.getName() + "\ttext =" + questionText );
        if( questionText == null )
        {
            questionText = "";
            Log.d( TAG, "Question text was null" );
        }
        // Get question text and strip HTML markup (if exists)
        else
        {
            questionText = new HtmlToPlainText().getPlainText( Jsoup.parse( questionText ) );
        }

        if ( Strings.isEmpty( questionText )) {
            questionText = question.getName();
        }
        if (Strings.isEmpty(questionText)) {
            return null;
        }

        int truncationLength = getResources().getInteger( R.integer.truncation_length );
        if (questionText.length() >= truncationLength ) {
            questionText = questionText.substring( 0, truncationLength - 1 );
            questionText += "...";
            Log.d( TAG, "Long string trimmed: " + questionText );
        }
        
        TextView answerView = new TextView(getContext());
        answerView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,getResources().getInteger( R.integer.submit_detail_text_height ));
        if (Strings.isEmpty(answerText)) {
            answerView.setText(questionText);
            answerView.setTextColor(getResources().getColor( R.color.missing_data));
            answerView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onMissingQuestionAnswer(question);
                }
            });
        } else {
            answerView.setText( questionText + " " + answerText );
        }
        return answerView;
    }

    private LinearLayout createPictureView (final Question question, SubjectAnswer answer)
    {
        // Empty container
        LinearLayout pictureContainer = new LinearLayout(getContext());

        // Add text link/summary to the view
        TextView questionTextView = generateCameraLabelView( question );
        pictureContainer.addView( questionTextView );

        // If question already answered, add photo here
        if (null != answer && answer.hasPictureAnswer()) {
            ImageView picture = new ImageView(getContext());
            Bitmap bitmap = answer.getPictureBitmap();
            if (bitmap == null) {
                String filePath = answer.getPictureLocation();
                if (filePath != null) {
                    bitmap = readScaledPhoto(filePath);
                }
            }

            if (bitmap != null) {
                picture.setImageBitmap(bitmap);
                pictureContainer.addView(picture);
            }
        }
        // If not, set the text to red
        else
        {
            questionTextView.setTextColor( getResources().getColor( R.color.missing_data ) );
        }

        return pictureContainer;
    }

    private TextView generateCameraLabelView (final Question question)
    {
        TextView cameraLabel = new TextView(getContext());
        String text = question.getText();
        if( text == null || TextUtils.isEmpty(text) )
        {
            Log.d( TAG, "Question text is empty" );
            cameraLabel.setText( question.getName() );
        }
        else
        {
            cameraLabel.setText( text );
        }

        cameraLabel.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick (View view)
            {
                listener.onMissingQuestionAnswer( question );
            }
        } );

        return cameraLabel;
    }

    /**
     * Read the bitmap stored at the specified location, then scale it to a standard size
     * @param photoPath
     * @return Scaled version of the image
     */
    public Bitmap readScaledPhoto(String photoPath) {
        if(Strings.isEmpty(photoPath)) {
            return null;
        }
        int targetW = 320;
        int targetH = 240;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TypeFaceSetter.setRobotoFont(getContext(), this);
        subjectInfoDisplayView = (SubjectInfoDisplayView) findViewById(R.id.subject_info);
        subjectInfoDisplayView.setViewListener(new SubjectInfoDisplayView.ViewListener() {
            @Override
            public void onMissingSubjectInformation(Subject subject) {
                listener.onMissingSubjectInfo(subject);
            }
        });
        extraQuestionsContainer = (LinearLayout) findViewById(R.id.question_answers_container);
        magicQuestionsContainer = (LinearLayout) findViewById(R.id.magic_question_answers_container);
        canvasserNameText = (TextView) findViewById(R.id.canvasser_name);

        // Signature
        signatureTitle = (TextView) findViewById(R.id.submit_signature_header);
        signatureImage = (ImageView) findViewById(R.id.signature_img);

        findViewById(R.id.submit_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNext();
            }
        });
        referrerContainer = (ViewGroup) findViewById(R.id.referral_listing);
        referrerLayout = findViewById(R.id.referral_layout);
        productView = (ProductDisplayView) findViewById(R.id.product_display);
        productContainer = findViewById(R.id.product_display_layout);
        appointmentView = (AppointmentSummaryView) findViewById(R.id.appointment_display);
        appointmentContainer = findViewById(R.id.appointment_display_layout);
    }
}

