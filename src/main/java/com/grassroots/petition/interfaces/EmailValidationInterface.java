package com.grassroots.petition.interfaces;

import android.app.Activity;

/**
 * Created by Sharika on 6/20/16.
 */
public interface EmailValidationInterface {

    void emailValidationFinished(String result, Activity activity);
}
