package com.grassroots.petition.interfaces;

import android.app.Activity;


public interface PhoneValidationInterface {

    void phoneValidationFinished(String result, Activity activity);
}
