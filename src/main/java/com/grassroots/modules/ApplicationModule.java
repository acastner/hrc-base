package com.grassroots.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.grassroots.petition.R;
import com.grassroots.utils.AuthenticationHeaderPreference;
import com.grassroots.utils.ImeiHeaderPreference;
import com.grassroots.utils.OAuthTokenPreference;
import com.grassroots.utils.SaleTokenPreference;
import com.grassroots.utils.StringPreference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jason on 12/21/2016.
 */

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(final Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Context context, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.base_url_dagger) + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    GrassrootsApiService provideGrassrootsApiService(final Retrofit retrofit) {
        return retrofit.create(GrassrootsApiService.class);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        //Add logging interceptor for debugging purposes
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }

    //////////////////////////////////////////////////////////////////////////////
    // Shared Preferences
    //////////////////////////////////////////////////////////////////////////////
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    @AuthenticationHeaderPreference
    StringPreference provideAuthenticationHeaderPreference(final SharedPreferences prefs) {
        return new StringPreference(prefs, "AuthenticationHeader", "");
    }

    @Provides
    @Singleton
    @ImeiHeaderPreference
    StringPreference provideImeiHeaderPreference(final SharedPreferences prefs) {
        return new StringPreference(prefs, "ImeiHeader", "");
    }

    @Provides
    @Singleton
    @OAuthTokenPreference
    StringPreference providetOAuthHeader(SharedPreferences prefs) {
        return new StringPreference(prefs, "OAuthHeader", "");
    }

    @Provides
    @Singleton
    @SaleTokenPreference
    StringPreference provideSaleToken(SharedPreferences prefs) {
        return new StringPreference(prefs, "SaleTokenPref", "");
    }

}