package com.grassroots.modules;


import com.grassroots.models.AddressFromZipResponse;
import com.grassroots.models.AddressValidation;
import com.grassroots.models.ExecuteSaleData;
import com.grassroots.models.ExecuteSaleResponse;
import com.grassroots.models.OAuthResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Jason on 12/28/2016.
 */

public interface GrassrootsApiService {

    //Authorization = Basic enQ6YQ==

    @GET("v2/utils/lookup-city-state")
    Observable<AddressFromZipResponse> getAddressFromZip(@Header("imei") String imei,
                                                         @Header("Authorization") String auth,
                                                         @Query("zipcode") String zip);


    @GET("v2/utils/validate-address")
    Observable<AddressValidation> getAddressValidation(@Header("imei") String imei,
                                                       @Header("Authorization") String auth,
                                                       @Query("city") String city,
                                                       @Query("state") String state,
                                                       @Query("address1") String address1,
                                                       @Query("address2") String address2,
                                                       @Query("countryCode") String countryCode,
                                                       @Query("isShipping") boolean isShipping);

    @POST("token")
    @FormUrlEncoded
    Observable<OAuthResponse> getOAuth(@Field("Grant_type") String grantType,
                                       @Field("Username") String username,
                                       @Field("Password") String password,
                                       @Field("Client_id") String clientId,
                                       @Field("Client_secret") String clientSecret);



    /*
    @GET("utils/lookup-city-state")
    Observable<AddressFromZipResponse.AddressFromZip> getAddresssFromZip(@Header("imei") String imei,
                                                                   @Header("Authorization") String auth,
                                                                   @Query("zipcode") String zip);
*/
    /*
    @PUT("petition/sale-status-lookup")
    Call<List<SaleStatus>> getSalesStatuses(@Header("imei") String imei,
                                            @Header("Authorization") String auth,
                                            @Body SaleUUIDList saleUUIDs);

    @POST("utils/edit-crewnotes")
    Call<String> editCrewNotes(@Header("imei") String imei,
                               @Header("Authorization") String auth,
                               @Body CrewNotes crewNotes);




    @GET("v2/petition/get-sale-token")
    Observable<LocalTokenPaypal> getPaypalToken(@Header("imei") String imei,
                                                @Header("Authorization") String auth,
                                                @Query("authorizationCode") String code);

                                                */

    @POST("v2/petition/execute-sale")
    Observable<ExecuteSaleResponse> executeSale(@Header("imei") String imei,
                                                @Header("Authorization") String auth,
                                                @Body ExecuteSaleData saleData);


}
