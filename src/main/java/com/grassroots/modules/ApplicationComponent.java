package com.grassroots.modules;

import com.grassroots.petition.activities.BaseACHDataActivity;
import com.grassroots.petition.activities.BaseCreditCardSwipeActivity;
import com.grassroots.petition.activities.BaseLoginActivity;
import com.grassroots.petition.activities.BasePetitionActivity;
import com.grassroots.petition.activities.BaseSubjectInfoActivity;
import com.grassroots.petition.activities.BaseSubmitActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by jason on 12/21/2016.
 */
@Singleton
@Component(modules={ApplicationModule.class})
public interface ApplicationComponent {
    void inject(BaseLoginActivity __);
    void inject(BaseSubjectInfoActivity __);

    void inject(BasePetitionActivity __);

    void inject(BaseCreditCardSwipeActivity __);

    void inject(BaseACHDataActivity __);

    void inject(BaseSubmitActivity __);

}

